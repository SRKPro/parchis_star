//
//  GameViewController.swift
//  Indian Ludo
//
//  Created by ramesh sanghar on 04/11/18.
//  Copyright © 2018 Om. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import UnityAds
import GoogleMobileAds



class GameViewController: UIViewController, UnityAdsDelegate, GADBannerViewDelegate {
    
    
    let bannerAd = GADBannerView()
    var bannerSize:GADAdSize = GADAdSize()
    var bannerAdX:CGFloat = CGFloat()
    var bannerAdY:CGFloat = CGFloat()
    
    
    
    
    //      Atho Size : 582.5 582.5
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    /*
    override func viewDidLoad() {
        super.viewDidLoad()
        
   // override func viewWillLayoutSubviews() {
    //    super.viewWillLayoutSubviews()
        
        
        
        
     //   if #available(iOS 11.0, *), let view = self.view {
        //    view.frame = self.view.bounds
            
           // scene?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
          //  self.view.s
           // self.view.safeAreaLayoutGuide.layoutFrame
            
           // self.view.safeAreaInsets.left
           // view.layer.borderColor?.copy(alpha: 50)
      //  }
       // let scene = HomeScene(size: HomeScene.)
        
     
        
        if let view = self.view as! SKView? {
            
        view.ignoresSiblingOrder = true
        view.showsFPS = true
        view.showsNodeCount = true
       // view.showsPhysics = true
        //view.showsDrawCount = true
        let scene = HomeScene(size: self.view.)
       // scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        scene.scaleMode = .aspectFit
        view.presentScene(scene)
        }
     
        
 //   }
   // }
    
    
    
   // override func viewDidLoad() {
  //      super.viewDidLoad()
  //      windo
  //      print("---")
  //      print("∙ \(type(of: self))")
   //     print("---")
  //  }
        
    // XR : 448.0 207.0       iPad : 512.0 384.0        5S :  284.0 160.0
        
        
     //
        
        
        
        
        
        
        
        
      //
        
        // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
        // including entities and graphs.
        if let scene = GKScene(fileNamed: "HomeScene") {
            
            // Get the SKScene from the loaded GKScene
            if let sceneNode = scene.rootNode as! HomeScene? {
                
                // Set the scale mode to scale to fit the window
                sceneNode.scaleMode = .aspectFit
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(sceneNode)
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = true
                    view.showsNodeCount = true
                }
            }
        }
    }
    */

    // VIEW DID LOAD EEND HERE :
    
    

    /*
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
            if let view = self.view as! SKView? {
                // Load a GameScene with the size of the view
                let scene = HomeScene(size: view.frame.size)
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                // Present the scene
                view.presentScene(scene)
                
                view.ignoresSiblingOrder = true
                view.showsFPS = true
                view.showsNodeCount = true
            }
            
        }
 
 */

    // View will layout Subviews ENDS HERE :
 
 
 
 
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        
        
        
        
        
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.playAd(notification:)), name: NSNotification.Name(rawValue: "playAdID"), object: nil)
        
        
    }
    
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        
        
        
        
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playAd(notification:)), name: NSNotification.Name(rawValue: "playAdID"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideBannerAd(notification:)), name: NSNotification.Name(rawValue: "hideBannerAd"), object: nil)
        
        
        if (UIDevice.current.userInterfaceIdiom == .phone) {
            
            
            bannerSize = kGADAdSizeBanner
            bannerAdX = self.view.bounds.width / 4
            bannerAdY = 0
            
            // if #available(iOS 11.0, *), let mview = self.view {
            //     mview.frame = self.view.safeAreaLayoutGuide.layoutFrame
            // }
            guard let mainview = self.view as! SKView? else { return }
            
            mainview.ignoresSiblingOrder = true
            
            // view.showsFPS = true
            // view.showsNodeCount = true
            // view.showsPhysics = true
            //view.showsDrawCount = true
            
            let scene = HomeScene(size:mainview.bounds.size)
            
            // print(scene.size.height, scene.size.width)
            scene.scaleMode = .aspectFit
            mainview.presentScene(scene)
            
            
            
        } else {
            
            bannerSize = kGADAdSizeFullBanner
            bannerAdX = self.view.bounds.width / 4 + 22
            bannerAdY = self.view.bounds.height - 50
            
            if let scene = SKScene(fileNamed: "HomeScenePad") {
                print("Pad SCENE :")
                
                
                //WelcomeScene(fileNamed:"WelcomeScene") {
                let skView = self.view as! SKView
                skView.ignoresSiblingOrder = true
                //  self.view.addSubview(newSkView)
                //    skView.showsFPS = true
                //    skView.showsNodeCount = true
                scene.scaleMode = .aspectFit
                skView.presentScene(scene)
            }
            
            
            
        }
        
        
        
        
        
        
        
        print("---")
        print("∙ \(type(of: self))")
        print("---")
        print(view.bounds.size)
        print(self.view.frame)
        
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let save = UserDefaults.standard
        
        if save.value(forKey: "Purchase") == nil {
            
            bannerAd.adSize = bannerSize
            bannerAd.frame = CGRect(x: bannerAdX, y: bannerAdY, width: bannerSize.size.width, height: bannerSize.size.height)
            bannerAd.delegate = self
            bannerAd.adUnitID = "ca-app-pub-2188730752108749/2359930024"
            bannerAd.rootViewController = self
            
            
            if self.view.bounds.width >= 667 {
                view.addSubview(bannerAd)
                bannerAd.load(GADRequest())
            }
            
        }
        
        
    }
    
    
    
    
    @objc func playAd(notification: NSNotification) {
        
        if UnityServices.isInitialized() {
            UnityAds.show(self, placementId: "rewardedVideo")
        }
        
    }
    
    @objc func hideBannerAd(notification: NSNotification) {
        if !bannerAd.isHidden {
            bannerAd.isHidden = true
        } else {
            bannerAd.isHidden = false
        }
    }
    
    
   
    
    
//    func playAd() {
//
//        if UnityServices.isInitialized() {
//            UnityAds.show(self, placementId: "rewardedVideo")
//        }
//
//    }
    
    
    
    
    func unityAdsReady(_ placementId: String) {
        
    }
    
    func unityAdsDidError(_ error: UnityAdsError, withMessage message: String) {
        
    }
    
    func unityAdsDidStart(_ placementId: String) {
        
    }
    
    func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) {
        
    }
    
    
    
    
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("22222222222222222222222222222222222222222222")
//        self.view.endEditing(true);
//
//        return false;
//    }
    
    
    
    
    /*

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
*/
    override var prefersStatusBarHidden: Bool {
        return true
    }
 
 
}
