//
//  HomeScenePad.swift
//  Indian Ludo
//
//  Created by ramesh sanghar on 09/01/19.
//  Copyright © 2019 Om. All rights reserved.
//

import Foundation
import SpriteKit
import StoreKit
import UnityAds

struct GlobalVariablesPad {
    
    static var audio = UserDefaults.standard.bool(forKey: "audio")
    static var backgroundColorName = UserDefaults.standard.integer(forKey: "backgroundColor")
    static var totalGames = UserDefaults.standard.integer(forKey: "totalGames")
    
    static let darkTextColor:UIColor = UIColor.black
    static let whiteTextColor:UIColor = UIColor.white
    
    static var PreviousScene:PadScenes = .VsComFourPad
    static var player1Text = ""
    static var player2Text = ""
    static var player3Text = ""
    static var player4Text = ""
}

class HomeScenePad: SKScene, UITextFieldDelegate, NewSpriteNodeButtonDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view!.endEditing(true)
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updateText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updateText.count < 13
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            
            
            if self.view?.frame.origin.y == 0 && !(firstPlayer.isEditing || secondPlayer.isEditing) {
                self.view?.frame.origin.y -= keyboardSize.height - 30
            }
        }
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view?.frame.origin.y != 0 {
            self.view?.frame.origin.y = 0
        }
        
    }
    
    
    
    
    
    let defaults = UserDefaults.standard
    
    var playerone:String = ""
    var playertwo:String = ""
    var playerthree:String = ""
    var playerfour:String = ""
    
    var ote:Int = 0
    
    let buttonSound = SKAction.playSoundFileNamed("multimedia_button_click_029.mp3", waitForCompletion: false)
    
    var vsCom:NewSpriteNodeButton = NewSpriteNodeButton()
    var vsFriend:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var twoPlayersCom:NewSpriteNodeButton = NewSpriteNodeButton()
    var threePlayersCom:NewSpriteNodeButton = NewSpriteNodeButton()
    var fourPlayersCom:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var twoPlayersFriend:NewSpriteNodeButton = NewSpriteNodeButton()
    var threePlayersFriend:NewSpriteNodeButton = NewSpriteNodeButton()
    var fourPlayersFriend:NewSpriteNodeButton = NewSpriteNodeButton()
    
    
    var play:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var settings:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var noAds:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var comingSoon:NewSpriteNodeButton = NewSpriteNodeButton()
    var howToPlay:NewSpriteNodeButton = NewSpriteNodeButton()
    var audio:NewSpriteNodeButton = NewSpriteNodeButton()
    var facebook:NewSpriteNodeButton = NewSpriteNodeButton()
    var rateUs:NewSpriteNodeButton = NewSpriteNodeButton()
    var share:NewSpriteNodeButton = NewSpriteNodeButton()
    var theme:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var cancel:NewSpriteNodeButton = NewSpriteNodeButton()
    
    
    let firstPlayer:UITextField = UITextField()
    let secondPlayer:UITextField = UITextField()
    let thirdPlayer:UITextField = UITextField()
    let fourthPlayer:UITextField = UITextField()
    
    let rules: UIView = UIView()
    
    let futureFeaters:UIView = UIView()
    
    
    lazy var scrollView: UIScrollView = {
        
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        
        return view
    }()
    
    let rotateAction:SKAction = SKAction.rotate(byAngle: 6.283, duration: 0.4)
    
    var moveAction:SKAction = SKAction()
    var moveAction1:SKAction = SKAction()
    var moveAction2:SKAction = SKAction()
    var moveAction3:SKAction = SKAction()
    
    
    var moveAction4:SKAction = SKAction()
    var moveAction5:SKAction = SKAction()
    var moveAction6:SKAction = SKAction()
    
    var moveAction10:SKAction = SKAction()
    
    let myImages = ["Safe Place.png","Kill to Enter.png","Win Ex.png","Kill Again.png","Plus Turn.png","Third turn burn.png"]
    let imageHeight:CGFloat = 150
    let imageWidth:CGFloat = 300
    var yPosition:CGFloat = 0
    var scrollViewSize:CGFloat = 0
    
    var blackShadow:SKSpriteNode = SKSpriteNode()
    
    
    var playGame:String = ""
    var textColore:UIColor = UIColor()
    
    
    override func didMove(to view: SKView) {
        
        if defaults.value(forKey: "Purchase") == nil {
        
            UnityAds.initialize("3420654")
        
        }
        
        if GlobalVariablesPad.backgroundColorName == 1 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            textColore = GlobalVariablesPad.darkTextColor
            
        } else if GlobalVariablesPad.backgroundColorName == 2 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
            textColore = GlobalVariablesPad.whiteTextColor
            
        } else if GlobalVariablesPad.backgroundColorName == 3 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
            textColore = GlobalVariablesPad.whiteTextColor
            
        }
        
        
        
        rules.addSubview(scrollView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        
        rotateAction.timingMode = .easeOut
        
        
        for image in myImages {
            
            
            let myImage:UIImage = UIImage(named: image)!
            let myImageView:UIImageView = UIImageView()
            myImageView.image = myImage
            
            myImageView.frame.size.width = imageWidth
            myImageView.frame.size.height = imageHeight
            myImageView.frame.origin.x = 0
            myImageView.frame.origin.y = yPosition
            
            scrollView.addSubview(myImageView)
            scrollView.showsHorizontalScrollIndicator = false
            yPosition += imageHeight + 8
            scrollViewSize += imageHeight
            
        }
        
        scrollView.contentSize = CGSize(width: imageWidth, height: scrollViewSize)
        
        
        
        moveAction = SKAction.move(to: CGPoint(x: 0, y: (scene?.size.height)! * 0.0), duration: 0.2)
        
        moveAction1 = SKAction.move(to: CGPoint(x: (scene?.size.width)! * 0.1605, y: (scene?.size.height)! * -0.10), duration: 0.2)
        
        moveAction2 = SKAction.move(to: CGPoint(x: (scene?.size.width)! * 0.264, y: (scene?.size.height)! * -0.25), duration: 0.2)
        
        moveAction3 = SKAction.move(to: CGPoint(x: (scene?.size.width)! * 0.3223, y: (scene?.size.height)! * -0.40), duration: 0.2)
        
        moveAction4 = SKAction.move(to: CGPoint(x: (scene?.size.width)! * -0.1605, y: (scene?.size.height)! * -0.10), duration: 0.2)
        
        moveAction5 = SKAction.move(to: CGPoint(x: (scene?.size.width)! * -0.264, y: (scene?.size.height)! * -0.25), duration: 0.2)
        
        moveAction6 = SKAction.move(to: CGPoint(x: (scene?.size.width)! * -0.3223, y: (scene?.size.height)! * -0.40), duration: 0.2)
        
        moveAction10 = SKAction.move(to: CGPoint(x: 0, y: (scene?.size.height)! * -0.40), duration: 0.2)
        
        
        
        blackShadow = SKSpriteNode(imageNamed: "Black Shadow")
        blackShadow.name = "blackshadow"
        blackShadow.position = CGPoint(x: 0, y: 0)
        blackShadow.zPosition = 15
        blackShadow.isHidden = true
        addChild(blackShadow)
        
        
        
        vsCom = NewSpriteNodeButton(imageNamed: "vs computer")
        vsCom.name = "vscom"
        vsCom.position = CGPoint(x: (scene?.size.width)! * -0.22, y: (scene?.size.height)! * 0.25)
        vsCom.size = CGSize(width: 201, height: 157)
        vsCom.zPosition = 1
        vsCom.isUserInteractionEnabled = true
        vsCom.delegate = self
        addChild(vsCom)
        
        vsFriend = NewSpriteNodeButton(imageNamed: "1 device multiplayer")
        vsFriend.name = "vsfriend"
        vsFriend.position = CGPoint(x: (scene?.size.width)! * 0.22, y: (scene?.size.height)! * 0.25)
        vsFriend.size = CGSize(width: 201, height: 157)
        vsFriend.zPosition = 1
        vsFriend.isUserInteractionEnabled = true
        vsFriend.delegate = self
        addChild(vsFriend)
        
        
        noAds = NewSpriteNodeButton(imageNamed: "No Ads")
        noAds.name = "noads"
        noAds.position = CGPoint(x: (scene?.size.width)! * 0.43, y: (scene?.size.height)! * -0.40)
        noAds.zPosition = 2
        noAds.size = CGSize(width: 77, height: 82)
        noAds.delegate = self
        noAds.isUserInteractionEnabled = true
        addChild(noAds)
        
        
        settings = NewSpriteNodeButton(imageNamed: "Settings")
        settings.name = "settings"
        settings.position = CGPoint(x: 0 , y: (scene?.size.height)! * -0.40)
        settings.size = CGSize(width: 77, height: 82)
        settings.zPosition = 2
        settings.isUserInteractionEnabled = true
        settings.delegate = self
        addChild(settings)
        
        
        comingSoon = NewSpriteNodeButton(imageNamed: "Coming soon")
        comingSoon.name = "comingsoon"
        comingSoon.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        comingSoon.size = CGSize(width: 77, height: 82)
        comingSoon.zPosition = 1
        comingSoon.isUserInteractionEnabled = true
        comingSoon.delegate = self
        comingSoon.isHidden = true
        addChild(comingSoon)
        
        
        theme = NewSpriteNodeButton(imageNamed: "Theme")
        theme.name = "theme"
        theme.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        theme.size = CGSize(width: 77, height: 82)
        theme.zPosition = 1
        theme.isUserInteractionEnabled = true
        theme.delegate = self
        theme.isHidden = true
        addChild(theme)
        
        
        howToPlay = NewSpriteNodeButton(imageNamed: "How to Play")
        howToPlay.name = "howtoplay"
        howToPlay.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        howToPlay.size = CGSize(width: 77, height: 82)
        howToPlay.zPosition = 1
        howToPlay.isUserInteractionEnabled = true
        howToPlay.delegate = self
        howToPlay.isHidden = true
        addChild(howToPlay)
        
        audio = NewSpriteNodeButton(imageNamed: "Audio")
        audio.name = "audio"
        audio.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        audio.size = CGSize(width: 77, height: 82)
        audio.zPosition = 1
        audio.isUserInteractionEnabled = true
        audio.delegate = self
        audio.isHidden = true
        addChild(audio)
        
        
        
        facebook = NewSpriteNodeButton(imageNamed: "Facebook")
        facebook.name = "facebook"
        facebook.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        facebook.size = CGSize(width: 77, height: 82)
        facebook.zPosition = 1
        facebook.isUserInteractionEnabled = true
        facebook.delegate = self
        facebook.isHidden = true
        addChild(facebook)
        
        rateUs = NewSpriteNodeButton(imageNamed: "Rate")
        rateUs.name = "rateUs"
        rateUs.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        rateUs.size = CGSize(width: 77, height: 82)
        rateUs.zPosition = 1
        rateUs.isUserInteractionEnabled = true
        rateUs.delegate = self
        rateUs.isHidden = true
        addChild(rateUs)
        
        share = NewSpriteNodeButton(imageNamed: "Share")
        share.name = "share"
        share.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.40)
        share.size = CGSize(width: 77, height: 82)
        share.zPosition = 1
        share.isUserInteractionEnabled = true
        share.delegate = self
        share.isHidden = true
        addChild(share)
        
        
        
        
        cancel = NewSpriteNodeButton(imageNamed: "Cancel")
        cancel.name = "cancel"
        cancel.position = CGPoint(x: (scene?.size.width)! * -0.465, y: (scene?.size.height)! * 0.44)
        cancel.size = CGSize(width: 35, height: 43)
        cancel.zPosition = 2
        cancel.isUserInteractionEnabled = true
        cancel.delegate = self
        cancel.isHidden = true
        addChild(cancel)
        
        
        play = NewSpriteNodeButton(imageNamed: "Play")
        play.name = "Play"
        play.position = CGPoint(x: 120, y: 0)
        play.size = CGSize(width: 159, height: 72)
        play.zPosition = 4
        play.isUserInteractionEnabled = true
        play.delegate = self
        play.isHidden = true
        addChild(play)
        
        
        twoPlayersCom = NewSpriteNodeButton(imageNamed: "2 Players Option")
        twoPlayersCom.name = "TwoPlayersCom"
        twoPlayersCom.position = CGPoint(x: (scene?.size.width)! * -0.262, y: (scene?.size.height)! * 0.255)
        twoPlayersCom.zPosition = 2
        twoPlayersCom.size = CGSize(width: 148, height: 156)
        twoPlayersCom.delegate = self
        twoPlayersCom.isUserInteractionEnabled = true
        twoPlayersCom.isHidden = true
        addChild(twoPlayersCom)
        
        
        threePlayersCom = NewSpriteNodeButton(imageNamed: "3 Players Option")
        threePlayersCom.name = "ThreePlayersCom"
        threePlayersCom.position = CGPoint(x: (scene?.size.width)! * 0.262, y: (scene?.size.height)! * 0.255)
        threePlayersCom.zPosition = 2
        threePlayersCom.size = CGSize(width: 148, height: 156)
        threePlayersCom.delegate = self
        threePlayersCom.isUserInteractionEnabled = true
        threePlayersCom.isHidden = true
        addChild(threePlayersCom)
        
        fourPlayersCom = NewSpriteNodeButton(imageNamed: "4 Players Option")
        fourPlayersCom.name = "FourPlayersCom"
        fourPlayersCom.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.255)
        fourPlayersCom.zPosition = 2
        fourPlayersCom.size = CGSize(width: 148, height: 156)
        fourPlayersCom.delegate = self
        fourPlayersCom.isUserInteractionEnabled = true
        fourPlayersCom.isHidden = true
        addChild(fourPlayersCom)
        
        
        
        
        twoPlayersFriend = NewSpriteNodeButton(imageNamed: "2 Players Option")
        twoPlayersFriend.name = "TwoPlayersFriend"
        twoPlayersFriend.position = CGPoint(x: (scene?.size.width)! * -0.262, y: (scene?.size.height)! * 0.255)
        twoPlayersFriend.zPosition = 2
        twoPlayersFriend.size = CGSize(width: 148, height: 156)
        twoPlayersFriend.delegate = self
        twoPlayersFriend.isUserInteractionEnabled = true
        twoPlayersFriend.isHidden = true
        addChild(twoPlayersFriend)
        
        threePlayersFriend = NewSpriteNodeButton(imageNamed: "3 Players Option")
        threePlayersFriend.name = "ThreePlayersFriend"
        threePlayersFriend.position = CGPoint(x: (scene?.size.width)! * 0.262, y: (scene?.size.height)! * 0.255)
        threePlayersFriend.zPosition = 2
        threePlayersFriend.size = CGSize(width: 148, height: 156)
        threePlayersFriend.delegate = self
        threePlayersFriend.isUserInteractionEnabled = true
        threePlayersFriend.isHidden = true
        addChild(threePlayersFriend)
        
        fourPlayersFriend = NewSpriteNodeButton(imageNamed: "4 Players Option")
        fourPlayersFriend.name = "FourPlayersFriend"
        fourPlayersFriend.position = CGPoint(x: 0, y: (scene?.size.height)! * -0.255)
        fourPlayersFriend.zPosition = 2
        fourPlayersFriend.size = CGSize(width: 148, height: 156)
        fourPlayersFriend.delegate = self
        fourPlayersFriend.isUserInteractionEnabled = true
        fourPlayersFriend.isHidden = true
        addChild(fourPlayersFriend)
        
        
        rules.frame = CGRect(origin: CGPoint(x: (self.scene?.size.width)! * 0.35, y: (self.scene?.size.height)! * 0.30), size: CGSize(width: 300, height: 300))
        rules.backgroundColor = UIColor.clear
        
        rules.isHidden = true
        self.view?.addSubview(rules)
        
        
        futureFeaters.frame = CGRect(origin: CGPoint(x: (self.scene?.size.width)! * 0.32, y: (self.scene?.size.height)! * 0.38), size: CGSize(width: 350, height: 200))
        futureFeaters.backgroundColor = UIColor.white
        futureFeaters.isHidden = true
        self.view?.addSubview(futureFeaters)
        
        
        firstPlayer.frame = CGRect(origin: CGPoint(x: (self.scene?.size.width)! * 0.21, y: (self.scene?.size.height)! * 0.32), size: CGSize(width: 200, height: 33))
        firstPlayer.placeholder = "First Player Name"
        firstPlayer.backgroundColor = UIColor.white
        firstPlayer.autocorrectionType = UITextAutocorrectionType.no
        firstPlayer.delegate = self
        firstPlayer.isHidden = true
        view.addSubview(firstPlayer)
        
        
        secondPlayer.frame = CGRect(origin: CGPoint(x: (self.scene?.size.width)! * 0.21, y: (self.scene?.size.height)! * 0.44), size: CGSize(width: 200, height: 33))
        secondPlayer.placeholder = "Second Player Name"
        secondPlayer.backgroundColor = UIColor.white
        secondPlayer.autocorrectionType = UITextAutocorrectionType.no
        secondPlayer.delegate = self
        secondPlayer.isHidden = true
        self.view?.addSubview(secondPlayer)
        
        thirdPlayer.frame = CGRect(origin: CGPoint(x: (self.scene?.size.width)! * 0.21, y: (self.scene?.size.height)! * 0.56), size: CGSize(width: 200, height: 33))
        thirdPlayer.placeholder = "Third Player Name"
        thirdPlayer.backgroundColor = UIColor.white
        thirdPlayer.autocorrectionType = UITextAutocorrectionType.no
        thirdPlayer.delegate = self
        thirdPlayer.isHidden = true
        self.view?.addSubview(thirdPlayer)
        
        fourthPlayer.frame = CGRect(origin: CGPoint(x: (self.scene?.size.width)! * 0.21, y: (self.scene?.size.height)! * 0.68), size: CGSize(width: 200, height: 33))
        fourthPlayer.placeholder = "Fourth Player Name"
        fourthPlayer.backgroundColor = UIColor.white
        fourthPlayer.autocorrectionType = UITextAutocorrectionType.no
        fourthPlayer.delegate = self
        fourthPlayer.isHidden = true
        self.view?.addSubview(fourthPlayer)
        
        
        setupScrollView()
        
        setupFutureFeatures()
    }
    
    
    
    func changeBGColor() {
        
        if GlobalVariablesPad.backgroundColorName == 1 {
            
            self.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
            GlobalVariablesPad.backgroundColorName = BgColor.Dark.rawValue
            defaults.set(GlobalVariablesPad.backgroundColorName, forKey: "backgroundColor")
            defaults.synchronize()
            
        } else if GlobalVariablesPad.backgroundColorName == 2 {
            
            self.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
            GlobalVariablesPad.backgroundColorName = BgColor.Grey.rawValue
            defaults.set(GlobalVariablesPad.backgroundColorName, forKey: "backgroundColor")
            defaults.synchronize()
            
        } else if GlobalVariablesPad.backgroundColorName == 3 {
            
            self.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            GlobalVariablesPad.backgroundColorName = BgColor.Light.rawValue
            defaults.set(GlobalVariablesPad.backgroundColorName, forKey: "backgroundColor")
            defaults.synchronize()
            
        }
        
    }
    
    
    
    override func willMove(from view: SKView) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    func setupFutureFeatures() {
        
        let firstThing = UILabel()
        firstThing.translatesAutoresizingMaskIntoConstraints = false
        firstThing.textColor = UIColor.black
        firstThing.textAlignment = .center
        firstThing.text = "- Select Difficulty Mode."
        futureFeaters.addSubview(firstThing)
        
        firstThing.centerXAnchor.constraint(equalTo: futureFeaters.centerXAnchor, constant: -79).isActive = true
        firstThing.topAnchor.constraint(equalTo: futureFeaters.topAnchor, constant: 20).isActive = true
        firstThing.widthAnchor.constraint(equalToConstant: 200).isActive = true
        firstThing.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        let secondThing = UILabel()
        secondThing.translatesAutoresizingMaskIntoConstraints = false
        secondThing.textColor = UIColor.black
        secondThing.textAlignment = .center
        secondThing.text = "- Play with Friends on Multiple Devices(LAN)."
        futureFeaters.addSubview(secondThing)
        
        secondThing.centerXAnchor.constraint(equalTo: futureFeaters.centerXAnchor, constant: 0).isActive = true
        secondThing.topAnchor.constraint(equalTo: futureFeaters.topAnchor, constant: 50).isActive = true
        secondThing.widthAnchor.constraint(equalToConstant: 350).isActive = true
        secondThing.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        let thirdThing = UILabel()
        thirdThing.translatesAutoresizingMaskIntoConstraints = false
        thirdThing.textColor = UIColor.black
        thirdThing.textAlignment = .left
        thirdThing.text = "- Play Online Mode WorldWide."
        futureFeaters.addSubview(thirdThing)
        
        thirdThing.centerXAnchor.constraint(equalTo: futureFeaters.centerXAnchor, constant: -22).isActive = true
        thirdThing.topAnchor.constraint(equalTo: futureFeaters.topAnchor, constant: 80).isActive = true
        thirdThing.widthAnchor.constraint(equalToConstant: 300).isActive = true
        thirdThing.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        let fourthThing = UILabel()
        fourthThing.translatesAutoresizingMaskIntoConstraints = false
        fourthThing.textColor = UIColor.black
        fourthThing.textAlignment = .left
        fourthThing.text = "- Batter Ai Player."
        futureFeaters.addSubview(fourthThing)
        
        fourthThing.centerXAnchor.constraint(equalTo: futureFeaters.centerXAnchor, constant: -72).isActive = true
        fourthThing.topAnchor.constraint(equalTo: futureFeaters.topAnchor, constant: 110).isActive = true
        fourthThing.widthAnchor.constraint(equalToConstant: 200).isActive = true
        fourthThing.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    
    func setupScrollView() {
        
        scrollView.topAnchor.constraint(equalTo: rules.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: rules.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: rules.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: rules.rightAnchor).isActive = true
        
    }
    
    
    func hideSettings() {
        
        if share.isHidden && audio.isHidden { self.settings.isHidden = true }
        howToPlay.run(rotateAction); howToPlay.run(moveAction10)
        audio.run(rotateAction); audio.run(moveAction10)
        facebook.run(rotateAction); facebook.run(moveAction10)
        rateUs.run(rotateAction); rateUs.run(moveAction10)
        comingSoon.run(rotateAction); comingSoon.run(moveAction10)
        theme.run(rotateAction); theme.run(moveAction10)
        share.run(rotateAction); share.run(moveAction10) {
            self.noAds.isHidden = true
            self.howToPlay.isHidden = true
            self.audio.isHidden = true
            self.facebook.isHidden = true
            self.rateUs.isHidden = true
            self.share.isHidden = true
            self.settings.isHidden = true
            self.comingSoon.isHidden = true
            self.theme.isHidden = true
            
        }
        
        
        
    }
    
    
    
    
    func share(scene: SKScene, text: String, image: UIImage?, link: URL?) {
        
        guard let image = image else {return}
        
        
        guard let link = link else {return}
        
        let shareItems = [text, image, link] as [Any]
        
        
        let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = scene.view
        
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
        
        
        scene.view?.window?.rootViewController?.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
    func rateApp() {
        
        if #available(iOS 10.3, *) {
            
            SKStoreReviewController.requestReview()
            
        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "id1493124693") {
            
            if #available(iOS 10, *) {
                
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                
            } else {
                
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    
    
    func playSound(soundName: SKAction) {
        if UserDefaults.standard.bool(forKey: "audio") {
            run(soundName)
        }
    }
    
    
    
    
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        for touch in touches {
            
            let location = touch.location(in: self)
            let nodes = atPoint(location)
            
            if (nodes.name == "blackshadow") {
                
                if !rules.isHidden {
                    blackShadow.isHidden = true
                    rules.isHidden = true
                } else if !futureFeaters.isHidden {
                    blackShadow.isHidden = true
                    futureFeaters.isHidden = true
                }
            }
            
        }
        
        
        if firstPlayer.isEditing || secondPlayer.isEditing || thirdPlayer.isEditing || fourthPlayer.isEditing {
            print("It's Writing SOMETHING!")
            self.view?.endEditing(true)
        }
        
        
        
    }
    
    
    //MARK: - ButtonFeelDelegate
    
    
    func spriteNodeButtonPressed(_ button: NewSpriteNodeButton) {
        
        
        if (button == settings && share.isHidden == false) {
            
            print("In DUDE :")
            
            playSound(soundName: buttonSound)
            
            comingSoon.run(rotateAction); comingSoon.run(moveAction10)
            howToPlay.run(rotateAction); howToPlay.run(moveAction10)
            audio.run(rotateAction); audio.run(moveAction10)
            facebook.run(rotateAction); facebook.run(moveAction10)
            rateUs.run(rotateAction); rateUs.run(moveAction10)
            theme.run(rotateAction); theme.run(moveAction10)
            share.run(rotateAction); share.run(moveAction10) {
                self.howToPlay.isHidden = true
                self.audio.isHidden = true
                self.facebook.isHidden = true
                self.rateUs.isHidden = true
                self.share.isHidden = true
                self.comingSoon.isHidden = true
                self.theme.isHidden = true
            }
            
            
            
        } else if (button == settings && share.isHidden == true) {
            
            ote += 1
            
            playSound(soundName: buttonSound)
            
            howToPlay.isHidden = false
            audio.isHidden = false
            facebook.isHidden = false
            rateUs.isHidden = false
            share.isHidden = false
            comingSoon.isHidden = false
            theme.isHidden = false
            
            howToPlay.run(rotateAction); howToPlay.run(moveAction)
            comingSoon.run(rotateAction); comingSoon.run(moveAction6)
            audio.run(rotateAction); audio.run(moveAction1)
            facebook.run(rotateAction); facebook.run(moveAction3)
            
            // howToPlay.zRotation = 60
            
            theme.run(rotateAction); theme.run(moveAction2)
            rateUs.run(rotateAction); rateUs.run(moveAction4)
            share.run(rotateAction); share.run(moveAction5)
            
        }
        
        
        
        switch button {
        case vsCom:
            
            hideSettings()
            playSound(soundName: buttonSound)
            
            twoPlayersCom.run(SKAction.init(named: "ComeOut")!)
            threePlayersCom.run(SKAction.init(named: "ComeOut")!)
            fourPlayersCom.run(SKAction.init(named: "ComeOut")!)
            
            twoPlayersCom.isHidden = false
            threePlayersCom.isHidden = false
            fourPlayersCom.isHidden = false
            
            
            
            vsCom.isHidden = true
            vsFriend.isHidden = true
            
            cancel.isHidden = false
            
        case vsFriend:
            
            hideSettings()
            playSound(soundName: buttonSound)
            
            twoPlayersFriend.run(SKAction.init(named: "ComeOut")!)
            threePlayersFriend.run(SKAction.init(named: "ComeOut")!)
            fourPlayersFriend.run(SKAction.init(named: "ComeOut")!)
            
            twoPlayersFriend.isHidden = false
            threePlayersFriend.isHidden = false
            fourPlayersFriend.isHidden = false
            
            
            cancel.isHidden = false
            
            vsCom.isHidden = true
            vsFriend.isHidden = true
            
        case share:
            playSound(soundName: buttonSound)
            print("In Share")
            // SHARE code Here :
            
//            share(scene: self, text: "Download This Board Game i Liked this Game a Lot.", image: UIImage(named: "Kakri"),  )
            
            share(scene: self, text: "I liked Playing this Game, i think you would like it too. Download from this link.", image: UIImage(named: "Game Photo"), link: URL(string: "http://itunes.apple.com/app/id1493124693"))
            
        case audio:
            
            if GlobalVariablesPad.audio {
                playSound(soundName: buttonSound)
                GlobalVariablesPad.audio = false
                defaults.set(GlobalVariablesPad.audio, forKey: "audio")
                defaults.synchronize()
            } else {
                GlobalVariablesPad.audio = true
                defaults.set(GlobalVariablesPad.audio, forKey: "audio")
                defaults.synchronize()
                playSound(soundName: buttonSound)
            }
            
        case facebook:
            
            playSound(soundName: buttonSound)
            
            let url = URL(string: "https://www.facebook.com/pg/RealLudoiOS")
            UIApplication.shared.openURL(url!)
            
            
        case comingSoon:
            
            playSound(soundName: buttonSound)
            
            if futureFeaters.isHidden {
                blackShadow.isHidden = false
                futureFeaters.isHidden = false
            } else {
                blackShadow.isHidden = true
                futureFeaters.isHidden = true
            }
            
        case rateUs:
            
            playSound(soundName: buttonSound)
            rateApp()
            
        case cancel:
            
            playSound(soundName: buttonSound)
            
            self.view?.endEditing(true)
            
            twoPlayersCom.isHidden = true
            threePlayersCom.isHidden = true
            fourPlayersCom.isHidden = true
            
            twoPlayersFriend.isHidden = true
            threePlayersFriend.isHidden = true
            fourPlayersFriend.isHidden = true
            
            firstPlayer.isHidden = true
            secondPlayer.isHidden = true
            thirdPlayer.isHidden = true
            fourthPlayer.isHidden = true
            
            settings.isHidden = false
            noAds.isHidden = false
            
            play.isHidden = true
            
            vsCom.isHidden = false
            vsFriend.isHidden = false
            
            cancel.isHidden = true
            
            
        case play:
            
            if (playGame == "vstwofriend") {
                
                self.view?.endEditing(true)
                playSound(soundName: buttonSound)
                if !(firstPlayer.text?.isEmpty)! { playerOne = firstPlayer.text!
                    GlobalVariablesPad.player1Text = firstPlayer.text!
                    
                }
                if !(secondPlayer.text?.isEmpty)! { playerTwo = secondPlayer.text!
                    GlobalVariablesPad.player2Text = secondPlayer.text!
                }
                
                firstPlayer.isHidden = true
                secondPlayer.isHidden = true
                thirdPlayer.isHidden = true
                fourthPlayer.isHidden = true
                
                
                let gamescene = SKScene(fileNamed: "VsFriendTwoPad")
                gamescene?.scaleMode = .aspectFit
                self.view?.presentScene(gamescene!, transition: SKTransition.fade(withDuration: 1))
                
            } else if (playGame == "vsthreefriend") {
                
                self.view?.endEditing(true)
                playSound(soundName: buttonSound)
                if !(firstPlayer.text?.isEmpty)! { playerOne = firstPlayer.text!
                    GlobalVariablesPad.player1Text = firstPlayer.text!
                }
                if !(secondPlayer.text?.isEmpty)! { playerTwo = secondPlayer.text!
                    GlobalVariablesPad.player2Text = secondPlayer.text!
                }
                if !(thirdPlayer.text?.isEmpty)! { playerThree = thirdPlayer.text!
                    GlobalVariablesPad.player3Text = thirdPlayer.text!
                }
                
                firstPlayer.isHidden = true
                secondPlayer.isHidden = true
                thirdPlayer.isHidden = true
                fourthPlayer.isHidden = true
                
                
                let vscomthree = SKScene(fileNamed: "VsFriendThreePad")
                vscomthree?.scaleMode = .aspectFit
                self.view?.presentScene(vscomthree!, transition: SKTransition.fade(withDuration: 1))
                
            } else if (playGame == "vsfourfriend") {
                
                self.view?.endEditing(true)
                playSound(soundName: buttonSound)
                if !(firstPlayer.text?.isEmpty)! { playerOne = firstPlayer.text!
                    GlobalVariablesPad.player1Text = firstPlayer.text!
                }
                if !(secondPlayer.text?.isEmpty)! { playerTwo = secondPlayer.text!
                    GlobalVariablesPad.player2Text = secondPlayer.text!
                }
                if !(thirdPlayer.text?.isEmpty)! { playerThree = thirdPlayer.text!
                    GlobalVariablesPad.player3Text = thirdPlayer.text!
                }
                if !(fourthPlayer.text?.isEmpty)! { playerFour = fourthPlayer.text!
                    GlobalVariablesPad.player4Text = fourthPlayer.text!
                }
                
                
                firstPlayer.isHidden = true
                secondPlayer.isHidden = true
                thirdPlayer.isHidden = true
                fourthPlayer.isHidden = true
                
                
                let vscomtwo = SKScene(fileNamed: "VsFriendFourPad")
                vscomtwo?.scaleMode = .aspectFit
                self.view?.presentScene(vscomtwo!, transition: SKTransition.fade(withDuration: 0.5))
                
            }
            
        case twoPlayersCom:
            
            playSound(soundName: buttonSound)
            
            twoPlayersCom.isHidden = true
            threePlayersCom.isHidden = true
            fourPlayersCom.isHidden = true
            
            
            let gamescene = SKScene(fileNamed: "VsComTwoPad")
            gamescene?.scaleMode = .aspectFit
            self.view?.presentScene(gamescene!, transition: SKTransition.fade(withDuration: 1))
            
        case threePlayersCom:
            
            playSound(soundName: buttonSound)
            
            twoPlayersCom.isHidden = true
            threePlayersCom.isHidden = true
            fourPlayersCom.isHidden = true
            
            
            
            firstPlayer.isHidden = true
            secondPlayer.isHidden = true
            thirdPlayer.isHidden = true
            fourthPlayer.isHidden = true
            
            
            let vscomthree = SKScene(fileNamed: "VsComThreePad")
            vscomthree?.scaleMode = .aspectFit
            self.view?.presentScene(vscomthree!, transition: SKTransition.fade(withDuration: 1))
            
        case fourPlayersCom:
            
            twoPlayersCom.isHidden = true
            threePlayersCom.isHidden = true
            fourPlayersCom.isHidden = true
            
            firstPlayer.isHidden = true
            secondPlayer.isHidden = true
            thirdPlayer.isHidden = true
            fourthPlayer.isHidden = true
            
            
            playSound(soundName: buttonSound)
            
            
            let vscomtwo = SKScene(fileNamed: "VsComFourPad")
            vscomtwo?.scaleMode = .aspectFit
            self.view?.presentScene(vscomtwo!, transition: SKTransition.fade(withDuration: 1))
            
        case twoPlayersFriend:
            
            playSound(soundName: buttonSound)
            playGame = "vstwofriend"
            
            play.isHidden = false
            
            twoPlayersFriend.isHidden = true
            threePlayersFriend.isHidden = true
            fourPlayersFriend.isHidden = true
            
            firstPlayer.isHidden = false
            secondPlayer.isHidden = false
            
        case threePlayersFriend:
            
            playSound(soundName: buttonSound)
            playGame = "vsthreefriend"
            play.isHidden = false
            
            twoPlayersFriend.isHidden = true
            threePlayersFriend.isHidden = true
            fourPlayersFriend.isHidden = true
            
            firstPlayer.isHidden = false
            secondPlayer.isHidden = false
            thirdPlayer.isHidden = false
            
        case fourPlayersFriend:
            
            playSound(soundName: buttonSound)
            playGame = "vsfourfriend"
            play.isHidden = false
            
            twoPlayersFriend.isHidden = true
            threePlayersFriend.isHidden = true
            fourPlayersFriend.isHidden = true
            
            firstPlayer.isHidden = false
            secondPlayer.isHidden = false
            thirdPlayer.isHidden = false
            fourthPlayer.isHidden = false
            
            
        case howToPlay:
            
            playSound(soundName: buttonSound)
            
            
            if !rules.isHidden {
                
                rules.isHidden = true
                blackShadow.isHidden = true
                
            } else {
                rules.isHidden = false
                blackShadow.isHidden = false
            }
            
        case theme:
            
            playSound(soundName: buttonSound)
            changeBGColor()
            
        case noAds:
            
            let purchase = SKScene(fileNamed: "PaymentPad")
            
            purchase?.scaleMode = .aspectFit
            self.view?.presentScene(purchase!, transition: SKTransition.fade(withDuration: 1))
            
            
        default:
            break
        }
        
        
        
        
    }
    
    
    deinit {
        
        self.removeAllChildren()
        self.removeAllActions()
        print("Everything Got Deinitialyed ! HomeScenePad")
    }
    
    
}
