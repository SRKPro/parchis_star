//
//  VsComTwoPad.swift
//  Indian Ludo
//
//  Created by ramesh sanghar on 03/01/19.
//  Copyright © 2019 Om. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import UnityAds


class VsComTwoPad: SKScene, NewSpriteNodeButtonDelegate {
    
    
    let defaults = UserDefaults.standard
    
    
    
    // Players
    
    enum Player:Int {
        
        case Player1 = 1, Player2
        
    }
    
    
    
    let buttonSound = SKAction.playSoundFileNamed("multimedia_button_click_029.mp3", waitForCompletion: false)
    let finalWon = SKAction.playSoundFileNamed("Final Won.wav", waitForCompletion: false)
    
    
    var playerone:String = "You"
    var playertwo:String = "Computer"
    
    let moveAudio = SKAction.playSoundFileNamed("multimedia_button_click_006.mp3", waitForCompletion: false)
    let diceRollSFX = SKAction.playSoundFileNamed("dice_roll_on_gameboard_01.mp3", waitForCompletion: false)
    let killedAudio = SKAction.playSoundFileNamed("24275 cartoon boy disappoint shout-full.wav", waitForCompletion: false)
    let oneWon = SKAction.playSoundFileNamed("1 Won.wav", waitForCompletion: false)
    let fourAndEight = SKAction.playSoundFileNamed("4 or 8 new.wav", waitForCompletion: true)
    
    
    
    
    
    
    // Player Position
    
    var currentSpacePlayer1Piece1:Int = 1
    var currentSpacePlayer1Piece2:Int = 1
    var currentSpacePlayer1Piece3:Int = 1
    var currentSpacePlayer1Piece4:Int = 1
    
    var currentSpacePlayer2Piece1:Int = 9
    var currentSpacePlayer2Piece2:Int = 9
    var currentSpacePlayer2Piece3:Int = 9
    var currentSpacePlayer2Piece4:Int = 9
    
    
    
    var playerTturns:Int = 0
    var computer1Tturns:Int = 0
    
    
    var p1p1TryToWin:Int = 0
    var p1p2TryToWin:Int = 0
    var p1p3TryToWin:Int = 0
    var p1p4TryToWin:Int = 0
    
    var p2p1TryToWin:Int = 0
    var p2p2TryToWin:Int = 0
    var p2p3TryToWin:Int = 0
    var p2p4TryToWin:Int = 0
    
    
    
    var firstPiece:SKSpriteNode = SKSpriteNode()
    var secondPiece:SKSpriteNode = SKSpriteNode()
    var thirdPiece:SKSpriteNode = SKSpriteNode()
    var fourthPiece:SKSpriteNode = SKSpriteNode()
    
    
    var player2FourthPiece:SKSpriteNode = SKSpriteNode()
    
    var playerPiece:SKSpriteNode = SKSpriteNode()
    
    var killedPiece:SKSpriteNode = SKSpriteNode()
    
    
    
    
    var Dice:SKSpriteNode = SKSpriteNode()
    var Dice2:SKSpriteNode = SKSpriteNode()
    var finalPasso:SKSpriteNode = SKSpriteNode()
    
    
    
    var player1moves:Int = 0
    var player2moves:Int = 0
    
    var OneMoreMove:Int = 0
    
    var place = [String]()
    
    
    // Roll Dice Variables From Here :
    
    var Rolls = [Int]()
    var Player1Rolls = [Int]()
    var Player2Rolls = [Int]()
    
    
    var i = 0
    var j = 0
    var k = 0
    var l = 0
    
    var player1rans : Int = 0
    var player2rans : Int = 0
    var totalRans : Int = 0
    
    
    var playerScore:Int = 0
    var com1Score:Int = 0
    
    
    var player1Piece1Score : Int = 1
    var player1Piece2Score : Int = 1
    var player1Piece3Score : Int = 1
    var player1Piece4Score : Int = 1
    
    var com1Piece1Score : Int = 1
    var com1Piece2Score : Int = 1
    var com1Piece3Score : Int = 1
    var com1Piece4Score : Int = 1
    
    
    var movesRemaining:Int = 0
    var movesRemainingBack:Int = 0
    var whosTurn:Player = .Player1
    
    var moveFinished: Bool = true
    
    
    
    
    var piece1AbletoEnter: Bool = true
    var piece2AbletoEnter: Bool = true
    var piece3AbletoEnter: Bool = true
    var piece4AbletoEnter: Bool = true
    var diceRolled: Bool = false
    var gameOver: Bool = false
    var rolling: Bool = false
    
    
    
    // <------------------- Just For Checking Vars From Here --------------------->
    
    private var eyesOpen: Bool = true
    
    private var p1P1Reached: Bool = false
    private var p1P2Reached: Bool = false
    private var p1P3Reached: Bool = false
    private var p1P4Reached: Bool = false
    
    private var p2P1Reached: Bool = false
    private var p2P2Reached: Bool = false
    private var p2P3Reached: Bool = false
    private var p2P4Reached: Bool = false
    
    
    var keepAnimating = true
    
    
    
    var BloodedPlayer1: Bool = false
    
    var BloodedPlayer2: Bool = false
    
    
    var rolledDice: Int = 0
    
    
    var p1p1Plus10: Bool = false
    var p1p2Plus10: Bool = false
    var p1p3Plus10: Bool = false
    var p1p4Plus10: Bool = false
    
    var p2p1Plus10: Bool = false
    var p2p2Plus10: Bool = false
    var p2p3Plus10: Bool = false
    var p2p4Plus10: Bool = false
    
    var dontScoreP1: Bool = false
    var dontScoreP2: Bool = false
    
    
    // Player Name Labels :
    var playerLabel = SKLabelNode()
    var computer1Label = SKLabelNode()
    
    
    var oneMoreMove = SKLabelNode()
    var cantMovePla = SKLabelNode()
    var threeLargeNumsinrowPla = SKLabelNode()
    
    
    var playerTotalTurns:SKLabelNode = SKLabelNode()
    var computer1TotalTurns:SKLabelNode = SKLabelNode()
    
    var plaTTtext:SKLabelNode = SKLabelNode()
    var com1TTtext:SKLabelNode = SKLabelNode()
    
    
    var playerScoreLabel:SKLabelNode = SKLabelNode()
    var com1ScoreLabel:SKLabelNode = SKLabelNode()
    
    
    var Player1Piece1:SKSpriteNode = SKSpriteNode()
    var Player1Piece2:SKSpriteNode = SKSpriteNode()
    var Player1Piece3:SKSpriteNode = SKSpriteNode()
    var Player1Piece4:SKSpriteNode = SKSpriteNode()
    
    
    var Player2Piece1:SKSpriteNode = SKSpriteNode()
    var Player2Piece2:SKSpriteNode = SKSpriteNode()
    var Player2Piece3:SKSpriteNode = SKSpriteNode()
    var Player2Piece4:SKSpriteNode = SKSpriteNode()
    
    
    var touchedNode:SKSpriteNode = SKSpriteNode()
    
    
    
    var Arrow1Red = SKSpriteNode(imageNamed: "Arrow 4 Red")
    var Arrow2Red = SKSpriteNode(imageNamed: "Arrow 3 Red")
    var Arrow3Red = SKSpriteNode(imageNamed: "Arrow 2 Red")
    var Arrow4Red = SKSpriteNode(imageNamed: "Arrow 1 Red")
    
    
    
    var player1Piece1eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece1eye2:SKSpriteNode = SKSpriteNode()
    
    var player1Piece2eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece2eye2:SKSpriteNode = SKSpriteNode()
    
    var player1Piece3eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece3eye2:SKSpriteNode = SKSpriteNode()
    
    var player1Piece4eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece4eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece1eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece1eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece2eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece2eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece3eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece3eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece4eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece4eye2:SKSpriteNode = SKSpriteNode()
    
    
    
    var redScore:SKSpriteNode = SKSpriteNode()
    var yellowScore:SKSpriteNode = SKSpriteNode()
    
    
    // UI Buttorn :
    
    var backButton:NewSpriteNodeButton = NewSpriteNodeButton()
    var PauseMenu:SKSpriteNode = SKSpriteNode()
    var cancel:NewSpriteNodeButton = NewSpriteNodeButton()
    var home:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var audioButton:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var closeEye = SKTexture(imageNamed: "Close Eye")
    var openEye = SKTexture(imageNamed: "Open Eye")
    
    
    
    let f1 = SKTexture.init(imageNamed: "Ani 1")
    let f2 = SKTexture.init(imageNamed: "Ani 2")
    let f3 = SKTexture.init(imageNamed: "Ani 3")
    let f4 = SKTexture.init(imageNamed: "Ani 4")
    let f5 = SKTexture.init(imageNamed: "Ani 5")
    let f6 = SKTexture.init(imageNamed: "Ani 6")
    let f7 = SKTexture.init(imageNamed: "Ani 7")
    let f8 = SKTexture.init(imageNamed: "Ani 8")
    let f9 = SKTexture.init(imageNamed: "Ani 9")
    let f10 = SKTexture.init(imageNamed: "Ani 10")
    let f11 = SKTexture.init(imageNamed: "Ani 11")
    let f12 = SKTexture.init(imageNamed: "Ani 12")
    let f13 = SKTexture.init(imageNamed: "Ani 13")
    let f14 = SKTexture.init(imageNamed: "Ani 14")
    let f15 = SKTexture.init(imageNamed: "Ani 15")
    let f16 = SKTexture.init(imageNamed: "Ani 16")
    let f17 = SKTexture.init(imageNamed: "Ani 17")
    
    
    var diceTextures: [SKTexture] = []
    var rolledDiceTexture = SKSpriteNode()
    
    var blackShadow:SKSpriteNode = SKSpriteNode()
    
    var textColore:UIColor = UIColor()
    
    override func didMove(to view: SKView) {
        
        
        GlobalVariablesPad.totalGames += 1
        defaults.set(GlobalVariablesPad.totalGames, forKey: "totalGames")
        defaults.synchronize()
        
        
        if GlobalVariablesPad.backgroundColorName == 1 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            textColore = GlobalVariablesPad.darkTextColor
            
        } else if GlobalVariablesPad.backgroundColorName == 2 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
            textColore = GlobalVariablesPad.whiteTextColor
            
        } else if GlobalVariablesPad.backgroundColorName == 3 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
            textColore = GlobalVariablesPad.whiteTextColor
            
        }
        
        
        if defaults.value(forKey: "Purchase") == nil {
            
            if UnityServices.isInitialized() {
                
                print("Calling Ads !")
                
                if GlobalVariablesPad.totalGames % 2 == 0 {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playAdID"), object: nil)
                    
                }
            }
            
        }
        
        
        diceTextures = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17]
        
        
        blackShadow = SKSpriteNode(imageNamed: "Black Shadow")
        blackShadow.name = "blackshadow"
        blackShadow.position = CGPoint(x: 0, y: 0)
        blackShadow.zPosition = 9
        blackShadow.isHidden = true
        addChild(blackShadow)
        
        
        Dice = SKSpriteNode(imageNamed: "Dice3")
        Dice.name = "Dice"
        Dice.isUserInteractionEnabled = false
        Dice.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
        Dice.zPosition = 3
        Dice.size = CGSize(width: 150.0, height: 150.0)
        Dice.isHidden = false
        addChild(Dice)
        
        
        
        Dice2 = SKSpriteNode(imageNamed: "Dice3")
        Dice2.name = "Dice2"
        Dice2.isUserInteractionEnabled = false
        Dice2.position = CGPoint(x: (scene?.size.width)! * -0.395, y: 0)
        Dice2.zPosition = 3
        Dice2.size = CGSize(width: 150.0, height: 150.0)
        Dice2.isHidden = true
        addChild(Dice2)
        
        
        
        backButton = NewSpriteNodeButton(imageNamed: "back")
        backButton.name = "backButton"
        backButton.position = CGPoint(x: (scene?.size.width)! * -0.484, y: 0)
        backButton.zPosition = 3
        backButton.size = CGSize(width: 29, height: 84)
        backButton.isUserInteractionEnabled = true
        backButton.delegate = self
        addChild(backButton)
        
        PauseMenu = SKSpriteNode(imageNamed: "Pause menu")
        PauseMenu.name = "PauseMenu"
        PauseMenu.position = CGPoint(x: 0, y: 0)
        PauseMenu.zPosition = 10
        PauseMenu.size = CGSize(width: 664, height: 377)
        PauseMenu.isHidden = true
        addChild(PauseMenu)
        
        cancel = NewSpriteNodeButton(imageNamed: "Cancel")
        cancel.name = "cancel"
        cancel.position = CGPoint(x: PauseMenu.size.width * 0.43, y: PauseMenu.size.height * 0.38)
        cancel.zPosition = 11
        cancel.size = CGSize(width: 35, height: 43)
        cancel.isUserInteractionEnabled = true
        cancel.delegate = self
        cancel.isHidden = true
        addChild(cancel)
        
        home = NewSpriteNodeButton(imageNamed: "Home")
        home.name = "home"
        home.position = CGPoint(x: PauseMenu.size.width * -0.13, y: PauseMenu.size.height * -0.14)
        home.zPosition = 11
        home.size = CGSize(width: 76, height: 81)
        home.isUserInteractionEnabled = true
        home.delegate = self
        home.isHidden = true
        addChild(home)
        
        
        audioButton = NewSpriteNodeButton(imageNamed: "Audio")
        audioButton.name = "audiobutton"
        audioButton.position = CGPoint(x: PauseMenu.size.width * 0.13, y: PauseMenu.size.height * -0.14)
        audioButton.zPosition = 11
        audioButton.size = CGSize(width: 76, height: 81)
        audioButton.delegate = self
        audioButton.isUserInteractionEnabled = true
        audioButton.isHidden = true
        addChild(audioButton)
        
        
        Player1Piece1 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece1.name = "Player1Piece1"
        Player1Piece1.isUserInteractionEnabled = false
        Player1Piece1.position = CGPoint(x: 0, y: -282)
        Player1Piece1.zPosition = 3
        Player1Piece1.size = CGSize(width: 40, height: 40)
        addChild(Player1Piece1)
        
        Player1Piece2 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece2.name = "Player1Piece2"
        Player1Piece2.isUserInteractionEnabled = false
        Player1Piece2.position = CGPoint(x: 33, y: -249)
        Player1Piece2.zPosition = 3
        Player1Piece2.size = CGSize(width: 40, height: 40)
        addChild(Player1Piece2)
        
        Player1Piece3 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece3.name = "Player1Piece3"
        Player1Piece3.isUserInteractionEnabled = false
        Player1Piece3.position = CGPoint(x: 0, y: -215)
        Player1Piece3.zPosition = 3
        Player1Piece3.size = CGSize(width: 40, height: 40)
        addChild(Player1Piece3)
        
        Player1Piece4 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece4.name = "Player1Piece4"
        Player1Piece4.isUserInteractionEnabled = false
        Player1Piece4.position = CGPoint(x: -33, y: -249)
        Player1Piece4.zPosition = 3
        Player1Piece4.size = CGSize(width: 40, height: 40)
        addChild(Player1Piece4)
        
        
        
        Player2Piece1 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece1.name = "Player2Piece1"
        Player2Piece1.isUserInteractionEnabled = false
        Player2Piece1.position = CGPoint(x: 1, y: 216)
        Player2Piece1.zPosition = 3
        Player2Piece1.size = CGSize(width: 40, height: 40)
        addChild(Player2Piece1)
        
        Player2Piece2 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece2.name = "Player2Piece2"
        Player2Piece2.isUserInteractionEnabled = false
        Player2Piece2.position = CGPoint(x: 34, y: 250)
        Player2Piece2.zPosition = 3
        Player2Piece2.size = CGSize(width: 40, height: 40)
        addChild(Player2Piece2)
        
        Player2Piece3 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece3.name = "Player2Piece3"
        Player2Piece3.isUserInteractionEnabled = false
        Player2Piece3.position = CGPoint(x: 1, y: 283)
        Player2Piece3.zPosition = 3
        Player2Piece3.size = CGSize(width: 40, height: 40)
        addChild(Player2Piece3)
        
        Player2Piece4 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece4.name = "Player2Piece4"
        Player2Piece4.isUserInteractionEnabled = false
        Player2Piece4.position = CGPoint(x: -32, y: 250)
        Player2Piece4.zPosition = 3
        Player2Piece4.size = CGSize(width: 40, height: 40)
        addChild(Player2Piece4)
        
        
        
        redScore = SKSpriteNode(imageNamed: "Score Red")
        redScore.name = "RedScore"
        redScore.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * -0.40)
        redScore.zPosition = 1
        redScore.size = CGSize(width: 116, height: 38)
        addChild(redScore)
        
        yellowScore = SKSpriteNode(imageNamed: "Score Yellow")
        yellowScore.name = "YellowScore"
        yellowScore.position = CGPoint(x: (scene?.size.width)! * -0.40, y: (scene?.size.height)! * 0.41)
        yellowScore.zPosition = 1
        yellowScore.size = CGSize(width: 116, height: 38)
        addChild(yellowScore)
        
        
        
        Arrow1Red.name = "arrow1red"
        Arrow1Red.position = CGPoint(x: 408, y: -108)
        Arrow1Red.zPosition = 1
        Arrow1Red.size = CGSize(width: 46, height: 29)
        Arrow1Red.isHidden = true
        addChild(Arrow1Red)
        
        Arrow2Red.name = "arrow2red"
        Arrow2Red.position = CGPoint(x: 408, y: -132)
        Arrow2Red.zPosition = 1
        Arrow2Red.size = CGSize(width: 46, height: 29)
        Arrow2Red.isHidden = true
        addChild(Arrow2Red)
        
        Arrow3Red.name = "arrow3red"
        Arrow3Red.position = CGPoint(x: 408, y: -156)
        Arrow3Red.zPosition = 1
        Arrow3Red.size = CGSize(width: 46, height: 29)
        Arrow3Red.isHidden = true
        addChild(Arrow3Red)
        
        Arrow4Red.name = "arrow4red"
        Arrow4Red.position = CGPoint(x: 408, y: -180)
        Arrow4Red.zPosition = 1
        Arrow4Red.size = CGSize(width: 46, height: 29)
        Arrow4Red.isHidden = true
        addChild(Arrow4Red)
        
        
        
        
        player1Piece1eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece1eye1.name = "player1Piece1eye1"
        player1Piece1eye1.position = CGPoint(x: -(Player1Piece1.size.width / 5), y: 1)
        player1Piece1eye1.zPosition = 4
        player1Piece1eye1.size = CGSize(width: 8, height: 8)
        player1Piece1eye1.isUserInteractionEnabled = false
        Player1Piece1.addChild(player1Piece1eye1)
        
        player1Piece1eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece1eye2.name = "player1Piece1eye2"
        player1Piece1eye2.position = CGPoint(x: Player1Piece1.size.width / 5, y: 1)
        player1Piece1eye2.zPosition = 4
        player1Piece1eye2.size = CGSize(width: 8, height: 8)
        player1Piece1eye2.isUserInteractionEnabled = false
        Player1Piece1.addChild(player1Piece1eye2)
        
        
        player1Piece2eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece2eye1.name = "player1Piece2eye1"
        player1Piece2eye1.position = CGPoint(x: -(Player1Piece2.size.width / 5), y: 1)
        player1Piece2eye1.zPosition = 4
        player1Piece2eye1.size = CGSize(width: 8, height: 8)
        player1Piece2eye1.isUserInteractionEnabled = false
        Player1Piece2.addChild(player1Piece2eye1)
        
        
        player1Piece2eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece2eye2.name = "player1Piece2eye2"
        player1Piece2eye2.position = CGPoint(x: Player1Piece2.size.width / 5, y: 1)
        player1Piece2eye2.zPosition = 4
        player1Piece2eye2.size = CGSize(width: 8, height: 8)
        player1Piece2eye2.isUserInteractionEnabled = false
        Player1Piece2.addChild(player1Piece2eye2)
        
        
        player1Piece3eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece3eye1.name = "player1Piece3eye1"
        player1Piece3eye1.position = CGPoint(x: -(Player1Piece3.size.width / 5), y: 1)
        player1Piece3eye1.zPosition = 4
        player1Piece3eye1.size = CGSize(width: 8, height: 8)
        player1Piece3eye1.isUserInteractionEnabled = false
        Player1Piece3.addChild(player1Piece3eye1)
        
        
        player1Piece3eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece3eye2.name = "player1Piece3eye2"
        player1Piece3eye2.position = CGPoint(x: Player1Piece3.size.width / 5, y: 1)
        player1Piece3eye2.zPosition = 4
        player1Piece3eye2.size = CGSize(width: 8, height: 8)
        player1Piece3eye2.isUserInteractionEnabled = false
        Player1Piece3.addChild(player1Piece3eye2)
        
        
        player1Piece4eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece4eye1.name = "player1Piece4eye1"
        player1Piece4eye1.position = CGPoint(x: -(Player1Piece4.size.width / 5), y: 1)
        player1Piece4eye1.zPosition = 4
        player1Piece4eye1.size = CGSize(width: 8, height: 8)
        player1Piece4eye1.isUserInteractionEnabled = false
        Player1Piece4.addChild(player1Piece4eye1)
        
        
        player1Piece4eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece4eye2.name = "player1Piece4eye2"
        player1Piece4eye2.position = CGPoint(x: Player1Piece4.size.width / 5, y: 1)
        player1Piece4eye2.zPosition = 4
        player1Piece4eye2.size = CGSize(width: 8, height: 8)
        player1Piece4eye2.isUserInteractionEnabled = false
        Player1Piece4.addChild(player1Piece4eye2)
        
        
        
        player2Piece1eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece1eye1.name = "player2Piece1eye1"
        player2Piece1eye1.position = CGPoint(x: -(Player2Piece1.size.width / 5), y: 1)
        player2Piece1eye1.zPosition = 4
        player2Piece1eye1.size = CGSize(width: 8, height: 8)
        player2Piece1eye1.isUserInteractionEnabled = false
        Player2Piece1.addChild(player2Piece1eye1)
        
        player2Piece1eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece1eye2.name = "player2Piece1eye2"
        player2Piece1eye2.position = CGPoint(x: Player2Piece1.size.width / 5, y: 1)
        player2Piece1eye2.zPosition = 4
        player2Piece1eye2.size = CGSize(width: 8, height: 8)
        player2Piece1eye2.isUserInteractionEnabled = false
        Player2Piece1.addChild(player2Piece1eye2)
        
        
        player2Piece2eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece2eye1.name = "player2Piece2eye1"
        player2Piece2eye1.position = CGPoint(x: -(Player2Piece2.size.width / 5), y: 1)
        player2Piece2eye1.zPosition = 4
        player2Piece2eye1.size = CGSize(width: 8, height: 8)
        player2Piece2eye1.isUserInteractionEnabled = false
        Player2Piece2.addChild(player2Piece2eye1)
        
        
        player2Piece2eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece2eye2.name = "player2Piece2eye2"
        player2Piece2eye2.position = CGPoint(x: Player2Piece2.size.width / 5, y: 1)
        player2Piece2eye2.zPosition = 4
        player2Piece2eye2.size = CGSize(width: 8, height: 8)
        player2Piece2eye2.isUserInteractionEnabled = false
        Player2Piece2.addChild(player2Piece2eye2)
        
        
        player2Piece3eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece3eye1.name = "player2Piece3eye1"
        player2Piece3eye1.position = CGPoint(x: -(Player2Piece3.size.width / 5), y: 1)
        player2Piece3eye1.zPosition = 4
        player2Piece3eye1.size = CGSize(width: 8, height: 8)
        player2Piece3eye1.isUserInteractionEnabled = false
        Player2Piece3.addChild(player2Piece3eye1)
        
        
        player2Piece3eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece3eye2.name = "player2Piece3eye2"
        player2Piece3eye2.position = CGPoint(x: Player2Piece3.size.width / 5, y: 1)
        player2Piece3eye2.zPosition = 4
        player2Piece3eye2.size = CGSize(width: 8, height: 8)
        player2Piece3eye2.isUserInteractionEnabled = false
        Player2Piece3.addChild(player2Piece3eye2)
        
        
        player2Piece4eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece4eye1.name = "player2Piece4eye1"
        player2Piece4eye1.position = CGPoint(x: -(Player2Piece4.size.width / 5), y: 1)
        player2Piece4eye1.zPosition = 4
        player2Piece4eye1.size = CGSize(width: 8, height: 8)
        player2Piece4eye1.isUserInteractionEnabled = false
        Player2Piece4.addChild(player2Piece4eye1)
        
        
        player2Piece4eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece4eye2.name = "player2Piece4eye2"
        player2Piece4eye2.position = CGPoint(x: Player2Piece4.size.width / 5, y: 1)
        player2Piece4eye2.zPosition = 4
        player2Piece4eye2.size = CGSize(width: 8, height: 8)
        player2Piece4eye2.isUserInteractionEnabled = false
        Player2Piece4.addChild(player2Piece4eye2)
        
        
        rolledDiceTexture.zPosition = 2
        rolledDiceTexture.isHidden = true
        addChild(rolledDiceTexture)
        
        
        setupLabels()
        
        flashTurn(player: whosTurn)
        animations()
        updateScore()
        
    }
    
    
    
    override func willMove(from view: SKView) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "playAdID"), object: nil)
        self.removeAllChildren()
        self.removeAllActions()
    }
    
    
    // MOVE FUNCTIONS FROM HERE : <-------------------------------------------------------------------
    
    
    func movePiece() {
        
        
        
        noWinList()
        
        
        removeFlashPlayer(player: whosTurn)
        
        checkWin(player:whosTurn)
        self.disabletoEnter()
        
        
        self.diceRolled = false
        
        print("Moves Remainig and OneMoreMove are :")
        print(self.movesRemaining)
        print(self.OneMoreMove)
        print(self.whosTurn)
        
        print("All TOTAL Turns :")
        print(playerTturns)
        print(computer1Tturns)
        
        if ( self.movesRemaining > 0) {
            
            
            self.moveFinished = false
            
            
            print("Moves Remaining =")
            print(self.movesRemaining)
            
            let currentSpace:Int = self.returnPlayerSpace(player: self.whosTurn)
            
            
            print("Who's Turn =")
            print(self.whosTurn)
            
            let spaceNumber:Int = currentSpace
            var nextSpace:Int = spaceNumber + 1
            
            
            
            if (nextSpace == 17 || nextSpace == 25 ) {
                
                nextSpace = self.realPath(nextPlace: nextSpace, player: self.whosTurn)
                
            }
            
            print("Blooded Or NOT ?")
            print(BloodedPlayer1)
            print(BloodedPlayer2)
            
            if (nextSpace == 17 || nextSpace == 9) {
                
                
                nextSpace = self.entrancePosition(nextPlace: nextSpace, player: self.whosTurn, cSpace: currentSpace)
                
            } else if (nextSpace == 23 || nextSpace == 21 || nextSpace == 19 || nextSpace == 25) {
                
                
                nextSpace = self.entrancePosition(nextPlace: nextSpace, player: self.whosTurn, cSpace: currentSpace)
                
            }
            
            
            // print("Next Space After Calling RealPath Func : ")
            // print(nextSpace)
            
            //Print
            print("Next Space =")
            print(nextSpace)
            
            // for node in children {
            // This maybe won't WORK Because your changing the node.name but it's already changed by for loop and 17 is running Right now But THIS can MAYBE WORK If It WILL Change the ACTUAL Node to 1
            // Try Using
            //  if (node.name == String(nextSpace)) {
            
            
            
            print("Node.Name AT Move Action : ")
            print(self.childNode(withName: String(nextSpace))!.name!)
            print(self.childNode(withName: String(nextSpace))!)
            
            if nextSpace != currentSpace {
                if whosTurn == .Player1 {
                    self.IncreasePlayerScore(player: self.whosTurn, playerpiece: self.touchedNode)
                } else {
                    self.IncreasePlayerScore(player: self.whosTurn, playerpiece: self.playerPiece)
                }
            }
            
            
            let moveAction:SKAction = SKAction.move(to: (self.childNode(withName: String(nextSpace))?.position)!, duration: 0.35)
            moveAction.timingMode = .easeOut
            
            
            
            
            
            let aud = SKAction.run {
                
                self.playSound(soundName: self.moveAudio)
                
                if nextSpace == 25 {
                    
                    if (self.whosTurn == .Player1 && ((self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26) || (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece4 == 26 && self.currentSpacePlayer1Piece3 == 26) || (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece4 == 26) || (self.currentSpacePlayer1Piece4 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26)) ) || (self.whosTurn == .Player2 && ((self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26) || (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece4 == 26 && self.currentSpacePlayer2Piece3 == 26) || (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece4 == 26) || (self.currentSpacePlayer2Piece4 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26)) ) {
                        
                        self.playSound(soundName: self.finalWon)
                        
                    } else {
                        
                        self.playSound(soundName: self.oneWon)
                        
                    }
                    
                }
                
            }
            
            
            
            let wait:SKAction = SKAction.wait(forDuration: 0.2)
            
            let runAction:SKAction = SKAction.run({
                
                
                
                if (self.movesRemaining == 1) {
                    
                    print("Inside MovesRemaining Kill HIM IF Condition")
                    
                    self.KillHim(player:self.whosTurn, nextspace:nextSpace)
                    
                    
                    
                }
                
                
                print(nextSpace)
                
                self.setThePlayerSpace(space: nextSpace, player:self.whosTurn)
                
                if (self.movesRemaining == 1) {
                    self.Adjust(node: self.childNode(withName: String(nextSpace))!)
                }
                
                self.movesRemaining = self.movesRemaining - 1
                
                
                print("Moves Remaining")
                print(self.movesRemaining)
                
                self.movePiece()
                
            })
            
            
            if self.whosTurn == .Player1 {
                
                // print("Touched Node In if condition :")
                // print(touchedNode.name!)
                
                print("The FINAL TIME Touched NODE is :")
                print(self.touchedNode)
                
                
                
                //  touchedNode.run(SKAction.sequence( [moveAction, wait, runAction] ) )
                
                self.touchedNode.run(SKAction.sequence([moveAction, aud, wait, runAction])) {
                    
                    print("The Action /WAs SuccessFully Runned : ")
                }
                
            } else {
                
                
                print("The FINAL TIME PLAYER PIECE is :")
                print(self.playerPiece)
                
                
                
                self.playerPiece.run(SKAction.sequence( [moveAction, aud, wait, runAction] ) )
                
                
            }
            
            
        } else {
            
            
            
            
            if (self.OneMoreMove > 0 && self.whosTurn == .Player1 && !gameOver) {
                
                self.OneMoreMove -= 1
                
                if ((currentSpacePlayer1Piece1 + currentSpacePlayer1Piece2 + currentSpacePlayer1Piece3 + currentSpacePlayer1Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                rolledDiceTexture.isHidden = true
                
                print("In .Player1")
                print(self.whosTurn)
                self.Dice.isHidden = false
                self.flashTurn(player: self.whosTurn)
                keepAnimating = true
                animations()
                self.moveFinished = true
                
                if (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26 && self.currentSpacePlayer1Piece4 == 26) {
                    
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player1
                    
                    print("All are AT Damn 26 Yeah!")
                    self.movesRemaining = 0
                    
                    self.Dice.isHidden = true
                    self.OneMoreMove = 0
                    self.movePiece()
                    
                }
                
            } else if (self.OneMoreMove > 0 && self.whosTurn == .Player2 && !gameOver) {
                
                self.OneMoreMove -= 1
                
                if ((currentSpacePlayer2Piece1 + currentSpacePlayer2Piece2 + currentSpacePlayer2Piece3 + currentSpacePlayer2Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                rolledDiceTexture.isHidden = true
                
                self.flashTurn(player: self.whosTurn)
                print("In == .Player2")
                print(self.whosTurn)
                self.moveFinished = true
                
                
                
                if (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26 && self.currentSpacePlayer2Piece4 == 26) {
                    
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player2
                    
                    print("All are AT Damn 26 Yeah!")
                    self.movesRemaining = 0
                    
                    self.Dice.isHidden = true
                    self.OneMoreMove = 0
                    self.movePiece()
                    
                } else {
                    self.rollDice(player: self.whosTurn)
                }
                
                
            } else if (self.whosTurn == .Player1 && !gameOver) {
                
                
                self.moveFinished = true
                
                if ((currentSpacePlayer1Piece1 + currentSpacePlayer1Piece2 + currentSpacePlayer1Piece3 + currentSpacePlayer1Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                print(self.whosTurn)
                
                
                if (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26 && self.currentSpacePlayer2Piece4 == 26) {
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player2
                    
                    print("All are AT Damn 26 Yeah!| In Who's Turn 2's Alternative | Player-2 |")
                    self.movesRemaining = 0
                    
                    self.movePiece()
                    
                } else {
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player2
                    print(self.whosTurn)
                    
                    updateScore()
                    
                    print("All are FINE! | WHOSTURN : Player 2 |")
                    
                    self.flashTurn(player: self.whosTurn)
                    
                    self.rollDice(player: self.whosTurn)
                }
                
                
                
            } else if (self.whosTurn == .Player2 && !gameOver) {
                
                self.moveFinished = true
                
                if ((currentSpacePlayer2Piece1 + currentSpacePlayer2Piece2 + currentSpacePlayer2Piece3 + currentSpacePlayer2Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                
                rolledDiceTexture.isHidden = true
                print(self.whosTurn)
                
                
                
                if (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26 && self.currentSpacePlayer1Piece4 == 26) {
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player1
                    self.movesRemaining = 0
                    print(self.whosTurn)
                    print("All are AT Damn 26 Yeah!| In Who's Turn 1's Alternative | Player-1 |")
                    self.movePiece()
                    
                } else {
                    
                    self.Dice.isHidden = false
                    
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player1
                    print(self.whosTurn)
                    keepAnimating = true
                    animations()
                    
                    self.flashTurn(player: self.whosTurn)
                    updateScore()
                    
                    print("All are FINE! | WHOSTURN : Player 1 |")
                    
                }
                
                
                
                
            } else if (gameOver == true) {
                
                
                
                print(playerScore)
                print(com1Score)
                print(playerone)
                print(playertwo)
                print("At End :")
                print(place.count)
                
                print(playerOne.isEmpty)
                
                if place.count > 0 {
                    print("Added Player One :!")
                    print(place[0])
                    playerOne = place[0]
                    print(playerOne)
                }
                if place.count > 1 {
                    print(place[1])
                    playerTwo = place[1]
                    print(playerTwo)
                }
                
                if playerOne.isEmpty && playerTwo.isEmpty {
                    
                    playerOne = "You"
                    playerTwo = "Computer"
                    print("In Last CON!")
                    
                }
                
                let delay = 0.5
                
                
                Adjust(node: childNode(withName: "25")!)
                GlobalVariablesPad.PreviousScene = .VsComTwoPad
                
                let gameOverScreen = SKScene(fileNamed: "EndScreenPad")
                gameOverScreen?.scaleMode = .aspectFit
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    
                    print("Yes!")
                    self.view?.presentScene(gameOverScreen!, transition: SKTransition.fade(withDuration: 1))
                    
                    
                }
                
                
            }
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // <------------------------------------------------------  RULES  ------------------------------------------------------->
    
    func KillHim(player:Player, nextspace:Int) {
        
        
        
        
        print("Inside Kill Him Func And nextspace is :")
        print(nextspace)
        print(player)
        
        
        
        
        if (nextspace == 1 || nextspace == 5 || nextspace == 9 || nextspace == 13 || nextspace == 25) {
            return
        }
        
        
        
        if (player == .Player1) {
            
            
            if (nextspace == currentSpacePlayer2Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece1)
                
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece1 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        print("Inside the if NODE name is == 5")
                        
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece1)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        
                        //Player2Piece1.run(moveAction) {
                        
                        playSound(soundName: killedAudio)
                        Player2Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece2 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        
                        print("Inside the if NODE name is == 5")
                        
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece2)
                        
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        playSound(soundName: killedAudio)
                        Player2Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece3 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece3)
                        
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        playSound(soundName: killedAudio)
                        Player2Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece4 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece4)
                        
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        playSound(soundName: killedAudio)
                        Player2Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            }
            
            
            
            //   <---------------------------------------------  PLAYER-2  -------------------------------------------------------------------------------->
            
            
            
        } else if (player == .Player2) {
            
            
            
            
            
            if (nextspace == currentSpacePlayer1Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece1 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece1)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece2 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece2)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece3 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece3)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece4 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece4)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
            }
            
            
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // FIRST OF ALL CLOSE THE MAIN WAY FOR 2,3 AND 4 PLAYERS THEN IT WILL WORK
    // Change the space with new positions so it will change the nextSpace to it so automatically nextSpace = EntrancePosition ;)
    // And also short those sentances so the code became clean and small and with less loops just like u did up There
    // Make that sentance a Variable
    func entrancePosition(nextPlace:Int, player:Player, cSpace:Int) -> Int {
        
        var newLocation = nextPlace
        
        if ( nextPlace == 9 || nextPlace == 17 ) {
            
            if (player == .Player1 && nextPlace == 17 && cSpace == 16) {
                
                
                if (BloodedPlayer1 == true) {
                    print("You are the BadAss Man!")
                    newLocation = 17
                    
                } else if (!BloodedPlayer1) {
                    print("Sorry Kid you aren't MAN Yet!")
                    newLocation = 16
                    movesRemaining = 1
                }
                
                
            } else if (player == .Player2 && nextPlace == 9) {
                
                if (BloodedPlayer2 == true) {
                    print("You are the BadAss Man!")
                    newLocation = 21
                    
                } else if (!BloodedPlayer2) {
                    print("Sorry Kid you aren't MAN Yet!")
                    newLocation = 8
                    movesRemaining = 1
                }
                
                
            }
            
        } else {
            
            if (player == .Player1 && nextPlace == 25) {
                
                if ((currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26)) {
                    
                    print("If ALL ANY 3 ARE AT 26 Player 1 :")
                    
                    if ((currentSpacePlayer1Piece1 + movesRemaining == 25) || (currentSpacePlayer1Piece2 + movesRemaining == 25) || (currentSpacePlayer1Piece3 + movesRemaining == 25) || (currentSpacePlayer1Piece4 + movesRemaining == 25)) {
                        
                        print("Inside GOT PERFECT ROLLLED DICE : PlaYER 1 :")
                        
                        newLocation = 25
                        
                        if touchedNode == Player1Piece1 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece1Score += 1
                        } else if touchedNode == Player1Piece2 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece2Score += 1
                        } else if touchedNode == Player1Piece3 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece3Score += 1
                        } else if touchedNode == Player1Piece4 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece4Score += 1
                        }
                        
                        playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
                        playerScoreLabel.text = "\(playerScore)"
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        
                    } else {
                        print("ALL 3 ARE AT 26 AND + Rolled Dice != 25 : PLayer 1")
                        newLocation = 17
                    }
                    
                } else {
                    print("In REGULAR ONE For GO TO 25 : Player 1 :")
                    newLocation = 25
                    
                    if self.oneMoreMove.isHidden {
                        self.notifyForPlusMove(label: self.oneMoreMove)
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                }
                
            } else if (player == .Player2 && nextPlace == 21) {
                
                
                if ((currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26)) {
                    
                    print("If ALL ANY 2 ARE AT 26 Player 3 :")
                    
                    if ((((currentSpacePlayer2Piece1 >= 21 && currentSpacePlayer2Piece1 <= 24) && (currentSpacePlayer2Piece1 + movesRemaining == 29)) || ((currentSpacePlayer2Piece1 >= 17 && currentSpacePlayer2Piece1 <= 20) && (currentSpacePlayer2Piece1 + movesRemaining == 21))) || (((currentSpacePlayer2Piece2 >= 21 && currentSpacePlayer2Piece2 <= 24) && (currentSpacePlayer2Piece2 + movesRemaining == 29)) || ((currentSpacePlayer2Piece2 >= 17 && currentSpacePlayer2Piece2 <= 20) && (currentSpacePlayer2Piece2 + movesRemaining == 21))) || (((currentSpacePlayer2Piece3 >= 21 && currentSpacePlayer2Piece3 <= 24) && (currentSpacePlayer2Piece3 + movesRemaining == 29)) || ((currentSpacePlayer2Piece3 >= 17 && currentSpacePlayer2Piece3 <= 20) && (currentSpacePlayer2Piece3 + movesRemaining == 21))) || (((currentSpacePlayer2Piece4 >= 21 && currentSpacePlayer2Piece4 <= 24) && (currentSpacePlayer2Piece4 + movesRemaining == 29)) || ((currentSpacePlayer2Piece4 >= 17 && currentSpacePlayer2Piece4 <= 20) && (currentSpacePlayer2Piece4 + movesRemaining == 21)))) {
                        
                        print("Inside GOT PERFECT ROLLLED DICE : PlaYER 2 :")
                        
                        newLocation = 25
                        
                        if playerPiece == Player2Piece1 {
                            print("In Inrease Pla2 Score For LASTA :")
                            com1Piece1Score += 1
                        } else if playerPiece == Player2Piece2 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com1Piece2Score += 1
                        } else if playerPiece == Player2Piece3 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com1Piece3Score += 1
                        } else if playerPiece == Player2Piece4 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com1Piece4Score += 1
                        }
                        
                        com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
                        com1ScoreLabel.text = "\(com1Score)"
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        
                    } else {
                        print("ALL 3 ARE AT 26 AND + Rolled Dice != 25 : PLayer 2")
                        newLocation = 21
                    }
                    
                } else {
                    print("In REGULAR ONE For GO TO 25 : Player 2 :")
                    newLocation = 25
                    
                    if self.oneMoreMove.isHidden {
                        self.notifyForPlusMove(label: self.oneMoreMove)
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                }
                
                
                
                
            }
            
        }
        
        
        
        //print("In Entrance NextSpace is :")
        //print(newLocation)
        return newLocation
    }
    
    
    
    
    func notBloodedAnyMore() {
        
        
        
        if (currentSpacePlayer1Piece1 == 1 && currentSpacePlayer1Piece2 == 1 && currentSpacePlayer1Piece3 == 1 && currentSpacePlayer1Piece4 == 1) {
            BloodedPlayer1 = false
            print("You LOST IT Player 1 !!!")
        }
        
        
        if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) {
            BloodedPlayer2 = false
            print("You LOST IT Player 2 !!!")
        }
        
        
        
    }
    
    
    func changeFace(player: Player) {
        
        if player == .Player1 {
            
            if BloodedPlayer1 {
                
                if currentSpacePlayer1Piece1 != 26 {
                    Player1Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                if currentSpacePlayer1Piece2 != 26 {
                    Player1Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                if currentSpacePlayer1Piece3 != 26 {
                    Player1Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                if currentSpacePlayer1Piece4 != 26 {
                    Player1Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                
                
            } else if !BloodedPlayer1 {
                
                
                if currentSpacePlayer1Piece1 != 26 {
                    Player1Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                if currentSpacePlayer1Piece2 != 26 {
                    Player1Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                if currentSpacePlayer1Piece3 != 26 {
                    Player1Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                if currentSpacePlayer1Piece4 != 26 {
                    Player1Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                
                
            }
            
            
        } else if player == .Player2 {
            
            if BloodedPlayer2 {
                
                if currentSpacePlayer2Piece1 != 26 {
                    Player2Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                if currentSpacePlayer2Piece2 != 26 {
                    Player2Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                if currentSpacePlayer2Piece3 != 26 {
                    Player2Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                if currentSpacePlayer2Piece4 != 26 {
                    Player2Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                
                
            } else if !BloodedPlayer2 {
                
                if currentSpacePlayer2Piece1 != 26 {
                    Player2Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                if currentSpacePlayer2Piece2 != 26 {
                    Player2Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                if currentSpacePlayer2Piece3 != 26 {
                    Player2Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                if currentSpacePlayer2Piece4 != 26 {
                    Player2Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                
                
            }
            
            
        }
        
        
        
    }
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    func returnPlayerPiece (player:Player) /* -> SKSpriteNode */ {
        
        
        
        print("Inside Return Player Piece : HA HA HHA")
        
        whosFirst(player: player)
        
        
        
        print("Inside Return Player Piece AFTER WHOS FIRST BHU BHU BHUT : HA HA HHA")
        
        
        print("OLD ONE:")
        
        
        
        
        
        
        
        if (player == .Player2) {
            
            
            print("INSIDE, Player 2 : ya ya ")
            
            
            
            
            
            
            
            
            // <-------------------------------------------------   Piece   ------------------------------------------------------->
            
            
            
            
            if (((currentSpacePlayer2Piece1 >= 21 && currentSpacePlayer2Piece1 < 25) && (currentSpacePlayer2Piece1 + rolledDice == 29)) || ((currentSpacePlayer2Piece1 >= 17 && currentSpacePlayer2Piece1 <= 20) && (currentSpacePlayer2Piece1 + rolledDice == 21))) && (currentSpacePlayer2Piece1 != 26) {
                
                print("Inside the Direct Wining Condition!")
                playerPiece = Player2Piece1
                //flashPlayer(player: player)
                
            } else if (((currentSpacePlayer2Piece2 >= 21 && currentSpacePlayer2Piece2 < 25) && (currentSpacePlayer2Piece2 + rolledDice == 29)) || ((currentSpacePlayer2Piece2 >= 17 && currentSpacePlayer2Piece2 <= 20) && (currentSpacePlayer2Piece2 + rolledDice == 21))) && (currentSpacePlayer2Piece2 != 26) {
                
                print("Inside the Direct Wining Condition!")
                playerPiece = Player2Piece2
                //flashPlayer(player: player)
                
            } else if (((currentSpacePlayer2Piece3 >= 21 && currentSpacePlayer2Piece3 < 25) && (currentSpacePlayer2Piece3 + rolledDice == 29)) || ((currentSpacePlayer2Piece3 >= 17 && currentSpacePlayer2Piece3 <= 20) && (currentSpacePlayer2Piece3 + rolledDice == 21))) && (currentSpacePlayer2Piece3 != 26) {
                
                print("Inside the Direct Wining Condition!")
                playerPiece = Player2Piece3
                //flashPlayer(player: player)
                
            } else if (((currentSpacePlayer2Piece4 >= 21 && currentSpacePlayer2Piece4 < 25) && (currentSpacePlayer2Piece4 + rolledDice == 29)) || ((currentSpacePlayer2Piece4 >= 17 && currentSpacePlayer2Piece4 <= 20) && (currentSpacePlayer2Piece4 + rolledDice == 21))) && (currentSpacePlayer2Piece4 != 26) {
                
                print("Inside the Direct Wining Condition!")
                playerPiece = Player2Piece4
                //flashPlayer(player: player)
                
            } else if (currentSpacePlayer2Piece1 >= 1 && currentSpacePlayer2Piece1 < 9) && (currentSpacePlayer2Piece1 + rolledDice >= 9) && (BloodedPlayer2) {
                
                print("Inside the GET inside Condition!")
                playerPiece = Player2Piece1
                //flashPlayer(player: player)
            } else if (currentSpacePlayer2Piece2 >= 1 && currentSpacePlayer2Piece2 < 9) && (currentSpacePlayer2Piece2 + rolledDice >= 9) && (BloodedPlayer2) {
                
                print("Inside the GET inside Condition!")
                playerPiece = Player2Piece2
                //flashPlayer(player: player)
            } else if (currentSpacePlayer2Piece3 >= 1 && currentSpacePlayer2Piece3 < 9) && (currentSpacePlayer2Piece3 + rolledDice >= 9) && (BloodedPlayer2) {
                
                print("Inside the GET inside Condition!")
                playerPiece = Player2Piece3
                //flashPlayer(player: player)
            } else if (currentSpacePlayer2Piece4 >= 1 && currentSpacePlayer2Piece4 < 9) && (currentSpacePlayer2Piece4 + rolledDice >= 9) && (BloodedPlayer2) {
                
                print("Inside the GET inside Condition!")
                playerPiece = Player2Piece4
                //flashPlayer(player: player)
            } else if ((currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23) && (currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece4) && ((currentSpacePlayer2Piece1 + rolledDice) < 25)) {
                
                print("Inside the Easly Inside Killing Condition! Player2Piece1 :")
                playerPiece = Player2Piece1
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23) && (currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece4) && ((currentSpacePlayer2Piece2 + rolledDice) < 25)) {
                
                print("Inside the Easly Inside Killing Condition! Player2Piece2 :")
                playerPiece = Player2Piece2
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23) && (currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece4) && ((currentSpacePlayer2Piece3 + rolledDice) < 25)) {
                
                print("Inside the Easly Inside Killing Condition! Player2Piece3 :")
                playerPiece = Player2Piece3
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23) && (currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece4) && ((currentSpacePlayer2Piece4 + rolledDice) < 25)) {
                
                print("Inside the Easly Inside Killing Condition! Player2Piece4 :")
                playerPiece = Player2Piece4
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24) && (currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece1 + 8 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece2 + 8 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece3 + 8 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece4 + 8)) && (rolledDice != 8) {
                
                print("Inside the Inside Killing Condition!")
                playerPiece = Player2Piece1
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24) && (currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece1 + 8 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece2 + 8 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece3 + 8 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece4 + 8)) && (rolledDice != 8) {
                
                print("Inside the Inside Killing Condition!")
                playerPiece = Player2Piece2
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24) && (currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece1 + 8 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece2 + 8 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece3 + 8 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece4 + 8)) && (rolledDice != 8) {
                
                print("Inside the Inside Killing Condition!")
                playerPiece = Player2Piece3
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24) && (currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece1 + 8 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece2 + 8 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece3 + 8 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece4 + 8)) && (rolledDice != 8) {
                
                print("Inside the Inside Killing Condition!")
                playerPiece = Player2Piece4
                //flashPlayer(player: player)
                
            } else if ((currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24 || currentSpacePlayer2Piece1 == 17) && (rolledDice == 4)/* && ((currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 3 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece1:")
                playerPiece = Player2Piece1
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24 || currentSpacePlayer2Piece1 == 17) && (rolledDice == 3)/* && ((currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece1:")
                playerPiece = Player2Piece1
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24 || currentSpacePlayer2Piece1 == 17 || currentSpacePlayer2Piece1 == 18) && (rolledDice == 2)/* && ((currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece1 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece1:")
                playerPiece = Player2Piece1
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24 || currentSpacePlayer2Piece1 == 17 || currentSpacePlayer2Piece1 == 18 || currentSpacePlayer2Piece1 == 19) && (rolledDice == 1) ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece1:")
                playerPiece = Player2Piece1
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24 || currentSpacePlayer2Piece2 == 17) && (rolledDice == 4)/* && ((currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 3 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece2:")
                playerPiece = Player2Piece2
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24 || currentSpacePlayer2Piece2 == 17) && (rolledDice == 3)/* && ((currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece2:")
                playerPiece = Player2Piece2
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24 || currentSpacePlayer2Piece2 == 17 || currentSpacePlayer2Piece2 == 18) && (rolledDice == 2)/* && ((currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece2 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece2:")
                playerPiece = Player2Piece2
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24 || currentSpacePlayer2Piece2 == 17 || currentSpacePlayer2Piece2 == 18 || currentSpacePlayer2Piece2 == 19) && (rolledDice == 1) ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece2:")
                playerPiece = Player2Piece2
                print(playerPiece)
                
            } else  if ((currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24 || currentSpacePlayer2Piece3 == 17) && (rolledDice == 4)/* && ((currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 3 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece3:")
                playerPiece = Player2Piece3
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24 || currentSpacePlayer2Piece3 == 17) && (rolledDice == 3)/* && ((currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece3:")
                playerPiece = Player2Piece3
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24 || currentSpacePlayer2Piece3 == 17 || currentSpacePlayer2Piece3 == 18) && (rolledDice == 2)/* && ((currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece3 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece3:")
                playerPiece = Player2Piece3
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24 || currentSpacePlayer2Piece3 == 17 || currentSpacePlayer2Piece3 == 18 || currentSpacePlayer2Piece3 == 19) && (rolledDice == 1) ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece3:")
                playerPiece = Player2Piece3
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24 || currentSpacePlayer2Piece4 == 17) && (rolledDice == 4)/* && ((currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 3 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece4:")
                playerPiece = Player2Piece4
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24 || currentSpacePlayer2Piece4 == 17) && (rolledDice == 3)/* && ((currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 2 != currentSpacePlayer4Piece4) ) && ((currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece4:")
                playerPiece = Player2Piece4
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24 || currentSpacePlayer2Piece4 == 17 || currentSpacePlayer2Piece4 == 18) && (rolledDice == 2)/* && ((currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer2Piece4) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece1) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece2) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece3) && (currentSpacePlayer2Piece4 + rolledDice - 1 != currentSpacePlayer4Piece4) )*/ ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece4:")
                playerPiece = Player2Piece4
                print(playerPiece)
                
            } else if ((currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24 || currentSpacePlayer2Piece4 == 17 || currentSpacePlayer2Piece4 == 18 || currentSpacePlayer2Piece4 == 19) && (rolledDice == 1) ) {
                
                print("|Inside HAVE TO MOVE INSIDE Condition| Player2Piece4:")
                playerPiece = Player2Piece4
                print(playerPiece)
                
            }else if ( ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) || ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) || ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) || ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) ) {
                
                print("|Inside DIRECT SAFE Condition| : Player2Piece1")
                
                if ( (firstPiece == Player2Piece1 && currentSpacePlayer2Piece1 != 26 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) ) || (firstPiece == Player2Piece2 && currentSpacePlayer2Piece2 != 26 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) ) || (firstPiece == Player2Piece3 && currentSpacePlayer2Piece3 != 26 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) ) || (firstPiece == Player2Piece4 && currentSpacePlayer2Piece4 != 26 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) ) ) {
                    
                    print("Inside First Piece :")
                    if (firstPiece == Player2Piece1 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) && !(currentSpacePlayer2Piece1 == 26) ) {
                        
                        print("|Inside FIRST PIECE SAFE Condition| : Player2Piece1")
                        
                        playerPiece = Player2Piece1
                        print(playerPiece)
                        
                    } else if (firstPiece == Player2Piece2 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) && !(currentSpacePlayer2Piece2 == 26) ) {
                        
                        print("|Inside FIRST PIECE SAFE Condition| : Player2Piece2")
                        
                        playerPiece = Player2Piece2
                        print(playerPiece)
                        
                    } else if (firstPiece == Player2Piece3 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) && !(currentSpacePlayer2Piece3 == 26) ) {
                        
                        print("|Inside FIRST PIECE SAFE Condition| : Player2Piece3")
                        
                        playerPiece = Player2Piece3
                        print(playerPiece)
                        
                    } else if (firstPiece == Player2Piece4 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) && !(currentSpacePlayer2Piece4 == 26) ) {
                        
                        print("|Inside FIRST PIECE SAFE Condition| : Player2Piece4")
                        
                        playerPiece = Player2Piece4
                        print(playerPiece)
                        
                    }
                    
                    
                } else if ( (secondPiece == Player2Piece1 && currentSpacePlayer2Piece1 != 26 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) ) || (secondPiece == Player2Piece2 && currentSpacePlayer2Piece2 != 26 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) ) || (secondPiece == Player2Piece3 && currentSpacePlayer2Piece3 != 26 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) ) || (secondPiece == Player2Piece4 && currentSpacePlayer2Piece4 != 26 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) ) ) {
                    print("Inside Seecond Piece :")
                    
                    if (secondPiece == Player2Piece1 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) && !(currentSpacePlayer2Piece1 == 26) ) {
                        
                        print("|InsideSeond PIECE SAFE Condition| : Player2Piece1")
                        
                        playerPiece = Player2Piece1
                        print(playerPiece)
                        
                    } else if (secondPiece == Player2Piece2 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) && !(currentSpacePlayer2Piece2 == 26) ) {
                        
                        print("|Inside Seond PIECE SAFE Condition| : Player2Piece2")
                        
                        playerPiece = Player2Piece2
                        print(playerPiece)
                        
                    } else if (secondPiece == Player2Piece3 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) && !(currentSpacePlayer2Piece3 == 26) ) {
                        
                        print("|Inside Seond PIECE SAFE Condition| : Player2Piece3")
                        
                        playerPiece = Player2Piece3
                        print(playerPiece)
                        
                    } else if (secondPiece == Player2Piece4 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) && !(currentSpacePlayer2Piece4 == 26) ) {
                        
                        print("|Inside Seond PIECE SAFE Condition| : Player2Piece4")
                        
                        playerPiece = Player2Piece4
                        print(playerPiece)
                        
                    }
                    
                    
                } else if ( (thirdPiece == Player2Piece1 && currentSpacePlayer2Piece1 != 26 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) ) || (thirdPiece == Player2Piece2 && currentSpacePlayer2Piece2 != 26 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) ) || (thirdPiece == Player2Piece3 && currentSpacePlayer2Piece3 != 26 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) ) || (thirdPiece == Player2Piece4 && currentSpacePlayer2Piece4 != 26 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) ) ) {
                    print("Inside Third Piece :")
                    
                    if (thirdPiece == Player2Piece1 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) && !(currentSpacePlayer2Piece1 == 26) ) {
                        
                        print("|Inside THIRD PIECE SAFE Condition| : Player2Piece1")
                        
                        playerPiece = Player2Piece1
                        print(playerPiece)
                        
                    } else if (thirdPiece == Player2Piece2 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) && !(currentSpacePlayer2Piece2 == 26) ) {
                        
                        print("|Inside THIRD PIECE SAFE Condition| : Player2Piece2")
                        
                        playerPiece = Player2Piece2
                        print(playerPiece)
                        
                    } else if (thirdPiece == Player2Piece3 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) && !(currentSpacePlayer2Piece3 == 26) ) {
                        
                        print("|Inside THIRD PIECE SAFE Condition| : Player2Piece3")
                        
                        playerPiece = Player2Piece3
                        print(playerPiece)
                        
                    } else if (thirdPiece == Player2Piece4 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) && !(currentSpacePlayer2Piece4 == 26) ) {
                        
                        print("|Inside THIRD PIECE SAFE Condition| : Player2Piece4")
                        
                        playerPiece = Player2Piece4
                        print(playerPiece)
                        
                    }
                    
                    
                } else if ( (fourthPiece == Player2Piece1 && currentSpacePlayer2Piece1 != 26 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) ) || (fourthPiece == Player2Piece2 && currentSpacePlayer2Piece2 != 26 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) ) || (fourthPiece == Player2Piece3 && currentSpacePlayer2Piece3 != 26 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) ) || (fourthPiece == Player2Piece4 && currentSpacePlayer2Piece4 != 26 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) ) ) {
                    print("Inside Fourth Piece :")
                    
                    if (fourthPiece == Player2Piece1 && ((currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 17 || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13) && !(currentSpacePlayer2Piece1 == 26) ) {
                        
                        print("|Inside FOURTH PIECE SAFE Condition| : Player2Piece1")
                        
                        playerPiece = Player2Piece1
                        print(playerPiece)
                        
                    } else if (fourthPiece == Player2Piece2 && ((currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 17 || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13) && !(currentSpacePlayer2Piece2 == 26) ) {
                        
                        print("|Inside FOURTH PIECE SAFE Condition| : Player2Piece2")
                        
                        playerPiece = Player2Piece2
                        print(playerPiece)
                        
                    } else if (fourthPiece == Player2Piece3 && ((currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 17 || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13) && !(currentSpacePlayer2Piece3 == 26) ) {
                        
                        print("|Inside FOURTH PIECE SAFE Condition| : Player2Piece3")
                        
                        playerPiece = Player2Piece3
                        print(playerPiece)
                        
                    } else if (fourthPiece == Player2Piece4 && ((currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 17 || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13) && !(currentSpacePlayer2Piece4 == 26) ) {
                        
                        print("|Inside FOURTH PIECE SAFE Condition| : Player2Piece4")
                        
                        playerPiece = Player2Piece4
                        print(playerPiece)
                        
                    }
                    
                    
                }
                
                
                
                
                
                
            }/* else if (currentSpacePlayer2Piece1 + rolledDice == 5 || (currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21)) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece1:")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece1
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece2 + rolledDice == 5 || (currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21)) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece2:")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece2
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece3 + rolledDice == 5 || (currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21)) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece3 :")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece3
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece4 + rolledDice == 5 || (currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21)) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece4 :")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece4
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece1 + rolledDice == 17) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece1:")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece1
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece2 + rolledDice == 17) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece2:")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece2
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece3 + rolledDice == 17) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece3 :")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece3
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece4 + rolledDice == 17) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece4 :")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece4
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece1 + rolledDice == 13 && currentSpacePlayer2Piece1 == 9) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece1:")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece1
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece2 + rolledDice == 13 && currentSpacePlayer2Piece2 == 9) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece2:")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece2
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece3 + rolledDice == 13 && currentSpacePlayer2Piece3 == 9) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece3 :")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece3
                 print(playerPiece)
                 
                 } else if (currentSpacePlayer2Piece4 + rolledDice == 13 && currentSpacePlayer2Piece4 == 9) {
                 
                 print("|Inside DIRECT SAFE Condition| Player2Piece4 :")
                 
                 //flashPlayer(player: player)
                 
                 playerPiece = Player2Piece4
                 print(playerPiece)
                 
             }*/ else if (((currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece1 + rolledDice != 17 && currentSpacePlayer2Piece1 + rolledDice != 9 && currentSpacePlayer2Piece1 + rolledDice != 13 && currentSpacePlayer2Piece1 + rolledDice != 25 && currentSpacePlayer2Piece1 + rolledDice != 5)) && (currentSpacePlayer2Piece1 + rolledDice < 21) && ((currentSpacePlayer2Piece1 != 1 && !(currentSpacePlayer2Piece1 == 5 && rolledDice == 1)) || !BloodedPlayer2 )) || (currentSpacePlayer2Piece1 == 5 && rolledDice == 3 && (currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece4) ) {
                
                print("|Inside DIRECT KILLING Condition| Player2Piece1:")
                
                
                playerPiece = Player2Piece1
                print(playerPiece)
                
                
            } else if (((currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece2 + rolledDice != 17 && currentSpacePlayer2Piece2 + rolledDice != 9 && currentSpacePlayer2Piece2 + rolledDice != 13 && currentSpacePlayer2Piece2 + rolledDice != 25 && currentSpacePlayer2Piece2 + rolledDice != 5)) && (currentSpacePlayer2Piece2 + rolledDice < 21) && ((currentSpacePlayer2Piece2 != 1 && !(currentSpacePlayer2Piece2 == 5 && rolledDice == 1)) || !BloodedPlayer2 )) || (currentSpacePlayer2Piece2 == 5 && rolledDice == 3 && (currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece4) ) {
                
                print("|Inside DIRECT KILLING Condition| Player2Piece2:")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece2
                print(playerPiece)
                
                
            } else if (((currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece3 + rolledDice != 17 && currentSpacePlayer2Piece3 + rolledDice != 9 && currentSpacePlayer2Piece3 + rolledDice != 13 && currentSpacePlayer2Piece3 + rolledDice != 25 && currentSpacePlayer2Piece3 + rolledDice != 5)) && (currentSpacePlayer2Piece3 + rolledDice < 21) && ((currentSpacePlayer2Piece3 != 1 && !(currentSpacePlayer2Piece3 == 5 && rolledDice == 1)) || !BloodedPlayer2 )) || (currentSpacePlayer2Piece3 == 5 && rolledDice == 3 && (currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece4) ) {
                
                print("|Inside DIRECT KILLING Condition| Player2Piece3 :")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece3
                print(playerPiece)
                
                
            } else if (((currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece4) && (currentSpacePlayer2Piece4 + rolledDice != 17 && currentSpacePlayer2Piece4 + rolledDice != 9 && currentSpacePlayer2Piece4 + rolledDice != 13 && currentSpacePlayer2Piece4 + rolledDice != 25 && currentSpacePlayer2Piece4 + rolledDice != 5)) && (currentSpacePlayer2Piece4 + rolledDice < 21) && ((currentSpacePlayer2Piece4 != 1 && !(currentSpacePlayer2Piece4 == 5 && rolledDice == 1)) || !BloodedPlayer2 )) || (currentSpacePlayer2Piece4 == 5 && rolledDice == 3 && (currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece1 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece2 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece3 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece4) ) {
                
                print("|Inside DIRECT KILLING Condition| Player2Piece4 :")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece4
                print(playerPiece)
                
                
            } else if (( currentSpacePlayer2Piece1 == 10 || currentSpacePlayer2Piece1 == 11 || currentSpacePlayer2Piece1 == 12 || currentSpacePlayer2Piece1 == 14 || currentSpacePlayer2Piece1 == 15 || currentSpacePlayer2Piece1 == 16) && (currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece1 + 16 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece2 + 16 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece3 + 16 || currentSpacePlayer2Piece1 + rolledDice == currentSpacePlayer1Piece4 + 16)) {
                
                print("|Inside Kill near 17 and 1 Condition| : Player2Piece1")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece1
                print(playerPiece)
            } else if (( currentSpacePlayer2Piece2 == 10 || currentSpacePlayer2Piece2 == 11 || currentSpacePlayer2Piece2 == 12 || currentSpacePlayer2Piece2 == 14 || currentSpacePlayer2Piece2 == 15 || currentSpacePlayer2Piece2 == 16) && (currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece1 + 16 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece2 + 16 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece3 + 16 || currentSpacePlayer2Piece2 + rolledDice == currentSpacePlayer1Piece4 + 16)) {
                
                print("|Inside Kill near 17 and 1 Condition| : Player2Piece2")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece2
                print(playerPiece)
            } else  if (( currentSpacePlayer2Piece3 == 10 || currentSpacePlayer2Piece3 == 11 || currentSpacePlayer2Piece3 == 12 || currentSpacePlayer2Piece3 == 14 || currentSpacePlayer2Piece3 == 15 || currentSpacePlayer2Piece3 == 16) && (currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece1 + 16 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece2 + 16 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece3 + 16 || currentSpacePlayer2Piece3 + rolledDice == currentSpacePlayer1Piece4 + 16)) {
                
                print("|Inside Kill near 17 and 1 Condition| : Player2Piece3")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece3
                print(playerPiece)
            } else  if (( currentSpacePlayer2Piece4 == 10 || currentSpacePlayer2Piece4 == 11 || currentSpacePlayer2Piece4 == 12 || currentSpacePlayer2Piece4 == 14 || currentSpacePlayer2Piece4 == 15 || currentSpacePlayer2Piece4 == 16) && (currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece1 + 16 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece2 + 16 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece3 + 16 || currentSpacePlayer2Piece4 + rolledDice == currentSpacePlayer1Piece4 + 16)) {
                
                print("|Inside Kill near 17 and 1 Condition| : Player2Piece4")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece4
                print(playerPiece)
            } else if (((currentSpacePlayer2Piece4 != 26 && currentSpacePlayer2Piece4 + rolledDice < 21 && !(currentSpacePlayer2Piece4 == 8 && !BloodedPlayer2)) || ((currentSpacePlayer2Piece4 >= 10 && currentSpacePlayer2Piece4 <= 16) && rolledDice == 8)) && Player2Piece4 == fourthPiece) {
                
                print("|Inside LAST ONE Condition| Player2Piece4:")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece4
                print(playerPiece)
                
            } else if (((currentSpacePlayer2Piece3 != 26 && currentSpacePlayer2Piece3 + rolledDice < 21 && !(currentSpacePlayer2Piece3 == 8 && !BloodedPlayer2)) || ((currentSpacePlayer2Piece3 >= 10 && currentSpacePlayer2Piece3 <= 16) && rolledDice == 8)) && Player2Piece3 == fourthPiece) {
                
                print("|Inside LAST ONE Condition| Player2Piece3:")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece3
                print(playerPiece)
                
            } else if (((currentSpacePlayer2Piece2 != 26 && currentSpacePlayer2Piece2 + rolledDice < 21 && !(currentSpacePlayer2Piece2 == 8 && !BloodedPlayer2)) || ((currentSpacePlayer2Piece2 >= 10 && currentSpacePlayer2Piece2 <= 16) && rolledDice == 8)) && Player2Piece2 == fourthPiece) {
                
                print("|Inside LAST ONE Condition| Player2Piece2:")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece2
                print(playerPiece)
                
            } else if (((currentSpacePlayer2Piece1 != 26 && currentSpacePlayer2Piece1 + rolledDice < 21 && !(currentSpacePlayer2Piece1 == 8 && !BloodedPlayer2)) || ((currentSpacePlayer2Piece1 >= 10 && currentSpacePlayer2Piece1 <= 16) && rolledDice == 8)) && Player2Piece1 == fourthPiece) {
                
                print("|Inside LAST ONE Condition| Player2Piece1:")
                
                //flashPlayer(player: player)
                
                playerPiece = Player2Piece1
                print(playerPiece)
                
            } else if (currentSpacePlayer2Piece1 != 26 && (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26)) {
                
                playerPiece = Player2Piece1
                print(playerPiece)
                
                print("|Inside ONE MAN REMAINING | Player2Piece1:")
            } else if (currentSpacePlayer2Piece2 != 26 && (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26)) {
                
                playerPiece = Player2Piece2
                print(playerPiece)
                
                print("|Inside ONE MAN REMAINING | Player2Piece2:")
            } else if (currentSpacePlayer2Piece3 != 26 && (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26)) {
                
                playerPiece = Player2Piece3
                print(playerPiece)
                
                print("|Inside ONE MAN REMAINING | Player2Piece3:")
            } else if (currentSpacePlayer2Piece4 != 26 && (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 26)) {
                
                playerPiece = Player2Piece4
                print(playerPiece)
                
                print("|Inside ONE MAN REMAINING | Player2Piece4:")
            }
            
            
            
            
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    func whosFirst(player:Player) {
        
        print("4th Piece :")
        print(fourthPiece)
        
        var changeFourth:Bool = false
        
        
        
        
        if player == .Player2 {
            
            if player2FourthPiece == Player2Piece1 {
                
                if currentSpacePlayer2Piece1 == 5 || currentSpacePlayer2Piece1 == 13 || currentSpacePlayer2Piece1 == 1 || currentSpacePlayer2Piece1 == 26 || currentSpacePlayer2Piece1 >= 17 {
                    
                    changeFourth = true
                    
                }
                
            } else if player2FourthPiece == Player2Piece2 {
                
                if currentSpacePlayer2Piece2 == 5 || currentSpacePlayer2Piece2 == 13 || currentSpacePlayer2Piece2 == 1 || currentSpacePlayer2Piece2 == 26 || currentSpacePlayer2Piece2 >= 17 {
                    
                    changeFourth = true
                    
                }
                
            } else if player2FourthPiece == Player2Piece3 {
                
                if currentSpacePlayer2Piece3 == 5 || currentSpacePlayer2Piece3 == 13 || currentSpacePlayer2Piece3 == 1 || currentSpacePlayer2Piece3 == 26 || currentSpacePlayer2Piece3 >= 17 {
                    
                    changeFourth = true
                    
                }
                
            } else if player2FourthPiece == Player2Piece4 {
                
                if currentSpacePlayer2Piece4 == 5 || currentSpacePlayer2Piece4 == 13 || currentSpacePlayer2Piece4 == 1 || currentSpacePlayer2Piece4 == 26 || currentSpacePlayer2Piece4 >= 17 {
                    
                    changeFourth = true
                    
                }
                
            }
            
            
        }
        
        
        
        
        var newcurrentSpacePlayer2Piece1 = 0
        var newcurrentSpacePlayer2Piece2 = 0
        var newcurrentSpacePlayer2Piece3 = 0
        var newcurrentSpacePlayer2Piece4 = 0
        
        
        
        
        
        if player == .Player2 {
            
            if (currentSpacePlayer2Piece1 <= 16 && currentSpacePlayer2Piece1 >= 9) {
                
                newcurrentSpacePlayer2Piece1 = currentSpacePlayer2Piece1
                
            } else if (currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22 || currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24) {
                
                newcurrentSpacePlayer2Piece1 = currentSpacePlayer2Piece1 + 4
                
            } else if currentSpacePlayer2Piece1 >= 1 && currentSpacePlayer2Piece1 <= 8 {
                
                newcurrentSpacePlayer2Piece1 = currentSpacePlayer2Piece1 + 16
                
            } else if currentSpacePlayer2Piece1 >= 17 && currentSpacePlayer2Piece1 <= 20 {
                
                newcurrentSpacePlayer2Piece1 = currentSpacePlayer2Piece1 + 13
                
            } else if currentSpacePlayer2Piece1 == 26 || currentSpacePlayer2Piece1 == 25 {
                
                newcurrentSpacePlayer2Piece1 = 50
                
            }
            
            if (currentSpacePlayer2Piece2 <= 16 && currentSpacePlayer2Piece2 >= 9) {
                
                newcurrentSpacePlayer2Piece2 = currentSpacePlayer2Piece2
                
            } else if (currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22 || currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24) {
                
                newcurrentSpacePlayer2Piece2 = currentSpacePlayer2Piece2 + 4
                
            } else if currentSpacePlayer2Piece2 >= 1 && currentSpacePlayer2Piece2 <= 8 {
                
                newcurrentSpacePlayer2Piece2 = currentSpacePlayer2Piece2 + 16
                
            } else if currentSpacePlayer2Piece2 >= 17 && currentSpacePlayer2Piece2 <= 20 {
                
                newcurrentSpacePlayer2Piece2 = currentSpacePlayer2Piece2 + 13
                
            } else if currentSpacePlayer2Piece2 == 26 || currentSpacePlayer2Piece2 == 25 {
                
                newcurrentSpacePlayer2Piece2 = 50
                
            }
            
            if (currentSpacePlayer2Piece3 <= 16 && currentSpacePlayer2Piece3 >= 9) {
                
                newcurrentSpacePlayer2Piece3 = currentSpacePlayer2Piece3
                
            } else if (currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22 || currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24) {
                
                newcurrentSpacePlayer2Piece3 = currentSpacePlayer2Piece3 + 4
                
            } else if currentSpacePlayer2Piece3 >= 1 && currentSpacePlayer2Piece3 <= 8 {
                
                newcurrentSpacePlayer2Piece3 = currentSpacePlayer2Piece3 + 16
                
            } else if currentSpacePlayer2Piece3 >= 17 && currentSpacePlayer2Piece3 <= 20 {
                
                newcurrentSpacePlayer2Piece3 = currentSpacePlayer2Piece3 + 13
                
            } else if currentSpacePlayer2Piece3 == 26 || currentSpacePlayer2Piece3 == 25 {
                
                newcurrentSpacePlayer2Piece3 = 50
                
            }
            
            if (currentSpacePlayer2Piece4 <= 16 && currentSpacePlayer2Piece4 >= 9) {
                
                newcurrentSpacePlayer2Piece4 = currentSpacePlayer2Piece4
                
            } else if (currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22 || currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24) {
                
                newcurrentSpacePlayer2Piece4 = currentSpacePlayer2Piece4 + 4
                
            } else if currentSpacePlayer2Piece4 >= 1 && currentSpacePlayer2Piece4 <= 8 {
                
                newcurrentSpacePlayer2Piece4 = currentSpacePlayer2Piece4 + 16
                
            } else if currentSpacePlayer2Piece4 >= 17 && currentSpacePlayer2Piece4 <= 20 {
                
                newcurrentSpacePlayer2Piece4 = currentSpacePlayer2Piece4 + 13
                
            } else if currentSpacePlayer2Piece4 == 26 || currentSpacePlayer2Piece4 == 25 {
                
                newcurrentSpacePlayer2Piece4 = 50
                
            }
            
            
            
        }
        
        
        
        
        
        
        
        if (player == .Player2) {
            
            
            
            if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) {
                
                firstPiece = Player2Piece1
                secondPiece = Player2Piece2
                thirdPiece = Player2Piece3
                fourthPiece = Player2Piece4
                
                print("It's PLAYER Piece At 9 So 1234:")
                print(firstPiece)
                print(secondPiece)
                print(thirdPiece)
                print(fourthPiece)
                
            } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece3 == 9) || (currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) || (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) || (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece4 == 9) {
                
                
                if (currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) {
                    
                    secondPiece = Player2Piece2
                    thirdPiece = Player2Piece3
                    fourthPiece = Player2Piece4
                    
                } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece3 == 9) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece2
                    fourthPiece = Player2Piece3
                    
                } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece3
                    fourthPiece = Player2Piece4
                    
                } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece4 == 9) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece2
                    fourthPiece = Player2Piece4
                    
                }
                
                print("It's PLAYER '3' LAST 3 PIECE BECAUSE CURRENTSPACE ISS '9'")
                print(secondPiece)
                print(thirdPiece)
                print(fourthPiece)
                
            } else if (currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) || (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9) || (currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece2 == 9) || (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece4 == 9) || (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece3 == 9) || (currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece4 == 9) {
                
                
                if (currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece4 == 9) {
                    
                    thirdPiece = Player2Piece3
                    fourthPiece = Player2Piece4
                    
                } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece2 == 9) {
                    
                    thirdPiece = Player2Piece1
                    fourthPiece = Player2Piece2
                    
                } else if (currentSpacePlayer2Piece3 == 9 && currentSpacePlayer2Piece2 == 9) {
                    
                    thirdPiece = Player2Piece3
                    fourthPiece = Player2Piece2
                    
                } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece4 == 9) {
                    
                    thirdPiece = Player2Piece1
                    fourthPiece = Player2Piece4
                    
                } else if (currentSpacePlayer2Piece1 == 9 && currentSpacePlayer2Piece3 == 9) {
                    
                    thirdPiece = Player2Piece1
                    fourthPiece = Player2Piece3
                    
                } else if (currentSpacePlayer2Piece2 == 9 && currentSpacePlayer2Piece4 == 9) {
                    
                    thirdPiece = Player2Piece2
                    fourthPiece = Player2Piece4
                    
                }
                
                
                print("It's PLAYER '3' LAST 2 PIECE BECAUSE CURRENTSPACE ISS '9'")
                print(thirdPiece)
                print(fourthPiece)
                
            } else if (currentSpacePlayer2Piece1 == 9 || currentSpacePlayer2Piece2 == 9 || currentSpacePlayer2Piece3 == 9 || currentSpacePlayer2Piece4 == 9) {
                
                
                if (currentSpacePlayer2Piece1 == 9) {
                    
                    fourthPiece = Player2Piece1
                    
                } else if (currentSpacePlayer2Piece2 == 9) {
                    
                    fourthPiece = Player2Piece2
                    
                } else if (currentSpacePlayer2Piece3 == 9) {
                    
                    fourthPiece = Player2Piece3
                    
                } else if (currentSpacePlayer2Piece4 == 9) {
                    
                    fourthPiece = Player2Piece4
                    
                }
                
                
                
                
                print("It's PLAYER '3' LAST 1 PIECE BECAUSE CURRENTSPACE ISS '9'")
                print(fourthPiece)
                
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
            //                                          Player2-Piece1
            
            
            if (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece4) {
                
                print("It's PLAYER '3' PIECE '1' BECAUSE IT'S BIGGER :")
                firstPiece = Player2Piece1
                print("First Piece is :")
                print(firstPiece)
                
            } else if ((newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3) || (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece4) || (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece4)) {
                
                
                print("It's PLAYER '3' PIECE '1' BECAUSE IT'S BIGGER :")
                secondPiece = Player2Piece1
                print("Second Piece is :")
                print(secondPiece)
                
            } else if (((newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece3) || (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece4) || (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece4)) && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3 || newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 || newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece4)) {
                
                
                print("It's PLAYER '3' PIECE '1' BECAUSE IT'S BIGGER :")
                thirdPiece = Player2Piece1
                print("Third Piece is :")
                print(thirdPiece)
                
                
            } else if (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece4) {
                
                print("It's PLAYER '3' PIECE '1' BECAUSE IT'S BIGGER :")
                fourthPiece = Player2Piece1
                print("Fourth Piece is :")
                print(fourthPiece)
                
            }
            
            
            
            
            //                                          Player2-Piece2
            
            if (newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece4) {
                
                print("It's PLAYER '3' PIECE '2' BECAUSE IT'S BIGGER :")
                firstPiece = Player2Piece2
                print("First Piece is :")
                print(firstPiece)
                
            } else if ((newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece3) || (newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece4) || (newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece4)) {
                
                print("It's PLAYER '3' PIECE '2' BECAUSE IT'S BIGGER :")
                secondPiece = Player2Piece2
                print("Second Piece is :")
                print(secondPiece)
                
            } else if (((newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece3) || (newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece4) || (newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece4)) && (newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1 || newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece3 || newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece4))  {
                
                
                print("It's PLAYER '3' PIECE '2' BECAUSE IT'S BIGGER :")
                thirdPiece = Player2Piece2
                print("Third Piece is :")
                print(thirdPiece)
                
                
            } else if (newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece4) {
                
                print("It's PLAYER '3' PIECE '2' BECAUSE IT'S BIGGER :")
                fourthPiece = Player2Piece2
                print("4 Piece is :")
                print(fourthPiece)
                
            }
            
            
            
            //                                          Player2-Piece3
            
            
            
            if ((newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece2) && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1) && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece4)) {
                
                print("It's PLAYER '3' PIECE '3' BECAUSE IT'S BIGGER :")
                firstPiece = Player2Piece3
                print("First Piece is :")
                print(firstPiece)
                
            } else if ((newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece2) || (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece4) || (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece4)) {
                
                print("It's PLAYER '3' PIECE '3' BECAUSE IT'S BIGGER :")
                secondPiece = Player2Piece3
                print("2 Piece is :")
                print(secondPiece)
                
            } else if (((newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece2) || (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece4) || (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece4)) && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 || newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece2 || newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece4)) {
                
                
                print("It's PLAYER '3' PIECE '3' BECAUSE IT'S BIGGER :")
                thirdPiece = Player2Piece3
                print("3 Piece is :")
                print(thirdPiece)
                
                
            } else if (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece4) {
                
                print("It's PLAYER '3' PIECE '3' BECAUSE IT'S BIGGER :")
                fourthPiece = Player2Piece3
                print("4 Piece is :")
                print(fourthPiece)
                
            }
            
            
            
            
            //                                          Player2-Piece4
            
            
            
            if ((newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece2) && (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece3) && (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1)) {
                
                print("It's PLAYER '3' PIECE '4' BECAUSE IT'S BIGGER :")
                firstPiece = Player2Piece4
                print("First Piece is :")
                print(firstPiece)
                
                // CHANGE HERE :::::::::::::::::::::
            } else if ((newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece2) || (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece3) || (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece3)) {
                
                print("It's PLAYER '3' PIECE '4' BECAUSE IT'S BIGGER :")
                secondPiece = Player2Piece4
                print("2 Piece is :")
                print(secondPiece)
                
            } else if (((newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece2) || (newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece3) || (newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece3)) && (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1 || newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece2 || newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece3)) {
                
                
                print("It's PLAYER '3' PIECE '4' BECAUSE IT'S BIGGER :")
                thirdPiece = Player2Piece4
                print("3 Piece is :")
                print(thirdPiece)
                
                
            } else if (newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1) {
                
                print("It's PLAYER '3' PIECE '4' BECAUSE IT'S BIGGER :")
                fourthPiece = Player2Piece4
                print("4 Piece is :")
                print(fourthPiece)
                
            }
            
            
            
            
            
            // If at same position then :
            
            
            
            
            
            if (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2 && currentSpacePlayer2Piece3 == currentSpacePlayer2Piece4 && currentSpacePlayer2Piece2 == currentSpacePlayer2Piece3) {
                
                firstPiece = Player2Piece1
                secondPiece = Player2Piece2
                thirdPiece = Player2Piece3
                fourthPiece = Player2Piece4
                
                print("Player 3 All Are At Same Position :")
                print("First Piece is :")
                print(firstPiece)
                print("2 Piece is :")
                print(secondPiece)
                print("3 Piece is :")
                print(thirdPiece)
                print("4 Piece is :")
                print(fourthPiece)
                
            } else if (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece3) || (currentSpacePlayer2Piece2 == currentSpacePlayer2Piece3 && currentSpacePlayer2Piece2 == currentSpacePlayer2Piece4) || (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece4) || (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece3 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece4) {
                
                if (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece3) {
                    
                    if (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece4) {
                        
                        firstPiece = Player2Piece1
                        secondPiece = Player2Piece2
                        thirdPiece = Player2Piece3
                        fourthPiece = Player2Piece4
                        
                    } else if (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece4) {
                        
                        firstPiece = Player2Piece4
                        secondPiece = Player2Piece1
                        thirdPiece = Player2Piece2
                        fourthPiece = Player2Piece3
                        
                    }
                    
                } else if (currentSpacePlayer2Piece2 == currentSpacePlayer2Piece3 && currentSpacePlayer2Piece2 == currentSpacePlayer2Piece4) {
                    
                    if (newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1) {
                        
                        firstPiece = Player2Piece4
                        secondPiece = Player2Piece2
                        thirdPiece = Player2Piece3
                        fourthPiece = Player2Piece1
                        
                    } else if (newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1) {
                        
                        firstPiece = Player2Piece1
                        secondPiece = Player2Piece4
                        thirdPiece = Player2Piece2
                        fourthPiece = Player2Piece3
                        
                    }
                    
                } else if (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece3) {
                    
                    if (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece4) {
                        
                        firstPiece = Player2Piece1
                        secondPiece = Player2Piece2
                        thirdPiece = Player2Piece3
                        fourthPiece = Player2Piece4
                        
                    } else if (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece4) {
                        
                        firstPiece = Player2Piece4
                        secondPiece = Player2Piece1
                        thirdPiece = Player2Piece2
                        fourthPiece = Player2Piece3
                        
                    }
                    
                } else if (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece4) {
                    
                    if (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3) {
                        
                        firstPiece = Player2Piece1
                        secondPiece = Player2Piece2
                        thirdPiece = Player2Piece4
                        fourthPiece = Player2Piece3
                        
                    } else if (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece3) {
                        
                        firstPiece = Player2Piece3
                        secondPiece = Player2Piece1
                        thirdPiece = Player2Piece2
                        fourthPiece = Player2Piece4
                        
                    }
                    
                } else if (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece3 && currentSpacePlayer2Piece1 == currentSpacePlayer2Piece4) {
                    
                    if (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2) {
                        
                        firstPiece = Player2Piece1
                        secondPiece = Player2Piece4
                        thirdPiece = Player2Piece3
                        fourthPiece = Player2Piece2
                        
                    } else if (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2) {
                        
                        firstPiece = Player2Piece2
                        secondPiece = Player2Piece1
                        thirdPiece = Player2Piece4
                        fourthPiece = Player2Piece3
                        
                    }
                    
                }
                
                print("Player 3 Three Are At Same Position :")
                print("First Piece is :")
                print(firstPiece)
                print("2 Piece is :")
                print(secondPiece)
                print("3 Piece is :")
                print(thirdPiece)
                print("4 Piece is :")
                print(fourthPiece)
                
            } else if ( (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece2/* && currentSpacePlayer2Piece1 != 26*/) || (currentSpacePlayer2Piece2 == currentSpacePlayer2Piece3/* && currentSpacePlayer2Piece2 != 26*/) || (currentSpacePlayer2Piece3 == currentSpacePlayer2Piece4/* && currentSpacePlayer2Piece3 != 26*/) || (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece4/* && currentSpacePlayer2Piece1 != 26*/) || (currentSpacePlayer2Piece1 == currentSpacePlayer2Piece3/* && currentSpacePlayer2Piece1 != 26*/) || (currentSpacePlayer2Piece2 == currentSpacePlayer2Piece4/* && currentSpacePlayer2Piece2 != 26*/) ) {
                
                
                if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece2 && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1)) {
                    
                    thirdPiece = Player2Piece1
                    fourthPiece = Player2Piece2
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece2 && (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1)) {
                    
                    firstPiece = Player2Piece1
                    secondPiece = Player2Piece2
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece2 && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1)) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece2
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece2 && (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1)) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece2
                    
                }
                
                
                if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece2)) {
                    
                    thirdPiece = Player2Piece3
                    fourthPiece = Player2Piece2
                    
                } else if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece2)) {
                    
                    firstPiece = Player2Piece3
                    secondPiece = Player2Piece2
                    
                } else if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece2)) {
                    
                    secondPiece = Player2Piece3
                    thirdPiece = Player2Piece2
                    
                } else if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece2)) {
                    
                    secondPiece = Player2Piece3
                    thirdPiece = Player2Piece2
                    
                }
                
                
                if (newcurrentSpacePlayer2Piece3 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece3)) {
                    
                    thirdPiece = Player2Piece3
                    fourthPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece3 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece3)) {
                    
                    firstPiece = Player2Piece3
                    secondPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece3 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece3)) {
                    
                    secondPiece = Player2Piece3
                    thirdPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece3 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece3 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece3)) {
                    
                    secondPiece = Player2Piece3
                    thirdPiece = Player2Piece4
                    
                }
                
                
                if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1)) {
                    
                    thirdPiece = Player2Piece1
                    fourthPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1)) {
                    
                    firstPiece = Player2Piece1
                    secondPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1)) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1)) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece4
                    
                }
                
                
                if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1)) {
                    
                    thirdPiece = Player2Piece1
                    fourthPiece = Player2Piece3
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1)) {
                    
                    firstPiece = Player2Piece1
                    secondPiece = Player2Piece3
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece4 > newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 < newcurrentSpacePlayer2Piece1)) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece3
                    
                } else if (newcurrentSpacePlayer2Piece1 == newcurrentSpacePlayer2Piece3 && (newcurrentSpacePlayer2Piece4 < newcurrentSpacePlayer2Piece1 && newcurrentSpacePlayer2Piece2 > newcurrentSpacePlayer2Piece1)) {
                    
                    secondPiece = Player2Piece1
                    thirdPiece = Player2Piece3
                    
                }
                
                
                if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece2)) {
                    
                    thirdPiece = Player2Piece2
                    fourthPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece2)) {
                    
                    firstPiece = Player2Piece2
                    secondPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 > newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 < newcurrentSpacePlayer2Piece2)) {
                    
                    secondPiece = Player2Piece2
                    thirdPiece = Player2Piece4
                    
                } else if (newcurrentSpacePlayer2Piece2 == newcurrentSpacePlayer2Piece4 && (newcurrentSpacePlayer2Piece1 < newcurrentSpacePlayer2Piece2 && newcurrentSpacePlayer2Piece3 > newcurrentSpacePlayer2Piece2)) {
                    
                    secondPiece = Player2Piece2
                    thirdPiece = Player2Piece4
                    
                }
                
                print("Player 3 Two Are At Same Position :")
                print("First Piece is :")
                print(firstPiece)
                print("2 Piece is :")
                print(secondPiece)
                print("3 Piece is :")
                print(thirdPiece)
                print("4 Piece is :")
                print(fourthPiece)
                
            }
            
            
            
            
            
            
            
            
            
            if !changeFourth && (player2FourthPiece == Player2Piece1 || player2FourthPiece == Player2Piece2 || player2FourthPiece == Player2Piece3 || player2FourthPiece == Player2Piece4) {
                
                
                if !(( fourthPiece == Player2Piece1 && (( currentSpacePlayer2Piece1 == 13 && currentSpacePlayer2Piece1 + rolledDice == 21) || currentSpacePlayer2Piece1 + rolledDice == 5 || currentSpacePlayer2Piece1 + rolledDice == 13 || currentSpacePlayer2Piece1 + rolledDice == 17)) || ( fourthPiece == Player2Piece2 && (( currentSpacePlayer2Piece2 == 13 && currentSpacePlayer2Piece2 + rolledDice == 21) || currentSpacePlayer2Piece2 + rolledDice == 5 || currentSpacePlayer2Piece2 + rolledDice == 13 || currentSpacePlayer2Piece2 + rolledDice == 17)) || (fourthPiece == Player2Piece3 && (( currentSpacePlayer2Piece3 == 13 && currentSpacePlayer2Piece3 + rolledDice == 21) || currentSpacePlayer2Piece3 + rolledDice == 5 || currentSpacePlayer2Piece3 + rolledDice == 13 || currentSpacePlayer2Piece3 + rolledDice == 17)) || (fourthPiece == Player2Piece4 && (( currentSpacePlayer2Piece4 == 13 && currentSpacePlayer2Piece4 + rolledDice == 21) || currentSpacePlayer2Piece4 + rolledDice == 5 || currentSpacePlayer2Piece4 + rolledDice == 13 || currentSpacePlayer2Piece4 + rolledDice == 17))) {
                    print("Changed Player 3 4th Piece!")
                    print(player2FourthPiece)
                    fourthPiece = player2FourthPiece
                    
                }
            }
            
            
            
            
            // }
            
            
        }
        
        
        // Add new Fourth Piece to the It's Player's Fourth Piece
        
        
        if player == .Player2 {
            
            player2FourthPiece = fourthPiece
            print("Player 2 Fourth Piece :")
            print(player2FourthPiece)
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    func setThePlayerSpace(space:Int, player:Player) {
        
        if (player == .Player1) {
            
            
            if touchedNode.name == "Player1Piece1" {
                
                currentSpacePlayer1Piece1 = space
                
            } else if touchedNode.name == "Player1Piece2" {
                
                currentSpacePlayer1Piece2 = space
                
            } else if touchedNode.name == "Player1Piece3" {
                
                currentSpacePlayer1Piece3 = space
                
            } else if touchedNode.name == "Player1Piece4" {
                
                currentSpacePlayer1Piece4 = space
                
            }
            
        } else if (player == .Player2) {
            
            
            if playerPiece.name == "Player2Piece1" {
                
                currentSpacePlayer2Piece1 = space
                
            } else if playerPiece.name == "Player2Piece2" {
                
                currentSpacePlayer2Piece2 = space
                
            } else if playerPiece.name == "Player2Piece3" {
                
                currentSpacePlayer2Piece3 = space
                
            } else if playerPiece.name == "Player2Piece4" {
                
                currentSpacePlayer2Piece4 = space
                
            }
            
            
        }
        
        //Print
        print("set the Player Space =")
        print(space)
        
    }
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    func returnPlayerSpace(player:Player) -> Int {
        
        var space:Int = 0
        
        
        
        if (player == .Player1) {
            
            if touchedNode.name == "Player1Piece1" {
                
                space = currentSpacePlayer1Piece1
                
            } else if touchedNode.name == "Player1Piece2" {
                
                space = currentSpacePlayer1Piece2
                
            } else if touchedNode.name == "Player1Piece3" {
                
                space = currentSpacePlayer1Piece3
                
            } else if touchedNode.name == "Player1Piece4" {
                
                space = currentSpacePlayer1Piece4
                
            }
            
        } else if (player == .Player2) {
            
            
            if playerPiece.name == "Player2Piece1" {
                
                space = currentSpacePlayer2Piece1
                
            } else if playerPiece.name == "Player2Piece2" {
                
                space = currentSpacePlayer2Piece2
                
            } else if playerPiece.name == "Player2Piece3" {
                
                space = currentSpacePlayer2Piece3
                
            } else if playerPiece.name == "Player2Piece4" {
                
                space = currentSpacePlayer2Piece4
                
            }
            
            
        }
        
        
        //Print
        print("Return Player Space =")
        print(space)
        
        return space
    }
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    func realPath( nextPlace:Int, player:Player) -> Int {
        
        var newLocation = nextPlace
        
        
        if (nextPlace == 17) {
            
            // Print
            print("Next Space in the ==17 IF CONDITION : ")
            print(nextPlace)
            
            if (player == .Player2) {
                
                newLocation = 1
                
                
                // Print
                
                print("Second If Condition Next Space : ")
                print(newLocation)
                print("WhosTurn in Second IF : ")
                print(whosTurn)
            }
        } else if (nextPlace == 25) {
            
            // Print
            print("Next Space in the ==25 IF CONDITION : ")
            print(nextPlace)
            
            if (player == .Player2) {
                
                newLocation = 17
                
                
                // Print
                
                print("Second If Condition Next Space : ")
                print(newLocation)
                print("WhosTurn in Second IF : ")
                print(whosTurn)
            }
        }
        
        
        
        
        
        
        return newLocation
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func HowtoMove(player:Player, dice:SKSpriteNode) {
        
        print("All TOTAL Turns :")
        print(playerTturns)
        print(computer1Tturns)
        
        print("Inside how to move:")
        
        if player == .Player1 {
            
            movesRemaining = rolledDice
            
            
            if (rolledDice == 0) {
                movesRemaining = 0
                OneMoreMove = 0
                playerTturns = 0
                updateScore()
                
                let delay = 0.5
                // self.DincreamentScore()
                
                //                self.threeLargeNumsinrowPla.isHidden = false
                
                print("Yes!")
                
                //                    self.threeLargeNumsinrowPla.setScale(1)
                
                //  self.notifyForPlusMove(label: self.threeLargeNumsinrowPla)
                self.notifyForTurnBurn(label: self.threeLargeNumsinrowPla)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    //                    self.threeLargeNumsinrowPla.isHidden = true
                    
                    self.movePiece()
                }
                
            } else {
                
                if ((currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26)) {
                    
                    // DincreamentScore()
                    print("Don't Do It.")
                    
                    self.blink(player: player)
                    
                    
                    
                    
                } else {
                    
                    
                    if (self.currentSpacePlayer1Piece1 == 18 || self.currentSpacePlayer1Piece1 == 19 || self.currentSpacePlayer1Piece1 == 20 || self.currentSpacePlayer1Piece1 == 21 || self.currentSpacePlayer1Piece1 == 22 || currentSpacePlayer1Piece1 == 23 || currentSpacePlayer1Piece1 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece1 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 22) {
                            
                            p1p1TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                                
                                print("In increament player1piece1 AT 22")
                                print(p1p1TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece1 == 23) {
                            
                            p1p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                                
                                print("In increament player1piece1 AT 23")
                                print(p1p1TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece1 == 24) {
                            
                            p1p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                piece1AbletoEnter = false
                                
                                print("In increament player1piece1 AT 24")
                                print(p1p1TryToWin)
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer1Piece2 == 18 || self.currentSpacePlayer1Piece2 == 19 || self.currentSpacePlayer1Piece2 == 20 || self.currentSpacePlayer1Piece2 == 21 || self.currentSpacePlayer1Piece2 == 22 || currentSpacePlayer1Piece2 == 23 || currentSpacePlayer1Piece2 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece2 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 22) {
                            
                            p1p2TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                                
                                print("In increament player1piece2 AT 22")
                                print(p1p2TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece2 == 23) {
                            
                            p1p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                                
                                print("In increament player1piece2 AT 23")
                                print(p1p2TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece2 == 24) {
                            
                            p1p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                piece2AbletoEnter = false
                                
                                print("In increament player1piece2 AT 24")
                                print(p1p2TryToWin)
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer1Piece3 == 18 || self.currentSpacePlayer1Piece3 == 19 || self.currentSpacePlayer1Piece3 == 20 || self.currentSpacePlayer1Piece3 == 21 || self.currentSpacePlayer1Piece3 == 22 || currentSpacePlayer1Piece3 == 23 || currentSpacePlayer1Piece3 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece3 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 22) {
                            
                            p1p3TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                                
                                print("In increament player1piece3 AT 22")
                                print(p1p3TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece3 == 23) {
                            
                            p1p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                                
                                print("In increament player1piece3 AT 23")
                                print(p1p3TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece3 == 24) {
                            
                            p1p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                piece3AbletoEnter = false
                                
                                print("In increament player1piece3 AT 24")
                                print(p1p3TryToWin)
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer1Piece4 == 18 || self.currentSpacePlayer1Piece4 == 19 || self.currentSpacePlayer1Piece4 == 20 || self.currentSpacePlayer1Piece4 == 21 || self.currentSpacePlayer1Piece4 == 22 || currentSpacePlayer1Piece4 == 23 || currentSpacePlayer1Piece4 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece4 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 22) {
                            
                            p1p4TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                                
                                print("In increament player1piece4 AT 22")
                                print(p1p4TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece4 == 23) {
                            
                            p1p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                                
                                print("In increament player1piece4 AT 23")
                                print(p1p4TryToWin)
                            }
                            
                        } else if (currentSpacePlayer1Piece4 == 24) {
                            
                            p1p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                piece4AbletoEnter = false
                                
                                print("In increament player1piece4 AT 24")
                                print(p1p4TryToWin)
                            }
                            
                        }
                        
                        
                    }
                    
                    blink(player: player)
                    
                }
                
                
                
                if ((currentSpacePlayer1Piece1 == 26 || piece1AbletoEnter == false) && (currentSpacePlayer1Piece2 == 26 || piece2AbletoEnter == false) && (currentSpacePlayer1Piece3 == 26 || piece3AbletoEnter == false) && (currentSpacePlayer1Piece4 == 26 || piece4AbletoEnter == false)) {
                    
                    print("All are AT Damn 26 Yeah!")
                    movesRemaining = 0
                    print(Dice.isHidden)
                    
                    
                    let delay = 0.5
                    
                    
                    print("Yes!")
                    
                    
                    self.notifyForCantMove(label: self.cantMovePla)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        
                        self.movePiece()
                    }
                    
                    
                    
                }
            }
            
        } else if player == .Player2 {
            
            
            print("Inside Dice Completion Handeler Player 2 :")
            
            if (rolledDice == 0) {
                movesRemaining = 0
                OneMoreMove = 0
                computer1Tturns = 0
                updateScore()
                
                let delay = 0.5
                
                
                
                print("Yes!")
                
                
                self.notifyForTurnBurn(label: self.threeLargeNumsinrowPla)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    self.movePiece()
                }
                
                
            } else {
                
                
                if ((currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26)) {
                    
                    print("??Rolled Dice For Moves Remaining :")
                    print(self.rolledDice)
                    movesRemaining = rolledDice
                    self.returnPlayerPiece(player: player)
                    print("??Moves Remaining After adding Rolled Dice to IT :")
                    print(self.movesRemaining)
                    blink(player: player)
                    removeFlashTurn(player: player)
                    movePiece()
                    
                    print("??IN SIDE Rolling in run Completion Handeler")
                    print(self.rolling)
                    
                } else {
                    
                    if (((currentSpacePlayer2Piece1 >= 17 && currentSpacePlayer2Piece1 <= 20 && currentSpacePlayer2Piece1 + rolledDice > 21) || (currentSpacePlayer2Piece1 > 21 && currentSpacePlayer2Piece1 <= 24 && rolledDice == 8) || (currentSpacePlayer2Piece1 == 26)) && ((currentSpacePlayer2Piece2 >= 17 && currentSpacePlayer2Piece2 <= 20 && currentSpacePlayer2Piece2 + rolledDice > 21) || (currentSpacePlayer2Piece2 > 21 && currentSpacePlayer2Piece2 <= 24 && rolledDice == 8) || (currentSpacePlayer2Piece2 == 26)) && ((currentSpacePlayer2Piece3 >= 17 && currentSpacePlayer2Piece3 <= 20 && currentSpacePlayer2Piece3 + rolledDice > 21) || (currentSpacePlayer2Piece3 > 21 && currentSpacePlayer2Piece3 <= 24 && rolledDice == 8) || (currentSpacePlayer2Piece3 == 26)) && ((currentSpacePlayer2Piece4 >= 17 && currentSpacePlayer2Piece4 <= 20 && currentSpacePlayer2Piece4 + rolledDice > 21) || (currentSpacePlayer2Piece4 > 21 && currentSpacePlayer2Piece4 <= 24 && rolledDice == 8) || (currentSpacePlayer2Piece4 == 26))) {
                        
                        print("Skipped The Turn Because No Piece Is Able to Move. || Player - 3 ||")
                        
                        movesRemaining = 0
                        
                        
                        
                        let delay = 0.5
                        
                        
                        print("Yes!")
                        
                        
                        self.notifyForCantMove(label: self.cantMovePla)
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                            
                            self.movePiece()
                        }
                        
                    } else {
                        
                        
                        print("Rolled Dice For Moves Remaining :")
                        print(self.rolledDice)
                        movesRemaining = rolledDice
                        self.returnPlayerPiece(player: player)
                        print("Moves Remaining After adding Rolled Dice to IT :")
                        print(self.movesRemaining)
                        blink(player: player)
                        removeFlashTurn(player: player)
                        self.movePiece()
                        
                        print("IN SIDE Rolling in run Completion Handeler")
                        print(self.rolling)
                        
                        
                    }
                    
                }
                
            }
            
            
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // MOVE FUNCTIONS TILL HERE : <-------------------------------------------------------------------
    
    
    
    
    
    func Adjust(node:SKNode) {
        
        var j:Int = 1
        var i:Int = 1
        var p:Int = 0
        var pieces: [SKSpriteNode] = []
        
        if (String(currentSpacePlayer1Piece1) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 1")
            p += 1
            pieces.append(Player1Piece1)
            
        }
        
        if (String(currentSpacePlayer1Piece2) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 2")
            p += 1
            pieces.append(Player1Piece2)
            
        }
        
        if (String(currentSpacePlayer1Piece3) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 3")
            p += 1
            pieces.append(Player1Piece3)
            
        }
        
        if (String(currentSpacePlayer1Piece4) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 4")
            p += 1
            pieces.append(Player1Piece4)
            
        }
        
        if (String(currentSpacePlayer2Piece1) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 1")
            p += 1
            pieces.append(Player2Piece1)
            
        }
        
        if (String(currentSpacePlayer2Piece2) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 2")
            p += 1
            pieces.append(Player2Piece2)
            
        }
        
        if (String(currentSpacePlayer2Piece3) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 3")
            p += 1
            pieces.append(Player2Piece3)
            
        }
        
        if (String(currentSpacePlayer2Piece4) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 4")
            p += 1
            pieces.append(Player2Piece4)
            
        }
        
        print(node.name!)
        
        print(pieces.isEmpty)
        
        print(childNode(withName: "a11")!)
        print(pieces.count)
        
        
        
        if (p > 1 && p <= 5) && (node.name != "1" && node.name != "5" && node.name != "9" && node.name != "13" && node.name != "25") {
            
            
            
            while (j <= 25) {
                print("hi")
                if (node.name == "\(j)") {
                    
                    
                    while (i <= 5) {
                        
                        if (pieces.count >= i) {
                            print("i and J and P :")
                            print(i)
                            print(j)
                            print(p)
                            
                            let nodeLoc:SKNode = childNode(withName: "a\(j)\(i)")!
                            let moveAction:SKAction = SKAction.move(to: nodeLoc.position, duration: 0.2)
                            
                            pieces[i - 1].run(moveAction)
                            
                        }
                        i += 1
                    }
                    
                    
                    
                }
                
                
                j += 1
            }
            
            
            
            // } else if (p >= 5) && (node.name != "1" && node.name != "5" && node.name != "9" && node.name != "13" && node.name != "25") {
            
            
            
            
            
        } else if (node.name == "1" || node.name == "5" || node.name == "9" || node.name == "13") {
            
            
            while (j <= 5) {
                print("hi")
                if ((node.name == "1" && j == 1) || (node.name == "5" && j == 2) || (node.name == "9" && j == 3) || (node.name == "13" && j == 4)) && (p > 1) {
                    
                    
                    while (i <= 9) {
                        
                        if (pieces.count >= i) {
                            print("i and J and P :")
                            print(i)
                            print(j)
                            print(p)
                            
                            let nodeLoc:SKNode = childNode(withName: "b\(j)\(i)")!
                            let moveAction:SKAction = SKAction.move(to: nodeLoc.position, duration: 0.2)
                            print(pieces[i - 1])
                            pieces[i - 1].run(moveAction)
                            
                        }
                        i += 1
                    }
                    
                    
                    
                }
                
                
                j += 1
            }
            
            
        } else if (node.name == "25") {
            print("Inside THE == 25 Condition : ")
            
            if (whosTurn == .Player1) {
                
                if (currentSpacePlayer1Piece1 == 25) {
                    
                    print("Inside Player 1 Piece 1 Adjust Action :")
                    
                    if !p1P1Reached {
                        Player1Piece1.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece1eye1.isHidden = true
                        player1Piece1eye2.isHidden = true
                        p1P1Reached = true
                    }
                    let loc:SKNode = childNode(withName: "c1")!
                    
                    
                    let moveaction : SKAction = SKAction.move(to: loc.position, duration: 0.2)
                    
                    Player1Piece1.run(moveaction)
                    
                    currentSpacePlayer1Piece1 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer1Piece2 == 25) {
                    
                    print("Inside Player 1 Piece 2 Adjust Action :")
                    
                    //let loc2:SKNode = childNode(withName: "c2")!
                    
                    if !p1P2Reached {
                        Player1Piece2.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece2eye1.isHidden = true
                        player1Piece2eye2.isHidden = true
                        p1P2Reached = true
                    }
                    
                    let moveaction : SKAction = SKAction.move(to: (childNode(withName: "c2")?.position)!, duration: 0.2)
                    
                    Player1Piece2.run(moveaction)
                    
                    currentSpacePlayer1Piece2 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer1Piece3 == 25) {
                    
                    print("Inside Player 1 Piece 3 Adjust Action :")
                    
                    if !p1P3Reached {
                        Player1Piece3.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece3eye1.isHidden = true
                        player1Piece3eye2.isHidden = true
                        p1P3Reached = true
                    }
                    
                    //let loc3:SKNode = childNode(withName: "c3")!
                    
                    let moveaction : SKAction = SKAction.move(to: (childNode(withName: "c3")?.position)!, duration: 0.2)
                    
                    Player1Piece3.run(moveaction)
                    
                    currentSpacePlayer1Piece3 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer1Piece4 == 25) {
                    
                    print("Inside Player 1 Piece 4 Adjust Action :")
                    
                    if !p1P4Reached {
                        Player1Piece4.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece4eye1.isHidden = true
                        player1Piece4eye2.isHidden = true
                        p1P4Reached = true
                    }
                    
                    //let loc4:SKNode = childNode(withName: "c4")!
                    
                    let moveaction : SKAction = SKAction.move(to: (childNode(withName: "c4")?.position)!, duration: 0.2)
                    
                    Player1Piece4.run(moveaction)
                    
                    currentSpacePlayer1Piece4 = 26
                    OneMoreMove += 1
                    updateScore()
                }
                
            } else if (whosTurn == .Player2) {
                
                if (currentSpacePlayer2Piece1 == 25) {
                    
                    if !p2P1Reached {
                        
                        Player2Piece1.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece1eye1.isHidden = true
                        player2Piece1eye2.isHidden = true
                        p2P1Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c21")!
                    Player2Piece1.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece1 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer2Piece2 == 25) {
                    
                    if !p2P2Reached {
                        
                        Player2Piece2.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece2eye1.isHidden = true
                        player2Piece2eye2.isHidden = true
                        p2P2Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c22")!
                    Player2Piece2.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece2 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer2Piece3 == 25) {
                    
                    if !p2P3Reached {
                        
                        Player2Piece3.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece3eye1.isHidden = true
                        player2Piece3eye2.isHidden = true
                        p2P3Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c23")!
                    Player2Piece3.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece3 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer2Piece4 == 25) {
                    
                    if !p2P4Reached {
                        
                        Player2Piece4.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece4eye1.isHidden = true
                        player2Piece4eye2.isHidden = true
                        p2P4Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c24")!
                    Player2Piece4.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece4 = 26
                    OneMoreMove += 1
                    updateScore()
                }
                
            }
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    func disabletoEnter() {
        piece1AbletoEnter = true
        piece2AbletoEnter = true
        piece3AbletoEnter = true
        piece4AbletoEnter = true
    }
    
    
    
    
    
    
    
    
    
    func checkWin(player:Player)  {
        
        if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) {
            
            
            
            gameOver = true
            
            
            print("EnTER THE GAME OVER MENU :")
            
        }
        
        
        if (player == .Player1 && ((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26)) ) {
            
            print(touchedNode)
            print(currentSpacePlayer1Piece1)
            print(currentSpacePlayer1Piece2)
            print(currentSpacePlayer1Piece3)
            print(currentSpacePlayer1Piece4)
            
            if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 24) {
                dontScoreP1 = true
            } else if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece2 == 24) {
                dontScoreP1 = true
            } else if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 24) {
                dontScoreP1 = true
            } else if (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 == 24) {
                dontScoreP1 = true
            }
            
        } else if (player == .Player2 && ((currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26)) ) {
            
            if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 20) {
                dontScoreP2 = true
            } else if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece2 == 20) {
                dontScoreP2 = true
            } else if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 20) {
                dontScoreP2 = true
            } else if (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 20) {
                dontScoreP2 = true
            }
            
        }
        
        
        
        if player == .Player1 {
            
            if (currentSpacePlayer1Piece1 == 26 && !p1p1Plus10) {
                
                player1Piece1Score += 11
                p1p1Plus10 = true
                
            } else if (currentSpacePlayer1Piece2 == 26 && !p1p2Plus10) {
                
                player1Piece2Score += 11
                p1p2Plus10 = true
                
            } else if (currentSpacePlayer1Piece3 == 26 && !p1p3Plus10) {
                
                player1Piece3Score += 11
                p1p3Plus10 = true
                
            } else if (currentSpacePlayer1Piece4 == 26 && !p1p4Plus10) {
                
                player1Piece4Score += 11
                p1p4Plus10 = true
                
            }
            
            playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
            playerScoreLabel.text = "\(playerScore)"
            
        } else if player == .Player2 {
            
            if (currentSpacePlayer2Piece1 == 26 && !p2p1Plus10) {
                
                com1Piece1Score += 11
                p2p1Plus10 = true
                
            } else if (currentSpacePlayer2Piece2 == 26 && !p2p2Plus10) {
                
                com1Piece2Score += 11
                p2p2Plus10 = true
                
            } else if (currentSpacePlayer2Piece3 == 26 && !p2p3Plus10) {
                
                com1Piece3Score += 11
                p2p3Plus10 = true
                
            } else if (currentSpacePlayer2Piece4 == 26 && !p2p4Plus10) {
                
                com1Piece4Score += 11
                p2p4Plus10 = true
                
            }
            
            com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
            com1ScoreLabel.text = "\(com1Score)"
            
        }
        
        
        
        
        
        if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) && (i == 0) {
            
            i += 1
            
            playerTturns = 0
            
            plaTTtext.text = "ToTal Turns:\(playerTturns)"
            
            
            place.append(playerone)
        }
        
        if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) && (j == 0) {
            
            j += 1
            
            computer1Tturns = 0
            
            com1TTtext.text = "ToTal Turns:\(computer1Tturns)"
            
            
            place.append(playertwo)
        }
        
        
        
        
        if (player == .Player1) {
            
            if (touchedNode == Player1Piece1) {
                
                if currentSpacePlayer1Piece1 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    // gameOver = true
                    // Player1Piece1.isPaused = true
                    // movesRemaining = 0
                    
                    //   OneMoreMove += 1
                }
            } else if (touchedNode == Player1Piece2) {
                
                if currentSpacePlayer1Piece2 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    // gameOver = true
                    // Player1Piece2.isPaused = true
                    // movesRemaining = 0
                    
                    //  OneMoreMove += 1
                }
            } else if (touchedNode == Player1Piece3) {
                
                
                if currentSpacePlayer1Piece3 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    // gameOver = true
                    //  Player1Piece3.isPaused = true
                    // movesRemaining = 0
                    
                    // OneMoreMove += 1
                }
            } else if (touchedNode == Player1Piece4) {
                
                
                if currentSpacePlayer1Piece4 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    //  gameOver = true
                    //  Player1Piece4.isPaused = true
                    // movesRemaining = 0
                    
                    // OneMoreMove += 1
                }
            }
            
            
        } else if (player == .Player2) {
            
            
            if (playerPiece == Player2Piece1) {
                
                if currentSpacePlayer2Piece1 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    //  gameOver = true
                    //   Player2Piece1.isPaused = true
                    // movesRemaining = 0
                    
                    //   OneMoreMove += 1
                }
            } else if (playerPiece == Player2Piece2) {
                
                if currentSpacePlayer2Piece2 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    //  gameOver = true
                    //   Player2Piece2.isPaused = true
                    // movesRemaining = 0
                    
                    //  OneMoreMove += 1
                }
            } else if (playerPiece == Player2Piece3) {
                
                
                if currentSpacePlayer2Piece3 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    // gameOver = true
                    //   Player2Piece3.isPaused = true
                    // movesRemaining = 0
                    //
                    //  OneMoreMove += 1
                }
            } else if (playerPiece == Player2Piece4) {
                
                
                if currentSpacePlayer2Piece4 == 26 {
                    
                    print("Inside The CheckWin")
                    
                    // gameOver = true
                    //   Player2Piece4.isPaused = true
                    // movesRemaining = 0
                    
                    //   OneMoreMove += 1
                }
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    func noWinList() {
        
        print("In Score Checking :")
        print("You :")
        print(playerScore)
        print("COMPUTER 1 :")
        print(com1Score)
        
        if !(currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) {
            print("Entered FIRST :")
            
            if playerScore > com1Score {
                
                playerOne = playerLabel.text!
                print("PlayerOne P1 :")
                print(playerOne)
                
            } else if (playerScore < com1Score) {
                
                playerTwo = playerLabel.text!
                print("PlayerFour P1 :")
                print(playerFour)
                
            }
            
        }
        
        
        if !(currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) {
            print("Entered SECOND :")
            
            if com1Score > playerScore {
                
                playerOne = computer1Label.text!
                print("PlayerOne P2 :")
                print(playerOne)
                
            } else if (com1Score < playerScore) {
                
                playerTwo = computer1Label.text!
                print("PlayerFour P2 :")
                print(playerFour)
                
            }
            
        }
        
        
        
        
        
        
        
        // SAmE Sore HERE :
        
        if (!(currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) || !(currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) ) {
            print("Entered COMPARING :")
            if playerScore == com1Score {
                
                playerOne = playerLabel.text!
                playerTwo = computer1Label.text!
                print("All At Same :")
                print(playerOne)
                print(playerTwo)
                
            }
            
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    // ROLL DICE YEAH YEAH :
    
    
    func rollDice(player:Player)  {
        
        
        
        
        
        
        
        // Print
        // print("Whos Turn In RollDice :")
        // print(player)
        // print(rolling)
        // print(moveFinished)
        
        
        print("All TOTAL Turns :")
        print(playerTturns)
        print(computer1Tturns)
        
        //let sides = [1, 2, 3, 4, 8]
        //let probabilities = [1.0, 1.0, 1.0, 1.2, 1.2]
        
        //let rand = randomNumber(probabilities: probabilities)
        //let roll = sides[rand]
        
        
        removeAnimation(player: whosTurn)
        
        // let sides = [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8]
        // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
        
        
        // if rolling && !moveFinished {
        //     return
        // }
        
        
        rolling = true
        moveFinished = false
        
        
        Dice.isHidden = true
        Dice2.isHidden = true
        
        
        playSound(soundName: diceRollSFX)
        
        
        
        
        
        var i : Int = 1
        var j : Int = 0
        
        let side = [2, 3]
        
        var num = side[Int(arc4random_uniform(UInt32(side.count)))]
        
        
        if ((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26)) {
            
            num = 4
            
        }
        
        
        while (i > j) {
            
            print("Add for Last human player piece in both com and vsFriend they will have more chances to get 4s")
            
            
            if ((p1p1TryToWin >= num && (currentSpacePlayer1Piece1 == 22 || currentSpacePlayer1Piece1 == 23 || currentSpacePlayer1Piece1 == 24) ) || (p1p2TryToWin >= num && (currentSpacePlayer1Piece2 == 22 || currentSpacePlayer1Piece2 == 23 || currentSpacePlayer1Piece2 == 24) ) || (p1p3TryToWin >= num && (currentSpacePlayer1Piece3 == 22 || currentSpacePlayer1Piece3 == 23 || currentSpacePlayer1Piece3 == 24) ) || (p1p4TryToWin >= num && (currentSpacePlayer1Piece4 == 22 || currentSpacePlayer1Piece4 == 23 || currentSpacePlayer1Piece4 == 24) )) && (whosTurn == .Player1) {
                
                if (p1p1TryToWin >= num && (currentSpacePlayer1Piece1 == 22 || currentSpacePlayer1Piece1 == 23 || currentSpacePlayer1Piece1 == 24) ) {
                    
                    //     if (25 - currentSpacePlayer1Piece1 == 3 || 25 - currentSpacePlayer1Piece1 == 2 || 25 - currentSpacePlayer1Piece1 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece1
                    
                    
                    print("Changed it To New One : Player1Piece1")
                    print(rolledDice)
                    print(p1p1TryToWin)
                    print(num)
                    
                    p1p1TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p1p2TryToWin >= num && (currentSpacePlayer1Piece2 == 22 || currentSpacePlayer1Piece2 == 23 || currentSpacePlayer1Piece2 == 24) ) {
                    
                    //   if (25 - currentSpacePlayer1Piece2 == 3 || 25 - currentSpacePlayer1Piece2 == 2 || 25 - currentSpacePlayer1Piece2 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece2
                    
                    
                    print("Changed it To New One : Player1Piece2")
                    print(rolledDice)
                    print(p1p2TryToWin)
                    print(num)
                    
                    p1p2TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                } else if (p1p3TryToWin >= num && (currentSpacePlayer1Piece3 == 22 || currentSpacePlayer1Piece3 == 23 || currentSpacePlayer1Piece3 == 24) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece3 == 3 || 25 - currentSpacePlayer1Piece3 == 2 || 25 - currentSpacePlayer1Piece3 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece3
                    
                    
                    print("Changed it To New One : Player1Piece3")
                    print(rolledDice)
                    print(p1p3TryToWin)
                    print(num)
                    
                    p1p3TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p1p4TryToWin >= num && (currentSpacePlayer1Piece4 == 22 || currentSpacePlayer1Piece4 == 23 || currentSpacePlayer1Piece4 == 24) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece4 == 3 || 25 - currentSpacePlayer1Piece4 == 2 || 25 - currentSpacePlayer1Piece4 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece4
                    
                    
                    print("Changed it To New One : Player1Piece4")
                    print(rolledDice)
                    print(p1p4TryToWin)
                    print(num)
                    
                    p1p4TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                }
                
            } else if (whosTurn == .Player1 && ((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 < 17) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece2 < 17) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 < 17) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 < 17))) {
                
                let sides = [1, 4, 2, 2, 3, 3, 4, 3, 8, 2, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("Got Simple Roll For Last Player : \(rolledDice)")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player1) {
                
                let sides = [1, 3, 2, 8, 3, 2, 4, 2, 3, 2, 3, 4, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("++++++++++Got Simple Roll Player-1 : \(rolledDice)++++++++++")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player2) {
                
                let sides = [1, 3, 1, 8, 1, 1, 2, 2, 3, 4, 2, 2, 3, 4, 3, 3, 2, 3, 2]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("++++++++++Got Simple Roll Player-2 : \(rolledDice)++++++++++")
                Rolls.append(rolledDice)
                
            }
            
            
            if whosTurn == .Player1 {
                
                print("Player 1 Total ROLLS :")
                Player1Rolls.append(rolledDice)
                print(Player1Rolls.count)
                
            } else if whosTurn == .Player2 {
                
                print("Player 2 Total ROLLS :")
                Player2Rolls.append(rolledDice)
                print(Player2Rolls.count)
                
            }
            
            print("Player's Index :")
            print(player1rans)
            print(player2rans)
            
            
            j += 1
            print("Total Rans : ")
            print(totalRans)
            print(Rolls.count)
            print(rolledDice)
            
            if (totalRans > 2) {
                print("Inside three totla rrans : ")
                print(Rolls[totalRans])
                print(Rolls[totalRans - 1])
                print(Rolls[totalRans - 2])
                if (Rolls[totalRans] == Rolls[totalRans - 1] && Rolls[totalRans] == Rolls[totalRans - 2]) {
                    i += 1
                    print("Inside of It Happened Thrice :")
                    print(Rolls[totalRans])
                    print(Rolls[totalRans - 1])
                    print(Rolls[totalRans - 2])
                    
                } else  if (player1rans > 2) && (whosTurn == .Player1) {
                    
                    print("Player 1 Data :")
                    print(Player1Rolls.count)
                    print(player1rans)
                    print(Player1Rolls[player1rans])
                    print(Player1Rolls[player1rans - 1])
                    print(Player1Rolls[player1rans - 2])
                    
                    if (Player1Rolls[player1rans] == Player1Rolls[player1rans - 1] && Player1Rolls[player1rans] == Player1Rolls[player1rans - 2]) {
                        
                        print("Inside of It Happened Thrice FOR THE SAME PLAYER YA YA YA || Player 1 || :")
                        
                        print(Player1Rolls[0])
                        print(Player1Rolls[1])
                        print(Player1Rolls[2])
                        print(Player1Rolls[player1rans])
                        print(Player1Rolls[player1rans - 1])
                        print(Player1Rolls[player1rans - 2])
                        
                        i += 1
                        
                    } else if ((Player1Rolls[player1rans] == 8 || Player1Rolls[player1rans] == 4) && (Player1Rolls[player1rans - 1] == 8 || Player1Rolls[player1rans - 1] == 4) && (Player1Rolls[player1rans - 2] == 8 || Player1Rolls[player1rans - 2] == 4)) {
                        
                        print("What will happen will it just skip the turn or do cool animation like Going Back :")
                        
                        print("Add Label in Middle from smaller to becaming larger and says Skipped Turn (maybe write for 3's)")
                        
                        print("ANSWER : Moving it back to three times rolled dice will make him lots of troubles and will have to move all again and his strategy will waste too , But just skipping his turn and let him be there and skipping 3rd try will not be too harsh and will also do the job PerfeCt.")
                        
                        print("Must ADD Last piece of human player will get more chances of 4 so won't have to wait and just for last piece player will feel it good and fast rather then always wondering for to get inside area. can very fastly get inside and then try to win.")
                        
                        // movesRemaining = 0
                        // OneMoreMove = 0
                        //  movePiece()
                        rolledDice = 0
                        Player1Rolls.append(rolledDice)
                        player1rans += 1
                    }
                    
                    
                } else if (player2rans > 2) && (whosTurn == .Player2) {
                    
                    if (Player2Rolls[player2rans] == Player2Rolls[player2rans - 1] && Player2Rolls[player2rans] == Player2Rolls[player2rans - 2]) {
                        
                        print("Player 2 Data :")
                        print(Player2Rolls.count)
                        print(player2rans)
                        print(Player2Rolls[player2rans])
                        print(Player2Rolls[player2rans - 1])
                        print(Player2Rolls[player2rans - 2])
                        
                        print("Inside of It Happened Thrice FOR THE SAME PLAYER YA YA YA || Player 2 || :")
                        print(Player2Rolls[player2rans])
                        print(Player2Rolls[player2rans - 1])
                        print(Player2Rolls[player2rans - 2])
                        
                        i += 1
                        
                    } else if ((Player2Rolls[player2rans] == 8 || Player2Rolls[player2rans] == 4) && (Player2Rolls[player2rans - 1] == 8 || Player2Rolls[player2rans - 1] == 4) && (Player2Rolls[player2rans - 2] == 8 || Player2Rolls[player2rans - 2] == 4)) {
                        
                        print("What will happen will it just skip the turn or do cool animation like Going Back :")
                        
                        // movesRemaining = 0
                        //OneMoreMove = 0
                        // movePiece()
                        rolledDice = 0
                        Player2Rolls.append(rolledDice)
                        player2rans += 1
                    }
                    
                }
                
                
                
            }
            
            if whosTurn == .Player1 {
                
                player1rans += 1
                
            } else if whosTurn == .Player2 {
                
                player2rans += 1
                
            }
            
            print("The total rolls after that.")
            
            print(player1rans)
            print(player2rans)
            
            
            totalRans += 1
            
        }
        
        
        
        
        
        // let sides = [1, 1, 2, 2, 3, 3, 4, 4, 4]
        // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
        // rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
        // let dostexture = SKTexture(imageNamed: "Dice\(rolledDice)")
        //  diceTextures.append(dostexture)
        
        // rolledDice = dice3d6.nextInt()
        
        
        
        // let imageName =
        //  let diceTexture = SKTexture(imageNamed: "Dice\(rolledDice)")
        //  diceTextures.append(diceTexture)
        
        
        print("Rolled Dice :")
        print(rolledDice)
        print(whosTurn)
        
        let dice = SKSpriteNode(imageNamed: "Dice1")
        dice.name = "dice"
        
        if player == .Player1 {
            
            dice.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
            dice.size = CGSize(width: 150.0, height: 150.0)
            
            rolledDiceTexture.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
            rolledDiceTexture.size = CGSize(width: 150.0, height: 150.0)
            
        } else if player == .Player2 {
            
            dice.position = CGPoint(x: (scene?.size.width)! * -0.395, y: 0)
            dice.size = CGSize(width: 150.0, height: 150.0)
            
            rolledDiceTexture.position = CGPoint(x: (scene?.size.width)! * -0.395, y: 0)
            rolledDiceTexture.size = CGSize(width: 150.0, height: 150.0)
            
        }
        
        addChild(dice)
        
        
        let diceAnimation = SKAction.animate(with: diceTextures, timePerFrame: 0.05, resize: true, restore: true)
        
        dice.run(diceAnimation) {
            
            
            self.enumerateChildNodes(withName: "dice") { (diceNode, stop) in
                diceNode.removeFromParent()
                print(self.childNode(withName: "dice") ?? "dice IS REMOVED//")
            }
            
            self.rolledDiceTexture.isHidden = false
            
            
            if self.rolledDice == 4 || self.rolledDice == 8 {
                self.OneMoreMove += 1
                
                self.playSound(soundName: self.fourAndEight)
                
                if (self.whosTurn == .Player1) {
                    
                    // self.updateScore() //self.OneMoreMove
                    
                    //  self.oneMoreMovePla.isHidden = false
                    
                    //    let delay = 1.5
                    //    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.updateScore()
                    
                    //   print("Yes!")
                    //  self.oneMoreMovePla.isHidden = true
                    if self.oneMoreMove.isHidden {
                        
                        self.notifyForPlusMove(label: self.oneMoreMove)
                        
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                    //                            self.oneMoreMovePla.setScale(1)
                    
                    // }
                    
                } else if (self.whosTurn == .Player2) {
                    
                    //  self.oneMoreMoveCom1.isHidden = false
                    
                    //  let delay = 1.5
                    //  DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.updateScore()
                    if self.oneMoreMove.isHidden {
                        
                        self.notifyForPlusMove(label: self.oneMoreMove)
                        
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                    
                    //        print("Yes!")
                    //        self.oneMoreMoveCom1.isHidden = true
                    //                            self.oneMoreMoveCom1.setScale(1)
                    
                    //  }
                    
                }
                
            }
            
            self.diceRolled = true
            self.rolling = false
            print("I'm Inside:")
            print(self.whosTurn)
            
            self.HowtoMove(player: player, dice: dice)
            
            print("-> Decreased Player Scores :")
            print(self.playerScore)
            print(self.com1Score)
            print(self.player1Piece1Score)
            print(self.player1Piece2Score)
            print(self.player1Piece3Score)
            print(self.player1Piece4Score)
            print(self.com1Piece1Score)
            print(self.com1Piece2Score)
            print(self.com1Piece3Score)
            print(self.com1Piece4Score)
            
        }
    }
    
    
    
    
    func playSound(soundName: SKAction) {
        
        if UserDefaults.standard.bool(forKey: "audio") {
            
            run(soundName)
            
        }
    }
    
    
    
    func animateLarge(one:SKSpriteNode) {
        
        one.run(SKAction.scale(to: 1.5, duration: 0.5))
        //  print("In Ani")
    }
    
    func animateSmall(one:SKSpriteNode) {
        
        one.run(SKAction.scale(to: 1, duration: 0.4))
        //  print("In Ani 2")
    }
    
    
    
    
    func animations() {
        
        
        if rolling {
            return
        }
        
        Arrow1Red.isHidden = false
        Arrow2Red.isHidden = false
        Arrow3Red.isHidden = false
        Arrow4Red.isHidden = false
        
        let action1 = SKAction.run {
            // print("In Action1")
            self.animateLarge(one: self.Arrow1Red)
        }
        
        let action2 = SKAction.run {
            //   print("In Action2")
            self.animateLarge(one: self.Arrow2Red)
        }
        
        let action3 = SKAction.run {
            //  print("In Actio3")
            self.animateLarge(one: self.Arrow3Red)
        }
        
        let action4 = SKAction.run {
            //  print("In Action4")
            self.animateLarge(one: self.Arrow4Red)
        }
        
        
        
        let acta1 = SKAction.run {
            self.animateSmall(one: self.Arrow1Red)
        }
        
        let acta2 = SKAction.run {
            self.animateSmall(one: self.Arrow2Red)
        }
        
        let acta3 = SKAction.run {
            self.animateSmall(one: self.Arrow3Red)
        }
        
        let acta4 = SKAction.run {
            self.animateSmall(one: self.Arrow4Red)
        }
        
        
        
        
        
        
        
        run(.sequence([
            
            
            SKAction.wait(forDuration: 0.4),
            SKAction.run({
                
                self.run(action4, completion: {
                    
                    
                    
                    self.run(.sequence([
                        
                        
                        SKAction.wait(forDuration: 0.2),
                        SKAction.run({
                            
                            self.run(action3, completion: {
                                
                                
                                
                                self.run(.sequence([
                                    
                                    
                                    SKAction.wait(forDuration: 0.2),
                                    SKAction.run({
                                        
                                        self.run(action2, completion: {
                                            
                                            
                                            
                                            self.run(.sequence([
                                                
                                                
                                                SKAction.wait(forDuration: 0.2),
                                                SKAction.run({
                                                    
                                                    self.run(action1, completion: {
                                                        
                                                        
                                                        
                                                        //   print("TO THE DOWN NOW!")
                                                        
                                                        self.run(.sequence([
                                                            
                                                            
                                                            SKAction.wait(forDuration: 0.4),
                                                            SKAction.run({
                                                                
                                                                self.run(acta4, completion: {
                                                                    
                                                                    
                                                                    
                                                                    self.run(.sequence([
                                                                        
                                                                        
                                                                        SKAction.wait(forDuration: 0.2),
                                                                        SKAction.run({
                                                                            
                                                                            self.run(acta3, completion: {
                                                                                
                                                                                
                                                                                
                                                                                self.run(.sequence([
                                                                                    
                                                                                    
                                                                                    SKAction.wait(forDuration: 0.2),
                                                                                    SKAction.run({
                                                                                        
                                                                                        self.run(acta2, completion: {
                                                                                            
                                                                                            
                                                                                            
                                                                                            self.run(.sequence([
                                                                                                
                                                                                                
                                                                                                SKAction.wait(forDuration: 0.2),
                                                                                                SKAction.run({
                                                                                                    
                                                                                                    self.run(acta1, completion: {
                                                                                                        
                                                                                                        
                                                                                                        
                                                                                                        //   print("REPEAT!")
                                                                                                        
                                                                                                        if self.keepAnimating {
                                                                                                            
                                                                                                            self.animations()
                                                                                                        } else {
                                                                                                            return
                                                                                                        }
                                                                                                        
                                                                                                        
                                                                                                    })
                                                                                                    
                                                                                                    
                                                                                                })
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                ]))
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        })
                                                                                        
                                                                                        
                                                                                    })
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    ]))
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            })
                                                                            
                                                                            
                                                                        })
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        ]))
                                                                    
                                                                    
                                                                    
                                                                    
                                                                })
                                                                
                                                                
                                                            })
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            ]))
                                                        
                                                        
                                                    })
                                                    
                                                    
                                                })
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                ]))
                                            
                                            
                                            
                                        })
                                        
                                        
                                    })
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    ]))
                                
                                
                                
                                
                            })
                            
                            
                        })
                        
                        
                        
                        
                        
                        
                        
                        ]))
                    
                    
                    
                })
                
                
            })
            
            
            
            
            
            
            
            ]))
        
    }
    
    
    
    
    
    func notifyForTurnBurn(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0 - 20
        let secondY: CGFloat = 0
        let thirdY: CGFloat = 0 + 20
        let fourthY = -middleLocation - 50
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
    }
    
    
    
    func notifyForCantMove(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0 - 20
        let secondY: CGFloat = 0
        let thirdY: CGFloat = 0 + 20
        let fourthY = -middleLocation - 50
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
    }
    
    
    
    func notifyForPlusMove2(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0
        let secondY: CGFloat = 0 + 20
        let thirdY: CGFloat = 0 + 40
        let fourthY = -middleLocation - 50
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
    }
    
    
    
    
    
    func notifyForPlusMove(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0 - 20
        let secondY: CGFloat = 0
        let thirdY: CGFloat = 0 + 20
        let fourthY = -middleLocation - 50
        //  let firstY =
        
        //        if (!oneMoreMove.isHidden || !cantMovePla.isHidden) || (!threeLargeNumsinrowPla.isHidden || !cantMovePla.isHidden) || (!oneMoreMove.isHidden || !threeLargeNumsinrowPla.isHidden) {
        //
        //            firstY = 0 - 50
        //            secondY = 0 - 30
        //            thirdY = 0 - 10
        //
        //
        //        } else if !cantMovePla.isHidden || !threeLargeNumsinrowPla.isHidden || !oneMoreMove.isHidden {
        //
        //            firstY = 0
        //            secondY = 0 + 20
        //            thirdY = 0 + 40
        //
        //        }
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
        
        //        let middleLocation:CGFloat = (scene?.size.width)! / 2
        //
        //        let firstMove = SKAction.moveTo(x: 0 - 40, duration: 0.6)
        //
        //        let secondMove = SKAction.moveTo(x: 0, duration: 0.3)
        //
        //        let wait = SKAction.wait(forDuration: 0.8)
        //
        //        let thirdMove = SKAction.moveTo(x: 0 + 40, duration: 0.3)
        //
        //        let fourthMove = SKAction.moveTo(x: -middleLocation - 50, duration: 0.6)
        //
        //        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
        //            label.position = CGPoint(x: ((self.scene?.size.width)! / 2) + 50, y: 0)
        //            label.isHidden = true
        //        }
        
        
        
    }
    
    
    
    
    
    func blink(player: Player) {
        
        var eyes = [SKSpriteNode]()
        //        var inc: Int = 1
        
        
        
        
        let firstFlip = SKAction.scaleY(to: 0.0, duration: 0.2)
        firstFlip.timingMode = .linear
        let secondFlip = SKAction.scaleY(to: 1.0, duration: 0.2)
        secondFlip.timingMode = .linear
        
        
        
        
        
        let wait = SKAction.wait(forDuration: 0.3)
        
        let wait2 = SKAction.wait(forDuration: 1.3)
        
        
        print("In SIde THe BliNKa :")
        
        if player == .Player1 {
            
            if piece1AbletoEnter && currentSpacePlayer1Piece1 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece1 == 16) {
                eyes.append(player1Piece1eye1)
                
                eyes.append(player1Piece1eye2)
            }
            if piece2AbletoEnter && currentSpacePlayer1Piece2 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece2 == 16) {
                eyes.append(player1Piece2eye1)
                eyes.append(player1Piece2eye2)
            }
            if piece3AbletoEnter && currentSpacePlayer1Piece3 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece3 == 16) {
                eyes.append(player1Piece3eye1)
                eyes.append(player1Piece3eye2)
            }
            if piece4AbletoEnter && currentSpacePlayer1Piece4 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece4 == 16) {
                eyes.append(player1Piece4eye1)
                eyes.append(player1Piece4eye2)
            }
            
        } else if player == .Player2 {
            
            if currentSpacePlayer2Piece1 != 26 {
                eyes.append(player2Piece1eye1)
                eyes.append(player2Piece1eye2)
            }
            if currentSpacePlayer2Piece2 != 26 {
                eyes.append(player2Piece2eye1)
                eyes.append(player2Piece2eye2)
            }
            if currentSpacePlayer2Piece3 != 26 {
                eyes.append(player2Piece3eye1)
                eyes.append(player2Piece3eye2)
            }
            if currentSpacePlayer2Piece4 != 26 {
                eyes.append(player2Piece4eye1)
                eyes.append(player2Piece4eye2)
            }
            
        }
        
        
        
        
        for eye in eyes {
            
            print(eye)
            
            eye.setScale(1)
            
            let sara = SKAction.run {
                
                eye.texture = self.openEye
                eye.size = CGSize(width: 8, height: 8)
                eye.yScale = 0
                eye.xScale = 1
                eye.run(secondFlip)
                
            }
            
            
            let kara = SKAction.run {
                
                eye.texture = self.closeEye
                eye.setScale(1)
                eye.size = CGSize(width: 8, height: 1)
                
            }
            
            let vel = SKAction.sequence([firstFlip, kara, wait, sara, wait2])
            
            
            
            eye.run(SKAction.repeatForever(vel)) {
                print("How's It?")
            }
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    func setupLabels() {
        
        
        oneMoreMove = SKLabelNode(fontNamed: "Courier")
        oneMoreMove.fontSize = 120
        oneMoreMove.fontColor = UIColor.darkGray
        oneMoreMove.text = "+1 Turn"
        oneMoreMove.zPosition = 15
        oneMoreMove.position = CGPoint(x: 0, y: ((scene?.size.width)! / 2) + 50)
        oneMoreMove.isHidden = true
        addChild(oneMoreMove)
        
        
        cantMovePla = SKLabelNode(fontNamed: "Courier")
        cantMovePla.fontSize = 70
        cantMovePla.fontColor = UIColor.darkGray
        cantMovePla.text = "Can't Move"
        cantMovePla.zPosition = 15
        cantMovePla.position = CGPoint(x: 0, y: ((scene?.size.width)! / 2) + 50)
        cantMovePla.isHidden = true
        addChild(cantMovePla)
        
        
        threeLargeNumsinrowPla = SKLabelNode(fontNamed: "Courier")
        threeLargeNumsinrowPla.fontSize = 60
        threeLargeNumsinrowPla.fontColor = UIColor.darkGray
        threeLargeNumsinrowPla.text = "Third Turn Burned!"
        threeLargeNumsinrowPla.zPosition = 15
        threeLargeNumsinrowPla.position = CGPoint(x: 0, y: ((scene?.size.width)! / 2) + 50)
        threeLargeNumsinrowPla.isHidden = true
        addChild(threeLargeNumsinrowPla)
        
        
        playerLabel = SKLabelNode(fontNamed: "Courier")
        playerLabel.fontSize = 28
        playerLabel.fontColor = textColore
        playerLabel.text = "You"
        playerLabel.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * -0.493)
        playerLabel.zPosition = 1
        addChild(playerLabel)
        
        
        
        computer1Label = SKLabelNode(fontNamed: "Courier")
        computer1Label.fontSize = 28
        computer1Label.fontColor = textColore
        computer1Label.text = "Computer"
        computer1Label.position = CGPoint(x: (scene?.size.width)! * -0.40, y: (scene?.size.height)! * 0.455)
        computer1Label.zPosition = 1
        addChild(computer1Label)
        
        
        
        
        
        
        plaTTtext = SKLabelNode(fontNamed: "Courier")
        plaTTtext.fontSize = 22
        plaTTtext.fontColor = textColore
        plaTTtext.text = "ToTal Turns:\(playerTturns)"
        plaTTtext.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * -0.355)
        plaTTtext.zPosition = 1
        addChild(plaTTtext)
        
        
        com1TTtext = SKLabelNode(fontNamed: "Courier")
        com1TTtext.fontSize = 22
        com1TTtext.fontColor = textColore
        com1TTtext.text = "ToTal Turns:\(computer1Tturns)"
        com1TTtext.position = CGPoint(x: (scene?.size.width)! * -0.399, y: (scene?.size.height)! * 0.355)
        com1TTtext.zPosition = 1
        addChild(com1TTtext)
        
        
        
        playerScoreLabel = SKLabelNode(fontNamed: "Courier")
        playerScoreLabel.fontSize = 45
        playerScoreLabel.fontColor = UIColor.black
        playerScoreLabel.text = "\(playerScore)"
        playerScoreLabel.horizontalAlignmentMode = .center
        playerScoreLabel.position = CGPoint(x: (scene?.size.width)! * 0.408, y: (scene?.size.height)! * -0.42)
        playerScoreLabel.zPosition = 2
        addChild(playerScoreLabel)
        
        com1ScoreLabel = SKLabelNode(fontNamed: "Courier")
        com1ScoreLabel.fontSize = 45
        com1ScoreLabel.fontColor = UIColor.black
        com1ScoreLabel.text = "\(com1Score)"
        com1ScoreLabel.horizontalAlignmentMode = .center
        com1ScoreLabel.position = CGPoint(x: (scene?.size.width)! * -0.401, y: (scene?.size.height)! * 0.39)
        com1ScoreLabel.zPosition = 2
        addChild(com1ScoreLabel)
        
    }
    
    
    
    func DincreamentScore() {
        
        print("In Dincreament :")
        
        if (whosTurn == .Player1) {
            
            print("In Dincreament Player 1 :")
            
            print(playerTturns)
            playerTturns -= 1
            print(playerTturns)
            plaTTtext.text = "ToTal Turns:\(playerTturns)"
            
        } else if (whosTurn == .Player2) {
            
            print("In Dincreament Player 2 :")
            
            print(computer1Tturns)
            computer1Tturns -= 1
            print(computer1Tturns)
            com1TTtext.text = "ToTal Turns:\(computer1Tturns)"
            
        }
        
    }
    
    
    
    
    func DincreamentPlayerScore(player: Player, killedPiece: SKSpriteNode) {
        
        print("-> Inside Dincriment Player Score !!!")
        
        if killedPiece == Player1Piece1 {
            
            player1Piece1Score = 1
            
            print("Decreased Player 1 Piece 1 : \(player1Piece1Score)")
            
        } else if killedPiece == Player1Piece2 {
            
            player1Piece2Score = 1
            
            print("Decreased Player 1 Piece 2 : \(player1Piece2Score)")
            
        } else if killedPiece == Player1Piece3 {
            
            player1Piece3Score = 1
            
            print("Decreased Player 1 Piece 3 : \(player1Piece3Score)")
            
        } else if killedPiece == Player1Piece4 {
            
            player1Piece4Score = 1
            
            print("Decreased Player 1 Piece 4 : \(player1Piece4Score)")
            
        }
        
        
        
        
        if killedPiece == Player2Piece1 {
            
            com1Piece1Score = 1
            
            print("Decreased Player 2 Piece 1 : \(com1Piece1Score)")
            
        } else if killedPiece == Player2Piece2 {
            
            com1Piece2Score = 1
            
            print("Decreased Player 2 Piece 2 : \(com1Piece2Score)")
            
        } else if killedPiece == Player2Piece3 {
            
            com1Piece3Score = 1
            
            print("Decreased Player 2 Piece 3 : \(com1Piece3Score)")
            
        } else if killedPiece == Player2Piece4 {
            
            com1Piece4Score = 1
            
            print("Decreased Player 2 Piece 4 : \(com1Piece4Score)")
            
        }
        
        
        
        
        
        
        playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
        com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
        
        print("-> Decreased Final Player :")
        print(playerScore)
        print(com1Score)
        
        
        playerScoreLabel.text = "\(playerScore)"
        com1ScoreLabel.text = "\(com1Score)"
        
    }
    
    
    
    
    
    func IncreasePlayerScore(player: Player, playerpiece: SKSpriteNode) {
        
        
        print("-> Decreased Player Scores :")
        print(playerScore)
        print(com1Score)
        print(player1Piece1Score)
        print(player1Piece2Score)
        print(player1Piece3Score)
        print(player1Piece4Score)
        print(com1Piece1Score)
        print(com1Piece2Score)
        print(com1Piece3Score)
        print(com1Piece4Score)
        
        if (player == .Player1 && !(((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26)) && (dontScoreP1)) ) {
            
            if playerpiece == Player1Piece1 {
                
                player1Piece1Score += 1
                
            } else if playerpiece == Player1Piece2 {
                
                player1Piece2Score += 1
                
            } else if playerpiece == Player1Piece3 {
                
                player1Piece3Score += 1
                
            } else if playerpiece == Player1Piece4 {
                
                player1Piece4Score += 1
                
            }
            
            playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
            
            playerScoreLabel.text = "\(playerScore)"
            
        } else if (player == .Player2 && !(((currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26)) && (dontScoreP2)) ) {
            
            if playerpiece == Player2Piece1 {
                
                com1Piece1Score += 1
                
            } else if playerpiece == Player2Piece2 {
                
                com1Piece2Score += 1
                
            } else if playerpiece == Player2Piece3 {
                
                com1Piece3Score += 1
                
            } else if playerpiece == Player2Piece4 {
                
                com1Piece4Score += 1
                
            }
            
            com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
            
            com1ScoreLabel.text = "\(com1Score)"
            
        }
        
        
        print("-> Decreased Player Scores :")
        print(playerScore)
        print(com1Score)
        print(player1Piece1Score)
        print(player1Piece2Score)
        print(player1Piece3Score)
        print(player1Piece4Score)
        print(com1Piece1Score)
        print(com1Piece2Score)
        print(com1Piece3Score)
        print(com1Piece4Score)
        
        
    }
    
    
    
    
    
    func updateScore() {
        
        print("In Update :")
        
        if (whosTurn == .Player1) {
            
            print("In Update Player 1 :")
            print(playerTturns)
            playerTturns += 1
            print(playerTturns)
            plaTTtext.text = "ToTal Turns:\(playerTturns)"
            
        } else if (whosTurn == .Player2) {
            
            print("In Update Player 2 :")
            print(computer1Tturns)
            computer1Tturns += 1
            print(computer1Tturns)
            com1TTtext.text = "ToTal Turns:\(computer1Tturns)"
            
        }
    }
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    func removeFlashTurn(player:Player) {
        
        
        if (player == .Player1) {
            
            
            playerLabel.removeAllActions()
            playerLabel.alpha = 1
            playerLabel.setScale(1)
            //  playerLabel.run(SKAction.init(named: "Pulse")!)
            
        } else if (player == .Player2) {
            
            computer1Label.removeAllActions()
            computer1Label.alpha = 1
            computer1Label.setScale(1)
            //  computer1Label.run(SKAction.init(named: "Pulse")!)
            
        }
        
        
    }
    
    
    
    
    func removeFlashPlayer(player:Player) {
        
        if (player == .Player1) {
            
            
            player1Piece1eye1.removeAllActions()
            player1Piece1eye2.removeAllActions()
            
            player1Piece1eye1.texture = self.openEye
            player1Piece1eye1.size = CGSize(width: 8, height: 8)
            player1Piece1eye1.yScale = 1
            player1Piece1eye1.xScale = 1
            
            player1Piece1eye2.texture = self.openEye
            player1Piece1eye2.size = CGSize(width: 8, height: 8)
            player1Piece1eye2.yScale = 1
            player1Piece1eye2.xScale = 1
            
            player1Piece2eye1.removeAllActions()
            player1Piece2eye2.removeAllActions()
            
            player1Piece2eye1.texture = self.openEye
            player1Piece2eye1.size = CGSize(width: 8, height: 8)
            player1Piece2eye1.yScale = 1
            player1Piece2eye1.xScale = 1
            
            player1Piece2eye2.texture = self.openEye
            player1Piece2eye2.size = CGSize(width: 8, height: 8)
            player1Piece2eye2.yScale = 1
            player1Piece2eye2.xScale = 1
            
            player1Piece3eye1.removeAllActions()
            player1Piece3eye2.removeAllActions()
            
            player1Piece3eye1.texture = self.openEye
            player1Piece3eye1.size = CGSize(width: 8, height: 8)
            player1Piece3eye1.yScale = 1
            player1Piece3eye1.xScale = 1
            
            player1Piece3eye2.texture = self.openEye
            player1Piece3eye2.size = CGSize(width: 8, height: 8)
            player1Piece3eye2.yScale = 1
            player1Piece3eye2.xScale = 1
            
            player1Piece4eye1.removeAllActions()
            player1Piece4eye2.removeAllActions()
            
            player1Piece4eye1.texture = self.openEye
            player1Piece4eye1.size = CGSize(width: 8, height: 8)
            player1Piece4eye1.yScale = 1
            player1Piece4eye1.xScale = 1
            
            player1Piece4eye2.texture = self.openEye
            player1Piece4eye2.size = CGSize(width: 8, height: 8)
            player1Piece4eye2.yScale = 1
            player1Piece4eye2.xScale = 1
            
        } else if (player == .Player2) {
            
            player2Piece1eye1.removeAllActions()
            player2Piece1eye2.removeAllActions()
            
            player2Piece1eye1.texture = self.openEye
            player2Piece1eye1.size = CGSize(width: 8, height: 8)
            player2Piece1eye1.yScale = 1
            player2Piece1eye1.xScale = 1
            
            player2Piece1eye2.texture = self.openEye
            player2Piece1eye2.size = CGSize(width: 8, height: 8)
            player2Piece1eye2.yScale = 1
            player2Piece1eye2.xScale = 1
            
            player2Piece2eye1.removeAllActions()
            player2Piece2eye2.removeAllActions()
            
            player2Piece2eye1.texture = self.openEye
            player2Piece2eye1.size = CGSize(width: 8, height: 8)
            player2Piece2eye1.yScale = 1
            player2Piece2eye1.xScale = 1
            
            player2Piece2eye2.texture = self.openEye
            player2Piece2eye2.size = CGSize(width: 8, height: 8)
            player2Piece2eye2.yScale = 1
            player2Piece2eye2.xScale = 1
            
            player2Piece3eye1.removeAllActions()
            player2Piece3eye2.removeAllActions()
            
            player2Piece3eye1.texture = self.openEye
            player2Piece3eye1.size = CGSize(width: 8, height: 8)
            player2Piece3eye1.yScale = 1
            player2Piece3eye1.xScale = 1
            
            player2Piece3eye2.texture = self.openEye
            player2Piece3eye2.size = CGSize(width: 8, height: 8)
            player2Piece3eye2.yScale = 1
            player2Piece3eye2.xScale = 1
            
            player2Piece4eye1.removeAllActions()
            player2Piece4eye2.removeAllActions()
            
            player2Piece4eye1.texture = self.openEye
            player2Piece4eye1.size = CGSize(width: 8, height: 8)
            player2Piece4eye1.yScale = 1
            player2Piece4eye1.xScale = 1
            
            player2Piece4eye2.texture = self.openEye
            player2Piece4eye2.size = CGSize(width: 8, height: 8)
            player2Piece4eye2.yScale = 1
            player2Piece4eye2.xScale = 1
            
        }
        
    }
    
    
    
    func removeAnimation(player:Player) {
        
        Arrow1Red.isHidden = true
        Arrow2Red.isHidden = true
        Arrow3Red.isHidden = true
        Arrow4Red.isHidden = true
        
        Arrow1Red.removeAllActions()
        Arrow1Red.setScale(1)
        
        Arrow2Red.removeAllActions()
        Arrow2Red.setScale(1)
        
        Arrow3Red.removeAllActions()
        Arrow3Red.setScale(1)
        
        Arrow4Red.removeAllActions()
        Arrow4Red.setScale(1)
        
        keepAnimating = false
    }
    
    
    
    
    
    func flashTurn(player:Player) {
        
        if (player == .Player1) {
            
            playerLabel.run(SKAction.init(named: "Pulse")!)
            
        } else if (player == .Player2) {
            
            computer1Label.run(SKAction.init(named: "Pulse")!)
            
        }
    }

    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        print("Inside Touches :")
        
        
        print("Who's Turn :")
        print(whosTurn)
        for touch in touches {
            print("Inside For LOOP")
            let location1 = touch.location(in: self)
            let node = atPoint(location1)
            
                print(node.name ?? "hi")
                if (node.name == "Dice" && whosTurn == .Player1 && moveFinished) {
                    
                    print("Here Inside!")
                    removeFlashTurn(player: whosTurn)
                    rollDice(player: whosTurn)
                    
                    
                }
            
            if (node.name == "blackshadow") {
                
                PauseMenu.removeAllActions()
                blackShadow.isHidden = true
                PauseMenu.run(SKAction.init(named: "Disapear")!)
                home.run(SKAction.init(named: "Disapear")!)
                cancel.run(SKAction.init(named: "Disapear")!)
                audioButton.run(SKAction.init(named: "Disapear")!)
          //      howToPlay.run(SKAction.init(named: "Disapear")!)
                
            }
            
        }
        
        
        
        
        if diceRolled == true {
            
            if whosTurn == .Player1 && !rolling /*&& !gameOver*/ {
                
                for touch in touches {
                    
                    print("Inside For in Touches :")
                    
                    let location = touch.location(in: self)
                    print(location)
                    
                    let node = atPoint(location)
                    
                    
                    print(node)
                    
                    
                    print("The Touched Node IS :")
                    print(node)
                    
                    
                    if ((node.name == "Player1Piece1" || node.name == "player1Piece1eye1" || node.name == "player1Piece1eye2") && piece1AbletoEnter == true && currentSpacePlayer1Piece1 != 26) {
                        
                        print("In Node in Nodes In Touches and it's :")
                        print(node.name!)
                        
                        touchedNode = Player1Piece1
                        
                        print("Rolling of Dice :")
                        print(rolling)
                        print(moveFinished)
                        
                        movesRemaining = rolledDice
                        
                        removeFlashPlayer(player: whosTurn)
                        
                        movePiece()
                        
                    } else if ((node.name == "Player1Piece2" || node.name == "player1Piece2eye1" || node.name == "player1Piece2eye2") && piece2AbletoEnter == true && currentSpacePlayer1Piece2 != 26) {
                        
                        print("In Node in Nodes In Touches and it's :")
                        print(node.name!)
                        
                        touchedNode = Player1Piece2
                        
                        print("Rolling of Dice :")
                        print(rolling)
                        print(moveFinished)
                        
                        movesRemaining = rolledDice
                        
                        removeFlashPlayer(player: whosTurn)
                        
                        movePiece()
                        
                    } else if ((node.name == "Player1Piece3" || node.name == "player1Piece3eye1" || node.name == "player1Piece3eye2") && piece3AbletoEnter == true && currentSpacePlayer1Piece3 != 26) {
                        
                        print("In Node in Nodes In Touches and it's :")
                        print(node.name!)
                        
                        touchedNode = Player1Piece3
                        
                        print("Rolling of Dice :")
                        print(rolling)
                        print(moveFinished)
                        
                        movesRemaining = rolledDice
                        
                        removeFlashPlayer(player: whosTurn)
                        
                        movePiece()
                        
                    } else if ((node.name == "Player1Piece4" || node.name == "player1Piece4eye1" || node.name == "player1Piece4eye2") && piece4AbletoEnter == true && currentSpacePlayer1Piece4 != 26) {
                        
                        print("In Node in Nodes In Touches and it's :")
                        print(node.name!)
                        
                        touchedNode = Player1Piece4
                        
                        print("Rolling of Dice :")
                        print(rolling)
                        print(moveFinished)
                        
                        movesRemaining = rolledDice
                        
                        removeFlashPlayer(player: whosTurn)
                        
                        movePiece()
                        
                    }
                    
                    
                }
                
            }
        }
        
        
        
    }
    
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        
    }
    
    
    
    //MARK: - ButtonFeelDelegate
    
    
    func spriteNodeButtonPressed(_ button: NewSpriteNodeButton) {
        
        
        switch button {
        case backButton:
            
            playSound(soundName: buttonSound)
            PauseMenu.removeAllActions()
            blackShadow.isHidden = false
            PauseMenu.run(SKAction.init(named: "ComeOut")!)
            home.run(SKAction.init(named: "ComeOut")!)
            cancel.run(SKAction.init(named: "ComeOut")!)
            audioButton.run(SKAction.init(named: "ComeOut")!)
            
            
        case cancel:
            
            playSound(soundName: buttonSound)
            PauseMenu.removeAllActions()
            blackShadow.isHidden = true
            PauseMenu.run(SKAction.init(named: "Disapear")!)
            home.run(SKAction.init(named: "Disapear")!)
            cancel.run(SKAction.init(named: "Disapear")!)
            audioButton.run(SKAction.init(named: "Disapear")!)
            
        case home:
            
            playSound(soundName: buttonSound)
            
            
            if place.count > 0 {
                playerOne = place[0]
            }
            if place.count > 1 {
                playerTwo = place[1]
            }
            
            if playerOne.isEmpty && playerTwo.isEmpty {
                
                playerOne = "You"
                playerTwo = "Computer"
                print("In Last CON!")
                
            }
            
            GlobalVariablesPad.PreviousScene = .VsComTwoPad
            
            let gamescene = SKScene(fileNamed: "EndScreenPad")
            gamescene?.scaleMode = .aspectFit
            self.view?.presentScene(gamescene!, transition: SKTransition.fade(withDuration: 1))
            
        case audioButton:
            
            if GlobalVariablesPad.audio {
                playSound(soundName: buttonSound)
                GlobalVariablesPad.audio = false
                defaults.set(GlobalVariablesPad.audio, forKey: "audio")
                defaults.synchronize()
            } else {
                GlobalVariablesPad.audio = true
                defaults.set(GlobalVariablesPad.audio, forKey: "audio")
                defaults.synchronize()
                playSound(soundName: buttonSound)
            }
            
            
            
        default:
            break
        }
        
        
    }
    
    
    deinit {
        
        self.removeAllChildren()
        self.removeAllActions()
        print("Everything Got Deinitialyed ! VsCom2Pad")
    }
    
}

