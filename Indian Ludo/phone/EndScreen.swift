//
//  EndScreen.swift
//  Indian Ludo
//
//  Created by ramesh sanghar on 08/06/19.
//  Copyright © 2019 Om. All rights reserved.
//

import Foundation
import SpriteKit


class EndScreen: SKScene, NewSpriteNodeButtonDelegate {
    
    
    
    let defaults = UserDefaults.standard
    
    let buttonSound = SKAction.playSoundFileNamed("multimedia_button_click_029.mp3", waitForCompletion: false)
    
    
    var homeBut:NewSpriteNodeButton = NewSpriteNodeButton()
    var replayBut:NewSpriteNodeButton = NewSpriteNodeButton()
    var share:NewSpriteNodeButton = NewSpriteNodeButton()
    var audioButton:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var nameone:String = ""
    var nametwo:String = ""
    var namethree:String = ""
    var namefour:String = ""
    
    
    var first:SKLabelNode = SKLabelNode()
    var second:SKLabelNode = SKLabelNode()
    var third:SKLabelNode = SKLabelNode()
    var fourth:SKLabelNode = SKLabelNode()
    
    var one:SKLabelNode = SKLabelNode()
    var two:SKLabelNode = SKLabelNode()
    var three:SKLabelNode = SKLabelNode()
    var four:SKLabelNode = SKLabelNode()
    
    var firstPlace:SKLabelNode = SKLabelNode()
    var secondPlace:SKLabelNode = SKLabelNode()
    var thirdPlace:SKLabelNode = SKLabelNode()
    var fourthPlace:SKLabelNode = SKLabelNode()
    
    var posX:CGFloat = CGFloat()
    var firstLineY:CGFloat = CGFloat()
    var playerX:CGFloat = CGFloat()
    var secondLineY:CGFloat = CGFloat()
    var numX:CGFloat = CGFloat()
    var thirdLineY:CGFloat = CGFloat()
    var fourthLineY:CGFloat = CGFloat()
    
    
    var textColore:UIColor = UIColor()
    
    override func didMove(to view: SKView) {
        
        if defaults.value(forKey: "Purchase") == nil {
        
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideBannerAd"), object: nil)
        
        }
        
        scene?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        if GlobalVariablesPhone.backgroundColorName == 1 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            textColore = GlobalVariablesPhone.darkTextColor
            
        } else if GlobalVariablesPhone.backgroundColorName == 2 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
            textColore = GlobalVariablesPhone.whiteTextColor
            
        } else if GlobalVariablesPhone.backgroundColorName == 3 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
            textColore = GlobalVariablesPhone.whiteTextColor
            
        }
        
        posX = (self.scene?.size.width)! * 0.24
        playerX = -(self.scene?.size.width)! * 0.02
        numX = -(self.scene?.size.width)! * 0.28
        
        firstLineY = (self.scene?.size.height)! * 0.29
        secondLineY = (self.scene?.size.height)! * 0.14
        thirdLineY = (self.scene?.size.height)! * 0.00
        fourthLineY = -(self.scene?.size.height)! * 0.14
        
        
        firstPlace = SKLabelNode(fontNamed: "Courier")
        firstPlace.fontSize = 35
        firstPlace.fontColor = textColore
        firstPlace.text = "1st"
        firstPlace.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        firstPlace.position = CGPoint(x: posX, y: firstLineY)
        firstPlace.zPosition = 1
        addChild(firstPlace)
        
        secondPlace = SKLabelNode(fontNamed: "Courier")
        secondPlace.fontSize = 25
        secondPlace.fontColor = textColore
        secondPlace.text = "2nd"
        secondPlace.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        secondPlace.position = CGPoint(x: posX, y: secondLineY)
        secondPlace.zPosition = 1
        addChild(secondPlace)
        
        if GlobalVariablesPhone.PreviousScene == .GameScene || GlobalVariablesPhone.PreviousScene == .VsComThree || GlobalVariablesPhone.PreviousScene == .VsFriendFour || GlobalVariablesPhone.PreviousScene == .VsFriendThree {
            
            thirdPlace = SKLabelNode(fontNamed: "Courier")
            thirdPlace.fontSize = 20
            thirdPlace.fontColor = textColore
            thirdPlace.text = "3rd"
            thirdPlace.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            thirdPlace.position = CGPoint(x: posX, y: thirdLineY)
            thirdPlace.zPosition = 1
            addChild(thirdPlace)
            
            third = SKLabelNode(fontNamed: "Courier")
            third.fontSize = 20
            third.fontColor = textColore
            third.text = "\(playerThree)"
            third.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            third.position = CGPoint(x: playerX, y: thirdLineY)
            third.zPosition = 3
            addChild(third)
            
            three = SKLabelNode(fontNamed: "Courier")
            three.fontSize = 20
            three.fontColor = textColore
            three.text = "3."
            three.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            three.position = CGPoint(x: numX, y: thirdLineY)
            three.zPosition = 1
            addChild(three)
            
        }
        
        
        if GlobalVariablesPhone.PreviousScene == .GameScene || GlobalVariablesPhone.PreviousScene == .VsFriendFour {
            
            fourthPlace = SKLabelNode(fontNamed: "Courier")
            fourthPlace.fontSize = 15
            fourthPlace.fontColor = textColore
            fourthPlace.text = "Loser"
            fourthPlace.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            fourthPlace.position = CGPoint(x: posX, y: fourthLineY)
            fourthPlace.zPosition = 1
            addChild(fourthPlace)
            
            fourth = SKLabelNode(fontNamed: "Courier")
            fourth.fontSize = 15
            fourth.fontColor = textColore
            fourth.text = "\(playerFour)"
            fourth.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            fourth.position = CGPoint(x: playerX, y: fourthLineY)
            fourth.zPosition = 3
            addChild(fourth)
            
            
            four = SKLabelNode(fontNamed: "Courier")
            four.fontSize = 15
            four.fontColor = textColore
            four.text = "4."
            four.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            four.position = CGPoint(x: numX, y: fourthLineY)
            four.zPosition = 1
            addChild(four)
            
        }
        
        
        
        
        
        
        print("Heda :")
        print((scene?.size.width)! * -0.132, (scene?.size.height)! * -0.313)
        
        first = SKLabelNode(fontNamed: "Courier")
        first.fontSize = 35
        first.fontColor = textColore
        first.text = "\(playerOne)"
        first.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        first.position = CGPoint(x: playerX, y: firstLineY)
        first.zPosition = 1
        addChild(first)
        
        one = SKLabelNode(fontNamed: "Courier")
        one.fontSize = 35
        one.fontColor = textColore
        one.text = "1."
        one.position = CGPoint(x: numX, y: firstLineY)
        one.zPosition = 1
        addChild(one)
        
        second = SKLabelNode(fontNamed: "Courier")
        second.fontSize = 25
        second.fontColor = textColore
        second.text = "\(playerTwo)"
        second.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        second.position = CGPoint(x: playerX, y: secondLineY)
        second.zPosition = 1
        addChild(second)
        
        two = SKLabelNode(fontNamed: "Courier")
        two.fontSize = 25
        two.fontColor = textColore
        two.text = "2."
        two.position = CGPoint(x: numX, y: secondLineY)
        two.zPosition = 1
        addChild(two)
        
        
        
        
        homeBut = NewSpriteNodeButton(imageNamed: "Home")
        homeBut.name = "Home"
        homeBut.position = CGPoint(x: (scene?.size.width)! * -0.330, y: (scene?.size.height)! * -0.313)
        homeBut.zPosition = 1
        homeBut.size = CGSize(width: 56, height: 60)
        homeBut.isUserInteractionEnabled = true
        homeBut.delegate = self
        addChild(homeBut)
        
        replayBut = NewSpriteNodeButton(imageNamed: "Replay")
        replayBut.name = "Replay"
        replayBut.position = CGPoint(x: (scene?.size.width)! * -0.110, y: (scene?.size.height)! * -0.313)
        replayBut.zPosition = 1
        replayBut.size = CGSize(width: 56, height: 60)
        replayBut.isUserInteractionEnabled = true
        replayBut.delegate = self
        addChild(replayBut)
        
        
        share = NewSpriteNodeButton(imageNamed: "Share")
        share.name = "share"
        share.position = CGPoint(x: (scene?.size.width)! * 0.330, y: (scene?.size.height)! * -0.313)
        share.zPosition = 1
        share.size = CGSize(width: 56, height: 60)
        share.isUserInteractionEnabled = true
        share.delegate = self
        addChild(share)
        
        audioButton = NewSpriteNodeButton(imageNamed: "Audio")
        audioButton.name = "audioButton"
        audioButton.position = CGPoint(x: (scene?.size.width)! * 0.110, y: (scene?.size.height)! * -0.313)
        audioButton.zPosition = 1
        audioButton.size = CGSize(width: 56, height: 60)
        audioButton.isUserInteractionEnabled = true
        audioButton.delegate = self
        addChild(audioButton)
        
        
        
    }
    
    override func willMove(from view: SKView) {
        self.removeAllChildren()
        self.removeAllActions()
    }
    
    
    func playSound(soundName: SKAction) {
        if UserDefaults.standard.bool(forKey: "audio") {
            run(soundName)
        }
    }
    
    
    func share(scene: SKScene, text: String, image: UIImage?, excludeActivityTypes: [UIActivity]) {
        
        guard let image = image else {return}
        
        let shareItems = [text, image] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = scene.view
        
        
        scene.view?.window?.rootViewController?.present(activityViewController, animated: true, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        
        
        
    }
    
    
    //MARK: - ButtonFeelDelegate
    
    
    func spriteNodeButtonPressed(_ button: NewSpriteNodeButton) {
        
        
        switch button {
        case homeBut:
            
            playerOne = ""
            playerTwo = ""
            playerThree = ""
            playerFour = ""
            GlobalVariablesPhone.player1Text = ""
            GlobalVariablesPhone.player2Text = ""
            GlobalVariablesPhone.player3Text = ""
            GlobalVariablesPhone.player4Text = ""
            playSound(soundName: buttonSound)
            
            
            let homescene = HomeScene(size:(self.view?.bounds.size)!)
            homescene.scaleMode = .aspectFit
            
            self.view?.presentScene(homescene, transition: SKTransition.fade(withDuration: 1))
            
        case audioButton:
            
            if GlobalVariablesPhone.audio {
                playSound(soundName: buttonSound)
                GlobalVariablesPhone.audio = false
                defaults.set(GlobalVariablesPhone.audio, forKey: "audio")
                defaults.synchronize()
            } else {
                GlobalVariablesPhone.audio = true
                defaults.set(GlobalVariablesPhone.audio, forKey: "audio")
                defaults.synchronize()
                playSound(soundName: buttonSound)
            }
            
        case replayBut:
            
            switch GlobalVariablesPhone.PreviousScene {
                
            case .GameScene:
                
                playSound(soundName: buttonSound)
                
                playerOne = ""
                playerTwo = ""
                playerThree = ""
                playerFour = ""
                GlobalVariablesPhone.player1Text = ""
                GlobalVariablesPhone.player2Text = ""
                GlobalVariablesPhone.player3Text = ""
                GlobalVariablesPhone.player4Text = ""
                
                let replaygame = GameScene(fileNamed: "GameScene")
                replaygame?.scaleMode = .aspectFit

                self.view?.presentScene(replaygame!, transition: SKTransition.fade(withDuration: 1))
                
                break
                
                
            case .VsComThree:
                
                playSound(soundName: buttonSound)
                
                playerOne = ""
                playerTwo = ""
                playerThree = ""
                playerFour = ""
                GlobalVariablesPhone.player1Text = ""
                GlobalVariablesPhone.player2Text = ""
                GlobalVariablesPhone.player3Text = ""
                GlobalVariablesPhone.player4Text = ""
                
                let replaygame = SKScene(fileNamed: "VsComThree")
                replaygame?.scaleMode = .aspectFit
                self.view?.presentScene(replaygame!, transition: SKTransition.fade(withDuration: 1))
                
                break
                
                
            case .VsComTwo:
                
                playSound(soundName: buttonSound)
                
                playerOne = ""
                playerTwo = ""
                playerThree = ""
                playerFour = ""
                GlobalVariablesPhone.player1Text = ""
                GlobalVariablesPhone.player2Text = ""
                GlobalVariablesPhone.player3Text = ""
                GlobalVariablesPhone.player4Text = ""
                
                let replaygame = SKScene(fileNamed: "VsComTwo")
                replaygame?.scaleMode = .aspectFit
                self.view?.presentScene(replaygame!, transition: SKTransition.fade(withDuration: 1))
                
                break
                
                
            case .VsFriendFour:
                
                playSound(soundName: buttonSound)
                if !(GlobalVariablesPhone.player1Text.isEmpty) {
                    playerOne = GlobalVariablesPhone.player1Text
                } else {
                    playerOne = "Player 1"
                }
                if !(GlobalVariablesPhone.player2Text.isEmpty) {
                    playerTwo = GlobalVariablesPhone.player2Text
                } else {
                    playerTwo = "Player 2"
                }
                if !(GlobalVariablesPhone.player3Text.isEmpty) {
                    playerThree = GlobalVariablesPhone.player3Text
                } else {
                    playerThree = "Player 3"
                }
                if !(GlobalVariablesPhone.player4Text.isEmpty) {
                    playerFour = GlobalVariablesPhone.player4Text
                } else {
                    playerFour = "Player 4"
                }
                let replaygame = SKScene(fileNamed: "VsFriendFour")
                replaygame?.scaleMode = .aspectFit
                self.view?.presentScene(replaygame!, transition: SKTransition.fade(withDuration: 1))
                
                break
                
                
            case .VsFriendThree:
                
                playSound(soundName: buttonSound)
                if !(GlobalVariablesPhone.player1Text.isEmpty) {
                    playerOne = GlobalVariablesPhone.player1Text
                } else {
                    playerOne = "Player 1"
                }
                if !(GlobalVariablesPhone.player2Text.isEmpty) {
                    playerTwo = GlobalVariablesPhone.player2Text
                } else {
                    playerTwo = "Player 2"
                }
                if !(GlobalVariablesPhone.player3Text.isEmpty) {
                    playerThree = GlobalVariablesPhone.player3Text
                } else {
                    playerThree = "Player 3"
                }
                let replaygame = SKScene(fileNamed: "VsFriendThree")
                replaygame?.scaleMode = .aspectFit
                self.view?.presentScene(replaygame!, transition: SKTransition.fade(withDuration: 1))
                
                break
                
                
            case .VsFriendTwo:
                
                playSound(soundName: buttonSound)
                if !(GlobalVariablesPhone.player1Text.isEmpty) {
                    playerOne = GlobalVariablesPhone.player1Text
                } else {
                    playerOne = "Player 1"
                }
                if !(GlobalVariablesPhone.player2Text.isEmpty) {
                    playerTwo = GlobalVariablesPhone.player2Text
                } else {
                    playerTwo = "Player 2"
                }
                let replaygame = SKScene(fileNamed: "VsFriendTwo")
                replaygame?.scaleMode = .aspectFit
                self.view?.presentScene(replaygame!, transition: SKTransition.fade(withDuration: 1))
                
                break
                
            }
            
        case share:
            
            playSound(soundName: buttonSound)
            
            share(scene: self, text: "I Enjoyed playing this game, So i would recomend you to try Playing it.", image: UIImage(named: "Kakri"), excludeActivityTypes: [])
            
            
            
        default:
            break
        }
        
        
    }
    
    
    
    deinit {
        
        self.removeAllChildren()
        self.removeAllActions()
        print("Everything Got Deinitialyed ! EndScreen")
    }
    
    
}




