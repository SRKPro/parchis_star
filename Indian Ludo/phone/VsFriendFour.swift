//
//  VsFriendFour.swift
//  Indian Ludo
//
//  Created by ramesh sanghar on 08/01/19.
//  Copyright © 2019 Om. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import UnityAds


class VsFriendFour: SKScene, NewSpriteNodeButtonDelegate {
    
    let defaults = UserDefaults.standard
    
    
    // Players
    
    enum Player:Int {
        
        case Player1 = 1, Player2, Player3, Player4
        
    }
    
    
    
    let buttonSound = SKAction.playSoundFileNamed("multimedia_button_click_029.mp3", waitForCompletion: false)
    let finalWon = SKAction.playSoundFileNamed("Final Won.wav", waitForCompletion: false)
    
    
    var playerone:String = ""
    var playertwo:String = ""
    var playerthree:String = ""
    var playerfour:String = ""
    
    let moveAudio = SKAction.playSoundFileNamed("multimedia_button_click_006.mp3", waitForCompletion: false)
    let diceRollSFX = SKAction.playSoundFileNamed("dice_roll_on_gameboard_01.mp3", waitForCompletion: false)
    let killedAudio = SKAction.playSoundFileNamed("24275 cartoon boy disappoint shout-full.wav", waitForCompletion: false)
    let oneWon = SKAction.playSoundFileNamed("1 Won.wav", waitForCompletion: false)
    let fourAndEight = SKAction.playSoundFileNamed("4 or 8 new.wav", waitForCompletion: true)
    
    
    
    
    
    // Player Position
    
    var currentSpacePlayer1Piece1:Int = 1
    var currentSpacePlayer1Piece2:Int = 1
    var currentSpacePlayer1Piece3:Int = 1
    var currentSpacePlayer1Piece4:Int = 1
    
    var currentSpacePlayer2Piece1:Int = 5
    var currentSpacePlayer2Piece2:Int = 5
    var currentSpacePlayer2Piece3:Int = 5
    var currentSpacePlayer2Piece4:Int = 5
    
    var currentSpacePlayer3Piece1:Int = 9
    var currentSpacePlayer3Piece2:Int = 9
    var currentSpacePlayer3Piece3:Int = 9
    var currentSpacePlayer3Piece4:Int = 9
    
    var currentSpacePlayer4Piece1:Int = 13
    var currentSpacePlayer4Piece2:Int = 13
    var currentSpacePlayer4Piece3:Int = 13
    var currentSpacePlayer4Piece4:Int = 13
    
    
    var playerTturns:Int = 0
    var player2Tturns:Int = 0
    var player3Tturns:Int = 0
    var player4Tturns:Int = 0
    
    
    
    var p1p1TryToWin:Int = 0
    var p1p2TryToWin:Int = 0
    var p1p3TryToWin:Int = 0
    var p1p4TryToWin:Int = 0
    
    var p2p1TryToWin:Int = 0
    var p2p2TryToWin:Int = 0
    var p2p3TryToWin:Int = 0
    var p2p4TryToWin:Int = 0
    
    var p3p1TryToWin:Int = 0
    var p3p2TryToWin:Int = 0
    var p3p3TryToWin:Int = 0
    var p3p4TryToWin:Int = 0
    
    var p4p1TryToWin:Int = 0
    var p4p2TryToWin:Int = 0
    var p4p3TryToWin:Int = 0
    var p4p4TryToWin:Int = 0
    
    
    var killedPiece:SKSpriteNode = SKSpriteNode()
    
    
    var Dice:SKSpriteNode = SKSpriteNode()
    var Dice2:SKSpriteNode = SKSpriteNode()
    var finalPasso:SKSpriteNode = SKSpriteNode()
    
    
    var player1moves:Int = 0
    var player2moves:Int = 0
    var player3moves:Int = 0
    var player4moves:Int = 0
    
    
    var OneMoreMove:Int = 0
    
    var place = [String]()
    
    
    
    // Roll Dice Variables From Here :
    
    var Rolls = [Int]()
    var Player1Rolls = [Int]()
    var Player2Rolls = [Int]()
    var Player3Rolls = [Int]()
    var Player4Rolls = [Int]()
    
    
    var i = 0
    var j = 0
    var k = 0
    var l = 0
    
    var player1rans : Int = 0
    var player2rans : Int = 0
    var player3rans : Int = 0
    var player4rans : Int = 0
    var totalRans : Int = 0
    
    
    var playerScore:Int = 0
    var com1Score:Int = 0
    var com2Score:Int = 0
    var com3Score:Int = 0
    
    
    var player1Piece1Score : Int = 1
    var player1Piece2Score : Int = 1
    var player1Piece3Score : Int = 1
    var player1Piece4Score : Int = 1
    
    var com1Piece1Score : Int = 1
    var com1Piece2Score : Int = 1
    var com1Piece3Score : Int = 1
    var com1Piece4Score : Int = 1
    
    var com2Piece1Score : Int = 1
    var com2Piece2Score : Int = 1
    var com2Piece3Score : Int = 1
    var com2Piece4Score : Int = 1
    
    var com3Piece1Score : Int = 1
    var com3Piece2Score : Int = 1
    var com3Piece3Score : Int = 1
    var com3Piece4Score : Int = 1
    
    
    
    var movesRemaining:Int = 0
    var movesRemainingBack:Int = 0
    
    var whosTurn:Player = .Player1
    
    var moveFinished: Bool = true
    
    
    
    var player1piece1AbletoEnter: Bool = true
    var player1piece2AbletoEnter: Bool = true
    var player1piece3AbletoEnter: Bool = true
    var player1piece4AbletoEnter: Bool = true
    
    var player2piece1AbletoEnter: Bool = true
    var player2piece2AbletoEnter: Bool = true
    var player2piece3AbletoEnter: Bool = true
    var player2piece4AbletoEnter: Bool = true
    
    var player3piece1AbletoEnter: Bool = true
    var player3piece2AbletoEnter: Bool = true
    var player3piece3AbletoEnter: Bool = true
    var player3piece4AbletoEnter: Bool = true
    
    var player4piece1AbletoEnter: Bool = true
    var player4piece2AbletoEnter: Bool = true
    var player4piece3AbletoEnter: Bool = true
    var player4piece4AbletoEnter: Bool = true
    
    
    
    
    var diceRolled: Bool = false
    var gameOver: Bool = false
    var rolling: Bool = false
    
    
    
    // <------------------- Just For Checking Vars From Here --------------------->
    
    private var eyesOpen: Bool = true
    
    private var p1P1Reached: Bool = false
    private var p1P2Reached: Bool = false
    private var p1P3Reached: Bool = false
    private var p1P4Reached: Bool = false
    
    private var p2P1Reached: Bool = false
    private var p2P2Reached: Bool = false
    private var p2P3Reached: Bool = false
    private var p2P4Reached: Bool = false
    
    private var p3P1Reached: Bool = false
    private var p3P2Reached: Bool = false
    private var p3P3Reached: Bool = false
    private var p3P4Reached: Bool = false
    
    private var p4P1Reached: Bool = false
    private var p4P2Reached: Bool = false
    private var p4P3Reached: Bool = false
    private var p4P4Reached: Bool = false
    
    
    var keepAnimating = true
    
    
    
    var BloodedPlayer1: Bool = false
    
    var BloodedPlayer2: Bool = false
    
    var BloodedPlayer3: Bool = false
    
    var BloodedPlayer4: Bool = false
    
    var rolledDice: Int = 0
    
    
    var p1p1Plus10: Bool = false
    var p1p2Plus10: Bool = false
    var p1p3Plus10: Bool = false
    var p1p4Plus10: Bool = false
    
    var p2p1Plus10: Bool = false
    var p2p2Plus10: Bool = false
    var p2p3Plus10: Bool = false
    var p2p4Plus10: Bool = false
    
    var p3p1Plus10: Bool = false
    var p3p2Plus10: Bool = false
    var p3p3Plus10: Bool = false
    var p3p4Plus10: Bool = false
    
    var p4p1Plus10: Bool = false
    var p4p2Plus10: Bool = false
    var p4p3Plus10: Bool = false
    var p4p4Plus10: Bool = false
    
    var dontScoreP1: Bool = false
    var dontScoreP2: Bool = false
    var dontScoreP3: Bool = false
    var dontScoreP4: Bool = false
    
    
    // Player Name Labels :
    var playerLabel = SKLabelNode()
    var player2Label = SKLabelNode()
    var player3Label = SKLabelNode()
    var player4Label = SKLabelNode()
    
    
    var oneMoreMove = SKLabelNode()
    var cantMove = SKLabelNode()
    var threeLargeNum = SKLabelNode()
    
    
    
    var playerTotalTurns:SKLabelNode = SKLabelNode()
    var computer1TotalTurns:SKLabelNode = SKLabelNode()
    var computer2TotalTurns:SKLabelNode = SKLabelNode()
    var computer3TotalTurns:SKLabelNode = SKLabelNode()
    
    var plaTTtext:SKLabelNode = SKLabelNode()
    var com1TTtext:SKLabelNode = SKLabelNode()
    var com2TTtext:SKLabelNode = SKLabelNode()
    var com3TTtext:SKLabelNode = SKLabelNode()
    
    
    var playerScoreLabel:SKLabelNode = SKLabelNode()
    var com1ScoreLabel:SKLabelNode = SKLabelNode()
    var com2ScoreLabel:SKLabelNode = SKLabelNode()
    var com3ScoreLabel:SKLabelNode = SKLabelNode()
    
    
    
    var Player1Piece1:SKSpriteNode = SKSpriteNode()
    var Player1Piece2:SKSpriteNode = SKSpriteNode()
    var Player1Piece3:SKSpriteNode = SKSpriteNode()
    var Player1Piece4:SKSpriteNode = SKSpriteNode()
    
    
    var Player2Piece1:SKSpriteNode = SKSpriteNode()
    var Player2Piece2:SKSpriteNode = SKSpriteNode()
    var Player2Piece3:SKSpriteNode = SKSpriteNode()
    var Player2Piece4:SKSpriteNode = SKSpriteNode()
    
    
    var Player3Piece1:SKSpriteNode = SKSpriteNode()
    var Player3Piece2:SKSpriteNode = SKSpriteNode()
    var Player3Piece3:SKSpriteNode = SKSpriteNode()
    var Player3Piece4:SKSpriteNode = SKSpriteNode()
    
    
    var Player4Piece1:SKSpriteNode = SKSpriteNode()
    var Player4Piece2:SKSpriteNode = SKSpriteNode()
    var Player4Piece3:SKSpriteNode = SKSpriteNode()
    var Player4Piece4:SKSpriteNode = SKSpriteNode()
    
    var touchedNode:SKSpriteNode = SKSpriteNode()
    
    
    var Arrow1Blue = SKSpriteNode(imageNamed: "Arrow 4 Blue")
    var Arrow2Blue = SKSpriteNode(imageNamed: "Arrow 3 Blue")
    var Arrow3Blue = SKSpriteNode(imageNamed: "Arrow 2 Blue")
    var Arrow4Blue = SKSpriteNode(imageNamed: "Arrow 1 Blue")
    var Arrow1Yellow = SKSpriteNode(imageNamed: "Arrow 4 Yellow")
    var Arrow2Yellow = SKSpriteNode(imageNamed: "Arrow 3 Yellow")
    var Arrow3Yellow = SKSpriteNode(imageNamed: "Arrow 2 Yellow")
    var Arrow4Yellow = SKSpriteNode(imageNamed: "Arrow 1 Yellow")
    var Arrow1Green = SKSpriteNode(imageNamed: "Arrow 4 Green")
    var Arrow2Green = SKSpriteNode(imageNamed: "Arrow 3 Green")
    var Arrow3Green = SKSpriteNode(imageNamed: "Arrow 2 Green")
    var Arrow4Green = SKSpriteNode(imageNamed: "Arrow 1 Green")
    var Arrow1Red = SKSpriteNode(imageNamed: "Arrow 4 Red")
    var Arrow2Red = SKSpriteNode(imageNamed: "Arrow 3 Red")
    var Arrow3Red = SKSpriteNode(imageNamed: "Arrow 2 Red")
    var Arrow4Red = SKSpriteNode(imageNamed: "Arrow 1 Red")
    
    
    
    var player1Piece1eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece1eye2:SKSpriteNode = SKSpriteNode()
    
    var player1Piece2eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece2eye2:SKSpriteNode = SKSpriteNode()
    
    var player1Piece3eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece3eye2:SKSpriteNode = SKSpriteNode()
    
    var player1Piece4eye1:SKSpriteNode = SKSpriteNode()
    var player1Piece4eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece1eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece1eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece2eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece2eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece3eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece3eye2:SKSpriteNode = SKSpriteNode()
    
    var player2Piece4eye1:SKSpriteNode = SKSpriteNode()
    var player2Piece4eye2:SKSpriteNode = SKSpriteNode()
    
    var player3Piece1eye1:SKSpriteNode = SKSpriteNode()
    var player3Piece1eye2:SKSpriteNode = SKSpriteNode()
    
    var player3Piece2eye1:SKSpriteNode = SKSpriteNode()
    var player3Piece2eye2:SKSpriteNode = SKSpriteNode()
    
    var player3Piece3eye1:SKSpriteNode = SKSpriteNode()
    var player3Piece3eye2:SKSpriteNode = SKSpriteNode()
    
    var player3Piece4eye1:SKSpriteNode = SKSpriteNode()
    var player3Piece4eye2:SKSpriteNode = SKSpriteNode()
    
    var player4Piece1eye1:SKSpriteNode = SKSpriteNode()
    var player4Piece1eye2:SKSpriteNode = SKSpriteNode()
    
    var player4Piece2eye1:SKSpriteNode = SKSpriteNode()
    var player4Piece2eye2:SKSpriteNode = SKSpriteNode()
    
    var player4Piece3eye1:SKSpriteNode = SKSpriteNode()
    var player4Piece3eye2:SKSpriteNode = SKSpriteNode()
    
    var player4Piece4eye1:SKSpriteNode = SKSpriteNode()
    var player4Piece4eye2:SKSpriteNode = SKSpriteNode()
    
    
    
    // UI Buttorn :
    
    var backButton:NewSpriteNodeButton = NewSpriteNodeButton()
    var PauseMenu:SKSpriteNode = SKSpriteNode()
    var cancel:NewSpriteNodeButton = NewSpriteNodeButton()
    var home:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var audioButton:NewSpriteNodeButton = NewSpriteNodeButton()
  //  var howToPlay:NewSpriteNodeButton = NewSpriteNodeButton()
    
    var closeEye = SKTexture(imageNamed: "Close Eye")
    var openEye = SKTexture(imageNamed: "Open Eye")
    
    
    
    var redScore:SKSpriteNode = SKSpriteNode()
    var yellowScore:SKSpriteNode = SKSpriteNode()
    var greenScore:SKSpriteNode = SKSpriteNode()
    var blueScore:SKSpriteNode = SKSpriteNode()
    
    
    let f1 = SKTexture.init(imageNamed: "Ani 1")
    let f2 = SKTexture.init(imageNamed: "Ani 2")
    let f3 = SKTexture.init(imageNamed: "Ani 3")
    let f4 = SKTexture.init(imageNamed: "Ani 4")
    let f5 = SKTexture.init(imageNamed: "Ani 5")
    let f6 = SKTexture.init(imageNamed: "Ani 6")
    let f7 = SKTexture.init(imageNamed: "Ani 7")
    let f8 = SKTexture.init(imageNamed: "Ani 8")
    let f9 = SKTexture.init(imageNamed: "Ani 9")
    let f10 = SKTexture.init(imageNamed: "Ani 10")
    let f11 = SKTexture.init(imageNamed: "Ani 11")
    let f12 = SKTexture.init(imageNamed: "Ani 12")
    let f13 = SKTexture.init(imageNamed: "Ani 13")
    let f14 = SKTexture.init(imageNamed: "Ani 14")
    let f15 = SKTexture.init(imageNamed: "Ani 15")
    let f16 = SKTexture.init(imageNamed: "Ani 16")
    let f17 = SKTexture.init(imageNamed: "Ani 17")
    
    
    var diceTextures: [SKTexture] = []
    var rolledDiceTexture = SKSpriteNode()
    
    var blackShadow:SKSpriteNode = SKSpriteNode()
    
    var textColore:UIColor = UIColor()
    
    override func didMove(to view: SKView) {
        
        
        
        GlobalVariablesPhone.totalGames += 1
        defaults.set(GlobalVariablesPhone.totalGames, forKey: "totalGames")
        defaults.synchronize()
        
        
        if GlobalVariablesPhone.backgroundColorName == 1 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            textColore = GlobalVariablesPhone.darkTextColor
            
        } else if GlobalVariablesPhone.backgroundColorName == 2 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
            textColore = GlobalVariablesPhone.whiteTextColor
            
        } else if GlobalVariablesPhone.backgroundColorName == 3 {
            print("IN COLORE#######")
            scene?.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
            textColore = GlobalVariablesPhone.whiteTextColor
            
        }
        
        
        if defaults.value(forKey: "Purchase") == nil {
            
            if UnityServices.isInitialized() {
                
                print("Calling Ads !")
                
                if GlobalVariablesPhone.totalGames % 2 == 0 {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playAdID"), object: nil)
                    
                }
            }
            
        }
        
        
        
        if defaults.value(forKey: "Purchase") == nil {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideBannerAd"), object: nil)
            
        }
        
        
        scene?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        diceTextures = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17]
        
        
        
        Dice = SKSpriteNode(imageNamed: "Dice3")
        Dice.name = "Dice"
        Dice.isUserInteractionEnabled = false
        Dice.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
        Dice.zPosition = 3
        Dice.size = CGSize(width: 90.0, height: 90.0)
        Dice.isHidden = false
        addChild(Dice)
        
        
        blackShadow = SKSpriteNode(imageNamed: "Black Shadow")
        blackShadow.name = "blackshadow"
        blackShadow.position = CGPoint(x: 0, y: 0)
        blackShadow.zPosition = 9
        blackShadow.isHidden = true
        addChild(blackShadow)
        
        
        Dice2 = SKSpriteNode(imageNamed: "Dice3")
        Dice2.name = "Dice2"
        Dice2.isUserInteractionEnabled = false
        Dice2.position = CGPoint(x: (scene?.size.width)! * -0.39, y: 0)
        Dice2.zPosition = 3
        Dice2.size = CGSize(width: 90.0, height: 90.0)
        Dice2.isHidden = true
        addChild(Dice2)
        
        backButton = NewSpriteNodeButton(imageNamed: "back")
        backButton.name = "backButton"
        backButton.position = CGPoint(x: (scene?.size.width)! * -0.479, y: 0)
        backButton.zPosition = 3
        backButton.isUserInteractionEnabled = true
        backButton.delegate = self
        addChild(backButton)
        
        PauseMenu = SKSpriteNode(imageNamed: "Pause menu")
        PauseMenu.name = "PauseMenu"
        PauseMenu.position = CGPoint(x: 0, y: 0)
        PauseMenu.size = CGSize(width: 501, height: 246)
        PauseMenu.zPosition = 10
        PauseMenu.isHidden = true
        addChild(PauseMenu)
        
        
        
        cancel = NewSpriteNodeButton(imageNamed: "Cancel")
        cancel.name = "cancel"
        cancel.position = CGPoint(x: PauseMenu.size.width * 0.43, y: PauseMenu.size.height * 0.37)
        cancel.zPosition = 11
        cancel.size = CGSize(width: 28, height: 35)
        cancel.isUserInteractionEnabled = true
        cancel.delegate = self
        cancel.isHidden = true
        addChild(cancel)
        
        home = NewSpriteNodeButton(imageNamed: "Home")
        home.name = "home"
        home.position = CGPoint(x: PauseMenu.size.width * -0.13, y: PauseMenu.size.height * -0.14)
        home.zPosition = 11
        home.size = CGSize(width: 56, height: 60)
        home.isUserInteractionEnabled = true
        home.delegate = self
        home.isHidden = true
        addChild(home)
        
        
        audioButton = NewSpriteNodeButton(imageNamed: "Audio")
        audioButton.name = "audiobutton"
        audioButton.position = CGPoint(x: PauseMenu.size.width * 0.13, y: PauseMenu.size.height * -0.14)
        audioButton.zPosition = 11
        audioButton.size = CGSize(width: 56, height: 60)
        audioButton.delegate = self
        audioButton.isUserInteractionEnabled = true
        audioButton.isHidden = true
        addChild(audioButton)
        
        
        
        Player1Piece1 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece1.name = "Player1Piece1"
        Player1Piece1.position = CGPoint(x: 1, y: -181)
        Player1Piece1.zPosition = 3
        Player1Piece1.size = CGSize(width: 30, height: 30)
        addChild(Player1Piece1)
        
        Player1Piece2 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece2.name = "Player1Piece2"
        Player1Piece2.position = CGPoint(x: 25, y: -158)
        Player1Piece2.zPosition = 3
        Player1Piece2.size = CGSize(width: 30, height: 30)
        addChild(Player1Piece2)
        
        Player1Piece3 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece3.name = "Player1Piece3"
        Player1Piece3.position = CGPoint(x: 1, y: -136)
        Player1Piece3.zPosition = 3
        Player1Piece3.size = CGSize(width: 30, height: 30)
        addChild(Player1Piece3)
        
        Player1Piece4 = SKSpriteNode(imageNamed: "Kakri")
        Player1Piece4.name = "Player1Piece4"
        Player1Piece4.position = CGPoint(x: -23, y: -158)
        Player1Piece4.zPosition = 3
        Player1Piece4.size = CGSize(width: 30, height: 30)
        addChild(Player1Piece4)
        
        print(Player1Piece1)
        
        
        
        
        
        Player2Piece1 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece1.name = "Player2Piece1"
        Player2Piece1.isUserInteractionEnabled = false
        Player2Piece1.position = CGPoint(x: 195, y: 0)
        Player2Piece1.zPosition = 3
        Player2Piece1.size = CGSize(width: 30, height: 30)
        addChild(Player2Piece1)
        
        Player2Piece2 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece2.name = "Player2Piece2"
        Player2Piece2.isUserInteractionEnabled = false
        Player2Piece2.position = CGPoint(x: 170, y: 23)
        Player2Piece2.zPosition = 3
        Player2Piece2.size = CGSize(width: 30, height: 30)
        addChild(Player2Piece2)
        
        Player2Piece3 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece3.name = "Player2Piece3"
        Player2Piece3.isUserInteractionEnabled = false
        Player2Piece3.position = CGPoint(x: 146, y: 0)
        Player2Piece3.zPosition = 3
        Player2Piece3.size = CGSize(width: 30, height: 30)
        addChild(Player2Piece3)
        
        Player2Piece4 = SKSpriteNode(imageNamed: "Kakri3")
        Player2Piece4.name = "Player2Piece4"
        Player2Piece4.isUserInteractionEnabled = false
        Player2Piece4.position = CGPoint(x: 170, y: -21)
        Player2Piece4.zPosition = 3
        Player2Piece4.size = CGSize(width: 30, height: 30)
        addChild(Player2Piece4)
        
        
        
        Player3Piece1 = SKSpriteNode(imageNamed: "Kakri2")
        Player3Piece1.name = "Player3Piece1"
        Player3Piece1.isUserInteractionEnabled = false
        Player3Piece1.position = CGPoint(x: 1, y: 182)
        Player3Piece1.zPosition = 3
        Player3Piece1.size = CGSize(width: 30, height: 30)
        addChild(Player3Piece1)
        
        Player3Piece2 = SKSpriteNode(imageNamed: "Kakri2")
        Player3Piece2.name = "Player3Piece2"
        Player3Piece2.isUserInteractionEnabled = false
        Player3Piece2.position = CGPoint(x: -23, y: 159)
        Player3Piece2.zPosition = 3
        Player3Piece2.size = CGSize(width: 30, height: 30)
        addChild(Player3Piece2)
        
        Player3Piece3 = SKSpriteNode(imageNamed: "Kakri2")
        Player3Piece3.name = "Player3Piece3"
        Player3Piece3.isUserInteractionEnabled = false
        Player3Piece3.position = CGPoint(x: 1, y: 137)
        Player3Piece3.zPosition = 3
        Player3Piece3.size = CGSize(width: 30, height: 30)
        addChild(Player3Piece3)
        
        Player3Piece4 = SKSpriteNode(imageNamed: "Kakri2")
        Player3Piece4.name = "Player3Piece4"
        Player3Piece4.isUserInteractionEnabled = false
        Player3Piece4.position = CGPoint(x: 25, y: 159)
        Player3Piece4.zPosition = 3
        Player3Piece4.size = CGSize(width: 30, height: 30)
        addChild(Player3Piece4)
        
        
        
        Player4Piece1 = SKSpriteNode(imageNamed: "Kakri4")
        Player4Piece1.name = "Player4Piece1"
        Player4Piece1.isUserInteractionEnabled = false
        Player4Piece1.position = CGPoint(x: -193, y: 0)
        Player4Piece1.zPosition = 3
        Player4Piece1.size = CGSize(width: 30, height: 30)
        addChild(Player4Piece1)
        
        Player4Piece2 = SKSpriteNode(imageNamed: "Kakri4")
        Player4Piece2.name = "Player4Piece2"
        Player4Piece2.isUserInteractionEnabled = false
        Player4Piece2.position = CGPoint(x: -169, y: -22)
        Player4Piece2.zPosition = 3
        Player4Piece2.size = CGSize(width: 30, height: 30)
        addChild(Player4Piece2)
        
        Player4Piece3 = SKSpriteNode(imageNamed: "Kakri4")
        Player4Piece3.name = "Player4Piece3"
        Player4Piece3.isUserInteractionEnabled = false
        Player4Piece3.position = CGPoint(x: -145, y: 0)
        Player4Piece3.zPosition = 3
        Player4Piece3.size = CGSize(width: 30, height: 30)
        addChild(Player4Piece3)
        
        Player4Piece4 = SKSpriteNode(imageNamed: "Kakri4")
        Player4Piece4.name = "Player4Piece4"
        Player4Piece4.isUserInteractionEnabled = false
        Player4Piece4.position = CGPoint(x: -169, y: 23)
        Player4Piece4.zPosition = 3
        Player4Piece4.size = CGSize(width: 30, height: 30)
        addChild(Player4Piece4)
        
        print(Player4Piece1)
        
        
        redScore = SKSpriteNode(imageNamed: "Score Red")
        redScore.name = "RedScore"
        redScore.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * -0.403)
        redScore.zPosition = 1
        redScore.size = CGSize(width: 116, height: 38)
        addChild(redScore)
        
        
        
        greenScore = SKSpriteNode(imageNamed: "Score Green")
        greenScore.name = "GreenScore"
        greenScore.position = CGPoint(x: (scene?.size.width)! * -0.40, y: (scene?.size.height)! * 0.39)
        greenScore.zPosition = 1
        greenScore.size = CGSize(width: 116, height: 38)
        addChild(greenScore)
        
        blueScore = SKSpriteNode(imageNamed: "Score Blue")
        blueScore.name = "BlueScore"
        blueScore.position = CGPoint(x: (scene?.size.width)! * -0.40, y: (scene?.size.height)! * -0.39)
        blueScore.zPosition = 1
        blueScore.size = CGSize(width: 116, height: 38)
        addChild(blueScore)
        
        yellowScore = SKSpriteNode(imageNamed: "Score Yellow")
        yellowScore.name = "YellowScore"
        yellowScore.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * 0.39)
        yellowScore.zPosition = 1
        yellowScore.size = CGSize(width: 116, height: 38)
        addChild(yellowScore)
        
        
        
        Arrow1Red.name = "arrow1red"
        Arrow1Red.position = CGPoint(x: 293, y: -60)
        Arrow1Red.zPosition = 1
        Arrow1Red.size = CGSize(width: 26, height: 17)
        Arrow1Red.isHidden = true
        addChild(Arrow1Red)
        
        Arrow2Red.name = "arrow2red"
        Arrow2Red.position = CGPoint(x: 293, y: -76)
        Arrow2Red.zPosition = 1
        Arrow2Red.size = CGSize(width: 26, height: 17)
        Arrow2Red.isHidden = true
        addChild(Arrow2Red)
        
        Arrow3Red.name = "arrow3red"
        Arrow3Red.position = CGPoint(x: 293, y: -92)
        Arrow3Red.zPosition = 1
        Arrow3Red.size = CGSize(width: 26, height: 17)
        Arrow3Red.isHidden = true
        addChild(Arrow3Red)
        
        Arrow4Red.name = "arrow4red"
        Arrow4Red.position = CGPoint(x: 293, y: -108)
        Arrow4Red.zPosition = 1
        Arrow4Red.size = CGSize(width: 26, height: 17)
        Arrow4Red.isHidden = true
        addChild(Arrow4Red)
        
        
        Arrow1Green.name = "arrow1green"
        Arrow1Green.position = CGPoint(x: -293, y: 60)
        Arrow1Green.zPosition = 1
        Arrow1Green.size = CGSize(width: 26, height: 17)
        Arrow1Green.isHidden = true
        addChild(Arrow1Green)
        
        Arrow2Green.name = "arrow2green"
        Arrow2Green.position = CGPoint(x: -293, y: 76)
        Arrow2Green.zPosition = 1
        Arrow2Green.size = CGSize(width: 26, height: 17)
        Arrow2Green.isHidden = true
        addChild(Arrow2Green)
        
        Arrow3Green.name = "arrow3green"
        Arrow3Green.position = CGPoint(x: -293, y: 92)
        Arrow3Green.zPosition = 1
        Arrow3Green.size = CGSize(width: 26, height: 17)
        Arrow3Green.isHidden = true
        addChild(Arrow3Green)
        
        Arrow4Green.name = "arrow4green"
        Arrow4Green.position = CGPoint(x: -293, y: 108)
        Arrow4Green.zPosition = 1
        Arrow4Green.size = CGSize(width: 26, height: 17)
        Arrow4Green.isHidden = true
        addChild(Arrow4Green)
        
        
        Arrow1Yellow.name = "arrow1yellow"
        Arrow1Yellow.position = CGPoint(x: 293, y: 60)
        Arrow1Yellow.zPosition = 1
        Arrow1Yellow.size = CGSize(width: 26, height: 17)
        Arrow1Yellow.isHidden = true
        addChild(Arrow1Yellow)
        
        Arrow2Yellow.name = "arrow2yellow"
        Arrow2Yellow.position = CGPoint(x: 293, y: 76)
        Arrow2Yellow.zPosition = 1
        Arrow2Yellow.size = CGSize(width: 26, height: 17)
        Arrow2Yellow.isHidden = true
        addChild(Arrow2Yellow)
        
        Arrow3Yellow.name = "arrow3yellow"
        Arrow3Yellow.position = CGPoint(x: 293, y: 92)
        Arrow3Yellow.zPosition = 1
        Arrow3Yellow.size = CGSize(width: 26, height: 17)
        Arrow3Yellow.isHidden = true
        addChild(Arrow3Yellow)
        
        Arrow4Yellow.name = "arrow4yellow"
        Arrow4Yellow.position = CGPoint(x: 293, y: 108)
        Arrow4Yellow.zPosition = 1
        Arrow4Yellow.size = CGSize(width: 26, height: 17)
        Arrow4Yellow.isHidden = true
        addChild(Arrow4Yellow)
        
        
        Arrow1Blue.name = "arrow1blue"
        Arrow1Blue.position = CGPoint(x: -293, y: -60)
        Arrow1Blue.zPosition = 1
        Arrow1Blue.size = CGSize(width: 26, height: 17)
        Arrow1Blue.isHidden = true
        addChild(Arrow1Blue)
        
        Arrow2Blue.name = "arrow2blue"
        Arrow2Blue.position = CGPoint(x: -293, y: -76)
        Arrow2Blue.zPosition = 1
        Arrow2Blue.size = CGSize(width: 26, height: 17)
        Arrow2Blue.isHidden = true
        addChild(Arrow2Blue)
        
        Arrow3Blue.name = "arrow3blue"
        Arrow3Blue.position = CGPoint(x: -293, y: -92)
        Arrow3Blue.zPosition = 1
        Arrow3Blue.size = CGSize(width: 26, height: 17)
        Arrow3Blue.isHidden = true
        addChild(Arrow3Blue)
        
        Arrow4Blue.name = "arrow4blue"
        Arrow4Blue.position = CGPoint(x: -293, y: -108)
        Arrow4Blue.zPosition = 1
        Arrow4Blue.size = CGSize(width: 26, height: 17)
        Arrow4Blue.isHidden = true
        addChild(Arrow4Blue)
        
        
        
        
        
        
        player1Piece1eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece1eye1.name = "player1Piece1eye1"
        player1Piece1eye1.position = CGPoint(x: -(Player1Piece1.size.width / 5), y: 0)
        player1Piece1eye1.zPosition = 4
        player1Piece1eye1.size = CGSize(width: 6, height: 6)
        player1Piece1eye1.isUserInteractionEnabled = false
        Player1Piece1.addChild(player1Piece1eye1)
        
        player1Piece1eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece1eye2.name = "player1Piece1eye2"
        player1Piece1eye2.position = CGPoint(x: Player1Piece1.size.width / 5, y: 0)
        player1Piece1eye2.zPosition = 4
        player1Piece1eye2.size = CGSize(width: 6, height: 6)
        player1Piece1eye2.isUserInteractionEnabled = false
        Player1Piece1.addChild(player1Piece1eye2)
        
        
        player1Piece2eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece2eye1.name = "player1Piece2eye1"
        player1Piece2eye1.position = CGPoint(x: -(Player1Piece2.size.width / 5), y: 0)
        player1Piece2eye1.zPosition = 4
        player1Piece2eye1.size = CGSize(width: 6, height: 6)
        player1Piece2eye1.isUserInteractionEnabled = false
        Player1Piece2.addChild(player1Piece2eye1)
        
        
        player1Piece2eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece2eye2.name = "player1Piece2eye2"
        player1Piece2eye2.position = CGPoint(x: Player1Piece2.size.width / 5, y: 0)
        player1Piece2eye2.zPosition = 4
        player1Piece2eye2.size = CGSize(width: 6, height: 6)
        player1Piece2eye2.isUserInteractionEnabled = false
        Player1Piece2.addChild(player1Piece2eye2)
        
        
        player1Piece3eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece3eye1.name = "player1Piece3eye1"
        player1Piece3eye1.position = CGPoint(x: -(Player1Piece3.size.width / 5), y: 0)
        player1Piece3eye1.zPosition = 4
        player1Piece3eye1.size = CGSize(width: 6, height: 6)
        player1Piece3eye1.isUserInteractionEnabled = false
        Player1Piece3.addChild(player1Piece3eye1)
        
        
        player1Piece3eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece3eye2.name = "player1Piece3eye2"
        player1Piece3eye2.position = CGPoint(x: Player1Piece3.size.width / 5, y: 0)
        player1Piece3eye2.zPosition = 4
        player1Piece3eye2.size = CGSize(width: 6, height: 6)
        player1Piece3eye2.isUserInteractionEnabled = false
        Player1Piece3.addChild(player1Piece3eye2)
        
        
        player1Piece4eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece4eye1.name = "player1Piece4eye1"
        player1Piece4eye1.position = CGPoint(x: -(Player1Piece4.size.width / 5), y: 0)
        player1Piece4eye1.zPosition = 4
        player1Piece4eye1.size = CGSize(width: 6, height: 6)
        player1Piece4eye1.isUserInteractionEnabled = false
        Player1Piece4.addChild(player1Piece4eye1)
        
        
        player1Piece4eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player1Piece4eye2.name = "player1Piece4eye2"
        player1Piece4eye2.position = CGPoint(x: Player1Piece4.size.width / 5, y: 0)
        player1Piece4eye2.zPosition = 4
        player1Piece4eye2.size = CGSize(width: 6, height: 6)
        player1Piece4eye2.isUserInteractionEnabled = false
        Player1Piece4.addChild(player1Piece4eye2)
        
        
        
        player2Piece1eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece1eye1.name = "player2Piece1eye1"
        player2Piece1eye1.position = CGPoint(x: -(Player2Piece1.size.width / 5), y: 0)
        player2Piece1eye1.zPosition = 4
        player2Piece1eye1.size = CGSize(width: 6, height: 6)
        player2Piece1eye1.isUserInteractionEnabled = false
        Player2Piece1.addChild(player2Piece1eye1)
        
        player2Piece1eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece1eye2.name = "player2Piece1eye2"
        player2Piece1eye2.position = CGPoint(x: Player2Piece1.size.width / 5, y: 0)
        player2Piece1eye2.zPosition = 4
        player2Piece1eye2.size = CGSize(width: 6, height: 6)
        player2Piece1eye2.isUserInteractionEnabled = false
        Player2Piece1.addChild(player2Piece1eye2)
        
        
        player2Piece2eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece2eye1.name = "player2Piece2eye1"
        player2Piece2eye1.position = CGPoint(x: -(Player2Piece2.size.width / 5), y: 0)
        player2Piece2eye1.zPosition = 4
        player2Piece2eye1.size = CGSize(width: 6, height: 6)
        player2Piece2eye1.isUserInteractionEnabled = false
        Player2Piece2.addChild(player2Piece2eye1)
        
        
        player2Piece2eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece2eye2.name = "player2Piece2eye2"
        player2Piece2eye2.position = CGPoint(x: Player2Piece2.size.width / 5, y: 0)
        player2Piece2eye2.zPosition = 4
        player2Piece2eye2.size = CGSize(width: 6, height: 6)
        player2Piece2eye2.isUserInteractionEnabled = false
        Player2Piece2.addChild(player2Piece2eye2)
        
        
        player2Piece3eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece3eye1.name = "player2Piece3eye1"
        player2Piece3eye1.position = CGPoint(x: -(Player2Piece3.size.width / 5), y: 0)
        player2Piece3eye1.zPosition = 4
        player2Piece3eye1.size = CGSize(width: 6, height: 6)
        player2Piece3eye1.isUserInteractionEnabled = false
        Player2Piece3.addChild(player2Piece3eye1)
        
        
        player2Piece3eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece3eye2.name = "player2Piece3eye2"
        player2Piece3eye2.position = CGPoint(x: Player2Piece3.size.width / 5, y: 0)
        player2Piece3eye2.zPosition = 4
        player2Piece3eye2.size = CGSize(width: 6, height: 6)
        player2Piece3eye2.isUserInteractionEnabled = false
        Player2Piece3.addChild(player2Piece3eye2)
        
        
        player2Piece4eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece4eye1.name = "player2Piece4eye1"
        player2Piece4eye1.position = CGPoint(x: -(Player2Piece4.size.width / 5), y: 0)
        player2Piece4eye1.zPosition = 4
        player2Piece4eye1.size = CGSize(width: 6, height: 6)
        player2Piece4eye1.isUserInteractionEnabled = false
        Player2Piece4.addChild(player2Piece4eye1)
        
        
        player2Piece4eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player2Piece4eye2.name = "player2Piece4eye2"
        player2Piece4eye2.position = CGPoint(x: Player2Piece4.size.width / 5, y: 0)
        player2Piece4eye2.zPosition = 4
        player2Piece4eye2.size = CGSize(width: 6, height: 6)
        player2Piece4eye2.isUserInteractionEnabled = false
        Player2Piece4.addChild(player2Piece4eye2)
        
        
        
        
        player3Piece1eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece1eye1.name = "player3Piece1eye1"
        player3Piece1eye1.position = CGPoint(x: -(Player3Piece1.size.width / 5), y: 0)
        player3Piece1eye1.zPosition = 4
        player3Piece1eye1.size = CGSize(width: 6, height: 6)
        player3Piece1eye1.isUserInteractionEnabled = false
        Player3Piece1.addChild(player3Piece1eye1)
        
        player3Piece1eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece1eye2.name = "player3Piece1eye2"
        player3Piece1eye2.position = CGPoint(x: Player3Piece1.size.width / 5, y: 0)
        player3Piece1eye2.zPosition = 4
        player3Piece1eye2.size = CGSize(width: 6, height: 6)
        player3Piece1eye2.isUserInteractionEnabled = false
        Player3Piece1.addChild(player3Piece1eye2)
        
        
        player3Piece2eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece2eye1.name = "player3Piece2eye1"
        player3Piece2eye1.position = CGPoint(x: -(Player3Piece2.size.width / 5), y: 0)
        player3Piece2eye1.zPosition = 4
        player3Piece2eye1.size = CGSize(width: 6, height: 6)
        player3Piece2eye1.isUserInteractionEnabled = false
        Player3Piece2.addChild(player3Piece2eye1)
        
        
        player3Piece2eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece2eye2.name = "player3Piece2eye2"
        player3Piece2eye2.position = CGPoint(x: Player3Piece2.size.width / 5, y: 0)
        player3Piece2eye2.zPosition = 4
        player3Piece2eye2.size = CGSize(width: 6, height: 6)
        player3Piece2eye2.isUserInteractionEnabled = false
        Player3Piece2.addChild(player3Piece2eye2)
        
        
        player3Piece3eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece3eye1.name = "player3Piece3eye1"
        player3Piece3eye1.position = CGPoint(x: -(Player3Piece3.size.width / 5), y: 0)
        player3Piece3eye1.zPosition = 4
        player3Piece3eye1.size = CGSize(width: 6, height: 6)
        player3Piece3eye1.isUserInteractionEnabled = false
        Player3Piece3.addChild(player3Piece3eye1)
        
        
        player3Piece3eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece3eye2.name = "player3Piece3eye2"
        player3Piece3eye2.position = CGPoint(x: Player3Piece3.size.width / 5, y: 0)
        player3Piece3eye2.zPosition = 4
        player3Piece3eye2.size = CGSize(width: 6, height: 6)
        player3Piece3eye2.isUserInteractionEnabled = false
        Player3Piece3.addChild(player3Piece3eye2)
        
        
        player3Piece4eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece4eye1.name = "player3Piece4eye1"
        player3Piece4eye1.position = CGPoint(x: -(Player3Piece4.size.width / 5), y: 0)
        player3Piece4eye1.zPosition = 4
        player3Piece4eye1.size = CGSize(width: 6, height: 6)
        player3Piece4eye1.isUserInteractionEnabled = false
        Player3Piece4.addChild(player3Piece4eye1)
        
        
        player3Piece4eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player3Piece4eye2.name = "player3Piece4eye2"
        player3Piece4eye2.position = CGPoint(x: Player3Piece4.size.width / 5, y: 0)
        player3Piece4eye2.zPosition = 4
        player3Piece4eye2.size = CGSize(width: 6, height: 6)
        player3Piece4eye2.isUserInteractionEnabled = false
        Player3Piece4.addChild(player3Piece4eye2)
        
        
        
        
        
        player4Piece1eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece1eye1.name = "player4Piece1eye1"
        player4Piece1eye1.position = CGPoint(x: -(Player4Piece1.size.width / 5), y: 1)
        player4Piece1eye1.zPosition = 4
        player4Piece1eye1.size = CGSize(width: 6, height: 6)
        player4Piece1eye1.isUserInteractionEnabled = false
        Player4Piece1.addChild(player4Piece1eye1)
        
        player4Piece1eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece1eye2.name = "player4Piece1eye2"
        player4Piece1eye2.position = CGPoint(x: Player4Piece1.size.width / 5, y: 1)
        player4Piece1eye2.zPosition = 4
        player4Piece1eye2.size = CGSize(width: 6, height: 6)
        player4Piece1eye2.isUserInteractionEnabled = false
        Player4Piece1.addChild(player4Piece1eye2)
        
        
        player4Piece2eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece2eye1.name = "player4Piece2eye1"
        player4Piece2eye1.position = CGPoint(x: -(Player4Piece2.size.width / 5), y: 1)
        player4Piece2eye1.zPosition = 4
        player4Piece2eye1.size = CGSize(width: 6, height: 6)
        player4Piece2eye1.isUserInteractionEnabled = false
        Player4Piece2.addChild(player4Piece2eye1)
        
        
        player4Piece2eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece2eye2.name = "player4Piece2eye2"
        player4Piece2eye2.position = CGPoint(x: Player4Piece2.size.width / 5, y: 1)
        player4Piece2eye2.zPosition = 4
        player4Piece2eye2.size = CGSize(width: 6, height: 6)
        player4Piece2eye2.isUserInteractionEnabled = false
        Player4Piece2.addChild(player4Piece2eye2)
        
        
        player4Piece3eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece3eye1.name = "player4Piece3eye1"
        player4Piece3eye1.position = CGPoint(x: -(Player4Piece3.size.width / 5), y: 1)
        player4Piece3eye1.zPosition = 4
        player4Piece3eye1.size = CGSize(width: 6, height: 6)
        player4Piece3eye1.isUserInteractionEnabled = false
        Player4Piece3.addChild(player4Piece3eye1)
        
        
        player4Piece3eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece3eye2.name = "player4Piece3eye2"
        player4Piece3eye2.position = CGPoint(x: Player4Piece3.size.width / 5, y: 1)
        player4Piece3eye2.zPosition = 4
        player4Piece3eye2.size = CGSize(width: 6, height: 6)
        player4Piece3eye2.isUserInteractionEnabled = false
        Player4Piece3.addChild(player4Piece3eye2)
        
        
        player4Piece4eye1 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece4eye1.name = "player4Piece4eye1"
        player4Piece4eye1.position = CGPoint(x: -(Player4Piece4.size.width / 5), y: 1)
        player4Piece4eye1.zPosition = 4
        player4Piece4eye1.size = CGSize(width: 6, height: 6)
        player4Piece4eye1.isUserInteractionEnabled = false
        Player4Piece4.addChild(player4Piece4eye1)
        
        
        player4Piece4eye2 = SKSpriteNode(imageNamed: "Open Eye")
        player4Piece4eye2.name = "player4Piece4eye2"
        player4Piece4eye2.position = CGPoint(x: Player4Piece4.size.width / 5, y: 1)
        player4Piece4eye2.zPosition = 4
        player4Piece4eye2.size = CGSize(width: 6, height: 6)
        player4Piece4eye2.isUserInteractionEnabled = false
        Player4Piece4.addChild(player4Piece4eye2)
        
        
        
        rolledDiceTexture.isHidden = true
        addChild(rolledDiceTexture)
        
        
        setupLabels()
        flashTurn(player: whosTurn)
        animations()
        updateScore()
        
    }
    
    
    
    override func willMove(from view: SKView) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "playAdID"), object: nil)
        self.removeAllChildren()
        self.removeAllActions()
    }
    
    
    // MOVE FUNCTIONS FROM HERE : <-------------------------------------------------------------------
    
    
    func movePiece() {
        
        
        
        
        noWinList()
        
        
        removeFlashPlayer(player: whosTurn)
        
        checkWin(player:whosTurn)
        self.disabletoEnter()
        
        
        self.diceRolled = false
        
        print("Moves Remainig and OneMoreMove are :")
        print(self.movesRemaining)
        print(self.OneMoreMove)
        print(self.whosTurn)
        
        print("All TOTAL Turns :")
        print(playerTturns)
        print(player2Tturns)
        print(player3Tturns)
        print(player4Tturns)
        
        if ( self.movesRemaining > 0) {
            
            
            self.moveFinished = false
            
            
            print("Moves Remaining =")
            print(self.movesRemaining)
            
            let currentSpace:Int = self.returnPlayerSpace(player: self.whosTurn)
            
            
            print("Who's Turn =")
            print(self.whosTurn)
            
            let spaceNumber:Int = currentSpace
            var nextSpace:Int = spaceNumber + 1
            
            
            
            if (nextSpace == 17 || nextSpace == 25 ) {
                
                nextSpace = self.realPath(nextPlace: nextSpace, player: self.whosTurn)
                
            }
            
            print("Blooded Or NOT ?")
            print(BloodedPlayer1)
            print(BloodedPlayer2)
            print(BloodedPlayer3)
            print(BloodedPlayer4)
            
            if (nextSpace == 17 || nextSpace == 5 || nextSpace == 9 || nextSpace == 13) {
                
                
                nextSpace = self.entrancePosition(nextPlace: nextSpace, player: self.whosTurn, cSpace: currentSpace)
                
            } else if (nextSpace == 23 || nextSpace == 21 || nextSpace == 19 || nextSpace == 25) {
                
                
                nextSpace = self.entrancePosition(nextPlace: nextSpace, player: self.whosTurn, cSpace: currentSpace)
                
            }
            
            
            
            //Print
            print("Next Space =")
            print(nextSpace)
            
            
            
            
            
            print("Node.Name AT Move Action : ")
            print(self.childNode(withName: String(nextSpace))!.name!)
            print(self.childNode(withName: String(nextSpace))!)
            
            if nextSpace != currentSpace {
                
                    self.IncreasePlayerScore(player: self.whosTurn, playerpiece: self.touchedNode)
                
            }
            
            
            let moveAction:SKAction = SKAction.move(to: (self.childNode(withName: String(nextSpace))?.position)!, duration: 0.35)
            moveAction.timingMode = .easeOut
            
            
            
            
            
            let aud = SKAction.run {
                
                self.playSound(soundName: self.moveAudio)
                
                if nextSpace == 25 {
                    
                    if (self.whosTurn == .Player1 && ((self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26) || (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece4 == 26 && self.currentSpacePlayer1Piece3 == 26) || (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece4 == 26) || (self.currentSpacePlayer1Piece4 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26)) ) || (self.whosTurn == .Player2 && ((self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26) || (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece4 == 26 && self.currentSpacePlayer2Piece3 == 26) || (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece4 == 26) || (self.currentSpacePlayer2Piece4 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26)) ) || (self.whosTurn == .Player3 && ((self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece3 == 26) || (self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece4 == 26 && self.currentSpacePlayer3Piece3 == 26) || (self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece4 == 26) || (self.currentSpacePlayer3Piece4 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece3 == 26)) ) || (self.whosTurn == .Player4 && ((self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece3 == 26) || (self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece4 == 26 && self.currentSpacePlayer4Piece3 == 26) || (self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece4 == 26) || (self.currentSpacePlayer4Piece4 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece3 == 26)) ) {
                        
                        self.playSound(soundName: self.finalWon)
                        
                    } else {
                        
                        self.playSound(soundName: self.oneWon)
                        
                    }
                    
                }
            }
            
            
            
            let wait:SKAction = SKAction.wait(forDuration: 0.2)
            
            let runAction:SKAction = SKAction.run({
                
                
                
                if (self.movesRemaining == 1) {
                    
                    print("Inside MovesRemaining Kill HIM IF Condition")
                    
                    self.KillHim(player:self.whosTurn, nextspace:nextSpace)
                    
                    //                    if nextSpace == 25 && ( ((self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26) || (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece4 == 26 && self.currentSpacePlayer1Piece3 == 26) || (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece4 == 26) || (self.currentSpacePlayer1Piece4 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26)) || ((self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26) || (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece4 == 26 && self.currentSpacePlayer2Piece3 == 26) || (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece4 == 26) || (self.currentSpacePlayer2Piece4 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26)) || ((self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece3 == 26) || (self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece4 == 26 && self.currentSpacePlayer3Piece3 == 26) || (self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece4 == 26) || (self.currentSpacePlayer3Piece4 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece3 == 26)) || ((self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece3 == 26) || (self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece4 == 26 && self.currentSpacePlayer4Piece3 == 26) || (self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece4 == 26) || (self.currentSpacePlayer4Piece4 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece3 == 26)) ) {
                    //
                    //                        if self.whosTurn == .Player1 {
                    //
                    //                            } else {
                    //
                    //                            }
                    //
                    //                    }
                    
                }
                
                
                print(nextSpace)
                
                self.setThePlayerSpace(space: nextSpace, player:self.whosTurn)
                
                if (self.movesRemaining == 1) {
                    self.Adjust(node: self.childNode(withName: String(nextSpace))!)
                }
                
                self.movesRemaining = self.movesRemaining - 1
                
                
                print("Moves Remaining")
                print(self.movesRemaining)
                
                self.movePiece()
                
            })
            
            
                
                print("The FINAL TIME Touched NODE is :")
                print(self.touchedNode)
                
                
            
                
                self.touchedNode.run(SKAction.sequence([moveAction, aud, wait, runAction])) {
                    
                    print("The Action /WAs SuccessFully Runned : ")
                }
            
            
            
            
        } else {
            
            // next player's turn
            
            
            
            
            if (self.OneMoreMove > 0 && self.whosTurn == .Player1 && !gameOver) {
                
                self.OneMoreMove -= 1
                
                if ((currentSpacePlayer1Piece1 + currentSpacePlayer1Piece2 + currentSpacePlayer1Piece3 + currentSpacePlayer1Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                print("In .Player1")
                print(self.whosTurn)
                self.Dice.isHidden = false
                self.flashTurn(player: self.whosTurn)
                keepAnimating = true
                animations()
                self.moveFinished = true
                
                if (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26 && self.currentSpacePlayer1Piece4 == 26) {
                    
                    //  enumerateChildNodes(withName: "dice") { (diceNode, stop) in
                    //      diceNode.removeFromParent()
                    //      print("Enumerated!")
                    //  }
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player1
                    
                    print("All are AT Damn 26 Yeah!")
                    self.movesRemaining = 0
                    
                    self.Dice.isHidden = true
                    self.OneMoreMove = 0
                    self.movePiece()
                    
                }
                
            } else if (self.OneMoreMove > 0 && self.whosTurn == .Player2 && !gameOver) {
                
                self.OneMoreMove -= 1
                
                if ((currentSpacePlayer2Piece1 + currentSpacePlayer2Piece2 + currentSpacePlayer2Piece3 + currentSpacePlayer2Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                self.flashTurn(player: self.whosTurn)
                print("In == .Player2")
                print(self.whosTurn)
                keepAnimating = true
                animations()
                self.Dice.isHidden = false
                self.moveFinished = true
                
                /////////-----  This Mostly Won't Work Because evantualy compiler will return here and Call the RollDice at the End. You Can Solve it by putting the existing Code into Else Condition, So it will Only Call The RollDice If the All Pieces Aren't At "25"       ---///////////
                
                
                if (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26 && self.currentSpacePlayer2Piece4 == 26) {
                    
                    // enumerateChildNodes(withName: "dice") { (diceNode, stop) in
                    //    diceNode.removeFromParent()
                    //     print("Enumerated!")
                    //  }
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player2
                    
                    print("All are AT Damn 26 Yeah!")
                    self.movesRemaining = 0
                    
                    self.Dice.isHidden = true
                    self.OneMoreMove = 0
                    self.movePiece()
                    
                }
                
                
                
                
                
                
            } else if (self.OneMoreMove > 0 && self.whosTurn == .Player3 && !gameOver) {
                
                self.OneMoreMove -= 1
                
                if ((currentSpacePlayer3Piece1 + currentSpacePlayer3Piece2 + currentSpacePlayer3Piece3 + currentSpacePlayer3Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                self.flashTurn(player: self.whosTurn)
                
                print("In == .Player3")
                print(self.whosTurn)
                Dice2.isHidden = false
                keepAnimating = true
                animations()
                self.moveFinished = true
                
                
                if (self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece3 == 26 && self.currentSpacePlayer3Piece4 == 26) {
                    
                    // enumerateChildNodes(withName: "dice") { (diceNode, stop) in
                    //    diceNode.removeFromParent()
                    //     print("Enumerated!")
                    //  }
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player3
                    
                    print("All are AT Damn 26 Yeah!")
                    self.movesRemaining = 0
                    
                    self.Dice2.isHidden = true
                    self.OneMoreMove = 0
                    self.movePiece()
                    
                }
                
                
                
                
                
            } else if (self.OneMoreMove > 0 && self.whosTurn == .Player4 && !gameOver) {
                
                self.OneMoreMove -= 1
                
                if ((currentSpacePlayer4Piece1 + currentSpacePlayer4Piece2 + currentSpacePlayer4Piece3 + currentSpacePlayer4Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                self.flashTurn(player: self.whosTurn)
                
                print("In == .Player4")
                print(self.whosTurn)
                Dice2.isHidden = false
                keepAnimating = true
                animations()
                self.moveFinished = true
                
                
                if (self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece3 == 26 && self.currentSpacePlayer4Piece4 == 26) {
                    
                    // enumerateChildNodes(withName: "dice") { (diceNode, stop) in
                    //    diceNode.removeFromParent()
                    //     print("Enumerated!")
                    //  }
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player4
                    
                    print("All are AT Damn 26 Yeah!")
                    self.movesRemaining = 0
                    
                    self.Dice2.isHidden = true
                    self.OneMoreMove = 0
                    self.movePiece()
                    
                }
                
                
                
                
                
            } else if (self.whosTurn == .Player1 && !gameOver) {
                
                
                self.moveFinished = true
                
                if ((currentSpacePlayer1Piece1 + currentSpacePlayer1Piece2 + currentSpacePlayer1Piece3 + currentSpacePlayer1Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                print(self.whosTurn)
                //flashPlayer(player: whosTurn)
                
                // flashTurn(player: whosTurn)
                
                //Print
                //  print("Who's Turn is Player2 NOW")
                
                
                if (self.currentSpacePlayer2Piece1 == 26 && self.currentSpacePlayer2Piece2 == 26 && self.currentSpacePlayer2Piece3 == 26 && self.currentSpacePlayer2Piece4 == 26) {
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player2
                    
                    print("All are AT Damn 26 Yeah!| In Who's Turn 2's Alternative | Player-2 |")
                    self.movesRemaining = 0
                    
                    self.movePiece()
                    
                } else {
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player2
                    // DincreamentScore()
                    print(self.whosTurn)
                    Dice.isHidden = false
                    
                    updateScore()
                    keepAnimating = true
                    animations()
                    
                    print("All are FINE! | WHOSTURN : Player 2 |")
                    
                    self.flashTurn(player: self.whosTurn)
                    
                }
                
                
                
                
                
            } else if (self.whosTurn == .Player2 && !gameOver) {
                
                // self.whosTurn = .Player3
                self.moveFinished = true
                
                if ((currentSpacePlayer2Piece1 + currentSpacePlayer2Piece2 + currentSpacePlayer2Piece3 + currentSpacePlayer2Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                //flashPlayer(player: whosTurn)
                
                rolledDiceTexture.isHidden = true
                print(self.whosTurn)
                // flashTurn(player: whosTurn)
                // print(whosTurn)
                
                
                // print("Who's Turn is Player3 NOW")
                
                
                if (self.currentSpacePlayer3Piece1 == 26 && self.currentSpacePlayer3Piece2 == 26 && self.currentSpacePlayer3Piece3 == 26 && self.currentSpacePlayer3Piece4 == 26) {
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player3
                    print(self.whosTurn)
                    Dice.isHidden = true
                    self.movesRemaining = 0
                    //flashTurn(player: whosTurn)
                    print("All are AT Damn 26 Yeah!| In Who's Turn 3's Alternative | Player-3 |")
                    self.movePiece()
                    
                } else {
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player3
                    // DincreamentScore()
                    Dice2.isHidden = false
                    print(self.whosTurn)
                    
                    updateScore()
                    keepAnimating = true
                    animations()
                    
                    self.flashTurn(player: self.whosTurn)
                    print("All are FINE! | WHOSTURN : Player 3 |")
                    
                    
                }
                
                
                
                
            } else if (self.whosTurn == .Player3 && !gameOver) {
                
                self.moveFinished = true
                
                if ((currentSpacePlayer3Piece1 + currentSpacePlayer3Piece2 + currentSpacePlayer3Piece3 + currentSpacePlayer3Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                rolledDiceTexture.isHidden = true
                print(self.whosTurn)
                
                if (self.currentSpacePlayer4Piece1 == 26 && self.currentSpacePlayer4Piece2 == 26 && self.currentSpacePlayer4Piece3 == 26 && self.currentSpacePlayer4Piece4 == 26) {
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player4
                    print(self.whosTurn)
                    //flashTurn(player: whosTurn)
                    self.movesRemaining = 0
                    print("All are AT Damn 26 Yeah!| In Who's Turn 4's Alternative | Player-4 |")
                    self.movePiece()
                    
                } else {
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player4
                    //DincreamentScore()
                    print(self.whosTurn)
                    Dice2.isHidden = false
                    
                    self.flashTurn(player: self.whosTurn)
                    
                    updateScore()
                    keepAnimating = true
                    animations()
                    
                    print("All are FINE! | WHOSTURN : Player 4 |")
                    
                }
                
                
                
                
            } else if (self.whosTurn == .Player4 && !gameOver) {
                
                
                
                if ((currentSpacePlayer4Piece1 + currentSpacePlayer4Piece2 + currentSpacePlayer4Piece3 + currentSpacePlayer4Piece4) < 104) {
                    
                    DincreamentScore()
                    
                }
                
                self.moveFinished = true
                rolledDiceTexture.isHidden = true
                print(self.whosTurn)
                if (self.currentSpacePlayer1Piece1 == 26 && self.currentSpacePlayer1Piece2 == 26 && self.currentSpacePlayer1Piece3 == 26 && self.currentSpacePlayer1Piece4 == 26) {
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.removeAnimation(player: self.whosTurn)
                    self.whosTurn = .Player1
                    self.movesRemaining = 0
                    Dice2.isHidden = true
                    print(self.whosTurn)
                    //flashTurn(player: whosTurn)
                    print("All are AT Damn 26 Yeah!| In Who's Turn 1's Alternative | Player-1 |")
                    self.movePiece()
                    
                } else {
                    
                    self.Dice.isHidden = false
                    
                    
                    self.removeFlashTurn(player: self.whosTurn)
                    self.whosTurn = .Player1
                    print(self.whosTurn)
                    //DincreamentScore()
                    keepAnimating = true
                    animations()
                    
                    self.flashTurn(player: self.whosTurn)
                    updateScore()
                    
                    print("All are FINE! | WHOSTURN : Player 1 |")
                    
                }
                
            } else if (gameOver == true) {
                print("Entered :")
                let gameOverScreen = SKScene(fileNamed: "EndScreen")
                
                Dice.isHidden = true
                Dice2.isHidden = true
                
                print(playerScore)
                print(com1Score)
                print(com2Score)
                print(com3Score)
                print(playerone)
                print(playertwo)
                print(playerthree)
                print(playerfour)
                print("At End :")
                print(place.count)
                
                print(playerOne.isEmpty)
                
                if place.count > 0 {
                    print("Added Player One :!")
                    print(place[0])
                    playerOne = place[0]
                    print(playerOne)
                }
                if place.count > 1 {
                    print(place[1])
                    playerTwo = place[1]
                    print(playerTwo)
                }
                if place.count > 2 {
                    print(place[2])
                    playerThree = place[2]
                    print(playerThree)
                }
                if place.count > 3 {
                    print(place[3])
                    playerFour = place[3]
                    print(playerFour)
                }
                
                if playerOne.isEmpty && playerTwo.isEmpty && playerThree.isEmpty && playerFour.isEmpty {
                    
                    playerOne = playerLabel.text!
                    playerTwo = player2Label.text!
                    playerThree = player3Label.text!
                    playerFour = player4Label.text!
                    print("In Last CON!")
                    
                }
                
                let delay = 0.5
                
                
                Adjust(node: childNode(withName: "25")!)
                GlobalVariablesPhone.PreviousScene = .VsFriendFour
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    
                    print("Yes!")
                    self.view?.presentScene(gameOverScreen!, transition: SKTransition.fade(withDuration: 1))
                    
                    
                }
                
                
            }
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // <------------------------------------------------------  RULES  ------------------------------------------------------->
    
    func KillHim(player:Player, nextspace:Int) {
        
        
        
        
        print("Inside Kill Him Func And nextspace is :")
        print(nextspace)
        print(player)
        print(currentSpacePlayer1Piece1)
        print(currentSpacePlayer1Piece2)
        print(currentSpacePlayer1Piece3)
        print(currentSpacePlayer1Piece4)
        
        print(currentSpacePlayer2Piece1)
        print(currentSpacePlayer2Piece2)
        print(currentSpacePlayer2Piece3)
        print(currentSpacePlayer2Piece4)
        
        print(currentSpacePlayer3Piece1)
        print(currentSpacePlayer3Piece2)
        print(currentSpacePlayer3Piece3)
        print(currentSpacePlayer3Piece4)
        
        print(currentSpacePlayer4Piece1)
        print(currentSpacePlayer4Piece2)
        print(currentSpacePlayer4Piece3)
        print(currentSpacePlayer4Piece4)
        
        
        
        
        if (nextspace == 1 || nextspace == 5 || nextspace == 9 || nextspace == 13 || nextspace == 25) {
            return
        }
        
        
        
        if (player == .Player1) {
            
            
            if (nextspace == currentSpacePlayer2Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece1)
                
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece1 = 5
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        print("Inside the if NODE name is == 5")
                        
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece1)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        
                        //Player2Piece1.run(moveAction) {
                        
                        playSound(soundName: killedAudio)
                        Player2Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece2 = 5
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        
                        print("Inside the if NODE name is == 5")
                        
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece2)
                        
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        playSound(soundName: killedAudio)
                        Player2Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece3 = 5
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece3)
                        
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        playSound(soundName: killedAudio)
                        Player2Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece4 = 5
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece4)
                        
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        
                        playSound(soundName: killedAudio)
                        Player2Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
            } else if (nextspace == currentSpacePlayer3Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece1 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece1)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece1.run(moveAction) {
                            
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece2 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece2)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece3 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece3)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece4 = 9
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece4)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer4Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece1 = 13
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece1)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece2 = 13
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece2)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece3 = 13
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece3)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece4 = 13
                        self.BloodedPlayer1 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece4)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                    }
                }
            }
            
            
            
            //   <---------------------------------------------  PLAYER-2  -------------------------------------------------------------------------------->
            
            
            
        } else if (player == .Player2) {
            
            
            
            
            
            if (nextspace == currentSpacePlayer1Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece1 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece1)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece2 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece2)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece3 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece3)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece4 = 1
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece4)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer3Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece1 = 9
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece1)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece2 = 9
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece2)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece3 = 9
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece3)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer3Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece4 = 9
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece4)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer4Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece1 = 13
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece1)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece2 = 13
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece2)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece3 = 13
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece3)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece4 = 13
                        self.BloodedPlayer2 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece4)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //   <---------------------------------------------  PLAYER-3  -------------------------------------------------------------------------------->
            
            
            
        } else if (player == .Player3) {
            
            
            
            
            
            
            if (nextspace == currentSpacePlayer1Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece1 = 1
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece1)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece2 = 1
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece2)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece3 = 1
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece3)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece4 = 1
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece4)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer2Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece1 = 5
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece1)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece2 = 5
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece2)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece3 = 5
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece3)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece4 = 5
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece4)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer4Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece1 = 13
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece1)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece2 = 13
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece2)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece3 = 13
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece3)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer4Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer4Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("13") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer4Piece4 = 13
                        self.BloodedPlayer3 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player4Piece4)
                        if dontScoreP4 {
                            dontScoreP4 = false
                        }
                        playSound(soundName: killedAudio)
                        Player4Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //   <---------------------------------------------  PLAYER-4  -------------------------------------------------------------------------------->
            
            
            
            
        }else if (player == .Player4) {
            
            
            
            
            if (nextspace == currentSpacePlayer1Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece1 = 1
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece1)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece2 = 1
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece2)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece3 = 1
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece3)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer1Piece4) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer1Piece4)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("1") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer1Piece4 = 1
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player1Piece4)
                        if dontScoreP1 {
                            dontScoreP1 = false
                        }
                        playSound(soundName: killedAudio)
                        Player1Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer2Piece1) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece1)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece1 = 5
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece1)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece2) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece2)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece2 = 5
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece2)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece3) {
                
                print("Next space and Current Space of Player :")
                print(nextspace)
                print(currentSpacePlayer2Piece3)
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece3 = 5
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece3)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer2Piece4) {
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("5") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer2Piece4 = 5
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player2Piece4)
                        if dontScoreP2 {
                            dontScoreP2 = false
                        }
                        playSound(soundName: killedAudio)
                        Player2Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
                
                
                
                
            } else if (nextspace == currentSpacePlayer3Piece1) {
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece1 = 9
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece1)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece1.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece2) {
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece2 = 9
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece2)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece2.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece3) {
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece3 = 9
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece3)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece3.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
                
            } else if (nextspace == currentSpacePlayer3Piece4) {
                
                for node in children {
                    
                    print("Inside the node in children :")
                    
                    if node.name == ("9") {
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        self.currentSpacePlayer3Piece4 = 9
                        self.BloodedPlayer4 = true
                        self.OneMoreMove += 1
                        self.changeFace(player: player)
                        
                        self.updateScore()
                        print("Inside the if NODE name is == 5")
                        
                        let moveAction:SKAction = SKAction.move(to: node.position, duration: 0.5)
                        
                        print(node.name!)
                        
                        DincreamentPlayerScore(player: player, killedPiece: Player3Piece4)
                        if dontScoreP3 {
                            dontScoreP3 = false
                        }
                        playSound(soundName: killedAudio)
                        Player3Piece4.run(moveAction) {
                            print("Move Action Was Runned :")
                            
                            
                            self.Adjust(node: node)
                            
                            self.notBloodedAnyMore()
                        }
                        
                        
                    }
                }
            }
            
            
            
            
        }
        print(currentSpacePlayer1Piece1)
        print(currentSpacePlayer1Piece2)
        print(currentSpacePlayer1Piece3)
        print(currentSpacePlayer1Piece4)
        
        print(currentSpacePlayer2Piece1)
        print(currentSpacePlayer2Piece2)
        print(currentSpacePlayer2Piece3)
        print(currentSpacePlayer2Piece4)
        
        print(currentSpacePlayer3Piece1)
        print(currentSpacePlayer3Piece2)
        print(currentSpacePlayer3Piece3)
        print(currentSpacePlayer3Piece4)
        
        print(currentSpacePlayer4Piece1)
        print(currentSpacePlayer4Piece2)
        print(currentSpacePlayer4Piece3)
        print(currentSpacePlayer4Piece4)
        
        print("All TOTAL Turns :")
        print(playerTturns)
        print(player2Tturns)
        print(player3Tturns)
        print(player4Tturns)
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // FIRST OF ALL CLOSE THE MAIN WAY FOR 2,3 AND 4 PLAYERS THEN IT WILL WORK
    // Change the space with new positions so it will change the nextSpace to it so automatically nextSpace = EntrancePosition ;)
    // And also short those sentances so the code became clean and small and with less loops just like u did up There
    // Make that sentance a Variable
    func entrancePosition(nextPlace:Int, player:Player, cSpace:Int) -> Int {
        
        var newLocation = nextPlace
        
        print("Indside Entrance :::::  CHHOO MANTAR ")
        
        print(currentSpacePlayer1Piece1)
        print(currentSpacePlayer1Piece2)
        print(currentSpacePlayer1Piece3)
        print(currentSpacePlayer1Piece4)
        
        print(currentSpacePlayer2Piece1)
        print(currentSpacePlayer2Piece2)
        print(currentSpacePlayer2Piece3)
        print(currentSpacePlayer2Piece4)
        
        print(currentSpacePlayer3Piece1)
        print(currentSpacePlayer3Piece2)
        print(currentSpacePlayer3Piece3)
        print(currentSpacePlayer3Piece4)
        
        print(currentSpacePlayer4Piece1)
        print(currentSpacePlayer4Piece2)
        print(currentSpacePlayer4Piece3)
        print(currentSpacePlayer4Piece4)
        
        
        
        if (nextPlace == 17 || nextPlace == 5 || nextPlace == 9 || nextPlace == 13) {
            
            if (player == .Player1 && nextPlace == 17 && cSpace == 16) {
                
                
                if (BloodedPlayer1 == true) {
                    print("You are the BadAss Man!")
                    newLocation = 17
                    print(BloodedPlayer1)
                    
                } else if (!BloodedPlayer1) {
                    print("Sorry Kid you aren't MAN Yet!")
                    newLocation = 16
                    movesRemaining = 1
                    print(BloodedPlayer1)
                    
                }
                
                
            } else if (player == .Player2 && nextPlace == 5) {
                
                if (BloodedPlayer2 == true) {
                    print("You are the BadAss Man!")
                    newLocation = 23
                    print(BloodedPlayer2)
                    
                } else if (!BloodedPlayer2) {
                    print("Sorry Kid you aren't MAN Yet!")
                    newLocation = 4
                    movesRemaining = 1
                    print(BloodedPlayer2)
                    
                }
                
                
            } else if (player == .Player3 && nextPlace == 9) {
                
                if (BloodedPlayer3 == true) {
                    print("You are the BadAss Man!")
                    newLocation = 21
                    print(BloodedPlayer3)
                    
                    
                } else if (!BloodedPlayer3) {
                    print("Sorry Kid you aren't MAN Yet!")
                    newLocation = 8
                    movesRemaining = 1
                    print(BloodedPlayer3)
                    
                }
                
            } else if (player == .Player4 && nextPlace == 13) {
                
                if (BloodedPlayer4 == true) {
                    
                    print("You are the BadAss Man!")
                    newLocation = 19
                    print(BloodedPlayer4)
                    
                } else if (!BloodedPlayer4) {
                    
                    print("Sorry Kid you aren't MAN Yet!")
                    newLocation = 12
                    movesRemaining = 1
                    print(BloodedPlayer4)
                }
                
            }
            
        } else {
            
            if (player == .Player1 && nextPlace == 25) {
                
                if ((currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26)) {
                    
                    print("If ALL ANY 3 ARE AT 26 Player 1 :")
                    
                    if ((currentSpacePlayer1Piece1 + movesRemaining == 25) || (currentSpacePlayer1Piece2 + movesRemaining == 25) || (currentSpacePlayer1Piece3 + movesRemaining == 25) || (currentSpacePlayer1Piece4 + movesRemaining == 25)) {
                        
                        print("Inside GOT PERFECT ROLLLED DICE : PlaYER 1 :")
                        
                        newLocation = 25
                        
                        if touchedNode == Player1Piece1 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece1Score += 1
                        } else if touchedNode == Player1Piece2 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece2Score += 1
                        } else if touchedNode == Player1Piece3 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece3Score += 1
                        } else if touchedNode == Player1Piece4 {
                            print("In Inrease Pla1 Score For LASTA :")
                            player1Piece4Score += 1
                        }
                        
                        playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
                        playerScoreLabel.text = "\(playerScore)"
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        
                    } else {
                        print("ALL 3 ARE AT 26 AND + Rolled Dice != 25 : PLayer 1")
                        newLocation = 17
                    }
                    
                } else {
                    print("In REGULAR ONE For GO TO 25 : Player 1 :")
                    newLocation = 25
                    
                    if self.oneMoreMove.isHidden {
                        self.notifyForPlusMove(label: self.oneMoreMove)
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                }
                
            } else if (player == .Player2 && nextPlace == 23) {
                
                
                
                if ((currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26)) {
                    
                    print("If ALL ANY 3 ARE AT 26 Player 2 :")
                    
                    if ((((currentSpacePlayer2Piece1 == 23 || currentSpacePlayer2Piece1 == 24) && (currentSpacePlayer2Piece1 + movesRemaining == 31)) || ((currentSpacePlayer2Piece1 >= 17 && currentSpacePlayer2Piece1 <= 22) && (currentSpacePlayer2Piece1 + movesRemaining == 23))) || (((currentSpacePlayer2Piece2 == 23 || currentSpacePlayer2Piece2 == 24) && (currentSpacePlayer2Piece2 + movesRemaining == 31)) || ((currentSpacePlayer2Piece2 >= 17 && currentSpacePlayer2Piece2 <= 22) && (currentSpacePlayer2Piece2 + movesRemaining == 23))) || (((currentSpacePlayer2Piece3 == 23 || currentSpacePlayer2Piece3 == 24) && (currentSpacePlayer2Piece3 + movesRemaining == 31)) || ((currentSpacePlayer2Piece3 >= 17 && currentSpacePlayer2Piece3 <= 22) && (currentSpacePlayer2Piece3 + movesRemaining == 23))) || (((currentSpacePlayer2Piece4 == 23 || currentSpacePlayer2Piece4 == 24) && (currentSpacePlayer2Piece4 + movesRemaining == 31)) || ((currentSpacePlayer2Piece4 >= 17 && currentSpacePlayer2Piece4 <= 22) && (currentSpacePlayer2Piece4 + movesRemaining == 23)))) {
                        
                        print("Inside GOT PERFECT ROLLLED DICE : PlaYER 2 :")
                        
                        newLocation = 25
                        
                        if touchedNode == Player2Piece1 {
                            print("In Inrease Pla2 Score For LASTA :")
                            com1Piece1Score += 1
                        } else if touchedNode == Player2Piece2 {
                            print("In Inrease Pla2 Score For LASTA :")
                            com1Piece2Score += 1
                        } else if touchedNode == Player2Piece3 {
                            print("In Inrease Pla2 Score For LASTA :")
                            com1Piece3Score += 1
                        } else if touchedNode == Player2Piece4 {
                            print("In Inrease Pla2 Score For LASTA :")
                            com1Piece4Score += 1
                        }
                        
                        com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
                        com1ScoreLabel.text = "\(com1Score)"
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        
                    } else {
                        print("ALL 3 ARE AT 26 AND + Rolled Dice != 25 : PLayer 2")
                        newLocation = 23
                    }
                    
                } else {
                    print("In REGULAR ONE For GO TO 25 : Player 2 :")
                    newLocation = 25
                    
                    if self.oneMoreMove.isHidden {
                        self.notifyForPlusMove(label: self.oneMoreMove)
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                }
                
                
                
            } else if (player == .Player3 && nextPlace == 21) {
                
                
                if ((currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece1 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26)) {
                    
                    print("If ALL ANY 3 ARE AT 26 Player 3 :")
                    
                    if ((((currentSpacePlayer3Piece1 >= 21 && currentSpacePlayer3Piece1 <= 24) && (currentSpacePlayer3Piece1 + movesRemaining == 29)) || ((currentSpacePlayer3Piece1 >= 17 && currentSpacePlayer3Piece1 <= 20) && (currentSpacePlayer3Piece1 + movesRemaining == 21))) || (((currentSpacePlayer3Piece2 >= 21 && currentSpacePlayer3Piece2 <= 24) && (currentSpacePlayer3Piece2 + movesRemaining == 29)) || ((currentSpacePlayer3Piece2 >= 17 && currentSpacePlayer3Piece2 <= 20) && (currentSpacePlayer3Piece2 + movesRemaining == 21))) || (((currentSpacePlayer3Piece3 >= 21 && currentSpacePlayer3Piece3 <= 24) && (currentSpacePlayer3Piece3 + movesRemaining == 29)) || ((currentSpacePlayer3Piece3 >= 17 && currentSpacePlayer3Piece3 <= 20) && (currentSpacePlayer3Piece3 + movesRemaining == 21))) || (((currentSpacePlayer3Piece4 >= 21 && currentSpacePlayer3Piece4 <= 24) && (currentSpacePlayer3Piece4 + movesRemaining == 29)) || ((currentSpacePlayer3Piece4 >= 17 && currentSpacePlayer3Piece4 <= 20) && (currentSpacePlayer3Piece4 + movesRemaining == 21)))) {
                        
                        print("Inside GOT PERFECT ROLLLED DICE : PlaYER 3 :")
                        
                        newLocation = 25
                        
                        if touchedNode == Player3Piece1 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com2Piece1Score += 1
                        } else if touchedNode == Player3Piece2 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com2Piece2Score += 1
                        } else if touchedNode == Player3Piece3 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com2Piece3Score += 1
                        } else if touchedNode == Player3Piece4 {
                            print("In Inrease Pla3 Score For LASTA :")
                            com2Piece4Score += 1
                        }
                        
                        com2Score = (com2Piece1Score - 1) + (com2Piece2Score - 1) + (com2Piece3Score - 1) + (com2Piece4Score - 1)
                        com2ScoreLabel.text = "\(com2Score)"
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        
                    } else {
                        print("ALL 3 ARE AT 26 AND + Rolled Dice != 25 : PLayer 3")
                        newLocation = 21
                    }
                    
                } else {
                    print("In REGULAR ONE For GO TO 25 : Player 3 :")
                    newLocation = 25
                    
                    if self.oneMoreMove.isHidden {
                        self.notifyForPlusMove(label: self.oneMoreMove)
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                }
                
                
                
                
            } else if (player == .Player4 && nextPlace == 19) {
                
                
                if ((currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece1 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26)) {
                    
                    print("If ALL ANY 3 ARE AT 26 Player 4 :")
                    
                    if ((((currentSpacePlayer4Piece1 == 17 || currentSpacePlayer4Piece1 == 18) && (currentSpacePlayer4Piece1 + movesRemaining == 19)) || ((currentSpacePlayer4Piece1 >= 19 && currentSpacePlayer4Piece1 <= 24) && (currentSpacePlayer4Piece1 + movesRemaining == 27))) || (((currentSpacePlayer4Piece2 == 17 || currentSpacePlayer4Piece2 == 18) && (currentSpacePlayer4Piece2 + movesRemaining == 19)) || ((currentSpacePlayer4Piece2 >= 19 && currentSpacePlayer4Piece2 <= 24) && (currentSpacePlayer4Piece2 + movesRemaining == 27))) || (((currentSpacePlayer4Piece3 == 19 || currentSpacePlayer4Piece3 == 18) && (currentSpacePlayer4Piece3 + movesRemaining == 19)) || ((currentSpacePlayer4Piece3 >= 19 && currentSpacePlayer4Piece3 <= 24) && (currentSpacePlayer4Piece3 + movesRemaining == 27))) || (((currentSpacePlayer4Piece4 == 17 || currentSpacePlayer4Piece4 == 18) && (currentSpacePlayer4Piece4 + movesRemaining == 19)) || ((currentSpacePlayer4Piece4 >= 19 && currentSpacePlayer4Piece4 <= 24) && (currentSpacePlayer4Piece4 + movesRemaining == 27)))) {
                        
                        print("Inside GOT PERFECT ROLLLED DICE : PlaYER 4 :")
                        
                        newLocation = 25
                        
                        if touchedNode == Player4Piece1 {
                            print("In Inrease Pla4 Score For LASTA :")
                            com3Piece1Score += 1
                        } else if touchedNode == Player4Piece2 {
                            print("In Inrease Pla4 Score For LASTA :")
                            com3Piece2Score += 1
                        } else if touchedNode == Player4Piece3 {
                            print("In Inrease Pla4 Score For LASTA :")
                            com3Piece3Score += 1
                        } else if touchedNode == Player4Piece4 {
                            print("In Inrease Pla4 Score For LASTA :")
                            com3Piece4Score += 1
                        }
                        
                        com3Score = (com3Piece1Score - 1) + (com3Piece2Score - 1) + (com3Piece3Score - 1) + (com3Piece4Score - 1)
                        com3ScoreLabel.text = "\(com3Score)"
                        
                        if self.oneMoreMove.isHidden {
                            self.notifyForPlusMove(label: self.oneMoreMove)
                        } else {
                            let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                            self.notifyForPlusMove2(label: label)
                        }
                        
                    } else {
                        print("ALL 3 ARE AT 26 AND + Rolled Dice != 25 : PLayer 4")
                        newLocation = 19
                    }
                    
                } else {
                    print("In REGULAR ONE For GO TO 25 : Player 4 :")
                    newLocation = 25
                    
                    if self.oneMoreMove.isHidden {
                        self.notifyForPlusMove(label: self.oneMoreMove)
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                }
                
                
                
            }
            
        }
        
        
        
        print("In Entrance NextSpace is :")
        print(newLocation)
        return newLocation
    }
    
    
    
    func notBloodedAnyMore() {
        
        
        
        if (currentSpacePlayer1Piece1 == 1 && currentSpacePlayer1Piece2 == 1 && currentSpacePlayer1Piece3 == 1 && currentSpacePlayer1Piece4 == 1) {
            BloodedPlayer1 = false
            self.changeFace(player: .Player1)
            print("You LOST IT Player 1 !!!")
        }
        
        
        if (currentSpacePlayer2Piece1 == 5 && currentSpacePlayer2Piece2 == 5 && currentSpacePlayer2Piece3 == 5 && currentSpacePlayer2Piece4 == 5) {
            BloodedPlayer2 = false
            self.changeFace(player: .Player2)
            print("You LOST IT Player 2 !!!")
        }
        
        
        if (currentSpacePlayer3Piece1 == 9 && currentSpacePlayer3Piece2 == 9 && currentSpacePlayer3Piece3 == 9 && currentSpacePlayer3Piece4 == 9) {
            BloodedPlayer3 = false
            self.changeFace(player: .Player3)
            print("You LOST IT Player 3 !!!")
        }
        
        
        if (currentSpacePlayer4Piece1 == 13 && currentSpacePlayer4Piece2 == 13 && currentSpacePlayer4Piece3 == 13 && currentSpacePlayer4Piece4 == 13) {
            BloodedPlayer4 = false
            self.changeFace(player: .Player4)
            print("You LOST IT Player 4 !!!")
        }
        
        
        
    }
    
    
    
    
    func changeFace(player: Player) {
        
        if player == .Player1 {
            
            if BloodedPlayer1 {
                
                if currentSpacePlayer1Piece1 != 26 {
                    Player1Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                if currentSpacePlayer1Piece2 != 26 {
                    Player1Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                if currentSpacePlayer1Piece3 != 26 {
                    Player1Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                if currentSpacePlayer1Piece4 != 26 {
                    Player1Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri Angry")!)
                }
                
                //                Player1Piece1 = SKSpriteNode(imageNamed: "Kakri Angry")
                //                Player1Piece2 = SKSpriteNode(imageNamed: "Kakri Angry")
                //                Player1Piece3 = SKSpriteNode(imageNamed: "Kakri Angry")
                //                Player1Piece4 = SKSpriteNode(imageNamed: "Kakri Angry")
                
            } else if !BloodedPlayer1 {
                
                
                if currentSpacePlayer1Piece1 != 26 {
                    Player1Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                if currentSpacePlayer1Piece2 != 26 {
                    Player1Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                if currentSpacePlayer1Piece3 != 26 {
                    Player1Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                if currentSpacePlayer1Piece4 != 26 {
                    Player1Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri")!)
                }
                
                //                Player1Piece1 = SKSpriteNode(imageNamed: "Kakri")
                //                Player1Piece2 = SKSpriteNode(imageNamed: "Kakri")
                //                Player1Piece3 = SKSpriteNode(imageNamed: "Kakri")
                //                Player1Piece4 = SKSpriteNode(imageNamed: "Kakri")
                
            }
            
            
        } else if player == .Player2 {
            
            if BloodedPlayer2 {
                
                if currentSpacePlayer2Piece1 != 26 {
                    Player2Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                if currentSpacePlayer2Piece2 != 26 {
                    Player2Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                if currentSpacePlayer2Piece3 != 26 {
                    Player2Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                if currentSpacePlayer2Piece4 != 26 {
                    Player2Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri3 Angry")!)
                }
                
                //                Player2Piece1 = SKSpriteNode(imageNamed: "Kakri3 Angry")
                //                Player2Piece2 = SKSpriteNode(imageNamed: "Kakri3 Angry")
                //                Player2Piece3 = SKSpriteNode(imageNamed: "Kakri3 Angry")
                //                Player2Piece4 = SKSpriteNode(imageNamed: "Kakri3 Angry")
                
            } else if !BloodedPlayer2 {
                
                if currentSpacePlayer2Piece1 != 26 {
                    Player2Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                if currentSpacePlayer2Piece2 != 26 {
                    Player2Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                if currentSpacePlayer2Piece3 != 26 {
                    Player2Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                if currentSpacePlayer2Piece4 != 26 {
                    Player2Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri3")!)
                }
                
                //                Player2Piece1 = SKSpriteNode(imageNamed: "Kakri3")
                //                Player2Piece2 = SKSpriteNode(imageNamed: "Kakri3")
                //                Player2Piece3 = SKSpriteNode(imageNamed: "Kakri3")
                //                Player2Piece4 = SKSpriteNode(imageNamed: "Kakri3")
                
            }
            
            
        } else if player == .Player3 {
            
            if BloodedPlayer3 {
                
                
                
                if currentSpacePlayer3Piece1 != 26 {
                    Player3Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri2 Angry")!)
                }
                if currentSpacePlayer3Piece2 != 26 {
                    Player3Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri2 Angry")!)
                }
                if currentSpacePlayer3Piece3 != 26 {
                    Player3Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri2 Angry")!)
                }
                if currentSpacePlayer3Piece4 != 26 {
                    Player3Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri2 Angry")!)
                }
                
                
                
                //                Player3Piece1 = SKSpriteNode(imageNamed: "Kakri2 Angry")
                //                Player3Piece2 = SKSpriteNode(imageNamed: "Kakri2 Angry")
                //                Player3Piece3 = SKSpriteNode(imageNamed: "Kakri2 Angry")
                //                Player3Piece4 = SKSpriteNode(imageNamed: "Kakri2 Angry")
                
            } else if !BloodedPlayer3 {
                
                if currentSpacePlayer3Piece1 != 26 {
                    Player3Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri2")!)
                }
                if currentSpacePlayer3Piece2 != 26 {
                    Player3Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri2")!)
                }
                if currentSpacePlayer3Piece3 != 26 {
                    Player3Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri2")!)
                }
                if currentSpacePlayer3Piece4 != 26 {
                    Player3Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri2")!)
                }
                
                //                Player3Piece1 = SKSpriteNode(imageNamed: "Kakri2")
                //                Player3Piece2 = SKSpriteNode(imageNamed: "Kakri2")
                //                Player3Piece3 = SKSpriteNode(imageNamed: "Kakri2")
                //                Player3Piece4 = SKSpriteNode(imageNamed: "Kakri2")
                
            }
            
            
        } else if player == .Player4 {
            
            if BloodedPlayer4 {
                
                if currentSpacePlayer4Piece1 != 26 {
                    Player4Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri4 Angry")!)
                }
                if currentSpacePlayer4Piece2 != 26 {
                    Player4Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri4 Angry")!)
                }
                if currentSpacePlayer4Piece3 != 26 {
                    Player4Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri4 Angry")!)
                }
                if currentSpacePlayer4Piece4 != 26 {
                    Player4Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri4 Angry")!)
                }
                
                //                Player4Piece1 = SKSpriteNode(imageNamed: "Kakri4 Angry")
                //                Player4Piece2 = SKSpriteNode(imageNamed: "Kakri4 Angry")
                //                Player4Piece3 = SKSpriteNode(imageNamed: "Kakri4 Angry")
                //                Player4Piece4 = SKSpriteNode(imageNamed: "Kakri4 Angry")
                
            } else if !BloodedPlayer4 {
                
                if currentSpacePlayer4Piece1 != 26 {
                    Player4Piece1.texture = SKTexture(image: UIImage.init(named: "Kakri4")!)
                }
                if currentSpacePlayer4Piece2 != 26 {
                    Player4Piece2.texture = SKTexture(image: UIImage.init(named: "Kakri4")!)
                }
                if currentSpacePlayer4Piece3 != 26 {
                    Player4Piece3.texture = SKTexture(image: UIImage.init(named: "Kakri4")!)
                }
                if currentSpacePlayer4Piece4 != 26 {
                    Player4Piece4.texture = SKTexture(image: UIImage.init(named: "Kakri4")!)
                }
                
                //                Player4Piece1 = SKSpriteNode(imageNamed: "Kakri4")
                //                Player4Piece2 = SKSpriteNode(imageNamed: "Kakri4")
                //                Player4Piece3 = SKSpriteNode(imageNamed: "Kakri4")
                //                Player4Piece4 = SKSpriteNode(imageNamed: "Kakri4")
                
            }
            
            
        }
        
        
        
    }
    
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    func setThePlayerSpaceBack(space:Int, Killed:SKSpriteNode) {
        
        print("Inside SetThePlayerSpaceBack :")
        
        
        if Killed.name == "Player1Piece1" {
            
            currentSpacePlayer1Piece1 = space
            
        } else if Killed.name == "Player1Piece2" {
            
            currentSpacePlayer1Piece2 = space
            
        } else if Killed.name == "Player1Piece3" {
            
            currentSpacePlayer1Piece3 = space
            
        } else if Killed.name == "Player1Piece4" {
            
            currentSpacePlayer1Piece4 = space
            
        } else if Killed.name == "Player2Piece1" {
            
            currentSpacePlayer2Piece1 = space
            
        } else if Killed.name == "Player2Piece2" {
            
            currentSpacePlayer2Piece2 = space
            
        } else if Killed.name == "Player2Piece3" {
            
            currentSpacePlayer2Piece3 = space
            
        } else if Killed.name == "Player2Piece4" {
            
            currentSpacePlayer2Piece4 = space
            
        } else if Killed.name == "Player3Piece1" {
            
            currentSpacePlayer3Piece1 = space
            
        } else if Killed.name == "Player3Piece2" {
            
            currentSpacePlayer3Piece2 = space
            
        } else if Killed.name == "Player3Piece3" {
            
            currentSpacePlayer3Piece3 = space
            
        } else if Killed.name == "Player3Piece4" {
            
            currentSpacePlayer3Piece4 = space
            
        } else if Killed.name == "Player4Piece1" {
            
            currentSpacePlayer4Piece1 = space
            
        } else if Killed.name == "Player4Piece2" {
            
            currentSpacePlayer4Piece2 = space
            
        } else if Killed.name == "Player4Piece3" {
            
            currentSpacePlayer4Piece3 = space
            
        } else if Killed.name == "Player4Piece4" {
            
            currentSpacePlayer4Piece4 = space
            
        }
        
        
        //Print
        print("Next space in set the player space is :")
        
        print(space)
        print("set the Player Space =")
        print(space)
        
    }
    
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    func setThePlayerSpace(space:Int, player:Player) {
        
        if (player == .Player1) {
            
            
            if touchedNode.name == "Player1Piece1" {
                
                currentSpacePlayer1Piece1 = space
                
            } else if touchedNode.name == "Player1Piece2" {
                
                currentSpacePlayer1Piece2 = space
                
            } else if touchedNode.name == "Player1Piece3" {
                
                currentSpacePlayer1Piece3 = space
                
            } else if touchedNode.name == "Player1Piece4" {
                
                currentSpacePlayer1Piece4 = space
                
            }
            
        } else if (player == .Player2) {
            
            
            if touchedNode.name == "Player2Piece1" {
                
                currentSpacePlayer2Piece1 = space
                
            } else if touchedNode.name == "Player2Piece2" {
                
                currentSpacePlayer2Piece2 = space
                
            } else if touchedNode.name == "Player2Piece3" {
                
                currentSpacePlayer2Piece3 = space
                
            } else if touchedNode.name == "Player2Piece4" {
                
                currentSpacePlayer2Piece4 = space
                
            }
            
            
        } else if (player == .Player3) {
            
            
            if touchedNode.name == "Player3Piece1" {
                
                currentSpacePlayer3Piece1 = space
                
            } else if touchedNode.name == "Player3Piece2" {
                
                currentSpacePlayer3Piece2 = space
                
            } else if touchedNode.name == "Player3Piece3" {
                
                currentSpacePlayer3Piece3 = space
                
            } else if touchedNode.name == "Player3Piece4" {
                
                currentSpacePlayer3Piece4 = space
                
            }
            
            
        } else if (player == .Player4) {
            
            
            if touchedNode.name == "Player4Piece1" {
                
                currentSpacePlayer4Piece1 = space
                
            } else if touchedNode.name == "Player4Piece2" {
                
                currentSpacePlayer4Piece2 = space
                
            } else if touchedNode.name == "Player4Piece3" {
                
                currentSpacePlayer4Piece3 = space
                
            } else if touchedNode.name == "Player4Piece4" {
                
                currentSpacePlayer4Piece4 = space
                
            }
            
            
        }
        
        //Print
        print("set the Player Space =")
        print(space)
        
    }
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    func returnPlayerSpace(player:Player) -> Int {
        
        var space:Int = 0
        
        
        
        if (player == .Player1) {
            
            if touchedNode.name == "Player1Piece1" {
                
                space = currentSpacePlayer1Piece1
                
            } else if touchedNode.name == "Player1Piece2" {
                
                space = currentSpacePlayer1Piece2
                
            } else if touchedNode.name == "Player1Piece3" {
                
                space = currentSpacePlayer1Piece3
                
            } else if touchedNode.name == "Player1Piece4" {
                
                space = currentSpacePlayer1Piece4
                
            }
            
        } else if (player == .Player2) {
            
            //print("Player NAME IN RETURN PLAYER SPACE IS :")
            // print(touchedNode.name!)
            
            if touchedNode.name == "Player2Piece1" {
                
                space = currentSpacePlayer2Piece1
                
            } else if touchedNode.name == "Player2Piece2" {
                
                space = currentSpacePlayer2Piece2
                
            } else if touchedNode.name == "Player2Piece3" {
                
                space = currentSpacePlayer2Piece3
                
            } else if touchedNode.name == "Player2Piece4" {
                
                space = currentSpacePlayer2Piece4
                
            }
            
            // space = currentSpacePlayer2Piece1
            
        }  else if (player == .Player3) {
            
            // print("Player NAME IN RETURN PLAYER SPACE IS :")
            // print(touchedNode.name!)
            
            if touchedNode.name == "Player3Piece1" {
                
                space = currentSpacePlayer3Piece1
                
            } else if touchedNode.name == "Player3Piece2" {
                
                space = currentSpacePlayer3Piece2
                
            } else if touchedNode.name == "Player3Piece3" {
                
                space = currentSpacePlayer3Piece3
                
            } else if touchedNode.name == "Player3Piece4" {
                
                space = currentSpacePlayer3Piece4
                
            }
            
            // space = currentSpacePlayer3Piece1
            
        } else if (player == .Player4) {
            
            // print("Player NAME IN RETURN PLAYER SPACE IS :")
            // print(touchedNode.name!)
            
            if touchedNode.name == "Player4Piece1" {
                
                space = currentSpacePlayer4Piece1
                
            } else if touchedNode.name == "Player4Piece2" {
                
                space = currentSpacePlayer4Piece2
                
            } else if touchedNode.name == "Player4Piece3" {
                
                space = currentSpacePlayer4Piece3
                
            } else if touchedNode.name == "Player4Piece4" {
                
                space = currentSpacePlayer4Piece4
                
            }
            
            // space = currentSpacePlayer4Piece1
            
        }
        
        
        //Print
        print("Return Player Space =")
        print(space)
        
        return space
    }
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    func realPath( nextPlace:Int, player:Player) -> Int {
        
        var newLocation = nextPlace
        
        
        if (nextPlace == 17) {
            
            
            print("Next Space in the ==17 IF CONDITION : ")
            print(nextPlace)
            
            if (player == .Player2 || player == .Player3 || player == .Player4) {
                
                newLocation = 1
                
                
                print("Second If Condition Next Space : ")
                print(newLocation)
                print("WhosTurn in Second IF : ")
                print(whosTurn)
            }
        } else if (nextPlace == 25) {
            
            
            print("Next Space in the ==25 IF CONDITION : ")
            print(nextPlace)
            
            if (player == .Player2 || player == .Player3 || player == .Player4) {
                
                newLocation = 17
                
                
                print("Second If Condition Next Space : ")
                print(newLocation)
                print("WhosTurn in Second IF : ")
                print(whosTurn)
            }
        }
        
        
        
        
        
        
        return newLocation
    }
    
    
    
    
    
    
    
    
    func HowtoMove(player:Player) {
        print("Inside how to move:")
        
        if player == .Player1 {
            
            movesRemaining = rolledDice
            
            
            if (rolledDice == 0) {
                
                movesRemaining = 0
                OneMoreMove = 0
                playerTturns = 0
                updateScore()
                
                let delay = 0.5
                
                
                print("Yes!")
                
                
                self.notifyForTurnBurn(label: self.threeLargeNum)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    
                    self.movePiece()
                }
                
            } else {
                
                if ((currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26)) {
                    
                    print("Don't Do It.")
                    
                    self.blink(player: player)
                    
                } else {
                    
                    
                    if (self.currentSpacePlayer1Piece1 == 18 || self.currentSpacePlayer1Piece1 == 19 || self.currentSpacePlayer1Piece1 == 20 || self.currentSpacePlayer1Piece1 == 21 || self.currentSpacePlayer1Piece1 == 22 || currentSpacePlayer1Piece1 == 23 || currentSpacePlayer1Piece1 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece1 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece1 == 22) {
                            
                            p1p1TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece1 == 23) {
                            
                            p1p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece1 == 24) {
                            
                            p1p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player1piece1AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer1Piece2 == 18 || self.currentSpacePlayer1Piece2 == 19 || self.currentSpacePlayer1Piece2 == 20 || self.currentSpacePlayer1Piece2 == 21 || self.currentSpacePlayer1Piece2 == 22 || currentSpacePlayer1Piece2 == 23 || currentSpacePlayer1Piece2 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece2 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece2 == 22) {
                            
                            p1p2TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece2 == 23) {
                            
                            p1p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece2 == 24) {
                            
                            p1p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player1piece2AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer1Piece3 == 18 || self.currentSpacePlayer1Piece3 == 19 || self.currentSpacePlayer1Piece3 == 20 || self.currentSpacePlayer1Piece3 == 21 || self.currentSpacePlayer1Piece3 == 22 || currentSpacePlayer1Piece3 == 23 || currentSpacePlayer1Piece3 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece3 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece3 == 22) {
                            
                            p1p3TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece3 == 23) {
                            
                            p1p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece3 == 24) {
                            
                            p1p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player1piece3AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer1Piece4 == 18 || self.currentSpacePlayer1Piece4 == 19 || self.currentSpacePlayer1Piece4 == 20 || self.currentSpacePlayer1Piece4 == 21 || self.currentSpacePlayer1Piece4 == 22 || currentSpacePlayer1Piece4 == 23 || currentSpacePlayer1Piece4 == 24) {
                        
                        
                        if (self.currentSpacePlayer1Piece4 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer1Piece4 == 22) {
                            
                            p1p4TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece4 == 23) {
                            
                            p1p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer1Piece4 == 24) {
                            
                            p1p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player1piece4AbletoEnter = false
                                
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    blink(player: player)
                }
                
                
                
                if ((currentSpacePlayer1Piece1 == 26 || player1piece1AbletoEnter == false) && (currentSpacePlayer1Piece2 == 26 || player1piece2AbletoEnter == false) && (currentSpacePlayer1Piece3 == 26 || player1piece3AbletoEnter == false) && (currentSpacePlayer1Piece4 == 26 || player1piece4AbletoEnter == false)) {
                    
                    print("All are AT Damn 26 Yeah!")
                    movesRemaining = 0
                    
                    
                    let delay = 0.5
                    
                    self.notifyForCantMove(label: self.cantMove)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        
                        
                        self.movePiece()
                    }
                    
                }
                
                
                
            }
            
            
            
            
        } else if player == .Player2 {
            
            
            
            movesRemaining = rolledDice
            
            
            
            if (rolledDice == 0) {
                
                
                movesRemaining = 0
                OneMoreMove = 0
                player2Tturns = 0
                updateScore()
                
                let delay = 0.5
                
                
                print("Yes!")
                
                
                self.notifyForTurnBurn(label: self.threeLargeNum)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    self.movePiece()
                }
                
                
            } else {
                
                
                if ((currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26)) {
                    
                    print("Don't Do It.")
                    self.blink(player: player)
                    
                } else {
                    
                    
                    if (self.currentSpacePlayer2Piece1 == 20 || self.currentSpacePlayer2Piece1 == 24 || self.currentSpacePlayer2Piece1 == 17 || self.currentSpacePlayer2Piece1 == 18 || self.currentSpacePlayer2Piece1 == 19 || currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22) {
                        
                        
                        if (self.currentSpacePlayer2Piece1 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece1 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece1 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece1 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece1 == 20) {
                            
                            p2p1TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece1 == 21) {
                            
                            p2p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece1 == 22) {
                            
                            p2p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player2piece1AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer2Piece2 == 24 || self.currentSpacePlayer2Piece2 == 17 || self.currentSpacePlayer2Piece2 == 18 || self.currentSpacePlayer2Piece2 == 19 || self.currentSpacePlayer2Piece2 == 20 || currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22) {
                        
                        
                        if (self.currentSpacePlayer2Piece2 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece2 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece2 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece2 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece2 == 20) {
                            
                            p2p2TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece2 == 21) {
                            
                            p2p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece2 == 22) {
                            
                            p2p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player2piece2AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer2Piece3 == 24 || self.currentSpacePlayer2Piece3 == 17 || self.currentSpacePlayer2Piece3 == 18 || self.currentSpacePlayer2Piece3 == 19 || self.currentSpacePlayer2Piece3 == 20 || currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22) {
                        
                        
                        if (self.currentSpacePlayer2Piece3 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece3 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece3 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece3 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece3 == 20) {
                            
                            p2p3TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece3 == 21) {
                            
                            p2p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece3 == 22) {
                            
                            p2p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player2piece3AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer2Piece4 == 24 || self.currentSpacePlayer2Piece4 == 17 || self.currentSpacePlayer2Piece4 == 18 || self.currentSpacePlayer2Piece4 == 19 || self.currentSpacePlayer2Piece4 == 20 || currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22) {
                        
                        
                        if (self.currentSpacePlayer2Piece4 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece4 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece4 == 18) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece4 == 19) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer2Piece4 == 20) {
                            
                            p2p4TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece4 == 21) {
                            
                            p2p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer2Piece4 == 22) {
                            
                            p2p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player2piece4AbletoEnter = false
                                
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    blink(player: player)
                }
                
                if ((currentSpacePlayer2Piece1 == 26 || player2piece1AbletoEnter == false) && (currentSpacePlayer2Piece2 == 26 || player2piece2AbletoEnter == false) && (currentSpacePlayer2Piece3 == 26 || player2piece3AbletoEnter == false) && (currentSpacePlayer2Piece4 == 26 || player2piece4AbletoEnter == false)) {
                    
                    print("All are AT Damn 26 Yeah!")
                    movesRemaining = 0
                    
                    
                    let delay = 0.5
                    
                    self.notifyForCantMove(label: self.cantMove)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        
                        
                        self.movePiece()
                    }
                    
                }
                
                
            }
            
            
            
            
            
            
        } else if player == .Player3 {
            
            movesRemaining = rolledDice
            
            
            
            if (rolledDice == 0) {
                
                
                
                movesRemaining = 0
                OneMoreMove = 0
                player2Tturns = 0
                updateScore()
                
                let delay = 0.5
                
                
                print("Yes!")
                
                self.notifyForTurnBurn(label: self.threeLargeNum)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    self.movePiece()
                }
                
                
                
            } else {
                
                
                
                if ((currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece1 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26)) {
                    
                    print("Don't Do It.")
                    self.blink(player: player)
                    
                } else {
                    
                    
                    if (self.currentSpacePlayer3Piece1 == 22 || self.currentSpacePlayer3Piece1 == 23 || self.currentSpacePlayer3Piece1 == 24 || self.currentSpacePlayer3Piece1 == 17 || self.currentSpacePlayer3Piece1 == 18 || currentSpacePlayer3Piece1 == 19 || currentSpacePlayer3Piece1 == 20) {
                        
                        
                        if (self.currentSpacePlayer3Piece1 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece1 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece1 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece1 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece1 == 18) {
                            
                            p3p1TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece1 == 19) {
                            
                            p3p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece1 == 20) {
                            
                            p3p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player3piece1AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer3Piece2 == 22 || self.currentSpacePlayer3Piece2 == 23 || self.currentSpacePlayer3Piece2 == 24 || self.currentSpacePlayer3Piece2 == 17 || self.currentSpacePlayer3Piece2 == 18 || currentSpacePlayer3Piece2 == 19 || currentSpacePlayer3Piece2 == 20) {
                        
                        
                        if (self.currentSpacePlayer3Piece2 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece2 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece2 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece2 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece2 == 18) {
                            
                            p3p2TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece2 == 19) {
                            
                            p3p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece2 == 20) {
                            
                            p3p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player3piece2AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer3Piece3 == 22 || self.currentSpacePlayer3Piece3 == 23 || self.currentSpacePlayer3Piece3 == 24 || self.currentSpacePlayer3Piece3 == 17 || self.currentSpacePlayer3Piece3 == 18 || currentSpacePlayer3Piece3 == 19 || currentSpacePlayer3Piece3 == 20) {
                        
                        
                        if (self.currentSpacePlayer3Piece3 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece3 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece3 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece3 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece3 == 18) {
                            
                            p3p3TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece3 == 19) {
                            
                            p3p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece3 == 20) {
                            
                            p3p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player3piece3AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer3Piece4 == 22 || self.currentSpacePlayer3Piece4 == 23 || self.currentSpacePlayer3Piece4 == 24 || self.currentSpacePlayer3Piece4 == 17 || self.currentSpacePlayer3Piece4 == 18 || currentSpacePlayer3Piece4 == 19 || currentSpacePlayer3Piece4 == 20) {
                        
                        
                        if (self.currentSpacePlayer3Piece4 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece4 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece4 == 24) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece4 == 17) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer3Piece4 == 18) {
                            
                            p3p4TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece4 == 19) {
                            
                            p3p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer3Piece4 == 20) {
                            
                            p3p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player3piece4AbletoEnter = false
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    blink(player: player)
                }
                
                
                
                
                if ((currentSpacePlayer3Piece1 == 26 || player3piece1AbletoEnter == false) && (currentSpacePlayer3Piece2 == 26 || player3piece2AbletoEnter == false) && (currentSpacePlayer3Piece3 == 26 || player3piece3AbletoEnter == false) && (currentSpacePlayer3Piece4 == 26 || player3piece4AbletoEnter == false)) {
                    
                    print("All are AT Damn 26 Yeah!")
                    movesRemaining = 0
                    
                    
                    let delay = 0.5
                    
                    self.notifyForCantMove(label: self.cantMove)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        
                        
                        self.movePiece()
                    }
                    
                }
                
                
                
                
            }
            
            
            
            
            
        } else if player == .Player4 {
            
            movesRemaining = rolledDice
            
            
            
            if (rolledDice == 0) {
                
                
                movesRemaining = 0
                OneMoreMove = 0
                player2Tturns = 0
                updateScore()
                
                let delay = 0.5
                
                
                print("Yes!")
                
                self.notifyForTurnBurn(label: self.threeLargeNum)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    
                    self.movePiece()
                }
                
                
            } else {
                
                
                if ((currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece1 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26)) {
                    
                    print("Don't Do It.")
                    
                    self.blink(player: player)
                    
                } else {
                    
                    
                    if (self.currentSpacePlayer4Piece1 == 20 || self.currentSpacePlayer4Piece1 == 21 || self.currentSpacePlayer4Piece1 == 22 || self.currentSpacePlayer4Piece1 == 23 || self.currentSpacePlayer4Piece1 == 24 || currentSpacePlayer4Piece1 == 17 || currentSpacePlayer4Piece1 == 18) {
                        
                        
                        if (self.currentSpacePlayer4Piece1 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece1 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece1 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece1 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece1 == 24) {
                            
                            p4p1TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece1 == 17) {
                            
                            p4p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece1 == 18) {
                            
                            p4p1TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece1 IS PAUSED!")
                                player4piece1AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer4Piece2 == 20 || self.currentSpacePlayer4Piece2 == 21 || self.currentSpacePlayer4Piece2 == 22 || self.currentSpacePlayer4Piece2 == 23 || self.currentSpacePlayer4Piece2 == 24 || currentSpacePlayer4Piece2 == 17 || currentSpacePlayer4Piece2 == 18) {
                        
                        
                        if (self.currentSpacePlayer4Piece2 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece2 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece2 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece2 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece2 == 24) {
                            
                            p4p2TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece2 == 17) {
                            
                            p4p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece2 == 18) {
                            
                            p4p2TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece2 IS PAUSED!")
                                player4piece2AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer4Piece3 == 20 || self.currentSpacePlayer4Piece3 == 21 || self.currentSpacePlayer4Piece3 == 22 || self.currentSpacePlayer4Piece3 == 23 || self.currentSpacePlayer4Piece3 == 24 || currentSpacePlayer4Piece3 == 17 || currentSpacePlayer4Piece3 == 18) {
                        
                        
                        if (self.currentSpacePlayer4Piece3 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece3 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece3 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece3 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece3 == 24) {
                            
                            p4p3TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece3 == 17) {
                            
                            p4p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece3 == 18) {
                            
                            p4p3TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece3 IS PAUSED!")
                                player4piece3AbletoEnter = false
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    if (self.currentSpacePlayer4Piece4 == 20 || self.currentSpacePlayer4Piece4 == 21 || self.currentSpacePlayer4Piece4 == 22 || self.currentSpacePlayer4Piece4 == 23 || self.currentSpacePlayer4Piece4 == 24 || currentSpacePlayer4Piece4 == 17 || currentSpacePlayer4Piece4 == 18) {
                        
                        
                        if (self.currentSpacePlayer4Piece4 == 20) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece4 == 21) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece4 == 22) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece4 == 23) {
                            
                            if (self.rolledDice == 8) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                            }
                            
                        } else if (self.currentSpacePlayer4Piece4 == 24) {
                            
                            p4p4TryToWin += 1
                            if (self.rolledDice == 8 || rolledDice == 4) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece4 == 17) {
                            
                            p4p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                            }
                            
                        } else if (currentSpacePlayer4Piece4 == 18) {
                            
                            p4p4TryToWin += 1
                            if (rolledDice == 8 || rolledDice == 4 || rolledDice == 3 || rolledDice == 2) {
                                print("Inside Player1Piece4 IS PAUSED!")
                                player4piece4AbletoEnter = false
                                
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    blink(player: player)
                }
                
                
                
                if ((currentSpacePlayer4Piece1 == 26 || player4piece1AbletoEnter == false) && (currentSpacePlayer4Piece2 == 26 || player4piece2AbletoEnter == false) && (currentSpacePlayer4Piece3 == 26 || player4piece3AbletoEnter == false) && (currentSpacePlayer4Piece4 == 26 || player4piece4AbletoEnter == false)) {
                    
                    print("All are AT Damn 26 Yeah!")
                    movesRemaining = 0
                    
                    
                    let delay = 0.5
                    
                    self.notifyForCantMove(label: self.cantMove)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        
                        
                        self.movePiece()
                    }
                    
                }
                
                
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // MOVE FUNCTIONS TILL HERE : <-------------------------------------------------------------------
    
    
    
    
    
    func Adjust(node:SKNode) {
        
        var j:Int = 1
        var i:Int = 1
        var p:Int = 0
        var pieces: [SKSpriteNode] = []
        
        if (String(currentSpacePlayer1Piece1) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 1")
            p += 1
            pieces.append(Player1Piece1)
            
        }
        
        if (String(currentSpacePlayer1Piece2) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 2")
            p += 1
            pieces.append(Player1Piece2)
            
        }
        
        if (String(currentSpacePlayer1Piece3) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 3")
            p += 1
            pieces.append(Player1Piece3)
            
        }
        
        if (String(currentSpacePlayer1Piece4) == node.name) {
            // Scale Down the pieces
            print("Added Player 1 Pieces 4")
            p += 1
            pieces.append(Player1Piece4)
            
        }
        
        if (String(currentSpacePlayer2Piece1) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 1")
            p += 1
            pieces.append(Player2Piece1)
            
        }
        
        if (String(currentSpacePlayer2Piece2) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 2")
            p += 1
            pieces.append(Player2Piece2)
            
        }
        
        if (String(currentSpacePlayer2Piece3) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 3")
            p += 1
            pieces.append(Player2Piece3)
            
        }
        
        if (String(currentSpacePlayer2Piece4) == node.name) {
            // Scale Down the pieces
            print("Added Player 2 Pieces 4")
            p += 1
            pieces.append(Player2Piece4)
            
        }
        
        if (String(currentSpacePlayer3Piece1) == node.name) {
            // Scale Down the pieces
            print("Added Player 3 Pieces 1")
            p += 1
            pieces.append(Player3Piece1)
            
        }
        
        if (String(currentSpacePlayer3Piece2) == node.name) {
            // Scale Down the pieces
            print("Added Player 3 Pieces 2")
            p += 1
            pieces.append(Player3Piece2)
            
        }
        
        if (String(currentSpacePlayer3Piece3) == node.name) {
            // Scale Down the pieces
            print("Added Player 3 Pieces 3")
            p += 1
            pieces.append(Player3Piece3)
            
        }
        
        if (String(currentSpacePlayer3Piece4) == node.name) {
            // Scale Down the pieces
            print("Added Player 3 Pieces 4")
            p += 1
            pieces.append(Player3Piece4)
            
        }
        
        if (String(currentSpacePlayer4Piece1) == node.name) {
            // Scale Down the pieces
            print("Added Player 4 Pieces 1")
            p += 1
            pieces.append(Player4Piece1)
            
        }
        
        if (String(currentSpacePlayer4Piece2) == node.name) {
            // Scale Down the pieces
            print("Added Player 4 Pieces 2")
            p += 1
            pieces.append(Player4Piece2)
            
        }
        
        if (String(currentSpacePlayer4Piece3) == node.name) {
            // Scale Down the pieces
            print("Added Player 4 Pieces 3")
            p += 1
            pieces.append(Player4Piece3)
            
        }
        
        if (String(currentSpacePlayer4Piece4) == node.name) {
            // Scale Down the pieces
            print("Added Player 4 Pieces 4")
            p += 1
            pieces.append(Player4Piece4)
            
        }
        
        print(node.name!)
        
        print(pieces.isEmpty)
        
        print(childNode(withName: "a11")!)
        print(pieces.count)
        
        
        
        if (p > 1 && p <= 5) && (node.name != "1" && node.name != "5" && node.name != "9" && node.name != "13" && node.name != "25") {
            
            
            
            while (j <= 25) {
                print("hi")
                if (node.name == "\(j)") {
                    
                    
                    while (i <= 5) {
                        
                        if (pieces.count >= i) {
                            print("i and J and P :")
                            print(i)
                            print(j)
                            print(p)
                            
                            let nodeLoc:SKNode = childNode(withName: "a\(j)\(i)")!
                            let moveAction:SKAction = SKAction.move(to: nodeLoc.position, duration: 0.2)
                            
                            pieces[i - 1].run(moveAction)
                            
                        }
                        i += 1
                    }
                    
                    
                    
                }
                
                
                j += 1
            }
            
            
            
            // } else if (p >= 5) && (node.name != "1" && node.name != "5" && node.name != "9" && node.name != "13" && node.name != "25") {
            
            
            
            
            
        } else if (node.name == "1" || node.name == "5" || node.name == "9" || node.name == "13") {
            
            
            while (j <= 5) {
                print("hi")
                if ((node.name == "1" && j == 1) || (node.name == "5" && j == 2) || (node.name == "9" && j == 3) || (node.name == "13" && j == 4)) && (p > 1) {
                    
                    
                    while (i <= 9) {
                        
                        if (pieces.count >= i) {
                            print("i and J and P :")
                            print(i)
                            print(j)
                            print(p)
                            
                            let nodeLoc:SKNode = childNode(withName: "b\(j)\(i)")!
                            let moveAction:SKAction = SKAction.move(to: nodeLoc.position, duration: 0.2)
                            print(pieces[i - 1])
                            pieces[i - 1].run(moveAction)
                            
                        }
                        i += 1
                    }
                    
                    
                    
                }
                
                
                j += 1
            }
            
            
        } else if (node.name == "25") {
            print("Inside THE == 25 Condition : ")
            
            if (whosTurn == .Player1) {
                
                if (currentSpacePlayer1Piece1 == 25) {
                    
                    print("Inside Player 1 Piece 1 Adjust Action :")
                    
                    if !p1P1Reached {
                        Player1Piece1.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece1eye1.isHidden = true
                        player1Piece1eye2.isHidden = true
                        p1P1Reached = true
                    }
                    let loc:SKNode = childNode(withName: "c1")!
                    
                    
                    let moveaction : SKAction = SKAction.move(to: loc.position, duration: 0.2)
                    
                    Player1Piece1.run(moveaction)
                    
                    currentSpacePlayer1Piece1 = 26
                    OneMoreMove += 1
                    updateScore()
                    
                } else if (currentSpacePlayer1Piece2 == 25) {
                    
                    print("Inside Player 1 Piece 2 Adjust Action :")
                    
                    //let loc2:SKNode = childNode(withName: "c2")!
                    
                    if !p1P2Reached {
                        Player1Piece2.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece2eye1.isHidden = true
                        player1Piece2eye2.isHidden = true
                        p1P2Reached = true
                    }
                    
                    let moveaction : SKAction = SKAction.move(to: (childNode(withName: "c2")?.position)!, duration: 0.2)
                    
                    Player1Piece2.run(moveaction)
                    
                    currentSpacePlayer1Piece2 = 26
                    OneMoreMove += 1
                    updateScore()
                    
                    
                } else if (currentSpacePlayer1Piece3 == 25) {
                    
                    print("Inside Player 1 Piece 3 Adjust Action :")
                    
                    if !p1P3Reached {
                        Player1Piece3.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece3eye1.isHidden = true
                        player1Piece3eye2.isHidden = true
                        p1P3Reached = true
                    }
                    
                    //let loc3:SKNode = childNode(withName: "c3")!
                    
                    let moveaction : SKAction = SKAction.move(to: (childNode(withName: "c3")?.position)!, duration: 0.2)
                    
                    Player1Piece3.run(moveaction)
                    
                    currentSpacePlayer1Piece3 = 26
                    OneMoreMove += 1
                    updateScore()
                    
                    
                } else if (currentSpacePlayer1Piece4 == 25) {
                    
                    print("Inside Player 1 Piece 4 Adjust Action :")
                    
                    if !p1P4Reached {
                        Player1Piece4.texture = SKTexture(imageNamed: "Over Kakri")
                        player1Piece4eye1.isHidden = true
                        player1Piece4eye2.isHidden = true
                        p1P4Reached = true
                    }
                    
                    //let loc4:SKNode = childNode(withName: "c4")!
                    
                    let moveaction : SKAction = SKAction.move(to: (childNode(withName: "c4")?.position)!, duration: 0.2)
                    
                    Player1Piece4.run(moveaction)
                    
                    currentSpacePlayer1Piece4 = 26
                    OneMoreMove += 1
                    updateScore()
                    
                    
                    
                }
                
            } else if (whosTurn == .Player2) {
                
                if (currentSpacePlayer2Piece1 == 25) {
                    
                    if !p2P1Reached {
                        
                        Player2Piece1.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece1eye1.isHidden = true
                        player2Piece1eye2.isHidden = true
                        p2P1Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c11")!
                    Player2Piece1.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece1 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer2Piece2 == 25) {
                    
                    if !p2P2Reached {
                        
                        Player2Piece2.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece2eye1.isHidden = true
                        player2Piece2eye2.isHidden = true
                        p2P2Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c12")!
                    Player2Piece2.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece2 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer2Piece3 == 25) {
                    
                    if !p2P3Reached {
                        
                        Player2Piece3.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece3eye1.isHidden = true
                        player2Piece3eye2.isHidden = true
                        p2P3Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c13")!
                    Player2Piece3.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece3 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer2Piece4 == 25) {
                    
                    if !p2P4Reached {
                        
                        Player2Piece4.texture = SKTexture(imageNamed: "Over Kakri3")
                        player2Piece4eye1.isHidden = true
                        player2Piece4eye2.isHidden = true
                        p2P4Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c14")!
                    Player2Piece4.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer2Piece4 = 26
                    OneMoreMove += 1
                    updateScore()
                }
                
            } else if (whosTurn == .Player3) {
                
                if (currentSpacePlayer3Piece1 == 25) {
                    
                    if !p3P1Reached {
                        
                        Player3Piece1.texture = SKTexture(imageNamed: "Over Kakri2")
                        player3Piece1eye1.isHidden = true
                        player3Piece1eye2.isHidden = true
                        p3P1Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c21")!
                    Player3Piece1.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer3Piece1 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer3Piece2 == 25) {
                    
                    if !p3P2Reached {
                        
                        Player3Piece2.texture = SKTexture(imageNamed: "Over Kakri2")
                        player3Piece2eye1.isHidden = true
                        player3Piece2eye2.isHidden = true
                        p3P2Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c22")!
                    Player3Piece2.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer3Piece2 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer3Piece3 == 25) {
                    
                    if !p3P3Reached {
                        
                        Player3Piece3.texture = SKTexture(imageNamed: "Over Kakri2")
                        player3Piece3eye1.isHidden = true
                        player3Piece3eye2.isHidden = true
                        p3P3Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c23")!
                    Player3Piece3.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer3Piece3 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer3Piece4 == 25) {
                    
                    if !p3P4Reached {
                        
                        Player3Piece4.texture = SKTexture(imageNamed: "Over Kakri2")
                        player3Piece4eye1.isHidden = true
                        player3Piece4eye2.isHidden = true
                        p3P4Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c24")!
                    Player3Piece4.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer3Piece4 = 26
                    OneMoreMove += 1
                    updateScore()
                }
                
            } else if (whosTurn == .Player4) {
                
                if (currentSpacePlayer4Piece1 == 25) {
                    
                    if !p4P1Reached {
                        
                        Player4Piece1.texture = SKTexture(imageNamed: "Over Kakri4")
                        player4Piece1eye1.isHidden = true
                        player4Piece1eye2.isHidden = true
                        p4P1Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c31")!
                    Player4Piece1.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer4Piece1 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer4Piece2 == 25) {
                    
                    if !p4P2Reached {
                        
                        Player4Piece2.texture = SKTexture(imageNamed: "Over Kakri4")
                        player4Piece2eye1.isHidden = true
                        player4Piece2eye2.isHidden = true
                        p4P2Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c32")!
                    Player4Piece2.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer4Piece2 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer4Piece3 == 25) {
                    
                    if !p4P3Reached {
                        
                        Player4Piece3.texture = SKTexture(imageNamed: "Over Kakri4")
                        player4Piece3eye1.isHidden = true
                        player4Piece3eye2.isHidden = true
                        p4P3Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c33")!
                    Player4Piece3.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer4Piece3 = 26
                    OneMoreMove += 1
                    updateScore()
                } else if (currentSpacePlayer4Piece4 == 25) {
                    
                    if !p4P4Reached {
                        
                        Player4Piece4.texture = SKTexture(imageNamed: "Over Kakri4")
                        player4Piece4eye1.isHidden = true
                        player4Piece4eye2.isHidden = true
                        p4P4Reached = true
                    }
                    
                    let loc:SKNode = childNode(withName: "c34")!
                    Player4Piece4.run(SKAction.move(to: loc.position, duration: 0.2))
                    currentSpacePlayer4Piece4 = 26
                    OneMoreMove += 1
                    updateScore()
                }
                
            }
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    func disabletoEnter() {
        
        
        player1piece1AbletoEnter = true
        player1piece2AbletoEnter = true
        player1piece3AbletoEnter = true
        player1piece4AbletoEnter = true
        
        
        player2piece1AbletoEnter = true
        player2piece2AbletoEnter = true
        player2piece3AbletoEnter = true
        player2piece4AbletoEnter = true
        
        player3piece1AbletoEnter = true
        player3piece2AbletoEnter = true
        player3piece3AbletoEnter = true
        player3piece4AbletoEnter = true
        
        player4piece1AbletoEnter = true
        player4piece2AbletoEnter = true
        player4piece3AbletoEnter = true
        player4piece4AbletoEnter = true
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func checkWin(player:Player)  {
        
        if ( ( playerScore == 140 && com1Score == 140 && com2Score == 140 ) || ( com3Score == 140 && com1Score == 140 && com2Score == 140 ) || ( playerScore == 140 && com1Score == 140 && com3Score == 140 ) || ( playerScore == 140 && com2Score == 140 && com3Score == 140 ) ) {
            
            
            
            gameOver = true
            
            
            print("EnTER THE GAME OVER MENU :")
            
        }
        
        
        if (player == .Player1 && ((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26)) ) {
            
            print(touchedNode)
            print(currentSpacePlayer1Piece1)
            print(currentSpacePlayer1Piece2)
            print(currentSpacePlayer1Piece3)
            print(currentSpacePlayer1Piece4)
            
            if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 24) {
                dontScoreP1 = true
            } else if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece2 == 24) {
                dontScoreP1 = true
            } else if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 24) {
                dontScoreP1 = true
            } else if (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 == 24) {
                dontScoreP1 = true
            }
            
        } else if (player == .Player2 && ((currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26)) ) {
            
            if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 22) {
                dontScoreP2 = true
            } else if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece2 == 22) {
                dontScoreP2 = true
            } else if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 22) {
                dontScoreP2 = true
            } else if (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 == 22) {
                dontScoreP2 = true
            }
            
        } else if (player == .Player3 && ((currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26)) ) {
            
            if (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 20) {
                dontScoreP3 = true
            } else if (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece2 == 20) {
                dontScoreP3 = true
            } else if (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 == 20) {
                dontScoreP3 = true
            } else if (currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece1 == 20) {
                dontScoreP3 = true
            }
            
        } else if (player == .Player4 && ((currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26)) ) {
            
            if (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 18) {
                dontScoreP4 = true
            } else if (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece2 == 18) {
                dontScoreP4 = true
            } else if (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 == 18) {
                dontScoreP4 = true
            } else if (currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece1 == 18) {
                dontScoreP4 = true
            }
            
        }
        
        
        
        if player == .Player1 {
            
            if (currentSpacePlayer1Piece1 == 26 && !p1p1Plus10) {
                
                player1Piece1Score += 11
                p1p1Plus10 = true
                
            } else if (currentSpacePlayer1Piece2 == 26 && !p1p2Plus10) {
                
                player1Piece2Score += 11
                p1p2Plus10 = true
                
            } else if (currentSpacePlayer1Piece3 == 26 && !p1p3Plus10) {
                
                player1Piece3Score += 11
                p1p3Plus10 = true
                
            } else if (currentSpacePlayer1Piece4 == 26 && !p1p4Plus10) {
                
                player1Piece4Score += 11
                p1p4Plus10 = true
                
            }
            
            playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
            playerScoreLabel.text = "\(playerScore)"
            
        } else if player == .Player2 {
            
            if (currentSpacePlayer2Piece1 == 26 && !p2p1Plus10) {
                
                com1Piece1Score += 11
                p2p1Plus10 = true
                
            } else if (currentSpacePlayer2Piece2 == 26 && !p2p2Plus10) {
                
                com1Piece2Score += 11
                p2p2Plus10 = true
                
            } else if (currentSpacePlayer2Piece3 == 26 && !p2p3Plus10) {
                
                com1Piece3Score += 11
                p2p3Plus10 = true
                
            } else if (currentSpacePlayer2Piece4 == 26 && !p2p4Plus10) {
                
                com1Piece4Score += 11
                p2p4Plus10 = true
                
            }
            
            com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
            com1ScoreLabel.text = "\(com1Score)"
            
        } else if player == .Player3 {
            
            if (currentSpacePlayer3Piece1 == 26 && !p3p1Plus10) {
                
                com2Piece1Score += 11
                p3p1Plus10 = true
                
            } else if (currentSpacePlayer3Piece2 == 26 && !p3p2Plus10) {
                
                com2Piece2Score += 11
                p3p2Plus10 = true
                
            } else if (currentSpacePlayer3Piece3 == 26 && !p3p3Plus10) {
                
                com2Piece3Score += 11
                p3p3Plus10 = true
                
            } else if (currentSpacePlayer3Piece4 == 26 && !p3p4Plus10) {
                
                com2Piece4Score += 11
                p3p4Plus10 = true
                
            }
            
            com2Score = (com2Piece1Score - 1) + (com2Piece2Score - 1) + (com2Piece3Score - 1) + (com2Piece4Score - 1)
            com2ScoreLabel.text = "\(com2Score)"
            
        } else if player == .Player4 {
            
            if (currentSpacePlayer4Piece1 == 26 && !p4p1Plus10) {
                
                com3Piece1Score += 11
                p4p1Plus10 = true
                
            } else if (currentSpacePlayer4Piece2 == 26 && !p4p2Plus10) {
                
                com3Piece2Score += 11
                p4p2Plus10 = true
                
            } else if (currentSpacePlayer4Piece3 == 26 && !p4p3Plus10) {
                
                com3Piece3Score += 11
                p4p3Plus10 = true
                
            } else if (currentSpacePlayer4Piece4 == 26 && !p4p4Plus10) {
                
                com3Piece4Score += 11
                p4p4Plus10 = true
                
            }
            
            com3Score = (com3Piece1Score - 1) + (com3Piece2Score - 1) + (com3Piece3Score - 1) + (com3Piece4Score - 1)
            com3ScoreLabel.text = "\(com3Score)"
            
        }
        
        
        
        
        
        if (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) && (i == 0) {
            
            i += 1
            
            playerTturns = 0
            
            plaTTtext.text = "ToTal Turns:\(playerTturns)"
            
            
            place.append(playerone)
        }
        
        if (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) && (j == 0) {
            
            j += 1
            
            player2Tturns = 0
            
            com1TTtext.text = "ToTal Turns:\(player2Tturns)"
            
            
            place.append(playertwo)
        }
        
        if (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26) && (k == 0) {
            
            k += 1
            
            player3Tturns = 0
            
            com2TTtext.text = "ToTal Turns:\(player3Tturns)"
            
            
            place.append(playerthree)
        }
        
        if (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26) && (l == 0) {
            
            l += 1
            
            player4Tturns = 0
            
            com3TTtext.text = "ToTal Turns:\(player4Tturns)"
            
            
            place.append(playerfour)
        }
        
        
        
        if (player == .Player1) {
            
            if (touchedNode == Player1Piece1) {
                
                if currentSpacePlayer1Piece1 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player1Piece2) {
                
                if currentSpacePlayer1Piece2 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player1Piece3) {
                
                
                if currentSpacePlayer1Piece3 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player1Piece4) {
                
                
                if currentSpacePlayer1Piece4 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            }
            
            
        } else if (player == .Player2) {
            
            
            if (touchedNode == Player2Piece1) {
                
                if currentSpacePlayer2Piece1 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player2Piece2) {
                
                if currentSpacePlayer2Piece2 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player2Piece3) {
                
                
                if currentSpacePlayer2Piece3 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player2Piece4) {
                
                
                if currentSpacePlayer2Piece4 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            }
            
        } else if (player == .Player3) {
            
            if (touchedNode == Player3Piece1) {
                
                if currentSpacePlayer3Piece1 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player3Piece2) {
                
                if currentSpacePlayer3Piece2 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player3Piece3) {
                
                
                if currentSpacePlayer3Piece3 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player3Piece4) {
                
                
                if currentSpacePlayer3Piece4 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            }
            
        } else if (player == .Player4) {
            
            if (touchedNode == Player4Piece1) {
                
                if currentSpacePlayer4Piece1 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player4Piece2) {
                
                if currentSpacePlayer4Piece2 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player4Piece3) {
                
                
                if currentSpacePlayer4Piece3 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            } else if (touchedNode == Player4Piece4) {
                
                
                if currentSpacePlayer4Piece4 == 26 {
                    
                    print("Inside The CheckWin")
                    
                }
            }
            
        }
        
    }
    
    
    
    
    
    func noWinList() {
        
        print("In Score Checking :")
        print("You :")
        print(playerScore)
        print("COMPUTER 1 :")
        print(com1Score)
        print("COMPUTER 2 :")
        print(com2Score)
        print("COMPUTER 3 :")
        print(com3Score)
        
        if !(currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) {
            print("Entered FIRST :")
            
            if playerScore > com1Score && playerScore > com2Score && playerScore > com3Score {
                
                playerOne = playerLabel.text!
                print("PlayerOne P1 :")
                print(playerOne)
                
            } else if (playerScore > com1Score && playerScore > com2Score) || (playerScore > com2Score && playerScore > com3Score) || (playerScore > com1Score && playerScore > com3Score) {
                
                playerTwo = playerLabel.text!
                print("PlayerTwo P1 :")
                print(playerTwo)
                
            } else if (playerScore > com1Score) || (playerScore > com2Score) || (playerScore > com3Score) {
                
                playerThree = playerLabel.text!
                print("PlayerThree P1 :")
                print(playerThree)
                
            } else if (playerScore < com1Score) && (playerScore < com2Score) && (playerScore < com3Score) {
                
                playerFour = playerLabel.text!
                print("PlayerFour P1 :")
                print(playerFour)
                
            }
            
        }
        
        
        if !(currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) {
            print("Entered SECOND :")
            
            if com1Score > playerScore && com1Score > com2Score && com1Score > com3Score {
                
                playerOne = player2Label.text!
                print("PlayerOne P2 :")
                print(playerOne)
                
            } else if (com1Score > playerScore && com1Score > com2Score) || (com1Score > com2Score && com1Score > com3Score) || (com1Score > playerScore && com1Score > com3Score) {
                
                playerTwo = player2Label.text!
                print("PlayerTwo P2 :")
                print(playerTwo)
                
            } else if (com1Score > playerScore) || (com1Score > com2Score) || (com1Score > com3Score) {
                
                playerThree = player2Label.text!
                print("PlayerThree P2 :")
                print(playerThree)
                
            } else if (com1Score < playerScore) && (com1Score < com2Score) && (com1Score < com3Score) {
                
                playerFour = player2Label.text!
                print("PlayerFour P2 :")
                print(playerFour)
                
            }
            
        }
        
        
        if !(currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26) {
            print("Entered THIRD :")
            
            if com2Score > playerScore && com2Score > com1Score && com2Score > com3Score {
                
                playerOne = player3Label.text!
                print("PlayerOne P3 :")
                print(playerOne)
                
            } else if (com2Score > playerScore && com2Score > com1Score) || (com2Score > com1Score && com2Score > com3Score) || (com2Score > playerScore && com2Score > com3Score) {
                
                playerTwo = player3Label.text!
                print("PlayerTwo P3 :")
                print(playerTwo)
                
            } else if (com2Score > playerScore) || (com2Score > com1Score) || (com2Score > com3Score) {
                
                playerThree = player3Label.text!
                print("PlayerThree P3 :")
                print(playerThree)
                
            } else if (com2Score < playerScore) && (com2Score < com1Score) && (com2Score < com3Score) {
                
                playerFour = player3Label.text!
                print("PlayerFour P3 :")
                print(playerFour)
                
            }
            
        }
        
        
        if !(currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26) {
            print("Entered FOURTH :")
            
            if com3Score > playerScore && com3Score > com1Score && com3Score > com2Score {
                
                playerOne = player4Label.text!
                print("PlayerOne P4 :")
                print(playerOne)
                
            } else if (com3Score > playerScore && com3Score > com1Score) || (com3Score > com1Score && com3Score > com2Score) || (com3Score > playerScore && com3Score > com2Score) {
                
                playerTwo = player4Label.text!
                print("PlayerTwo P4 :")
                print(playerTwo)
                
            } else if (com3Score > playerScore) || (com3Score > com1Score) || (com3Score > com2Score) {
                
                playerThree = player4Label.text!
                print("PlayerThree P4 :")
                print(playerThree)
                
            } else if (com3Score < playerScore) && (com3Score < com1Score) && (com3Score < com2Score) {
                
                playerFour = player4Label.text!
                print("PlayerFour P4 :")
                print(playerFour)
                
            }
            
        }
        
        
        // SAmE Sore HERE :
        
        if (!(currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 == 26) || !(currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 == 26) || !(currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 == 26) || !(currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 == 26)) {
            print("Entered COMPARING :")
            if playerScore == com1Score && com2Score == com3Score && com1Score == com3Score {
                
                playerOne = playerLabel.text!
                playerTwo = player2Label.text!
                playerThree = player3Label.text!
                playerFour = player4Label.text!
                print("All At Same :")
                print(playerOne)
                print(playerTwo)
                print(playerThree)
                print(playerFour)
                
            } else if (playerScore == com1Score && com1Score == com2Score) || (com1Score == com2Score && com2Score == com3Score) || (playerScore == com2Score && com2Score == com3Score) || (playerScore == com1Score && com1Score == com3Score) {
                print("Three At Same :")
                if (playerScore == com1Score && com1Score == com2Score) {
                    
                    if (playerScore > com3Score) {
                        
                        playerOne = playerLabel.text!
                        playerTwo = player2Label.text!
                        playerThree = player3Label.text!
                        playerFour = player4Label.text!
                        
                        print("123 BIG :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    } else if (playerScore < com3Score) {
                        
                        playerTwo = playerLabel.text!
                        playerThree = player2Label.text!
                        playerFour = player3Label.text!
                        playerOne = player4Label.text!
                        
                        print("123 SMALL :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                    }
                    
                } else if (com1Score == com2Score && com2Score == com3Score) {
                    
                    if (com1Score > playerScore) {
                        
                        playerFour = playerLabel.text!
                        playerOne = player2Label.text!
                        playerTwo = player3Label.text!
                        playerThree = player4Label.text!
                        print("234 BIG :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    } else if (com1Score < playerScore) {
                        
                        playerOne = playerLabel.text!
                        playerTwo = player2Label.text!
                        playerThree = player3Label.text!
                        playerFour = player4Label.text!
                        print("234 SMALL :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    }
                    
                } else if (playerScore == com2Score && com2Score == com3Score) {
                    
                    if (playerScore > com1Score) {
                        
                        playerOne = playerLabel.text!
                        playerFour = player2Label.text!
                        playerTwo = player3Label.text!
                        playerThree = player4Label.text!
                        print("134 BIG :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    } else if (playerScore < com1Score) {
                        
                        playerTwo = playerLabel.text!
                        playerOne = player2Label.text!
                        playerThree = player3Label.text!
                        playerFour = player4Label.text!
                        
                        print("134 SMALL :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    }
                    
                } else if (playerScore == com1Score && com1Score == com3Score) {
                    
                    if (playerScore > com2Score) {
                        
                        playerOne = playerLabel.text!
                        playerTwo = player2Label.text!
                        playerFour = player3Label.text!
                        playerThree = player4Label.text!
                        print("124 BIG :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    } else if (playerScore < com2Score) {
                        
                        playerTwo = playerLabel.text!
                        playerThree = player2Label.text!
                        playerOne = player3Label.text!
                        playerFour = player4Label.text!
                        print("124 SMALL :")
                        print(playerOne)
                        print(playerTwo)
                        print(playerThree)
                        print(playerFour)
                        
                    }
                    
                }
                
            } else if (playerScore == com1Score) || (com1Score == com2Score) || (com2Score == com3Score) || (com3Score == playerScore) || (playerScore == com2Score) || (com1Score == com3Score) {
                print("TWO At Same :")
                
                if (playerScore == com1Score && (playerScore < com2Score && playerScore < com3Score) ) {
                    playerThree = playerLabel.text!
                    playerFour = player2Label.text!
                    print("12 AT 34 :")
                    print(playerThree)
                    print(playerFour)
                } else if (playerScore == com1Score && (playerScore > com2Score && playerScore > com3Score) ) {
                    playerOne = playerLabel.text!
                    playerTwo = player2Label.text!
                    print("12 AT 12 :")
                    print(playerOne)
                    print(playerTwo)
                } else if (playerScore == com1Score && (playerScore > com2Score && playerScore < com3Score) ) {
                    playerTwo = playerLabel.text!
                    playerThree = player2Label.text!
                    print("12 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                } else if (playerScore == com1Score && (playerScore < com2Score && playerScore > com3Score) ) {
                    playerTwo = playerLabel.text!
                    playerThree = player2Label.text!
                    print("12 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                }
                
                
                if (com1Score == com2Score && (com1Score < playerScore && com1Score < com3Score) ) {
                    playerThree = player2Label.text!
                    playerFour = player3Label.text!
                    print("23 AT 34 :")
                    print(playerThree)
                    print(playerFour)
                } else if (com1Score == com2Score && (com1Score > playerScore && com1Score > com3Score) ) {
                    playerOne = player2Label.text!
                    playerTwo = player3Label.text!
                    print("23 AT 12 :")
                    print(playerOne)
                    print(playerTwo)
                } else if (com1Score == com2Score && (com1Score > playerScore && com1Score < com3Score) ) {
                    playerTwo = player2Label.text!
                    playerThree = player3Label.text!
                    print("23 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                } else if (com1Score == com2Score && (com1Score < playerScore && com1Score > com3Score) ) {
                    playerTwo = player2Label.text!
                    playerThree = player3Label.text!
                    print("23 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                }
                
                
                if (com2Score == com3Score && (com2Score < playerScore && com2Score < com1Score) ) {
                    playerThree = player3Label.text!
                    playerFour = player4Label.text!
                    print("34 AT 34 :")
                    print(playerThree)
                    print(playerFour)
                } else if (com2Score == com3Score && (com2Score > playerScore && com2Score > com1Score) ) {
                    playerOne = player3Label.text!
                    playerTwo = player4Label.text!
                    print("34 AT 12 :")
                    print(playerOne)
                    print(playerTwo)
                } else if (com2Score == com3Score && (com2Score > playerScore && com2Score < com1Score) ) {
                    playerTwo = player3Label.text!
                    playerThree = player4Label.text!
                    print("34 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                } else if (com2Score == com3Score && (com2Score < playerScore && com2Score > com1Score) ) {
                    playerTwo = player3Label.text!
                    playerThree = player4Label.text!
                    print("34 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                }
                
                
                if (com3Score == playerScore && (playerScore < com2Score && playerScore < com1Score) ) {
                    playerThree = playerLabel.text!
                    playerFour = player4Label.text!
                    print("41 AT 34 :")
                    print(playerThree)
                    print(playerFour)
                } else if (com3Score == playerScore && (playerScore > com2Score && playerScore > com1Score) ) {
                    playerOne = playerLabel.text!
                    playerTwo = player4Label.text!
                    print("41 AT 12 :")
                    print(playerOne)
                    print(playerTwo)
                } else if (com3Score == playerScore && (playerScore > com2Score && playerScore < com1Score) ) {
                    playerTwo = playerLabel.text!
                    playerThree = player4Label.text!
                    print("41 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                } else if (com3Score == playerScore && (playerScore < com2Score && playerScore > com1Score) ) {
                    playerTwo = playerLabel.text!
                    playerThree = player4Label.text!
                    print("41 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                }
                
                
                if (playerScore == com2Score && (playerScore < com1Score && playerScore < com3Score) ) {
                    playerThree = playerLabel.text!
                    playerFour = player3Label.text!
                    print("31 AT 34 :")
                    print(playerThree)
                    print(playerFour)
                } else if (playerScore == com2Score && (playerScore > com1Score && playerScore > com3Score) ) {
                    playerOne = playerLabel.text!
                    playerTwo = player3Label.text!
                    print("31 AT 12 :")
                    print(playerOne)
                    print(playerTwo)
                } else if (playerScore == com2Score && (playerScore > com1Score && playerScore < com3Score) ) {
                    playerTwo = playerLabel.text!
                    playerThree = player3Label.text!
                    print("31 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                } else if (playerScore == com2Score && (playerScore < com1Score && playerScore > com3Score) ) {
                    playerTwo = playerLabel.text!
                    playerThree = player3Label.text!
                    print("31 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                }
                
                
                if (com1Score == com3Score && (com1Score < playerScore && com1Score < com2Score) ) {
                    playerThree = player2Label.text!
                    playerFour = player4Label.text!
                    print("24 AT 34 :")
                    print(playerThree)
                    print(playerFour)
                } else if (com1Score == com3Score && (com1Score > playerScore && com1Score > com2Score) ) {
                    playerOne = player2Label.text!
                    playerTwo = player4Label.text!
                    print("24 AT 12 :")
                    print(playerOne)
                    print(playerTwo)
                } else if (com1Score == com3Score && (com1Score > playerScore && com1Score < com2Score) ) {
                    playerTwo = player2Label.text!
                    playerThree = player4Label.text!
                    print("24 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                } else if (com1Score == com3Score && (com1Score < playerScore && com1Score > com2Score) ) {
                    playerTwo = player2Label.text!
                    playerThree = player4Label.text!
                    print("24 AT 23 :")
                    print(playerTwo)
                    print(playerThree)
                }
                
            }/* else if () {
             
             
             
             }*/
            
            
        }
        
        
        
        // No Even Score HErE:
        
        
        
        
        
    }
    
    
    
    
    
    // ROLL DICE YEAH YEAH :
    
    
    func rollDice(player:Player)  {
        
        
        // Print
        // print("Whos Turn In RollDice :")
        // print(player)
        // print(rolling)
        // print(moveFinished)
        
        print("-> Decreased Player Scores :")
        print(playerScore)
        print(com1Score)
        print(com2Score)
        print(com3Score)
        print(player1Piece1Score)
        print(player1Piece2Score)
        print(player1Piece3Score)
        print(player1Piece4Score)
        print(com1Piece1Score)
        print(com1Piece2Score)
        print(com1Piece3Score)
        print(com1Piece4Score)
        print(com2Piece1Score)
        print(com2Piece2Score)
        print(com2Piece3Score)
        print(com2Piece4Score)
        print(com3Piece1Score)
        print(com3Piece2Score)
        print(com3Piece3Score)
        print(com3Piece4Score)
        
        print("All TOTAL Turns :")
        print(playerTturns)
        print(player2Tturns)
        print(player3Tturns)
        print(player4Tturns)
        
        //let sides = [1, 2, 3, 4, 8]
        //let probabilities = [1.0, 1.0, 1.0, 1.2, 1.2]
        
        //let rand = randomNumber(probabilities: probabilities)
        //let roll = sides[rand]
        
        
        removeAnimation(player: whosTurn)
        
        // let sides = [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8]
        // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
        
        
        // if rolling && !moveFinished {
        //     return
        // }
        
        
        rolling = true
        moveFinished = false
        
        
        Dice.isHidden = true
        Dice2.isHidden = true
        
        
        playSound(soundName: diceRollSFX)
        
        
        
        
        var i : Int = 1
        var j : Int = 0
        
        let side = [2, 3]
        
        var num = side[Int(arc4random_uniform(UInt32(side.count)))]
        
        
        if ((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26)) && whosTurn == .Player1 {
            
            num = 4
            
        } else if ((currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26)) && whosTurn == .Player2 {
            
            num = 4
            
        } else if ((currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26)) && whosTurn == .Player3 {
            
            num = 4
            
        } else if ((currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26)) && whosTurn == .Player4 {
            
            num = 4
            
        }
        
        
        while (i > j) {
            
            print("Add for Last human player piece in both com and vsFriend they will have more chances to get 4s")
            
            
            if ((p1p1TryToWin >= num && (currentSpacePlayer1Piece1 == 22 || currentSpacePlayer1Piece1 == 23 || currentSpacePlayer1Piece1 == 24) ) || (p1p2TryToWin >= num && (currentSpacePlayer1Piece2 == 22 || currentSpacePlayer1Piece2 == 23 || currentSpacePlayer1Piece2 == 24) ) || (p1p3TryToWin >= num && (currentSpacePlayer1Piece3 == 22 || currentSpacePlayer1Piece3 == 23 || currentSpacePlayer1Piece3 == 24) ) || (p1p4TryToWin >= num && (currentSpacePlayer1Piece4 == 22 || currentSpacePlayer1Piece4 == 23 || currentSpacePlayer1Piece4 == 24) )) && (whosTurn == .Player1) {
                
                if (p1p1TryToWin >= num && (currentSpacePlayer1Piece1 == 22 || currentSpacePlayer1Piece1 == 23 || currentSpacePlayer1Piece1 == 24) ) {
                    
                    //     if (25 - currentSpacePlayer1Piece1 == 3 || 25 - currentSpacePlayer1Piece1 == 2 || 25 - currentSpacePlayer1Piece1 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece1
                    
                    
                    print("Changed it To New One : Player1Piece1")
                    print(rolledDice)
                    print(p1p1TryToWin)
                    print(num)
                    
                    p1p1TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p1p2TryToWin >= num && (currentSpacePlayer1Piece2 == 22 || currentSpacePlayer1Piece2 == 23 || currentSpacePlayer1Piece2 == 24) ) {
                    
                    //   if (25 - currentSpacePlayer1Piece2 == 3 || 25 - currentSpacePlayer1Piece2 == 2 || 25 - currentSpacePlayer1Piece2 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece2
                    
                    
                    print("Changed it To New One : Player1Piece2")
                    print(rolledDice)
                    print(p1p2TryToWin)
                    print(num)
                    
                    p1p2TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                } else if (p1p3TryToWin >= num && (currentSpacePlayer1Piece3 == 22 || currentSpacePlayer1Piece3 == 23 || currentSpacePlayer1Piece3 == 24) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece3 == 3 || 25 - currentSpacePlayer1Piece3 == 2 || 25 - currentSpacePlayer1Piece3 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece3
                    
                    
                    print("Changed it To New One : Player1Piece3")
                    print(rolledDice)
                    print(p1p3TryToWin)
                    print(num)
                    
                    p1p3TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p1p4TryToWin >= num && (currentSpacePlayer1Piece4 == 22 || currentSpacePlayer1Piece4 == 23 || currentSpacePlayer1Piece4 == 24) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece4 == 3 || 25 - currentSpacePlayer1Piece4 == 2 || 25 - currentSpacePlayer1Piece4 == 1) {
                    
                    rolledDice = 25 - currentSpacePlayer1Piece4
                    
                    
                    print("Changed it To New One : Player1Piece4")
                    print(rolledDice)
                    print(p1p4TryToWin)
                    print(num)
                    
                    p1p4TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                }
                
            } else if ((p2p1TryToWin >= num && (currentSpacePlayer2Piece1 == 20 || currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22) ) || (p2p2TryToWin >= num && (currentSpacePlayer2Piece2 == 20 || currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22) ) || (p2p3TryToWin >= num && (currentSpacePlayer2Piece3 == 20 || currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22) ) || (p2p4TryToWin >= num && (currentSpacePlayer2Piece4 == 20 || currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22) )) && (whosTurn == .Player2) {
                
                if (p2p1TryToWin >= num && (currentSpacePlayer2Piece1 == 20 || currentSpacePlayer2Piece1 == 21 || currentSpacePlayer2Piece1 == 22) ) {
                    
                    //     if (25 - currentSpacePlayer1Piece1 == 3 || 25 - currentSpacePlayer1Piece1 == 2 || 25 - currentSpacePlayer1Piece1 == 1) {
                    
                    rolledDice = 23 - currentSpacePlayer2Piece1
                    
                    
                    print("Changed it To New One : Player2Piece1")
                    print(rolledDice)
                    print(p2p1TryToWin)
                    print(num)
                    
                    p2p1TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p2p2TryToWin >= num && (currentSpacePlayer2Piece2 == 20 || currentSpacePlayer2Piece2 == 21 || currentSpacePlayer2Piece2 == 22) ) {
                    
                    //   if (25 - currentSpacePlayer1Piece2 == 3 || 25 - currentSpacePlayer1Piece2 == 2 || 25 - currentSpacePlayer1Piece2 == 1) {
                    
                    rolledDice = 23 - currentSpacePlayer2Piece2
                    
                    
                    print("Changed it To New One : Player2Piece2")
                    print(rolledDice)
                    print(p2p2TryToWin)
                    print(num)
                    
                    p2p2TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                } else if (p2p3TryToWin >= num && (currentSpacePlayer2Piece3 == 20 || currentSpacePlayer2Piece3 == 21 || currentSpacePlayer2Piece3 == 22) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece3 == 3 || 25 - currentSpacePlayer1Piece3 == 2 || 25 - currentSpacePlayer1Piece3 == 1) {
                    
                    rolledDice = 23 - currentSpacePlayer2Piece3
                    
                    
                    print("Changed it To New One : Player2Piece3")
                    print(rolledDice)
                    print(p2p3TryToWin)
                    print(num)
                    
                    p2p3TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p2p4TryToWin >= num && (currentSpacePlayer2Piece4 == 20 || currentSpacePlayer2Piece4 == 21 || currentSpacePlayer2Piece4 == 22) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece4 == 3 || 25 - currentSpacePlayer1Piece4 == 2 || 25 - currentSpacePlayer1Piece4 == 1) {
                    
                    rolledDice = 23 - currentSpacePlayer2Piece4
                    
                    
                    print("Changed it To New One : Player2Piece4")
                    print(rolledDice)
                    print(p2p4TryToWin)
                    print(num)
                    
                    p2p4TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                }
                
            } else if ((p3p1TryToWin >= num && (currentSpacePlayer3Piece1 == 18 || currentSpacePlayer3Piece1 == 19 || currentSpacePlayer3Piece1 == 20) ) || (p3p2TryToWin >= num && (currentSpacePlayer3Piece2 == 18 || currentSpacePlayer3Piece2 == 19 || currentSpacePlayer3Piece2 == 20) ) || (p3p3TryToWin >= num && (currentSpacePlayer3Piece3 == 18 || currentSpacePlayer3Piece3 == 19 || currentSpacePlayer3Piece3 == 20) ) || (p3p4TryToWin >= num && (currentSpacePlayer3Piece4 == 18 || currentSpacePlayer3Piece4 == 19 || currentSpacePlayer3Piece4 == 20) )) && (whosTurn == .Player3) {
                
                if (p3p1TryToWin >= num && (currentSpacePlayer3Piece1 == 18 || currentSpacePlayer3Piece1 == 19 || currentSpacePlayer3Piece1 == 20) ) {
                    
                    //     if (25 - currentSpacePlayer1Piece1 == 3 || 25 - currentSpacePlayer1Piece1 == 2 || 25 - currentSpacePlayer1Piece1 == 1) {
                    
                    rolledDice = 21 - currentSpacePlayer3Piece1
                    
                    
                    print("Changed it To New One : Player3Piece1")
                    print(rolledDice)
                    print(p3p1TryToWin)
                    print(num)
                    
                    p3p1TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p3p2TryToWin >= num && (currentSpacePlayer3Piece2 == 18 || currentSpacePlayer3Piece2 == 19 || currentSpacePlayer3Piece2 == 20) ) {
                    
                    //   if (25 - currentSpacePlayer1Piece2 == 3 || 25 - currentSpacePlayer1Piece2 == 2 || 25 - currentSpacePlayer1Piece2 == 1) {
                    
                    rolledDice = 21 - currentSpacePlayer3Piece2
                    
                    
                    print("Changed it To New One : Player3Piece2")
                    print(rolledDice)
                    print(p3p2TryToWin)
                    print(num)
                    
                    p3p2TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                } else if (p3p3TryToWin >= num && (currentSpacePlayer3Piece3 == 18 || currentSpacePlayer3Piece3 == 19 || currentSpacePlayer3Piece3 == 20) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece3 == 3 || 25 - currentSpacePlayer1Piece3 == 2 || 25 - currentSpacePlayer1Piece3 == 1) {
                    
                    rolledDice = 21 - currentSpacePlayer3Piece3
                    
                    
                    print("Changed it To New One : Player3Piece3")
                    print(rolledDice)
                    print(p3p3TryToWin)
                    print(num)
                    
                    p3p3TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p3p4TryToWin >= num && (currentSpacePlayer3Piece4 == 18 || currentSpacePlayer3Piece4 == 19 || currentSpacePlayer3Piece4 == 20) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece4 == 3 || 25 - currentSpacePlayer1Piece4 == 2 || 25 - currentSpacePlayer1Piece4 == 1) {
                    
                    rolledDice = 21 - currentSpacePlayer3Piece4
                    
                    
                    print("Changed it To New One : Player3Piece4")
                    print(rolledDice)
                    print(p3p4TryToWin)
                    print(num)
                    
                    p3p4TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                }
                
            } else if ((p4p1TryToWin >= num && (currentSpacePlayer4Piece1 == 24 || currentSpacePlayer4Piece1 == 17 || currentSpacePlayer4Piece1 == 18) ) || (p4p2TryToWin >= num && (currentSpacePlayer4Piece2 == 24 || currentSpacePlayer4Piece2 == 17 || currentSpacePlayer4Piece2 == 18) ) || (p4p3TryToWin >= num && (currentSpacePlayer4Piece3 == 24 || currentSpacePlayer4Piece3 == 17 || currentSpacePlayer4Piece3 == 18) ) || (p4p4TryToWin >= num && (currentSpacePlayer4Piece4 == 24 || currentSpacePlayer4Piece4 == 17 || currentSpacePlayer4Piece4 == 18) )) && (whosTurn == .Player4) {
                
                if (p4p1TryToWin >= num && (currentSpacePlayer4Piece1 == 24 || currentSpacePlayer4Piece1 == 17 || currentSpacePlayer4Piece1 == 18) ) {
                    
                    //     if (25 - currentSpacePlayer1Piece1 == 3 || 25 - currentSpacePlayer1Piece1 == 2 || 25 - currentSpacePlayer1Piece1 == 1) {
                    
                    rolledDice = 19 - currentSpacePlayer4Piece1
                    
                    if currentSpacePlayer4Piece1 == 24 {
                        
                        rolledDice = 3
                        
                    }
                    
                    
                    print("Changed it To New One : Player4Piece1")
                    print(rolledDice)
                    print(p4p1TryToWin)
                    print(num)
                    
                    p4p1TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p4p2TryToWin >= num && (currentSpacePlayer4Piece2 == 24 || currentSpacePlayer4Piece2 == 17 || currentSpacePlayer4Piece2 == 18) ) {
                    
                    //   if (25 - currentSpacePlayer1Piece2 == 3 || 25 - currentSpacePlayer1Piece2 == 2 || 25 - currentSpacePlayer1Piece2 == 1) {
                    
                    rolledDice = 19 - currentSpacePlayer4Piece2
                    
                    if currentSpacePlayer4Piece2 == 24 {
                        
                        rolledDice = 3
                        
                    }
                    
                    print("Changed it To New One : Player4Piece2")
                    print(rolledDice)
                    print(p4p2TryToWin)
                    print(num)
                    
                    p4p2TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                } else if (p4p3TryToWin >= num && (currentSpacePlayer4Piece3 == 24 || currentSpacePlayer4Piece3 == 17 || currentSpacePlayer4Piece3 == 18) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece3 == 3 || 25 - currentSpacePlayer1Piece3 == 2 || 25 - currentSpacePlayer1Piece3 == 1) {
                    
                    rolledDice = 19 - currentSpacePlayer4Piece3
                    
                    if currentSpacePlayer4Piece3 == 24 {
                        
                        rolledDice = 3
                        
                    }
                    
                    print("Changed it To New One : Player4Piece3")
                    print(rolledDice)
                    print(p4p3TryToWin)
                    print(num)
                    
                    p4p3TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //   }
                    
                } else if (p4p4TryToWin >= num && (currentSpacePlayer4Piece4 == 24 || currentSpacePlayer4Piece4 == 17 || currentSpacePlayer4Piece4 == 18) ) {
                    
                    //    if (25 - currentSpacePlayer1Piece4 == 3 || 25 - currentSpacePlayer1Piece4 == 2 || 25 - currentSpacePlayer1Piece4 == 1) {
                    
                    rolledDice = 19 - currentSpacePlayer4Piece4
                    
                    if currentSpacePlayer4Piece4 == 24 {
                        
                        rolledDice = 3
                        
                    }
                    
                    print("Changed it To New One : Player4Piece4")
                    print(rolledDice)
                    print(p4p4TryToWin)
                    print(num)
                    
                    p4p4TryToWin = 0
                    
                    rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                    
                    Rolls.append(rolledDice)
                    //    }
                    
                }
                
            } else if (whosTurn == .Player1 && ((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece4 < 17) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece2 < 17) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 < 17) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26 && currentSpacePlayer1Piece1 < 17))) {
                
                let sides = [1, 4, 2, 3, 3, 4, 3, 8, 2, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("Got Simple Roll For Last Player : \(rolledDice)")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player1) {
                
                let sides = [1, 3, 2, 1, 8, 3, 2, 4, 2, 3, 2, 3, 4, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("++++++++++Got Simple Roll Player-1 : \(rolledDice)++++++++++")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player2 && ((currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece4 < 17) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece2 < 17) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 < 17) || (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26 && currentSpacePlayer2Piece1 < 17))) {
                
                let sides = [1, 4, 2, 3, 3, 4, 3, 8, 2, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("Got Simple Roll For Last Player : \(rolledDice)")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player2) {
                
                let sides = [1, 3, 2, 1, 8, 3, 2, 4, 2, 3, 2, 3, 4, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("++++++++++Got Simple Roll Player-2 : \(rolledDice)++++++++++")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player3 && ((currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece4 < 17) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece2 < 17) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 < 17) || (currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26 && currentSpacePlayer3Piece1 < 17))) {
                
                let sides = [1, 4, 2, 3, 3, 4, 3, 8, 2, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("Got Simple Roll For Last Player : \(rolledDice)")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player3) {
                
                let sides = [1, 3, 2, 1, 8, 3, 2, 4, 2, 3, 2, 3, 4, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("++++++++++Got Simple Roll Player-3 : \(rolledDice)++++++++++")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player4 && ((currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece4 < 17) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece2 < 17) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 < 17) || (currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26 && currentSpacePlayer4Piece1 < 17))) {
                
                let sides = [1, 4, 2, 3, 3, 4, 3, 8, 2, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("Got Simple Roll For Last Player : \(rolledDice)")
                Rolls.append(rolledDice)
                
            } else if (whosTurn == .Player4) {
                
                let sides = [1, 3, 2, 1, 8, 3, 2, 4, 2, 3, 2, 3, 4, 1]
                // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
                rolledDiceTexture.texture = SKTexture(imageNamed: "Dice\(rolledDice)")
                print("++++++++++Got Simple Roll Player-4 : \(rolledDice)++++++++++")
                Rolls.append(rolledDice)
                
            }
            
            
            if whosTurn == .Player1 {
                
                print("Player 1 Total ROLLS :")
                Player1Rolls.append(rolledDice)
                print(Player1Rolls.count)
                
            } else if whosTurn == .Player2 {
                
                print("Player 2 Total ROLLS :")
                Player2Rolls.append(rolledDice)
                print(Player2Rolls.count)
                
            } else if whosTurn == .Player3 {
                
                print("Player 3 Total ROLLS :")
                Player3Rolls.append(rolledDice)
                print(Player3Rolls.count)
                
            } else if whosTurn == .Player4 {
                
                print("Player 4 Total ROLLS :")
                Player4Rolls.append(rolledDice)
                print(Player4Rolls.count)
                
            }
            
            print("Player's Index :")
            print(player1rans)
            print(player2rans)
            print(player3rans)
            print(player4rans)
            
            
            j += 1
            print("Total Rans : ")
            print(totalRans)
            print(Rolls.count)
            print(rolledDice)
            
            if (totalRans > 2) {
                print("Inside three totla rrans : ")
                print(Rolls[totalRans])
                print(Rolls[totalRans - 1])
                print(Rolls[totalRans - 2])
                if (Rolls[totalRans] == Rolls[totalRans - 1] && Rolls[totalRans] == Rolls[totalRans - 2]) {
                    i += 1
                    print("Inside of It Happened Thrice :")
                    print(Rolls[totalRans])
                    print(Rolls[totalRans - 1])
                    print(Rolls[totalRans - 2])
                    
                } else  if (player1rans > 2) && (whosTurn == .Player1) {
                    
                    print("Player 1 Data :")
                    print(Player1Rolls.count)
                    print(player1rans)
                    print(Player1Rolls[player1rans])
                    print(Player1Rolls[player1rans - 1])
                    print(Player1Rolls[player1rans - 2])
                    
                    if (Player1Rolls[player1rans] == Player1Rolls[player1rans - 1] && Player1Rolls[player1rans] == Player1Rolls[player1rans - 2]) {
                        
                        print("Inside of It Happened Thrice FOR THE SAME PLAYER YA YA YA || Player 1 || :")
                        
                        print(Player1Rolls[0])
                        print(Player1Rolls[1])
                        print(Player1Rolls[2])
                        print(Player1Rolls[player1rans])
                        print(Player1Rolls[player1rans - 1])
                        print(Player1Rolls[player1rans - 2])
                        
                        i += 1
                        
                    } else if ((Player1Rolls[player1rans] == 8 || Player1Rolls[player1rans] == 4) && (Player1Rolls[player1rans - 1] == 8 || Player1Rolls[player1rans - 1] == 4) && (Player1Rolls[player1rans - 2] == 8 || Player1Rolls[player1rans - 2] == 4)) {
                        
                        print("What will happen will it just skip the turn or do cool animation like Going Back :")
                        
                        print("Add Label in Middle from smaller to becaming larger and says Skipped Turn (maybe write for 3's)")
                        
                        print("ANSWER : Moving it back to three times rolled dice will make him lots of troubles and will have to move all again and his strategy will waste too , But just skipping his turn and let him be there and skipping 3rd try will not be too harsh and will also do the job PerfeCt.")
                        
                        print("Must ADD Last piece of human player will get more chances of 4 so won't have to wait and just for last piece player will feel it good and fast rather then always wondering for to get inside area. can very fastly get inside and then try to win.")
                        
                        // movesRemaining = 0
                        // OneMoreMove = 0
                        //  movePiece()
                        rolledDice = 0
                        Player1Rolls.append(rolledDice)
                        player1rans += 1
                    }
                    
                    
                } else if (player2rans > 2) && (whosTurn == .Player2) {
                    
                    if (Player2Rolls[player2rans] == Player2Rolls[player2rans - 1] && Player2Rolls[player2rans] == Player2Rolls[player2rans - 2]) {
                        
                        print("Player 2 Data :")
                        print(Player2Rolls.count)
                        print(player2rans)
                        print(Player2Rolls[player2rans])
                        print(Player2Rolls[player2rans - 1])
                        print(Player2Rolls[player2rans - 2])
                        
                        print("Inside of It Happened Thrice FOR THE SAME PLAYER YA YA YA || Player 2 || :")
                        print(Player2Rolls[player2rans])
                        print(Player2Rolls[player2rans - 1])
                        print(Player2Rolls[player2rans - 2])
                        
                        i += 1
                        
                    } else if ((Player2Rolls[player2rans] == 8 || Player2Rolls[player2rans] == 4) && (Player2Rolls[player2rans - 1] == 8 || Player2Rolls[player2rans - 1] == 4) && (Player2Rolls[player2rans - 2] == 8 || Player2Rolls[player2rans - 2] == 4)) {
                        
                        print("What will happen will it just skip the turn or do cool animation like Going Back :")
                        
                        // movesRemaining = 0
                        //OneMoreMove = 0
                        // movePiece()
                        rolledDice = 0
                        Player2Rolls.append(rolledDice)
                        player2rans += 1
                    }
                    
                } else if (player3rans > 2) && (whosTurn == .Player3) {
                    
                    print("Player 3 Data :")
                    print(Player3Rolls.count)
                    print(player3rans)
                    print(Player3Rolls[player3rans])
                    print(Player3Rolls[player3rans - 1])
                    print(Player3Rolls[player3rans - 2])
                    
                    if (Player3Rolls[player3rans] == Player3Rolls[player3rans - 1] && Player3Rolls[player3rans] == Player3Rolls[player3rans - 2]) {
                        
                        print("Inside of It Happened Thrice FOR THE SAME PLAYER YA YA YA || Player 3 || :")
                        print(Player3Rolls[player3rans])
                        print(Player3Rolls[player3rans - 1])
                        print(Player3Rolls[player3rans - 2])
                        
                        i += 1
                        
                    } else if ((Player3Rolls[player3rans] == 8 || Player3Rolls[player3rans] == 4) && (Player3Rolls[player3rans - 1] == 8 || Player3Rolls[player3rans - 1] == 4) && (Player3Rolls[player3rans - 2] == 8 || Player3Rolls[player3rans - 2] == 4)) {
                        
                        print("What will happen will it just skip the turn or do cool animation like Going Back :")
                        
                        // movesRemaining = 0
                        // OneMoreMove = 0
                        //  movePiece()
                        rolledDice = 0
                        Player3Rolls.append(rolledDice)
                        player3rans += 1
                    }
                    
                } else if (player4rans > 2) && (whosTurn == .Player4) {
                    
                    print("Player 3 Data :")
                    print(Player4Rolls.count)
                    print(player4rans)
                    print(Player4Rolls[player4rans])
                    print(Player4Rolls[player4rans - 1])
                    print(Player4Rolls[player4rans - 2])
                    
                    if (Player4Rolls[player4rans] == Player4Rolls[player4rans - 1] && Player4Rolls[player4rans] == Player4Rolls[player4rans - 2]) {
                        
                        print("Inside of It Happened Thrice FOR THE SAME PLAYER YA YA YA || Player 4 || :")
                        print(Player4Rolls[player4rans])
                        print(Player4Rolls[player4rans - 1])
                        print(Player4Rolls[player4rans - 2])
                        
                        i += 1
                        
                    } else if ((Player4Rolls[player4rans] == 8 || Player4Rolls[player4rans] == 4) && (Player4Rolls[player4rans - 1] == 8 || Player4Rolls[player4rans - 1] == 4) && (Player4Rolls[player4rans - 2] == 8 || Player4Rolls[player4rans - 2] == 4)) {
                        
                        print("What will happen will it just skip the turn or do cool animation like Going Back :")
                        
                        //  movesRemaining = 0
                        //  OneMoreMove = 0
                        //  movePiece()
                        rolledDice = 0
                        Player4Rolls.append(rolledDice)
                        player4rans += 1
                    }
                    
                }
                
                
                
            }
            
            if whosTurn == .Player1 {
                
                player1rans += 1
                
            } else if whosTurn == .Player2 {
                
                player2rans += 1
                
            } else if whosTurn == .Player3 {
                
                player3rans += 1
                
            } else if whosTurn == .Player4 {
                
                player4rans += 1
                
            }
            
            print("The total rolls after that.")
            
            print(player1rans)
            print(player2rans)
            print(player3rans)
            print(player4rans)
            
            
            totalRans += 1
            
        }
        
        
        
        
        
        // let sides = [1, 1, 2, 2, 3, 3, 4, 4, 4]
        // let rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
        // rolledDice = sides[Int(arc4random_uniform(UInt32(sides.count)))]
        // let dostexture = SKTexture(imageNamed: "Dice\(rolledDice)")
        //  diceTextures.append(dostexture)
        
        // rolledDice = dice3d6.nextInt()
        
        
        
        // let imageName =
        //  let diceTexture = SKTexture(imageNamed: "Dice\(rolledDice)")
        //  diceTextures.append(diceTexture)
        
        
        print("Rolled Dice :")
        print(rolledDice)
        print(whosTurn)
        
        let dice = SKSpriteNode(imageNamed: "Dice1")
        dice.name = "dice"
        
        if player == .Player1 {
            
            dice.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
            dice.size = CGSize(width: 90.0, height: 90.0)
            
            rolledDiceTexture.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
            rolledDiceTexture.size = CGSize(width: 90, height: 90)
            
        } else if player == .Player2 {
            
            dice.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
            dice.size = CGSize(width: 90.0, height: 90.0)
            
            rolledDiceTexture.position = CGPoint(x: (scene?.size.width)! * 0.401, y: 0)
            rolledDiceTexture.size = CGSize(width: 90, height: 90)
            
        } else if player == .Player3 {
            
            dice.position = CGPoint(x: (scene?.size.width)! * -0.39, y: 0)
            dice.size = CGSize(width: 90.0, height: 90.0)
            
            rolledDiceTexture.position = CGPoint(x: (scene?.size.width)! * -0.39, y: 0)
            rolledDiceTexture.size = CGSize(width: 90, height: 90)
            
        } else if player == .Player4 {
            
            dice.position = CGPoint(x: (scene?.size.width)! * -0.39, y: 0)
            dice.size = CGSize(width: 90.0, height: 90.0)
            
            rolledDiceTexture.position = CGPoint(x: (scene?.size.width)! * -0.39, y: 0)
            rolledDiceTexture.size = CGSize(width: 90, height: 90)
            
        }
        
        addChild(dice)
        
        
        let diceAnimation = SKAction.animate(with: diceTextures, timePerFrame: 0.05, resize: true, restore: true)
        
        dice.run(diceAnimation) {
            
            
            self.enumerateChildNodes(withName: "dice") { (diceNode, stop) in
                diceNode.removeFromParent()
                print(self.childNode(withName: "dice") ?? "dice IS REMOVED//")
            }
            
            self.rolledDiceTexture.isHidden = false
            
            
            if self.rolledDice == 4 || self.rolledDice == 8 {
                self.OneMoreMove += 1
                
                self.playSound(soundName: self.fourAndEight)
                
                if (self.whosTurn == .Player1) {
                    
                    // self.updateScore() //self.OneMoreMove
                    
                    //  self.oneMoreMovePla.isHidden = false
                    
                    //    let delay = 1.5
                    //    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.updateScore()
                    
                    //   print("Yes!")
                    //  self.oneMoreMovePla.isHidden = true
                    if self.oneMoreMove.isHidden {
                        
                        self.notifyForPlusMove(label: self.oneMoreMove)
                        
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                    //                            self.oneMoreMovePla.setScale(1)
                    
                    // }
                    
                } else if (self.whosTurn == .Player2) {
                    
                    //  self.oneMoreMoveCom1.isHidden = false
                    
                    //  let delay = 1.5
                    //  DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.updateScore()
                    if self.oneMoreMove.isHidden {
                        
                        self.notifyForPlusMove(label: self.oneMoreMove)
                        
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                    
                    //        print("Yes!")
                    //        self.oneMoreMoveCom1.isHidden = true
                    //                            self.oneMoreMoveCom1.setScale(1)
                    
                    //  }
                    
                } else if (self.whosTurn == .Player3) {
                    
                    //    self.oneMoreMoveCom2.isHidden = false
                    // oneMoreMoveCom2.run(SKAction.)
                    
                    //   let delay = 1.5
                    //   DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.updateScore()
                    if self.oneMoreMove.isHidden {
                        
                        self.notifyForPlusMove(label: self.oneMoreMove)
                        
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                    
                    //          print("Yes!")//
                    //         self.oneMoreMoveCom2.isHidden = true
                    //                            self.oneMoreMoveCom2.setScale(1)
                    
                    //   }
                    
                } else if (self.whosTurn == .Player4) {
                    
                    //  self.oneMoreMoveCom3.isHidden = false
                    
                    //   let delay = 1.5
                    //   DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    self.updateScore()
                    
                    
                    if self.oneMoreMove.isHidden {
                        
                        self.notifyForPlusMove(label: self.oneMoreMove)
                        
                    } else {
                        let label:SKLabelNode = SKLabelNode(text: "+1 Turn")
                        self.notifyForPlusMove2(label: label)
                    }
                    
                    //         print("Yes!")
                    //        self.oneMoreMoveCom3.isHidden = true
                    //                            self.oneMoreMoveCom3.setScale(1)
                    
                    //  }
                    
                }
                
            }
            
            self.diceRolled = true
            self.rolling = false
            print("I'm Inside:")
            print(self.whosTurn)
            
            self.HowtoMove(player: player)
            
            print("-> Decreased Player Scores :")
            print(self.playerScore)
            print(self.com1Score)
            print(self.com2Score)
            print(self.com3Score)
            print(self.player1Piece1Score)
            print(self.player1Piece2Score)
            print(self.player1Piece3Score)
            print(self.player1Piece4Score)
            print(self.com1Piece1Score)
            print(self.com1Piece2Score)
            print(self.com1Piece3Score)
            print(self.com1Piece4Score)
            print(self.com2Piece1Score)
            print(self.com2Piece2Score)
            print(self.com2Piece3Score)
            print(self.com2Piece4Score)
            print(self.com3Piece1Score)
            print(self.com3Piece2Score)
            print(self.com3Piece3Score)
            print(self.com3Piece4Score)
            
        }
        
    }
    
    
    
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    
    func playSound(soundName: SKAction) {
        
        if UserDefaults.standard.bool(forKey: "audio") {
            
            run(soundName)
            
        }
    }
    
    
    
    func animateLarge(one:SKSpriteNode) {
        
        one.run(SKAction.scale(to: 1.5, duration: 0.5))
        //  print("In Ani")
    }
    
    func animateSmall(one:SKSpriteNode) {
        
        one.run(SKAction.scale(to: 1, duration: 0.4))
        //  print("In Ani 2")
    }
    
    
    
    
    
    func animations() {
        
        
        if rolling {
            return
        }
        
        if whosTurn == .Player1 {
            Arrow1Red.isHidden = false
            Arrow2Red.isHidden = false
            Arrow3Red.isHidden = false
            Arrow4Red.isHidden = false
        } else if whosTurn == .Player2 {
            Arrow1Yellow.isHidden = false
            Arrow2Yellow.isHidden = false
            Arrow3Yellow.isHidden = false
            Arrow4Yellow.isHidden = false
        } else if whosTurn == .Player3 {
            Arrow1Green.isHidden = false
            Arrow2Green.isHidden = false
            Arrow3Green.isHidden = false
            Arrow4Green.isHidden = false
        } else if whosTurn == .Player4 {
            Arrow1Blue.isHidden = false
            Arrow2Blue.isHidden = false
            Arrow3Blue.isHidden = false
            Arrow4Blue.isHidden = false
        }
        
        
        let action1 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateLarge(one: self.Arrow1Red)}
            if self.whosTurn == .Player2 {self.animateLarge(one: self.Arrow1Yellow)}
            if self.whosTurn == .Player3 {self.animateLarge(one: self.Arrow1Green)}
            if self.whosTurn == .Player4 {self.animateLarge(one: self.Arrow1Blue)}
        }
        
        let action2 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateLarge(one: self.Arrow2Red)}
            if self.whosTurn == .Player2 {self.animateLarge(one: self.Arrow2Yellow)}
            if self.whosTurn == .Player3 {self.animateLarge(one: self.Arrow2Green)}
            if self.whosTurn == .Player4 {self.animateLarge(one: self.Arrow2Blue)}
        }
        
        let action3 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateLarge(one: self.Arrow3Red)}
            if self.whosTurn == .Player2 {self.animateLarge(one: self.Arrow3Yellow)}
            if self.whosTurn == .Player3 {self.animateLarge(one: self.Arrow3Green)}
            if self.whosTurn == .Player4 {self.animateLarge(one: self.Arrow3Blue)}
        }
        
        let action4 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateLarge(one: self.Arrow4Red)}
            if self.whosTurn == .Player2 {self.animateLarge(one: self.Arrow4Yellow)}
            if self.whosTurn == .Player3 {self.animateLarge(one: self.Arrow4Green)}
            if self.whosTurn == .Player4 {self.animateLarge(one: self.Arrow4Blue)}
        }
        
        
        
        let acta1 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateSmall(one: self.Arrow1Red)}
            if self.whosTurn == .Player2 {self.animateSmall(one: self.Arrow1Yellow)}
            if self.whosTurn == .Player3 {self.animateSmall(one: self.Arrow1Green)}
            if self.whosTurn == .Player4 {self.animateSmall(one: self.Arrow1Blue)}
        }
        
        let acta2 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateSmall(one: self.Arrow2Red)}
            if self.whosTurn == .Player2 {self.animateSmall(one: self.Arrow2Yellow)}
            if self.whosTurn == .Player3 {self.animateSmall(one: self.Arrow2Green)}
            if self.whosTurn == .Player4 {self.animateSmall(one: self.Arrow2Blue)}
        }
        
        let acta3 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateSmall(one: self.Arrow3Red)}
            if self.whosTurn == .Player2 {self.animateSmall(one: self.Arrow3Yellow)}
            if self.whosTurn == .Player3 {self.animateSmall(one: self.Arrow3Green)}
            if self.whosTurn == .Player4 {self.animateSmall(one: self.Arrow3Blue)}
        }
        
        let acta4 = SKAction.run {
            if self.whosTurn == .Player1 {self.animateSmall(one: self.Arrow4Red)}
            if self.whosTurn == .Player2 {self.animateSmall(one: self.Arrow4Yellow)}
            if self.whosTurn == .Player3 {self.animateSmall(one: self.Arrow4Green)}
            if self.whosTurn == .Player4 {self.animateSmall(one: self.Arrow4Blue)}
        }
        
        
        
        
        
        
        
        run(.sequence([
            
            
            SKAction.wait(forDuration: 0.4),
            SKAction.run({
                
                self.run(action4, completion: {
                    
                    
                    
                    self.run(.sequence([
                        
                        
                        SKAction.wait(forDuration: 0.2),
                        SKAction.run({
                            
                            self.run(action3, completion: {
                                
                                
                                
                                self.run(.sequence([
                                    
                                    
                                    SKAction.wait(forDuration: 0.2),
                                    SKAction.run({
                                        
                                        self.run(action2, completion: {
                                            
                                            
                                            
                                            self.run(.sequence([
                                                
                                                
                                                SKAction.wait(forDuration: 0.2),
                                                SKAction.run({
                                                    
                                                    self.run(action1, completion: {
                                                        
                                                        
                                                        
                                                        //   print("TO THE DOWN NOW!")
                                                        
                                                        self.run(.sequence([
                                                            
                                                            
                                                            SKAction.wait(forDuration: 0.4),
                                                            SKAction.run({
                                                                
                                                                self.run(acta4, completion: {
                                                                    
                                                                    
                                                                    
                                                                    self.run(.sequence([
                                                                        
                                                                        
                                                                        SKAction.wait(forDuration: 0.2),
                                                                        SKAction.run({
                                                                            
                                                                            self.run(acta3, completion: {
                                                                                
                                                                                
                                                                                
                                                                                self.run(.sequence([
                                                                                    
                                                                                    
                                                                                    SKAction.wait(forDuration: 0.2),
                                                                                    SKAction.run({
                                                                                        
                                                                                        self.run(acta2, completion: {
                                                                                            
                                                                                            
                                                                                            
                                                                                            self.run(.sequence([
                                                                                                
                                                                                                
                                                                                                SKAction.wait(forDuration: 0.2),
                                                                                                SKAction.run({
                                                                                                    
                                                                                                    self.run(acta1, completion: {
                                                                                                        
                                                                                                        
                                                                                                        
                                                                                                        //   print("REPEAT!")
                                                                                                        
                                                                                                        if self.keepAnimating {
                                                                                                            
                                                                                                            self.animations()
                                                                                                        } else {
                                                                                                            return
                                                                                                        }
                                                                                                        
                                                                                                        
                                                                                                    })
                                                                                                    
                                                                                                    
                                                                                                })
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                                ]))
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        })
                                                                                        
                                                                                        
                                                                                    })
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    ]))
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            })
                                                                            
                                                                            
                                                                        })
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        ]))
                                                                    
                                                                    
                                                                    
                                                                    
                                                                })
                                                                
                                                                
                                                            })
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            ]))
                                                        
                                                        
                                                    })
                                                    
                                                    
                                                })
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                ]))
                                            
                                            
                                            
                                        })
                                        
                                        
                                    })
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    ]))
                                
                                
                                
                                
                            })
                            
                            
                        })
                        
                        
                        
                        
                        
                        
                        
                        ]))
                    
                    
                    
                })
                
                
            })
            
            
            
            
            
            
            
            ]))
        
    }
    
    
    
    
    
    func notifyForTurnBurn(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0 - 20
        let secondY: CGFloat = 0
        let thirdY: CGFloat = 0 + 20
        let fourthY = -middleLocation - 50
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
    }
    
    
    
    func notifyForCantMove(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0 - 20
        let secondY: CGFloat = 0
        let thirdY: CGFloat = 0 + 20
        let fourthY = -middleLocation - 50
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
    }
    
    
    
    func notifyForPlusMove2(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0
        let secondY: CGFloat = 0 + 20
        let thirdY: CGFloat = 0 + 40
        let fourthY = -middleLocation - 50
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
    }
    
    
    
    
    
    func notifyForPlusMove(label:SKLabelNode) {
        
        
        let middleLocation: CGFloat = (scene?.size.width)! / 2
        let firstY: CGFloat = 0 - 20
        let secondY: CGFloat = 0
        let thirdY: CGFloat = 0 + 20
        let fourthY = -middleLocation - 50
        //  let firstY =
        
        //        if (!oneMoreMove.isHidden || !cantMovePla.isHidden) || (!threeLargeNumsinrowPla.isHidden || !cantMovePla.isHidden) || (!oneMoreMove.isHidden || !threeLargeNumsinrowPla.isHidden) {
        //
        //            firstY = 0 - 50
        //            secondY = 0 - 30
        //            thirdY = 0 - 10
        //
        //
        //        } else if !cantMovePla.isHidden || !threeLargeNumsinrowPla.isHidden || !oneMoreMove.isHidden {
        //
        //            firstY = 0
        //            secondY = 0 + 20
        //            thirdY = 0 + 40
        //
        //        }
        
        
        
        label.isHidden = false
        
        
        
        let firstMove = SKAction.moveTo(y: firstY, duration: 0.7)
        
        let secondMove = SKAction.moveTo(y: secondY, duration: 0.2)
        
        let wait = SKAction.wait(forDuration: 0.6)
        
        let thirdMove = SKAction.moveTo(y: thirdY, duration: 0.2)
        
        let fourthMove = SKAction.moveTo(y: fourthY, duration: 0.7)
        
        label.run(SKAction.sequence([firstMove, secondMove, wait, thirdMove, fourthMove])) {
            label.position = CGPoint(x: 0, y: middleLocation + 50)
            label.isHidden = true
        }
        
        
        
        
    }
    
    
    
    
    func blink(player: Player) {
        
        var eyes = [SKSpriteNode]()
        //        var inc: Int = 1
        
        
        
        
        let firstFlip = SKAction.scaleY(to: 0.0, duration: 0.2)
        firstFlip.timingMode = .linear
        let secondFlip = SKAction.scaleY(to: 1.0, duration: 0.2)
        secondFlip.timingMode = .linear
        
        
        
        
        
        let wait = SKAction.wait(forDuration: 0.3)
        
        let wait2 = SKAction.wait(forDuration: 1.3)
        
        
        print("In SIde THe BliNKa :")
        
        if player == .Player1 {
            
            if player1piece1AbletoEnter && currentSpacePlayer1Piece1 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece1 == 16) {
                eyes.append(player1Piece1eye1)
                
                eyes.append(player1Piece1eye2)
            }
            if player1piece2AbletoEnter && currentSpacePlayer1Piece2 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece2 == 16) {
                eyes.append(player1Piece2eye1)
                eyes.append(player1Piece2eye2)
            }
            if player1piece3AbletoEnter && currentSpacePlayer1Piece3 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece3 == 16) {
                eyes.append(player1Piece3eye1)
                eyes.append(player1Piece3eye2)
            }
            if player1piece4AbletoEnter && currentSpacePlayer1Piece4 != 26 && !(!BloodedPlayer1 && currentSpacePlayer1Piece4 == 16) {
                eyes.append(player1Piece4eye1)
                eyes.append(player1Piece4eye2)
            }
            
        } else if player == .Player2 {
            
            if player2piece1AbletoEnter && currentSpacePlayer2Piece1 != 26 && !(!BloodedPlayer2 && currentSpacePlayer2Piece1 == 4) {
                eyes.append(player2Piece1eye1)
                eyes.append(player2Piece1eye2)
            }
            if player2piece2AbletoEnter && currentSpacePlayer2Piece2 != 26 && !(!BloodedPlayer2 && currentSpacePlayer2Piece2 == 4) {
                eyes.append(player2Piece2eye1)
                eyes.append(player2Piece2eye2)
            }
            if player2piece3AbletoEnter && currentSpacePlayer2Piece3 != 26 && !(!BloodedPlayer2 && currentSpacePlayer2Piece3 == 4) {
                eyes.append(player2Piece3eye1)
                eyes.append(player2Piece3eye2)
            }
            if player2piece4AbletoEnter && currentSpacePlayer2Piece4 != 26 && !(!BloodedPlayer2 && currentSpacePlayer2Piece4 == 4) {
                eyes.append(player2Piece4eye1)
                eyes.append(player2Piece4eye2)
            }
            
        } else if player == .Player3 {
            
            if player3piece1AbletoEnter && currentSpacePlayer3Piece1 != 26 && !(!BloodedPlayer3 && currentSpacePlayer3Piece1 == 8) {
                eyes.append(player3Piece1eye1)
                eyes.append(player3Piece1eye2)
            }
            if player3piece2AbletoEnter && currentSpacePlayer3Piece2 != 26 && !(!BloodedPlayer3 && currentSpacePlayer3Piece2 == 8) {
                eyes.append(player3Piece2eye1)
                eyes.append(player3Piece2eye2)
            }
            if player3piece3AbletoEnter && currentSpacePlayer3Piece3 != 26 && !(!BloodedPlayer3 && currentSpacePlayer3Piece3 == 8) {
                eyes.append(player3Piece3eye1)
                eyes.append(player3Piece3eye2)
            }
            if player3piece4AbletoEnter && currentSpacePlayer3Piece4 != 26 && !(!BloodedPlayer3 && currentSpacePlayer3Piece4 == 8) {
                eyes.append(player3Piece4eye1)
                eyes.append(player3Piece4eye2)
            }
            
        } else if player == .Player4 {
            
            if player4piece1AbletoEnter && currentSpacePlayer4Piece1 != 26 && !(!BloodedPlayer4 && currentSpacePlayer4Piece1 == 12) {
                eyes.append(player4Piece1eye1)
                eyes.append(player4Piece1eye2)
            }
            if player4piece2AbletoEnter && currentSpacePlayer4Piece2 != 26 && !(!BloodedPlayer4 && currentSpacePlayer4Piece2 == 12) {
                eyes.append(player4Piece2eye1)
                eyes.append(player4Piece2eye2)
            }
            if player4piece3AbletoEnter && currentSpacePlayer4Piece3 != 26 && !(!BloodedPlayer4 && currentSpacePlayer4Piece3 == 12) {
                eyes.append(player4Piece3eye1)
                eyes.append(player4Piece3eye2)
            }
            if player4piece4AbletoEnter && currentSpacePlayer4Piece4 != 26 && !(!BloodedPlayer4 && currentSpacePlayer4Piece4 == 12) {
                eyes.append(player4Piece4eye1)
                eyes.append(player4Piece4eye2)
            }
            
        }
        
        
        
        
        for eye in eyes {
            
            print(eye)
            
            eye.setScale(1)
            
            let sara = SKAction.run {
                
                eye.texture = self.openEye
                eye.size = CGSize(width: 6, height: 6)
                eye.yScale = 0
                eye.xScale = 1
                eye.run(secondFlip)
                
            }
            
            
            let kara = SKAction.run {
                
                eye.texture = self.closeEye
                eye.setScale(1)
                eye.size = CGSize(width: 6, height: 1)
            }
            
            let vel = SKAction.sequence([firstFlip, kara, wait, sara, wait2])
            
            
            
            eye.run(SKAction.repeatForever(vel)) {
                print("How's It?")
            }
            
        }
        
        
        
    }
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    func setupLabels() {
        
        
        oneMoreMove = SKLabelNode(fontNamed: "Courier")
        oneMoreMove.fontSize = 90
        oneMoreMove.fontColor = UIColor.darkGray
        oneMoreMove.text = "+1 Turn"
        oneMoreMove.zPosition = 15
        oneMoreMove.position = CGPoint(x: 0, y: ((scene?.size.width)! / 2) + 50)
        oneMoreMove.isHidden = true
        addChild(oneMoreMove)
        
        
        cantMove = SKLabelNode(fontNamed: "Courier")
        cantMove.fontSize = 70
        cantMove.fontColor = UIColor.darkGray
        cantMove.text = "Can't Move"
        cantMove.zPosition = 15
        cantMove.position = CGPoint(x: 0, y: ((scene?.size.width)! / 2) + 50)
        cantMove.isHidden = true
        addChild(cantMove)
        
        
        threeLargeNum = SKLabelNode(fontNamed: "Courier")
        threeLargeNum.fontSize = 60
        threeLargeNum.fontColor = UIColor.darkGray
        threeLargeNum.text = "Third Turn Burned!"
        threeLargeNum.zPosition = 15
        threeLargeNum.position = CGPoint(x: 0, y: ((scene?.size.width)! / 2) + 50)
        threeLargeNum.isHidden = true
        addChild(threeLargeNum)
        
        
        if playerOne.isEmpty {
            
            playerOne = "Player 1"
            
        }
        
        playerLabel = SKLabelNode(fontNamed: "Courier")
        playerLabel.fontSize = 20
        playerLabel.fontColor = textColore
        playerLabel.text = playerOne
        playerLabel.position = CGPoint(x: (scene?.size.width)! * 0.40, y: (scene?.size.height)! * -0.493)
        playerLabel.zPosition = 1
        addChild(playerLabel)
        
        if playerTwo.isEmpty {
            
            playerTwo = "Player 2"
            
        }
        
        player2Label = SKLabelNode(fontNamed: "Courier")
        player2Label.fontSize = 20
        player2Label.fontColor = textColore
        player2Label.text = playerTwo
        player2Label.position = CGPoint(x: (scene?.size.width)! * 0.40, y: (scene?.size.height)! * 0.455)
        player2Label.zPosition = 1
        addChild(player2Label)
        
        if playerThree.isEmpty {
            
            playerThree = "Player 3"
            
        }
        
        player3Label = SKLabelNode(fontNamed: "Courier")
        player3Label.fontSize = 20
        player3Label.fontColor = textColore
        player3Label.text = playerThree
        player3Label.position = CGPoint(x: (scene?.size.width)! * -0.40, y: (scene?.size.height)! * 0.455)
        player3Label.zPosition = 1
        addChild(player3Label)
        
        if playerFour.isEmpty {
            
            playerFour = "Player 4"
            
        }
        
        player4Label = SKLabelNode(fontNamed: "Courier")
        player4Label.fontSize = 20
        player4Label.fontColor = textColore
        player4Label.text = playerFour
        player4Label.position = CGPoint(x: (scene?.size.width)! * -0.40, y: (scene?.size.height)! * -0.48)
        player4Label.zPosition = 1
        addChild(player4Label)
        
        
        
        plaTTtext = SKLabelNode(fontNamed: "Courier")
        plaTTtext.fontSize = 15
        plaTTtext.fontColor = textColore
        plaTTtext.text = "ToTal Turns:\(playerTturns)"
        plaTTtext.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * -0.34)
        plaTTtext.zPosition = 1
        addChild(plaTTtext)
        
        
        com1TTtext = SKLabelNode(fontNamed: "Courier")
        com1TTtext.fontSize = 15
        com1TTtext.fontColor = textColore
        com1TTtext.text = "ToTal Turns:\(player2Tturns)"
        com1TTtext.position = CGPoint(x: (scene?.size.width)! * 0.409, y: (scene?.size.height)! * 0.306)
        com1TTtext.zPosition = 1
        addChild(com1TTtext)
        
        
        
        com2TTtext = SKLabelNode(fontNamed: "Courier")
        com2TTtext.fontSize = 15
        com2TTtext.fontColor = textColore
        com2TTtext.text = "ToTal Turns:\(player3Tturns)"
        com2TTtext.position = CGPoint(x: (scene?.size.width)! * -0.399, y: (scene?.size.height)! * 0.306)
        com2TTtext.zPosition = 1
        addChild(com2TTtext)
        
        
        com3TTtext = SKLabelNode(fontNamed: "Courier")
        com3TTtext.fontSize = 15
        com3TTtext.fontColor = textColore
        com3TTtext.text = "ToTal Turns:\(player4Tturns)"
        com3TTtext.position = CGPoint(x: (scene?.size.width)! * -0.399, y: (scene?.size.height)! * -0.33)
        com3TTtext.zPosition = 1
        addChild(com3TTtext)
        
        
        playerScoreLabel = SKLabelNode(fontNamed: "Courier")
        playerScoreLabel.fontSize = 45
        playerScoreLabel.fontColor = UIColor.black
        playerScoreLabel.text = "\(playerScore)"
        playerScoreLabel.horizontalAlignmentMode = .center
        playerScoreLabel.position = CGPoint(x: (scene?.size.width)! * 0.408, y: (scene?.size.height)! * -0.44)
        playerScoreLabel.zPosition = 2
        addChild(playerScoreLabel)
        
        com1ScoreLabel = SKLabelNode(fontNamed: "Courier")
        com1ScoreLabel.fontSize = 45
        com1ScoreLabel.fontColor = UIColor.black
        com1ScoreLabel.text = "\(com1Score)"
        com1ScoreLabel.horizontalAlignmentMode = .center
        com1ScoreLabel.position = CGPoint(x: (scene?.size.width)! * 0.408, y: (scene?.size.height)! * 0.353)
        com1ScoreLabel.zPosition = 2
        addChild(com1ScoreLabel)
        
        com2ScoreLabel = SKLabelNode(fontNamed: "Courier")
        com2ScoreLabel.fontSize = 45
        com2ScoreLabel.fontColor = UIColor.black
        com2ScoreLabel.text = "\(com2Score)"
        com2ScoreLabel.horizontalAlignmentMode = .center
        com2ScoreLabel.position = CGPoint(x: (scene?.size.width)! * -0.401, y: (scene?.size.height)! * 0.353)
        com2ScoreLabel.zPosition = 2
        addChild(com2ScoreLabel)
        
        com3ScoreLabel = SKLabelNode(fontNamed: "Courier")
        com3ScoreLabel.fontSize = 45
        com3ScoreLabel.fontColor = UIColor.black
        com3ScoreLabel.text = "\(com3Score)"
        com3ScoreLabel.horizontalAlignmentMode = .center
        com3ScoreLabel.position = CGPoint(x: (scene?.size.width)! * -0.401, y: (scene?.size.height)! * -0.425)
        com3ScoreLabel.zPosition = 2
        addChild(com3ScoreLabel)
        
        
        if !(playerLabel.text?.isEmpty)! {
            
            playerone = playerLabel.text!
            
        } else {
            playerone = "Player 1"
        }
        
        if !(player2Label.text?.isEmpty)! {
            
            playertwo = player2Label.text!
            
        } else {
            playertwo = "Player 2"
        }
        
        if !(player3Label.text?.isEmpty)! {
            
            playerthree = player3Label.text!
            
        } else {
            playerthree = "Player 3"
        }
        
        
        if !(player4Label.text?.isEmpty)! {
            
            playerfour = player4Label.text!
            
        } else {
            playerfour = "Player 4"
        }
        
        
    }
    
    
    
    
    
    func DincreamentScore() {
        
        print("In Dincreament :")
        
        if (whosTurn == .Player1) {
            
            print("In Dincreament Player 1 :")
            
            print(playerTturns)
            playerTturns -= 1
            print(playerTturns)
            plaTTtext.text = "ToTal Turns:\(playerTturns)"
            
        } else if (whosTurn == .Player2) {
            
            print("In Dincreament Player 2 :")
            
            print(player2Tturns)
            player2Tturns -= 1
            print(player2Tturns)
            com1TTtext.text = "ToTal Turns:\(player2Tturns)"
            
        } else if (whosTurn == .Player3) {
            
            print("In Dincreament Player 3 :")
            
            print(player3Tturns)
            player3Tturns -= 1
            print(player3Tturns)
            com2TTtext.text = "ToTal Turns:\(player3Tturns)"
            
        } else if (whosTurn == .Player4) {
            
            print("In Dincreament Player 4 :")
            
            print(player4Tturns)
            player4Tturns -= 1
            print(player4Tturns)
            com3TTtext.text = "ToTal Turns:\(player4Tturns)"
            
        }
        
    }
    
    
    
    
    func DincreamentPlayerScore(player: Player, killedPiece: SKSpriteNode) {
        
        print("-> Inside Dincriment Player Score !!!")
        
        if killedPiece == Player1Piece1 {
            
            player1Piece1Score = 1
            
            print("Decreased Player 1 Piece 1 : \(player1Piece1Score)")
            
        } else if killedPiece == Player1Piece2 {
            
            player1Piece2Score = 1
            
            print("Decreased Player 1 Piece 2 : \(player1Piece2Score)")
            
        } else if killedPiece == Player1Piece3 {
            
            player1Piece3Score = 1
            
            print("Decreased Player 1 Piece 3 : \(player1Piece3Score)")
            
        } else if killedPiece == Player1Piece4 {
            
            player1Piece4Score = 1
            
            print("Decreased Player 1 Piece 4 : \(player1Piece4Score)")
            
        }
        
        
        
        
        if killedPiece == Player2Piece1 {
            
            com1Piece1Score = 1
            
            print("Decreased Player 2 Piece 1 : \(com1Piece1Score)")
            
        } else if killedPiece == Player2Piece2 {
            
            com1Piece2Score = 1
            
            print("Decreased Player 2 Piece 2 : \(com1Piece2Score)")
            
        } else if killedPiece == Player2Piece3 {
            
            com1Piece3Score = 1
            
            print("Decreased Player 2 Piece 3 : \(com1Piece3Score)")
            
        } else if killedPiece == Player2Piece4 {
            
            com1Piece4Score = 1
            
            print("Decreased Player 2 Piece 4 : \(com1Piece4Score)")
            
        }
        
        
        
        
        
        if killedPiece == Player3Piece1 {
            
            com2Piece1Score = 1
            
            print("Decreased Player 3 Piece 1 : \(com2Piece1Score)")
            
        } else if killedPiece == Player3Piece2 {
            
            com2Piece2Score = 1
            
            print("Decreased Player 3 Piece 2 : \(com2Piece2Score)")
            
        } else if killedPiece == Player3Piece3 {
            
            com2Piece3Score = 1
            
            print("Decreased Player 3 Piece 3 : \(com2Piece3Score)")
            
        } else if killedPiece == Player3Piece4 {
            
            com2Piece4Score = 1
            
            print("Decreased Player 3 Piece 4 : \(com2Piece4Score)")
            
        }
        
        
        
        
        
        if killedPiece == Player4Piece1 {
            
            com3Piece1Score = 1
            
            print("Decreased Player 4 Piece 1 : \(com3Piece1Score)")
            
        } else if killedPiece == Player4Piece2 {
            
            com3Piece2Score = 1
            
            print("Decreased Player 4 Piece 2 : \(com3Piece2Score)")
            
        } else if killedPiece == Player4Piece3 {
            
            com3Piece3Score = 1
            
            print("Decreased Player 4 Piece 3 : \(com3Piece3Score)")
            
        } else if killedPiece == Player4Piece4 {
            
            com3Piece4Score = 1
            
            print("Decreased Player 4 Piece 4 : \(com3Piece4Score)")
            
        }
        
        
        
        
        playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
        com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
        com2Score = (com2Piece1Score - 1) + (com2Piece2Score - 1) + (com2Piece3Score - 1) + (com2Piece4Score - 1)
        com3Score = (com3Piece1Score - 1) + (com3Piece2Score - 1) + (com3Piece3Score - 1) + (com3Piece4Score - 1)
        
        print("-> Decreased Final Player :")
        print(playerScore)
        print(com1Score)
        print(com2Score)
        print(com3Score)
        
        
        playerScoreLabel.text = "\(playerScore)"
        com1ScoreLabel.text = "\(com1Score)"
        com2ScoreLabel.text = "\(com2Score)"
        com3ScoreLabel.text = "\(com3Score)"
        
    }
    
    
    func IncreasePlayerScore(player: Player, playerpiece: SKSpriteNode) {
        
        
        print("-> Decreased Player Scores :")
        print(playerScore)
        print(com1Score)
        print(com2Score)
        print(com3Score)
        print(player1Piece1Score)
        print(player1Piece2Score)
        print(player1Piece3Score)
        print(player1Piece4Score)
        print(com1Piece1Score)
        print(com1Piece2Score)
        print(com1Piece3Score)
        print(com1Piece4Score)
        print(com2Piece1Score)
        print(com2Piece2Score)
        print(com2Piece3Score)
        print(com2Piece4Score)
        print(com3Piece1Score)
        print(com3Piece2Score)
        print(com3Piece3Score)
        print(com3Piece4Score)
        
        if (player == .Player1 && !(((currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece3 == 26) || (currentSpacePlayer1Piece1 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece4 == 26) || (currentSpacePlayer1Piece4 == 26 && currentSpacePlayer1Piece2 == 26 && currentSpacePlayer1Piece3 == 26)) && (dontScoreP1)) ) {
            
            if playerpiece == Player1Piece1 {
                
                player1Piece1Score += 1
                
            } else if playerpiece == Player1Piece2 {
                
                player1Piece2Score += 1
                
            } else if playerpiece == Player1Piece3 {
                
                player1Piece3Score += 1
                
            } else if playerpiece == Player1Piece4 {
                
                player1Piece4Score += 1
                
            }
            
            playerScore = (player1Piece1Score - 1) + (player1Piece2Score - 1) + (player1Piece3Score - 1) + (player1Piece4Score - 1)
            
            playerScoreLabel.text = "\(playerScore)"
            
        } else if (player == .Player2 && !(((currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece3 == 26) || (currentSpacePlayer2Piece1 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece4 == 26) || (currentSpacePlayer2Piece4 == 26 && currentSpacePlayer2Piece2 == 26 && currentSpacePlayer2Piece3 == 26)) && (dontScoreP2)) ) {
            
            if playerpiece == Player2Piece1 {
                
                com1Piece1Score += 1
                
            } else if playerpiece == Player2Piece2 {
                
                com1Piece2Score += 1
                
            } else if playerpiece == Player2Piece3 {
                
                com1Piece3Score += 1
                
            } else if playerpiece == Player2Piece4 {
                
                com1Piece4Score += 1
                
            }
            
            com1Score = (com1Piece1Score - 1) + (com1Piece2Score - 1) + (com1Piece3Score - 1) + (com1Piece4Score - 1)
            
            com1ScoreLabel.text = "\(com1Score)"
            
        } else if (player == .Player3 && !(((currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece3 == 26) || (currentSpacePlayer3Piece1 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece4 == 26) || (currentSpacePlayer3Piece4 == 26 && currentSpacePlayer3Piece2 == 26 && currentSpacePlayer3Piece3 == 26)) && (dontScoreP3)) ) {
            
            if playerpiece == Player3Piece1 {
                
                com2Piece1Score += 1
                
            } else if playerpiece == Player3Piece2 {
                
                com2Piece2Score += 1
                
            } else if playerpiece == Player3Piece3 {
                
                com2Piece3Score += 1
                
            } else if playerpiece == Player3Piece4 {
                
                com2Piece4Score += 1
                
            }
            
            com2Score = (com2Piece1Score - 1) + (com2Piece2Score - 1) + (com2Piece3Score - 1) + (com2Piece4Score - 1)
            
            com2ScoreLabel.text = "\(com2Score)"
            
        } else if (player == .Player4 && !(((currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece3 == 26) || (currentSpacePlayer4Piece1 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece4 == 26) || (currentSpacePlayer4Piece4 == 26 && currentSpacePlayer4Piece2 == 26 && currentSpacePlayer4Piece3 == 26)) && (dontScoreP4)) ) {
            
            if playerpiece == Player4Piece1 {
                
                com3Piece1Score += 1
                
            } else if playerpiece == Player4Piece2 {
                
                com3Piece2Score += 1
                
            } else if playerpiece == Player4Piece3 {
                
                com3Piece3Score += 1
                
            } else if playerpiece == Player4Piece4 {
                
                com3Piece4Score += 1
                
            }
            
            com3Score = (com3Piece1Score - 1) + (com3Piece2Score - 1) + (com3Piece3Score - 1) + (com3Piece4Score - 1)
            
            com3ScoreLabel.text = "\(com3Score)"
            
        }
        
        
        print("-> Decreased Player Scores :")
        print(playerScore)
        print(com1Score)
        print(com2Score)
        print(com3Score)
        print(player1Piece1Score)
        print(player1Piece2Score)
        print(player1Piece3Score)
        print(player1Piece4Score)
        print(com1Piece1Score)
        print(com1Piece2Score)
        print(com1Piece3Score)
        print(com1Piece4Score)
        print(com2Piece1Score)
        print(com2Piece2Score)
        print(com2Piece3Score)
        print(com2Piece4Score)
        print(com3Piece1Score)
        print(com3Piece2Score)
        print(com3Piece3Score)
        print(com3Piece4Score)
        
        
    }
    
    
    func updateScore() {
        
        print("In Update :")
        
        if (whosTurn == .Player1) {
            
            print("In Update Player 1 :")
            print(playerTturns)
            playerTturns += 1
            print(playerTturns)
            plaTTtext.text = "ToTal Turns:\(playerTturns)"
            
        } else if (whosTurn == .Player2) {
            
            print("In Update Player 2 :")
            print(player2Tturns)
            player2Tturns += 1
            print(player2Tturns)
            com1TTtext.text = "ToTal Turns:\(player2Tturns)"
            
        } else if (whosTurn == .Player3) {
            
            print("In Update Player 3 :")
            print(player3Tturns)
            player3Tturns += 1
            print(player3Tturns)
            com2TTtext.text = "ToTal Turns:\(player3Tturns)"
            
        } else if (whosTurn == .Player4) {
            
            print("In Update Player 4 :")
            print(player4Tturns)
            player4Tturns += 1
            print(player4Tturns)
            com3TTtext.text = "ToTal Turns:\(player4Tturns)"
            
        }
    }
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    func removeFlashTurn(player:Player) {
        
        
        if (player == .Player1) {
            
            
            playerLabel.removeAllActions()
            playerLabel.alpha = 1
            playerLabel.setScale(1)
            
        } else if (player == .Player2) {
            
            player2Label.removeAllActions()
            player2Label.alpha = 1
            player2Label.setScale(1)
            
        } else if (player == .Player3) {
            
            player3Label.removeAllActions()
            player3Label.alpha = 1
            player3Label.setScale(1)
            
        } else if (player == .Player4) {
            
            player4Label.removeAllActions()
            player4Label.alpha = 1
            player4Label.setScale(1)
            
        }
        
        
    }
    
    
    func removeFlashPlayer(player:Player) {
        
        if (player == .Player1) {
            
            
            player1Piece1eye1.removeAllActions()
            player1Piece1eye2.removeAllActions()
            
            player1Piece1eye1.texture = self.openEye
            player1Piece1eye1.size = CGSize(width: 6, height: 6)
            player1Piece1eye1.yScale = 1
            player1Piece1eye1.xScale = 1
            
            player1Piece1eye2.texture = self.openEye
            player1Piece1eye2.size = CGSize(width: 6, height: 6)
            player1Piece1eye2.yScale = 1
            player1Piece1eye2.xScale = 1
            
            player1Piece2eye1.removeAllActions()
            player1Piece2eye2.removeAllActions()
            
            player1Piece2eye1.texture = self.openEye
            player1Piece2eye1.size = CGSize(width: 6, height: 6)
            player1Piece2eye1.yScale = 1
            player1Piece2eye1.xScale = 1
            
            player1Piece2eye2.texture = self.openEye
            player1Piece2eye2.size = CGSize(width: 6, height: 6)
            player1Piece2eye2.yScale = 1
            player1Piece2eye2.xScale = 1
            
            player1Piece3eye1.removeAllActions()
            player1Piece3eye2.removeAllActions()
            
            player1Piece3eye1.texture = self.openEye
            player1Piece3eye1.size = CGSize(width: 6, height: 6)
            player1Piece3eye1.yScale = 1
            player1Piece3eye1.xScale = 1
            
            player1Piece3eye2.texture = self.openEye
            player1Piece3eye2.size = CGSize(width: 6, height: 6)
            player1Piece3eye2.yScale = 1
            player1Piece3eye2.xScale = 1
            
            player1Piece4eye1.removeAllActions()
            player1Piece4eye2.removeAllActions()
            
            player1Piece4eye1.texture = self.openEye
            player1Piece4eye1.size = CGSize(width: 6, height: 6)
            player1Piece4eye1.yScale = 1
            player1Piece4eye1.xScale = 1
            
            player1Piece4eye2.texture = self.openEye
            player1Piece4eye2.size = CGSize(width: 6, height: 6)
            player1Piece4eye2.yScale = 1
            player1Piece4eye2.xScale = 1
            
        } else if (player == .Player2) {
            
            player2Piece1eye1.removeAllActions()
            player2Piece1eye2.removeAllActions()
            
            player2Piece1eye1.texture = self.openEye
            player2Piece1eye1.size = CGSize(width: 6, height: 6)
            player2Piece1eye1.yScale = 1
            player2Piece1eye1.xScale = 1
            
            player2Piece1eye2.texture = self.openEye
            player2Piece1eye2.size = CGSize(width: 6, height: 6)
            player2Piece1eye2.yScale = 1
            player2Piece1eye2.xScale = 1
            
            player2Piece2eye1.removeAllActions()
            player2Piece2eye2.removeAllActions()
            
            player2Piece2eye1.texture = self.openEye
            player2Piece2eye1.size = CGSize(width: 6, height: 6)
            player2Piece2eye1.yScale = 1
            player2Piece2eye1.xScale = 1
            
            player2Piece2eye2.texture = self.openEye
            player2Piece2eye2.size = CGSize(width: 6, height: 6)
            player2Piece2eye2.yScale = 1
            player2Piece2eye2.xScale = 1
            
            player2Piece3eye1.removeAllActions()
            player2Piece3eye2.removeAllActions()
            
            player2Piece3eye1.texture = self.openEye
            player2Piece3eye1.size = CGSize(width: 6, height: 6)
            player2Piece3eye1.yScale = 1
            player2Piece3eye1.xScale = 1
            
            player2Piece3eye2.texture = self.openEye
            player2Piece3eye2.size = CGSize(width: 6, height: 6)
            player2Piece3eye2.yScale = 1
            player2Piece3eye2.xScale = 1
            
            player2Piece4eye1.removeAllActions()
            player2Piece4eye2.removeAllActions()
            
            player2Piece4eye1.texture = self.openEye
            player2Piece4eye1.size = CGSize(width: 6, height: 6)
            player2Piece4eye1.yScale = 1
            player2Piece4eye1.xScale = 1
            
            player2Piece4eye2.texture = self.openEye
            player2Piece4eye2.size = CGSize(width: 6, height: 6)
            player2Piece4eye2.yScale = 1
            player2Piece4eye2.xScale = 1
            
        } else if (player == .Player3) {
            
            player3Piece1eye1.removeAllActions()
            player3Piece1eye2.removeAllActions()
            
            player3Piece1eye1.texture = self.openEye
            player3Piece1eye1.size = CGSize(width: 6, height: 6)
            player3Piece1eye1.yScale = 1
            player3Piece1eye1.xScale = 1
            
            player3Piece1eye2.texture = self.openEye
            player3Piece1eye2.size = CGSize(width: 6, height: 6)
            player3Piece1eye2.yScale = 1
            player3Piece1eye2.xScale = 1
            
            player3Piece2eye1.removeAllActions()
            player3Piece2eye2.removeAllActions()
            
            player3Piece2eye1.texture = self.openEye
            player3Piece2eye1.size = CGSize(width: 6, height: 6)
            player3Piece2eye1.yScale = 1
            player3Piece2eye1.xScale = 1
            
            player3Piece2eye2.texture = self.openEye
            player3Piece2eye2.size = CGSize(width: 6, height: 6)
            player3Piece2eye2.yScale = 1
            player3Piece2eye2.xScale = 1
            
            player3Piece3eye1.removeAllActions()
            player3Piece3eye2.removeAllActions()
            
            player3Piece3eye1.texture = self.openEye
            player3Piece3eye1.size = CGSize(width: 6, height: 6)
            player3Piece3eye1.yScale = 1
            player3Piece3eye1.xScale = 1
            
            player3Piece3eye2.texture = self.openEye
            player3Piece3eye2.size = CGSize(width: 6, height: 6)
            player3Piece3eye2.yScale = 1
            player3Piece3eye2.xScale = 1
            
            player3Piece4eye1.removeAllActions()
            player3Piece4eye2.removeAllActions()
            
            player3Piece4eye1.texture = self.openEye
            player3Piece4eye1.size = CGSize(width: 6, height: 6)
            player3Piece4eye1.yScale = 1
            player3Piece4eye1.xScale = 1
            
            player3Piece4eye2.texture = self.openEye
            player3Piece4eye2.size = CGSize(width: 6, height: 6)
            player3Piece4eye2.yScale = 1
            player3Piece4eye2.xScale = 1
            
            
        } else if (player == .Player4) {
            
            player4Piece1eye1.removeAllActions()
            player4Piece1eye2.removeAllActions()
            
            player4Piece1eye1.texture = self.openEye
            player4Piece1eye1.size = CGSize(width: 6, height: 6)
            player4Piece1eye1.yScale = 1
            player4Piece1eye1.xScale = 1
            
            player4Piece1eye2.texture = self.openEye
            player4Piece1eye2.size = CGSize(width: 6, height: 6)
            player4Piece1eye2.yScale = 1
            player4Piece1eye2.xScale = 1
            
            player4Piece2eye1.removeAllActions()
            player4Piece2eye2.removeAllActions()
            
            player4Piece2eye1.texture = self.openEye
            player4Piece2eye1.size = CGSize(width: 6, height: 6)
            player4Piece2eye1.yScale = 1
            player4Piece2eye1.xScale = 1
            
            player4Piece2eye2.texture = self.openEye
            player4Piece2eye2.size = CGSize(width: 6, height: 6)
            player4Piece2eye2.yScale = 1
            player4Piece2eye2.xScale = 1
            
            player4Piece3eye1.removeAllActions()
            player4Piece3eye2.removeAllActions()
            
            player4Piece3eye1.texture = self.openEye
            player4Piece3eye1.size = CGSize(width: 6, height: 6)
            player4Piece3eye1.yScale = 1
            player4Piece3eye1.xScale = 1
            
            player4Piece3eye2.texture = self.openEye
            player4Piece3eye2.size = CGSize(width: 6, height: 6)
            player4Piece3eye2.yScale = 1
            player4Piece3eye2.xScale = 1
            
            player4Piece4eye1.removeAllActions()
            player4Piece4eye2.removeAllActions()
            
            player4Piece4eye1.texture = self.openEye
            player4Piece4eye1.size = CGSize(width: 6, height: 6)
            player4Piece4eye1.yScale = 1
            player4Piece4eye1.xScale = 1
            
            player4Piece4eye2.texture = self.openEye
            player4Piece4eye2.size = CGSize(width: 6, height: 6)
            player4Piece4eye2.yScale = 1
            player4Piece4eye2.xScale = 1
            
            
        }
        
    }
    
    
    
    func removeAnimation(player:Player) {
        
        Arrow1Red.isHidden = true
        Arrow2Red.isHidden = true
        Arrow3Red.isHidden = true
        Arrow4Red.isHidden = true
        
        Arrow1Green.isHidden = true
        Arrow2Green.isHidden = true
        Arrow3Green.isHidden = true
        Arrow4Green.isHidden = true
        
        Arrow1Blue.isHidden = true
        Arrow2Blue.isHidden = true
        Arrow3Blue.isHidden = true
        Arrow4Blue.isHidden = true
        
        Arrow1Yellow.isHidden = true
        Arrow2Yellow.isHidden = true
        Arrow3Yellow.isHidden = true
        Arrow4Yellow.isHidden = true
        
        Arrow1Red.removeAllActions()
        Arrow1Red.setScale(1)
        Arrow2Red.removeAllActions()
        Arrow2Red.setScale(1)
        Arrow3Red.removeAllActions()
        Arrow3Red.setScale(1)
        Arrow4Red.removeAllActions()
        Arrow4Red.setScale(1)
        
        Arrow1Green.removeAllActions()
        Arrow1Green.setScale(1)
        Arrow2Green.removeAllActions()
        Arrow2Green.setScale(1)
        Arrow3Green.removeAllActions()
        Arrow3Green.setScale(1)
        Arrow4Green.removeAllActions()
        Arrow4Green.setScale(1)
        
        Arrow1Yellow.removeAllActions()
        Arrow1Yellow.setScale(1)
        Arrow2Yellow.removeAllActions()
        Arrow2Yellow.setScale(1)
        Arrow3Yellow.removeAllActions()
        Arrow3Yellow.setScale(1)
        Arrow4Yellow.removeAllActions()
        Arrow4Yellow.setScale(1)
        
        Arrow1Blue.removeAllActions()
        Arrow1Blue.setScale(1)
        Arrow2Blue.removeAllActions()
        Arrow2Blue.setScale(1)
        Arrow3Blue.removeAllActions()
        Arrow3Blue.setScale(1)
        Arrow4Blue.removeAllActions()
        Arrow4Blue.setScale(1)
        
        keepAnimating = false
    }
    
    
    
    
    func flashTurn(player:Player) {
        
        if (player == .Player1) {
            
            playerLabel.run(SKAction.init(named: "Pulse")!)
            
        } else if (player == .Player2) {
            
            player2Label.run(SKAction.init(named: "Pulse")!)
            
        } else if (player == .Player3) {
            
            player3Label.run(SKAction.init(named: "Pulse")!)
            
        } else if (player == .Player4) {
            
            player4Label.run(SKAction.init(named: "Pulse")!)
            
        }
    }
    
    
    
    
    
    // <-------------------------------------------------------------------------------------------------------->
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        print("Inside Touches :")
        print(OneMoreMove)
        print(movesRemaining)
        
        
        print("Who's Turn :")
        print(whosTurn)
        for touch in touches {
            print("Inside For LOOP")
            let location1 = touch.location(in: self)
            let nodes = self.nodes(at: location1)
            print(location1)
            for node in nodes {
                print(node.name ?? "hi")
                if (node.name == "Dice" && moveFinished && (whosTurn == .Player1 || whosTurn == .Player2)) {
                    
                    print("Here Inside!")
                    
                    removeFlashTurn(player: whosTurn)
                    
                    rollDice(player: whosTurn)
                    
                } else if (node.name == "Dice2" && moveFinished && (whosTurn == .Player3 || whosTurn == .Player4)) {
                    
                    print("Here Inside!")
                    
                    removeFlashTurn(player: whosTurn)
                    
                    rollDice(player: whosTurn)
                    
                }
                
                if (node.name == "blackshadow") {
                    
                    PauseMenu.removeAllActions()
                    blackShadow.isHidden = true
                    PauseMenu.run(SKAction.init(named: "Disapear")!)
                    home.run(SKAction.init(named: "Disapear")!)
                    cancel.run(SKAction.init(named: "Disapear")!)
                    audioButton.run(SKAction.init(named: "Disapear")!)
               //     howToPlay.run(SKAction.init(named: "Disapear")!)
                    
                }
                
            }
        }
        
        
        
        //var touchedNode:SKSpriteNode = SKSpriteNode()
        
        
        if diceRolled {
            
            if !rolling /*&& !gameOver*/ {
                
                for touch in touches {
                    
                    print("Inside For in Touches :")
                    
                    let location = touch.location(in: self)
                    print(location)
                    
                    let node = atPoint(location)
                    
                        
                        
                        if (whosTurn == .Player1) {
                        
                        
                            if ((node.name == "Player1Piece1" || node.name == "player1Piece1eye1" || node.name == "player1Piece1eye2" ) && player1piece1AbletoEnter == true && currentSpacePlayer1Piece1 != 26) {
                            
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                            
                                touchedNode = Player1Piece1
                            
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                            
                                movesRemaining = rolledDice
                            
                                removeFlashPlayer(player: whosTurn)
                            
                                movePiece()
                            
                            } else if ((node.name == "Player1Piece2" || node.name == "player1Piece2eye1" || node.name == "player1Piece2eye2") && player1piece2AbletoEnter == true && currentSpacePlayer1Piece2 != 26) {
                            
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                            
                                touchedNode = Player1Piece2
                            
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                            
                                movesRemaining = rolledDice
                            
                                removeFlashPlayer(player: whosTurn)
                            
                                movePiece()
                            
                            } else if ((node.name == "Player1Piece3" || node.name == "player1Piece3eye1" || node.name == "player1Piece3eye2") && player1piece3AbletoEnter == true && currentSpacePlayer1Piece3 != 26) {
                            
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                            
                                touchedNode = Player1Piece3
                            
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                            
                                movesRemaining = rolledDice
                            
                                removeFlashPlayer(player: whosTurn)
                            
                                movePiece()
                            
                            } else if ((node.name == "Player1Piece4" || node.name == "player1Piece4eye1" || node.name == "player1Piece4eye2") && player1piece4AbletoEnter == true && currentSpacePlayer1Piece4 != 26) {
                            
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                            
                                touchedNode = Player1Piece4
                            
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                            
                                movesRemaining = rolledDice
                            
                                removeFlashPlayer(player: whosTurn)
                            
                                movePiece()
                            
                            }
                            
                        } else if (whosTurn == .Player2) {
                            
                            
                            if ((node.name == "Player2Piece1" || node.name == "player2Piece1eye1" || node.name == "player2Piece1eye2" ) && player2piece1AbletoEnter == true && currentSpacePlayer2Piece1 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player2Piece1
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player2Piece2" || node.name == "player2Piece2eye1" || node.name == "player2Piece2eye2") && player2piece2AbletoEnter == true && currentSpacePlayer2Piece2 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player2Piece2
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player2Piece3" || node.name == "player2Piece3eye1" || node.name == "player2Piece3eye2") && player2piece3AbletoEnter == true && currentSpacePlayer2Piece3 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player2Piece3
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player2Piece4" || node.name == "player2Piece4eye1" || node.name == "player2Piece4eye2") && player2piece4AbletoEnter == true && currentSpacePlayer2Piece4 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player2Piece4
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            }
                            
                        } else if (whosTurn == .Player3) {
                            
                            if ((node.name == "Player3Piece1" || node.name == "player3Piece1eye1" || node.name == "player3Piece1eye2" ) && player3piece1AbletoEnter == true && currentSpacePlayer3Piece1 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player3Piece1
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player3Piece2" || node.name == "player3Piece2eye1" || node.name == "player3Piece2eye2") && player3piece2AbletoEnter == true && currentSpacePlayer3Piece2 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player3Piece2
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player3Piece3" || node.name == "player3Piece3eye1" || node.name == "player3Piece3eye2") && player3piece3AbletoEnter == true && currentSpacePlayer3Piece3 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player3Piece3
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player3Piece4" || node.name == "player3Piece4eye1" || node.name == "player3Piece4eye2") && player3piece4AbletoEnter == true && currentSpacePlayer3Piece4 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player3Piece4
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            }
                            
                        } else if (whosTurn == .Player4) {
                            
                            if ((node.name == "Player4Piece1" || node.name == "player4Piece1eye1" || node.name == "player4Piece1eye2" ) && player4piece1AbletoEnter == true && currentSpacePlayer4Piece1 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player4Piece1
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player4Piece2" || node.name == "player4Piece2eye1" || node.name == "player4Piece2eye2") && player4piece2AbletoEnter == true && currentSpacePlayer4Piece2 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player4Piece2
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player4Piece3" || node.name == "player4Piece3eye1" || node.name == "player4Piece3eye2") && player4piece3AbletoEnter == true && currentSpacePlayer4Piece3 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player4Piece3
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            } else if ((node.name == "Player4Piece4" || node.name == "player4Piece4eye1" || node.name == "player4Piece4eye2") && player4piece4AbletoEnter == true && currentSpacePlayer4Piece4 != 26) {
                                
                                print("In Node in Nodes In Touches and it's :")
                                print(node.name!)
                                
                                touchedNode = Player4Piece4
                                
                                print("Rolling of Dice :")
                                print(rolling)
                                print(moveFinished)
                                
                                movesRemaining = rolledDice
                                
                                removeFlashPlayer(player: whosTurn)
                                
                                movePiece()
                                
                            }
                            
                        }
                        
                    
                    
                }
                
            }
        }
        
        
        
        
        
        
        }
    
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        
    }
    
    
    //MARK: - ButtonFeelDelegate
    
    
    
    func spriteNodeButtonPressed(_ button: NewSpriteNodeButton) {
        
        
        
        switch button {
        case backButton:
            
            playSound(soundName: buttonSound)
            PauseMenu.removeAllActions()
            blackShadow.isHidden = false
            PauseMenu.run(SKAction.init(named: "ComeOut")!)
            home.run(SKAction.init(named: "ComeOut")!)
            cancel.run(SKAction.init(named: "ComeOut")!)
            audioButton.run(SKAction.init(named: "ComeOut")!)
        //    howToPlay.run(SKAction.init(named: "ComeOut")!)
            
            
        case cancel:
            
            playSound(soundName: buttonSound)
            PauseMenu.removeAllActions()
            blackShadow.isHidden = true
            PauseMenu.run(SKAction.init(named: "Disapear")!)
            home.run(SKAction.init(named: "Disapear")!)
            cancel.run(SKAction.init(named: "Disapear")!)
            audioButton.run(SKAction.init(named: "Disapear")!)
        //    howToPlay.run(SKAction.init(named: "Disapear")!)
            
        case home:
            
            playSound(soundName: buttonSound)
            
            
            if place.count > 0 {
                playerOne = place[0]
            }
            if place.count > 1 {
                playerTwo = place[1]
            }
            if place.count > 2 {
                playerThree = place[2]
            }
            if place.count > 3 {
                playerFour = place[3]
            }
            
            if playerOne.isEmpty && playerTwo.isEmpty && playerThree.isEmpty && playerFour.isEmpty {
                
                playerOne = playerLabel.text!
                playerTwo = player2Label.text!
                playerThree = player3Label.text!
                playerFour = player4Label.text!
                print("In Last CON!")
                
            }
            
            GlobalVariablesPhone.PreviousScene = .VsFriendFour
            
            let gamescene = SKScene(fileNamed: "EndScreen")
            //  gamescene?.scaleMode = .aspectFill
            self.view?.presentScene(gamescene!, transition: SKTransition.fade(withDuration: 1))
            
        case audioButton:
            
            if GlobalVariablesPhone.audio {
                playSound(soundName: buttonSound)
                GlobalVariablesPhone.audio = false
                defaults.set(GlobalVariablesPhone.audio, forKey: "audio")
                defaults.synchronize()
            } else {
                GlobalVariablesPhone.audio = true
                defaults.set(GlobalVariablesPhone.audio, forKey: "audio")
                defaults.synchronize()
                playSound(soundName: buttonSound)
            }
            
            
            
        default:
            break
        }
        
        
        
        
    }
    
    
    deinit {
        self.removeAllActions()
        self.removeAllChildren()
        print("Everything Got Deinitialyed ! VsFriendFour")
    }
    
    
}
