//
//  NewSpriteNodeButton.swift
//  Indian Ludo
//
//  Created by ramesh sanghar on 08/09/19.
//  Copyright © 2019 Om. All rights reserved.
//

import SpriteKit

protocol NewSpriteNodeButtonDelegate: class {
    func spriteNodeButtonPressed(_ button: NewSpriteNodeButton)
}

class NewSpriteNodeButton: SKSpriteNode {
    
    weak var delegate: NewSpriteNodeButtonDelegate?
    
    func touchDown(atPoint pos : CGPoint) {
//        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
//            n.position = pos
//            n.strokeColor = SKColor.green
//            self.addChild(n)
//        }
        
       // alpha = 0.5
        setScale(0.95)
    }
    
    
    func touchUp(atPoint pos : CGPoint) {
        
        if contains(pos) {
            delegate?.spriteNodeButtonPressed(self)
        }
        
        
      //  alpha = 1
        setScale(1)
    }
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
        
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            
            if parent != nil {
                self.touchUp(atPoint: t.location(in: parent!))
            }
            
            
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            if parent != nil {
                self.touchUp(atPoint: t.location(in: parent!))
            }
        }
    }
    
    
}
